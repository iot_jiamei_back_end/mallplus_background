package com.ruoyi.project.mall.deliveryExpressCompany.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.mall.service.domain.PmsGoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.deliveryExpressCompany.domain.OmsDeliveryExpressCompany;
import com.ruoyi.project.mall.deliveryExpressCompany.service.IOmsDeliveryExpressCompanyService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * deliveryExpressCompanyController
 *
 * @author mallplus
 * @date 2020-03-18
 */
@Controller
@RequestMapping("/mall/deliveryExpressCompany")
public class OmsDeliveryExpressCompanyController extends BaseController {
    private String prefix = "mall/deliveryExpressCompany";

    @Autowired
    private IOmsDeliveryExpressCompanyService omsDeliveryExpressCompanyService;

    @RequiresPermissions("mall:deliveryExpressCompany:view")
    @GetMapping()
    public String deliveryExpressCompany() {
        return prefix + "/deliveryExpressCompany";
    }

    /**
     * 查询deliveryExpressCompany列表
     */
    @RequiresPermissions("mall:deliveryExpressCompany:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(OmsDeliveryExpressCompany entity, Integer pageNum, Integer pageSize) {
        System.out.println("pageNum111:" + pageNum);
        System.out.println("pageSize111:" + pageSize);
        IPage<OmsDeliveryExpressCompany> page = omsDeliveryExpressCompanyService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(entity));
        System.out.println("pageInfo:" + page.toString());
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

//    /**
//     * 导出deliveryExpressCompany列表
//     */
//    @RequiresPermissions("mall:deliveryExpressCompany:export")
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(OmsDeliveryExpressCompany omsDeliveryExpressCompany) {
//        List<OmsDeliveryExpressCompany> list = omsDeliveryExpressCompanyService.selectOmsDeliveryExpressCompanyList(omsDeliveryExpressCompany);
//        ExcelUtil<OmsDeliveryExpressCompany> util = new ExcelUtil<OmsDeliveryExpressCompany>(OmsDeliveryExpressCompany.class);
//        return util.exportExcel(list, "deliveryExpressCompany");
//    }

    /**
     * 新增deliveryExpressCompany
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存deliveryExpressCompany
     */
    @RequiresPermissions("mall:deliveryExpressCompany:add")
    @Log(title = "deliveryExpressCompany", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(OmsDeliveryExpressCompany omsDeliveryExpressCompany) {
        return toAjax(omsDeliveryExpressCompanyService.save(omsDeliveryExpressCompany));
    }

    /**
     * 修改deliveryExpressCompany
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        OmsDeliveryExpressCompany omsDeliveryExpressCompany = omsDeliveryExpressCompanyService.getById(id);
        mmap.put("omsDeliveryExpressCompany", omsDeliveryExpressCompany);
        return prefix + "/edit";
    }

    /**
     * 修改保存deliveryExpressCompany
     */
    @RequiresPermissions("mall:deliveryExpressCompany:edit")
    @Log(title = "deliveryExpressCompany", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OmsDeliveryExpressCompany omsDeliveryExpressCompany) {
        return toAjax(omsDeliveryExpressCompanyService.updateById(omsDeliveryExpressCompany));
    }

    /**
     * 删除deliveryExpressCompany
     */
    @RequiresPermissions("mall:deliveryExpressCompany:remove")
    @Log(title = "deliveryExpressCompany", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        String[] strArra = ids.split(",");
        for (String str : strArra) {
            omsDeliveryExpressCompanyService.removeById(str);
        }
        return toAjax(true);
    }
}
