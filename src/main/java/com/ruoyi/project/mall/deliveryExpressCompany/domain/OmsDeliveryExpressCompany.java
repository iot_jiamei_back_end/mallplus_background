package com.ruoyi.project.mall.deliveryExpressCompany.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@Data
@TableName("oms_delivery_express_company")
public class OmsDeliveryExpressCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 物流公司
     */
    private String express;

    /**
     * 物流编号
     */
    @TableField("com_code")
    private String comCode;

    /**
     * 快递类型：1 : 国内快递 2:国际快递
     */
    private String type;

    /**
     * 删除状态：0 : 未删除 1: 已删除
     */
    @TableField("is_delete")
    private Integer isDelete;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;


}
