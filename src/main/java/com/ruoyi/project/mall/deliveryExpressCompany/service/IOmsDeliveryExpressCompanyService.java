package com.ruoyi.project.mall.deliveryExpressCompany.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.deliveryExpressCompany.domain.OmsDeliveryExpressCompany;
import java.util.List;

/**
 * deliveryExpressCompanyService接口
 * 
 * @author mallplus
 * @date 2020-03-18
 */
public interface IOmsDeliveryExpressCompanyService extends IService<OmsDeliveryExpressCompany>
{
    /**
     * 查询deliveryExpressCompany
     * 
     * @param id deliveryExpressCompanyID
     * @return deliveryExpressCompany
     */
    public OmsDeliveryExpressCompany selectOmsDeliveryExpressCompanyById(Long id);

    /**
     * 查询deliveryExpressCompany列表
     * 
     * @param omsDeliveryExpressCompany deliveryExpressCompany
     * @return deliveryExpressCompany集合
     */
    public List<OmsDeliveryExpressCompany> selectOmsDeliveryExpressCompanyList(OmsDeliveryExpressCompany omsDeliveryExpressCompany);

    /**
     * 新增deliveryExpressCompany
     * 
     * @param omsDeliveryExpressCompany deliveryExpressCompany
     * @return 结果
     */
    public int insertOmsDeliveryExpressCompany(OmsDeliveryExpressCompany omsDeliveryExpressCompany);

    /**
     * 修改deliveryExpressCompany
     * 
     * @param omsDeliveryExpressCompany deliveryExpressCompany
     * @return 结果
     */
    public int updateOmsDeliveryExpressCompany(OmsDeliveryExpressCompany omsDeliveryExpressCompany);

    /**
     * 批量删除deliveryExpressCompany
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOmsDeliveryExpressCompanyByIds(String ids);

    /**
     * 删除deliveryExpressCompany信息
     * 
     * @param id deliveryExpressCompanyID
     * @return 结果
     */
    public int deleteOmsDeliveryExpressCompanyById(Long id);
}
