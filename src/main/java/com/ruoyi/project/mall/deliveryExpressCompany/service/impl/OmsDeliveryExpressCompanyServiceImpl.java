package com.ruoyi.project.mall.deliveryExpressCompany.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.deliveryExpressCompany.mapper.OmsDeliveryExpressCompanyMapper;
import com.ruoyi.project.mall.deliveryExpressCompany.domain.OmsDeliveryExpressCompany;
import com.ruoyi.project.mall.deliveryExpressCompany.service.IOmsDeliveryExpressCompanyService;
import com.ruoyi.common.utils.text.Convert;

/**
 * deliveryExpressCompanyService业务层处理
 * 
 * @author mallplus
 * @date 2020-03-18
 */
@Service
@Primary
public class OmsDeliveryExpressCompanyServiceImpl extends ServiceImpl<OmsDeliveryExpressCompanyMapper, OmsDeliveryExpressCompany> implements IOmsDeliveryExpressCompanyService
{
    @Autowired
    private OmsDeliveryExpressCompanyMapper omsDeliveryExpressCompanyMapper;

    /**
     * 查询deliveryExpressCompany
     * 
     * @param id deliveryExpressCompanyID
     * @return deliveryExpressCompany
     */
    @Override
    public OmsDeliveryExpressCompany selectOmsDeliveryExpressCompanyById(Long id)
    {
        return omsDeliveryExpressCompanyMapper.selectOmsDeliveryExpressCompanyById(id);
    }

    /**
     * 查询deliveryExpressCompany列表
     * 
     * @param omsDeliveryExpressCompany deliveryExpressCompany
     * @return deliveryExpressCompany
     */
    @Override
    public List<OmsDeliveryExpressCompany> selectOmsDeliveryExpressCompanyList(OmsDeliveryExpressCompany omsDeliveryExpressCompany)
    {
        return omsDeliveryExpressCompanyMapper.selectOmsDeliveryExpressCompanyList(omsDeliveryExpressCompany);
    }

    /**
     * 新增deliveryExpressCompany
     * 
     * @param omsDeliveryExpressCompany deliveryExpressCompany
     * @return 结果
     */
    @Override
    public int insertOmsDeliveryExpressCompany(OmsDeliveryExpressCompany omsDeliveryExpressCompany)
    {
        //omsDeliveryExpressCompany.setCreateTime(DateUtils.getNowDate());
        return omsDeliveryExpressCompanyMapper.insertOmsDeliveryExpressCompany(omsDeliveryExpressCompany);
    }

    /**
     * 修改deliveryExpressCompany
     * 
     * @param omsDeliveryExpressCompany deliveryExpressCompany
     * @return 结果
     */
    @Override
    public int updateOmsDeliveryExpressCompany(OmsDeliveryExpressCompany omsDeliveryExpressCompany)
    {
        //omsDeliveryExpressCompany.setUpdateTime(DateUtils.getNowDate());
        return omsDeliveryExpressCompanyMapper.updateOmsDeliveryExpressCompany(omsDeliveryExpressCompany);
    }

    /**
     * 删除deliveryExpressCompany对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteOmsDeliveryExpressCompanyByIds(String ids)
    {
        return omsDeliveryExpressCompanyMapper.deleteOmsDeliveryExpressCompanyByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除deliveryExpressCompany信息
     * 
     * @param id deliveryExpressCompanyID
     * @return 结果
     */
    @Override
    public int deleteOmsDeliveryExpressCompanyById(Long id)
    {
        return omsDeliveryExpressCompanyMapper.deleteOmsDeliveryExpressCompanyById(id);
    }
}
