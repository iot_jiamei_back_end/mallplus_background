package com.ruoyi.project.mall.help.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.help.domain.CmsHelp;

/**
 * <p>
 * 帮助表 服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
public interface ICmsHelpService extends IService<CmsHelp> {

}
