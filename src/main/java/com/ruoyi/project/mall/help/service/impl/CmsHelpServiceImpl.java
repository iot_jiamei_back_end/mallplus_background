package com.ruoyi.project.mall.help.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.mall.help.domain.CmsHelp;
import com.ruoyi.project.mall.help.mapper.CmsHelpMapper;
import com.ruoyi.project.mall.help.service.ICmsHelpService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 帮助表 服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@Service
@Primary
public class CmsHelpServiceImpl extends ServiceImpl<CmsHelpMapper, CmsHelp> implements ICmsHelpService {

}
