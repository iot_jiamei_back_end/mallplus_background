package com.ruoyi.project.mall.help.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.mall.category.domain.CmsHelpCategory;
import com.ruoyi.project.mall.category.service.ICmsHelpCategoryService;
import com.ruoyi.project.mall.orderSetting.domain.OmsOrderSetting;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.help.domain.CmsHelp;
import com.ruoyi.project.mall.help.service.ICmsHelpService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 帮助Controller
 *
 * @author mallplus
 * @date 2020-03-18
 */
@Controller
@RequestMapping("/mall/cmsHelp")
public class CmsHelpController extends BaseController {
    private String prefix = "mall/help";

    @Autowired
    private ICmsHelpService cmsHelpService;

    @Autowired
    private ICmsHelpCategoryService cmsHelpCategoryService;

    @RequiresPermissions("mall:help:view")
    @GetMapping()
    public String help(ModelMap mmap) {
        List<CmsHelpCategory> cmsHelpCategoryList = cmsHelpCategoryService.list(null);
        mmap.put("cmsHelpCategoryList", cmsHelpCategoryList);
        return prefix + "/help";
    }

    /**
     * 查询帮助列表
     */
    @RequiresPermissions("mall:help:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsHelp cmsHelp, Integer pageNum, Integer pageSize) {
        if (StringUtils.isEmpty(cmsHelp.getTitle())) {
            cmsHelp.setTitle(null);
        }
        IPage<CmsHelp> page = cmsHelpService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(cmsHelp));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

    /**
     * 新增帮助
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        List<CmsHelpCategory> cmsHelpCategoryList = cmsHelpCategoryService.list(null);
        System.out.println("cmsHelpCategoryList:" + cmsHelpCategoryList.size());
        mmap.put("cmsHelpCategoryList", cmsHelpCategoryList);
        return prefix + "/add";
    }

    /**
     * 新增保存帮助
     */
    @RequiresPermissions("mall:help:add")
    @Log(title = "帮助", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsHelp cmsHelp) {
        try {
            boolean is = cmsHelpService.save(cmsHelp);
            return toAjax(true);
        } catch (Exception e) {
            e.printStackTrace();
            return toAjax(false);
        }

    }

    /**
     * 修改帮助
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        CmsHelp cmsHelp = cmsHelpService.getById(id);

        List<CmsHelpCategory> cmsHelpCategoryList = cmsHelpCategoryService.list(null);
        mmap.put("cmsHelpCategoryList", cmsHelpCategoryList);
        mmap.put("cmsHelp", cmsHelp);
        return prefix + "/edit";
    }

    /**
     * 修改保存帮助
     */
    @RequiresPermissions("mall:help:edit")
    @Log(title = "帮助", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsHelp cmsHelp) {
        return toAjax(cmsHelpService.updateById(cmsHelp));
    }

    /**
     * 删除帮助
     */
    @RequiresPermissions("mall:help:remove")
    @Log(title = "帮助", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        String[] strArra = ids.split(",");
        for (String str : strArra) {
            System.out.println("id为：" + str);
            cmsHelpService.removeById(Long.parseLong(str));
        }
        return toAjax(true);
    }
}
