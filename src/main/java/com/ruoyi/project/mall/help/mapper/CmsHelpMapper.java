package com.ruoyi.project.mall.help.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.help.domain.CmsHelp;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 帮助表 Mapper 接口
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@Mapper
public interface CmsHelpMapper extends BaseMapper<CmsHelp> {

}
