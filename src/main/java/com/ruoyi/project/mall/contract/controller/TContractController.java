package com.ruoyi.project.mall.contract.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.mall.contract.entity.dto.ContractDto;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.contract.domain.TContract;
import com.ruoyi.project.mall.contract.service.ITContractService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【合同管理】Controller
 *
 * @author mallplus
 * @date 2020-03-17
 */
@Controller
@RequestMapping("/mall/contract")
public class TContractController extends BaseController {

    private String prefix = "mall/contract";

    @Autowired
    private ITContractService tContractService;


    @RequiresPermissions("mall:contract:view")
    @GetMapping()
    public String contract() {
        return prefix + "/contract";
    }

    /**
     * 获取合同列表*
     *
     * @param
     * @return
     */
    @RequiresPermissions("mall:contract:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageNum, Integer pageSize, TContract tContract) {
        if (StringUtils.isEmpty(tContract.getCorporateName())) {
            tContract.setCorporateName(null);
        }

        TableDataInfo tableDataInfo = new TableDataInfo();
        IPage<TContract> page = tContractService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(tContract));
        tableDataInfo.setTotal(page.getTotal());
        tableDataInfo.setRows(page.getRecords());
        return tableDataInfo;
    }


    /**
     * 查看合同详情
     *
     * @param id
     * @return
     */
    @GetMapping("/viewContract")
    public String viewContract(Long id, ModelMap model) {
        TContract tContract = tContractService.getById(id);
        model.put("tContract", tContract);
        return prefix + "/dtails";
    }

    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 添加合同
     *
     * @param tContract
     * @return
     */
    @PostMapping("/addContract")
    @ResponseBody
    public AjaxResult addContract(TContract tContract) {
        tContractService.addContract(tContract);
        return AjaxResult.success();
    }

    @GetMapping("/edit")
    public String edit(Long id, ModelMap mmap) {
        TContract tContract = tContractService.getById(id);
        mmap.put("tContract", tContract);
        return prefix + "/edit";
    }


    /**
     * 编辑合同
     *
     * @param tContract
     * @return
     */

    @PostMapping("/editContract")
    @ResponseBody
    public AjaxResult editContract(TContract tContract) {
        tContract.setCreateTime(System.currentTimeMillis());
        tContract.setUpdateTime(System.currentTimeMillis());
        tContractService.updateById(tContract);
        return AjaxResult.success();
    }


    @GetMapping("/renew")
    public String renew(Long id,ModelMap mmap){
        TContract tContract = tContractService.getById(id);
        mmap.put("tContract", tContract);
        return prefix + "/renew";
    }
    /**
     * 续签合同
     *
     * @param tContract
     * @return
     */

    @PostMapping("/renewContract")
    @ResponseBody
    public AjaxResult renewContract(TContract tContract) {
        tContractService.addContract(tContract);
        return AjaxResult.success();
    }


}
