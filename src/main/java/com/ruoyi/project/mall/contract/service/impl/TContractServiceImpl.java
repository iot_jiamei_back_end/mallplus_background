package com.ruoyi.project.mall.contract.service.impl;

import java.util.List;
import java.util.regex.Pattern;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.mall.contract.entity.dto.ContractDto;
import com.ruoyi.project.mall.contract.entity.po.ContractPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.contract.mapper.TContractMapper;
import com.ruoyi.project.mall.contract.domain.TContract;
import com.ruoyi.project.mall.contract.service.ITContractService;
import com.ruoyi.common.utils.text.Convert;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author mallplus
 * @date 2020-03-17
 */
@Service
public class TContractServiceImpl extends ServiceImpl<TContractMapper, TContract> implements ITContractService {
    @Resource
    private TContractMapper tContractMapper;

    /**
     * 查询【请填写功能名称】
     *
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public TContract selectTContractById(Long id) {
        return tContractMapper.selectTContractById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     *
     * @param tContract 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TContract> selectTContractList(TContract tContract) {
        return tContractMapper.selectTContractList(tContract);
    }

    /**
     * 新增【请填写功能名称】
     *
     * @param tContract 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTContract(TContract tContract) {

        return tContractMapper.insertTContract(tContract);
    }

    /**
     * 修改【请填写功能名称】
     *
     * @param tContract 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTContract(TContract tContract) {

        return tContractMapper.updateTContract(tContract);
    }

    /**
     * 删除【请填写功能名称】对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTContractByIds(String ids) {
        return tContractMapper.deleteTContractByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     *
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteTContractById(Long id) {
        return tContractMapper.deleteTContractById(id);
    }

    @Override
    public List<ContractPo> list(ContractDto contractDto) {

        String contractId = contractDto.getContractId();

        boolean chinese = isChinese(contractId);
        if (chinese) {
            return tContractMapper.list1(contractDto);
        } else {
            return tContractMapper.list(contractDto);
        }
    }

    @Override
    public TContract viewContract(Long id) {
        return tContractMapper.selectTContractById(id);
    }

    @Override
    public void addContract(TContract tContract) {
        tContract.setContractId(generateContractNumber());
        tContract.setContractStart(0L);
        tContract.setState(0L);
        tContract.setCreateTime(System.currentTimeMillis());
        tContract.setUpdateTime(System.currentTimeMillis());
        tContractMapper.insert(tContract);
    }

    public String generateContractNumber() {
        String theMaximumIdValue = tContractMapper.getTheMaximumIdValue();
        String substring = theMaximumIdValue.substring(5, 6);
        Integer integer = Integer.valueOf(substring);
        return "wjlm" + integer + 1;
    }


    public boolean isChinese(String keyword) {
        for (int i = 0; i < keyword.length(); i = i + 1) {
            if (!Pattern.compile("[\u4e00-\u9fa5]").matcher(
                    String.valueOf(keyword.charAt(i))).find()) {
                return false;
            }
        }
        return true;
    }
}
