package com.ruoyi.project.mall.contract.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.contract.domain.TContract;
import com.ruoyi.project.mall.contract.entity.dto.ContractDto;
import com.ruoyi.project.mall.contract.entity.po.ContractPo;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-17
 */
public interface TContractMapper extends BaseMapper<TContract>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public TContract selectTContractById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tContract 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TContract> selectTContractList(TContract tContract);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tContract 【请填写功能名称】
     * @return 结果
     */
    public int insertTContract(TContract tContract);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tContract 【请填写功能名称】
     * @return 结果
     */
    public int updateTContract(TContract tContract);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteTContractById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTContractByIds(String[] ids);

    List<ContractPo> list(ContractDto contractDto);

    List<ContractPo> list1(ContractDto contractDto);

    String getTheMaximumIdValue();
}
