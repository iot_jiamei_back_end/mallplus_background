package com.ruoyi.project.mall.contract.entity.po;

import lombok.Data;

import java.util.Date;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class ContractPo {

    private Long id;

    private String companyName;

    private String name;

    private String contactMobile;

    private String contractId;

    private Date startTime;

    private Date endTime;

    private Integer contractStart;


}
