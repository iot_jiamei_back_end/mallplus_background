package com.ruoyi.project.mall.contract.entity.dto;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class ContractDto {

    /**
     * 合同编号
     */
    private String contractId;

    /**
     * 合同开始时间
     */
    private String startTime;

    /**
     * 合同结束时间
     */
    private String endTime;

    /**
     * 合同状态
     */
    private Integer contractStart;


}
