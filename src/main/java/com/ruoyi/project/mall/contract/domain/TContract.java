package com.ruoyi.project.mall.contract.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Date;

/**
 * 【请填写功能名称】对象 t_contract
 *
 * @author mallplus
 * @date 2020-03-17
 */
@Data
@TableName("t_contract")
public class TContract {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private Long id;

    /**
     * 合同id
     */
    private String contractId;


    private String corporateName;

    /**
     * 合同开始时间
     */
    private Date startTime;

    /**
     * 合同结束时间
     */
    private Date endTime;

    /**
     * 合同扫描件
     */
    private String contractImg;

    /**
     * 开户行
     */
    private String bank;

    /**
     * 开户账号
     */
    private String bankNumber;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 姓名
     */
    private String name;

    /**
     * 固定电话
     */
    private String telephone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 合同状态0表示正常1过期、2失效
     */

    private Long contractStart;

    /**
     * 状态、0表示有效1表示无效
     */
    private Long state;

    private Long createTime;

    private Long updateTime;
}
