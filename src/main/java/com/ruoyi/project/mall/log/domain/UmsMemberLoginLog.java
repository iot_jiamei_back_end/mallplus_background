package com.ruoyi.project.mall.log.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 会员登录记录对象 ums_member_login_log
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class UmsMemberLoginLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long memberId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String ip;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String city;

    /** 登录类型：0->PC；1->android;2->ios;3->小程序 */
    @Excel(name = "登录类型：0->PC；1->android;2->ios;3->小程序")
    private Integer loginType;

    /** $column.columnComment */
    @Excel(name = "登录类型：0->PC；1->android;2->ios;3->小程序")
    private String province;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setLoginType(Integer loginType) 
    {
        this.loginType = loginType;
    }

    public Integer getLoginType() 
    {
        return loginType;
    }
    public void setProvince(String province) 
    {
        this.province = province;
    }

    public String getProvince() 
    {
        return province;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("createTime", getCreateTime())
            .append("ip", getIp())
            .append("city", getCity())
            .append("loginType", getLoginType())
            .append("province", getProvince())
            .append("storeId", getStoreId())
            .toString();
    }
}
