package com.ruoyi.project.mall.log.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.log.mapper.UmsMemberLoginLogMapper;
import com.ruoyi.project.mall.log.domain.UmsMemberLoginLog;
import com.ruoyi.project.mall.log.service.IUmsMemberLoginLogService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 会员登录记录Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class UmsMemberLoginLogServiceImpl implements IUmsMemberLoginLogService 
{
    @Autowired
    private UmsMemberLoginLogMapper umsMemberLoginLogMapper;

    /**
     * 查询会员登录记录
     * 
     * @param id 会员登录记录ID
     * @return 会员登录记录
     */
    @Override
    public UmsMemberLoginLog selectUmsMemberLoginLogById(Long id)
    {
        return umsMemberLoginLogMapper.selectUmsMemberLoginLogById(id);
    }

    /**
     * 查询会员登录记录列表
     * 
     * @param umsMemberLoginLog 会员登录记录
     * @return 会员登录记录
     */
    @Override
    public List<UmsMemberLoginLog> selectUmsMemberLoginLogList(UmsMemberLoginLog umsMemberLoginLog)
    {
        return umsMemberLoginLogMapper.selectUmsMemberLoginLogList(umsMemberLoginLog);
    }

    /**
     * 新增会员登录记录
     * 
     * @param umsMemberLoginLog 会员登录记录
     * @return 结果
     */
    @Override
    public int insertUmsMemberLoginLog(UmsMemberLoginLog umsMemberLoginLog)
    {
        umsMemberLoginLog.setCreateTime(DateUtils.getNowDate());
        return umsMemberLoginLogMapper.insertUmsMemberLoginLog(umsMemberLoginLog);
    }

    /**
     * 修改会员登录记录
     * 
     * @param umsMemberLoginLog 会员登录记录
     * @return 结果
     */
    @Override
    public int updateUmsMemberLoginLog(UmsMemberLoginLog umsMemberLoginLog)
    {
        umsMemberLoginLog.setUpdateTime(DateUtils.getNowDate());
        return umsMemberLoginLogMapper.updateUmsMemberLoginLog(umsMemberLoginLog);
    }

    /**
     * 删除会员登录记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUmsMemberLoginLogByIds(String ids)
    {
        return umsMemberLoginLogMapper.deleteUmsMemberLoginLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员登录记录信息
     * 
     * @param id 会员登录记录ID
     * @return 结果
     */
    @Override
    public int deleteUmsMemberLoginLogById(Long id)
    {
        return umsMemberLoginLogMapper.deleteUmsMemberLoginLogById(id);
    }
}
