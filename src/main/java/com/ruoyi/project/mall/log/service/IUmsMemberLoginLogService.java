package com.ruoyi.project.mall.log.service;

import com.ruoyi.project.mall.log.domain.UmsMemberLoginLog;
import java.util.List;

/**
 * 会员登录记录Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IUmsMemberLoginLogService 
{
    /**
     * 查询会员登录记录
     * 
     * @param id 会员登录记录ID
     * @return 会员登录记录
     */
    public UmsMemberLoginLog selectUmsMemberLoginLogById(Long id);

    /**
     * 查询会员登录记录列表
     * 
     * @param umsMemberLoginLog 会员登录记录
     * @return 会员登录记录集合
     */
    public List<UmsMemberLoginLog> selectUmsMemberLoginLogList(UmsMemberLoginLog umsMemberLoginLog);

    /**
     * 新增会员登录记录
     * 
     * @param umsMemberLoginLog 会员登录记录
     * @return 结果
     */
    public int insertUmsMemberLoginLog(UmsMemberLoginLog umsMemberLoginLog);

    /**
     * 修改会员登录记录
     * 
     * @param umsMemberLoginLog 会员登录记录
     * @return 结果
     */
    public int updateUmsMemberLoginLog(UmsMemberLoginLog umsMemberLoginLog);

    /**
     * 批量删除会员登录记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUmsMemberLoginLogByIds(String ids);

    /**
     * 删除会员登录记录信息
     * 
     * @param id 会员登录记录ID
     * @return 结果
     */
    public int deleteUmsMemberLoginLogById(Long id);
}
