package com.ruoyi.project.mall.address.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 会员收货地址对象 ums_member_receive_address
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class UmsMemberReceiveAddress extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long memberId;

    /** 收货人名称 */
    @Excel(name = "收货人名称")
    private String name;

    /** $column.columnComment */
    @Excel(name = "收货人名称")
    private String phoneNumber;

    /** 是否为默认 */
    @Excel(name = "是否为默认")
    private Integer defaultStatus;

    /** 邮政编码 */
    @Excel(name = "邮政编码")
    private String postCode;

    /** 省份/直辖市 */
    @Excel(name = "省份/直辖市")
    private String province;

    /** 城市 */
    @Excel(name = "城市")
    private String city;

    /** 区 */
    @Excel(name = "区")
    private String region;

    /** 详细地址(街道) */
    @Excel(name = "详细地址(街道)")
    private String detailAddress;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setPhoneNumber(String phoneNumber) 
    {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() 
    {
        return phoneNumber;
    }
    public void setDefaultStatus(Integer defaultStatus) 
    {
        this.defaultStatus = defaultStatus;
    }

    public Integer getDefaultStatus() 
    {
        return defaultStatus;
    }
    public void setPostCode(String postCode) 
    {
        this.postCode = postCode;
    }

    public String getPostCode() 
    {
        return postCode;
    }
    public void setProvince(String province) 
    {
        this.province = province;
    }

    public String getProvince() 
    {
        return province;
    }
    public void setCity(String city) 
    {
        this.city = city;
    }

    public String getCity() 
    {
        return city;
    }
    public void setRegion(String region) 
    {
        this.region = region;
    }

    public String getRegion() 
    {
        return region;
    }
    public void setDetailAddress(String detailAddress) 
    {
        this.detailAddress = detailAddress;
    }

    public String getDetailAddress() 
    {
        return detailAddress;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("memberId", getMemberId())
            .append("name", getName())
            .append("phoneNumber", getPhoneNumber())
            .append("defaultStatus", getDefaultStatus())
            .append("postCode", getPostCode())
            .append("province", getProvince())
            .append("city", getCity())
            .append("region", getRegion())
            .append("detailAddress", getDetailAddress())
            .append("storeId", getStoreId())
            .toString();
    }
}
