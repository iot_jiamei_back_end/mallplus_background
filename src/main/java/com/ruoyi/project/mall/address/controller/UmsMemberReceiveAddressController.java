package com.ruoyi.project.mall.address.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.address.domain.UmsMemberReceiveAddress;
import com.ruoyi.project.mall.address.service.IUmsMemberReceiveAddressService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 会员收货地址Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/system/address")
public class UmsMemberReceiveAddressController extends BaseController
{
    private String prefix = "system/address";

    @Autowired
    private IUmsMemberReceiveAddressService umsMemberReceiveAddressService;

    @RequiresPermissions("system:address:view")
    @GetMapping()
    public String address()
    {
        return prefix + "/address";
    }

    /**
     * 查询会员收货地址列表
     */
    @RequiresPermissions("system:address:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UmsMemberReceiveAddress umsMemberReceiveAddress)
    {
        startPage();
        List<UmsMemberReceiveAddress> list = umsMemberReceiveAddressService.selectUmsMemberReceiveAddressList(umsMemberReceiveAddress);
        return getDataTable(list);
    }

    /**
     * 导出会员收货地址列表
     */
    @RequiresPermissions("system:address:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UmsMemberReceiveAddress umsMemberReceiveAddress)
    {
        List<UmsMemberReceiveAddress> list = umsMemberReceiveAddressService.selectUmsMemberReceiveAddressList(umsMemberReceiveAddress);
        ExcelUtil<UmsMemberReceiveAddress> util = new ExcelUtil<UmsMemberReceiveAddress>(UmsMemberReceiveAddress.class);
        return util.exportExcel(list, "address");
    }

    /**
     * 新增会员收货地址
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员收货地址
     */
    @RequiresPermissions("system:address:add")
    @Log(title = "会员收货地址", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UmsMemberReceiveAddress umsMemberReceiveAddress)
    {
        return toAjax(umsMemberReceiveAddressService.insertUmsMemberReceiveAddress(umsMemberReceiveAddress));
    }

    /**
     * 修改会员收货地址
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        UmsMemberReceiveAddress umsMemberReceiveAddress = umsMemberReceiveAddressService.selectUmsMemberReceiveAddressById(id);
        mmap.put("umsMemberReceiveAddress", umsMemberReceiveAddress);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员收货地址
     */
    @RequiresPermissions("system:address:edit")
    @Log(title = "会员收货地址", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UmsMemberReceiveAddress umsMemberReceiveAddress)
    {
        return toAjax(umsMemberReceiveAddressService.updateUmsMemberReceiveAddress(umsMemberReceiveAddress));
    }

    /**
     * 删除会员收货地址
     */
    @RequiresPermissions("system:address:remove")
    @Log(title = "会员收货地址", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(umsMemberReceiveAddressService.deleteUmsMemberReceiveAddressByIds(ids));
    }
}
