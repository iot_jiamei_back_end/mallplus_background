package com.ruoyi.project.mall.orderSetting.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.orderSetting.domain.OmsOrderSetting;
import java.util.List;

/**
 * 订单设置Service接口
 * 
 * @author mallplus
 * @date 2020-03-18
 */
public interface IOmsOrderSettingService extends IService<OmsOrderSetting>
{
    /**
     * 查询订单设置
     * 
     * @param id 订单设置ID
     * @return 订单设置
     */
    public OmsOrderSetting selectOmsOrderSettingById(Long id);

    /**
     * 查询订单设置列表
     * 
     * @param omsOrderSetting 订单设置
     * @return 订单设置集合
     */
    public List<OmsOrderSetting> selectOmsOrderSettingList(OmsOrderSetting omsOrderSetting);

    /**
     * 新增订单设置
     * 
     * @param omsOrderSetting 订单设置
     * @return 结果
     */
    public int insertOmsOrderSetting(OmsOrderSetting omsOrderSetting);

    /**
     * 修改订单设置
     * 
     * @param omsOrderSetting 订单设置
     * @return 结果
     */
    public int updateOmsOrderSetting(OmsOrderSetting omsOrderSetting);

    /**
     * 批量删除订单设置
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOmsOrderSettingByIds(String ids);

    /**
     * 删除订单设置信息
     * 
     * @param id 订单设置ID
     * @return 结果
     */
    public int deleteOmsOrderSettingById(Long id);
}
