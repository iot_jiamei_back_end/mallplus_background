package com.ruoyi.project.mall.orderSetting.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.orderSetting.mapper.OmsOrderSettingMapper;
import com.ruoyi.project.mall.orderSetting.domain.OmsOrderSetting;
import com.ruoyi.project.mall.orderSetting.service.IOmsOrderSettingService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 订单设置Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-18
 */
@Service
@Primary
public class OmsOrderSettingServiceImpl extends ServiceImpl<OmsOrderSettingMapper, OmsOrderSetting>  implements IOmsOrderSettingService
{
    @Autowired
    private OmsOrderSettingMapper omsOrderSettingMapper;

    /**
     * 查询订单设置
     * 
     * @param id 订单设置ID
     * @return 订单设置
     */
    @Override
    public OmsOrderSetting selectOmsOrderSettingById(Long id)
    {
        return omsOrderSettingMapper.selectOmsOrderSettingById(id);
    }

    /**
     * 查询订单设置列表
     * 
     * @param omsOrderSetting 订单设置
     * @return 订单设置
     */
    @Override
    public List<OmsOrderSetting> selectOmsOrderSettingList(OmsOrderSetting omsOrderSetting)
    {
        return omsOrderSettingMapper.selectOmsOrderSettingList(omsOrderSetting);
    }

    /**
     * 新增订单设置
     * 
     * @param omsOrderSetting 订单设置
     * @return 结果
     */
    @Override
    public int insertOmsOrderSetting(OmsOrderSetting omsOrderSetting)
    {
        return omsOrderSettingMapper.insertOmsOrderSetting(omsOrderSetting);
    }

    /**
     * 修改订单设置
     * 
     * @param omsOrderSetting 订单设置
     * @return 结果
     */
    @Override
    public int updateOmsOrderSetting(OmsOrderSetting omsOrderSetting)
    {
        return omsOrderSettingMapper.updateOmsOrderSetting(omsOrderSetting);
    }

    /**
     * 删除订单设置对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteOmsOrderSettingByIds(String ids)
    {
        return omsOrderSettingMapper.deleteOmsOrderSettingByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单设置信息
     * 
     * @param id 订单设置ID
     * @return 结果
     */
    @Override
    public int deleteOmsOrderSettingById(Long id)
    {
        return omsOrderSettingMapper.deleteOmsOrderSettingById(id);
    }
}
