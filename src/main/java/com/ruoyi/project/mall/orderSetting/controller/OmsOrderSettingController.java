package com.ruoyi.project.mall.orderSetting.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.mall.deliveryExpressCompany.domain.OmsDeliveryExpressCompany;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.orderSetting.domain.OmsOrderSetting;
import com.ruoyi.project.mall.orderSetting.service.IOmsOrderSettingService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 订单设置Controller
 *
 * @author mallplus
 * @date 2020-03-18
 */
@Controller
@RequestMapping("/mall/orderSetting")
public class OmsOrderSettingController extends BaseController {
    private String prefix = "mall/orderSetting";

    @Autowired
    private IOmsOrderSettingService omsOrderSettingService;

    @RequiresPermissions("mall:orderSetting:view")
    @GetMapping()
    public String orderSetting() {
        return prefix + "/orderSetting";
    }

    /**
     * 查询订单设置列表
     */
    @RequiresPermissions("mall:orderSetting:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(OmsOrderSetting omsOrderSetting) {
        IPage<OmsOrderSetting> page = omsOrderSettingService.page(new Page<>(1, 1), new QueryWrapper<>(null));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

//    /**
//     * 导出订单设置列表
//     */
//    @RequiresPermissions("mall:orderSetting:export")
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(OmsOrderSetting omsOrderSetting)
//    {
//        List<OmsOrderSetting> list = omsOrderSettingService.selectOmsOrderSettingList(omsOrderSetting);
//        ExcelUtil<OmsOrderSetting> util = new ExcelUtil<OmsOrderSetting>(OmsOrderSetting.class);
//        return util.exportExcel(list, "orderSetting");
//    }

//    /**
//     * 新增订单设置
//     */
//    @GetMapping("/add")
//    public String add()
//    {
//        return prefix + "/add";
//    }
//
//    /**
//     * 新增保存订单设置
//     */
//    @RequiresPermissions("mall:orderSetting:add")
//    @Log(title = "订单设置", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    @ResponseBody
//    public AjaxResult addSave(OmsOrderSetting omsOrderSetting)
//    {
//        return toAjax(omsOrderSettingService.insertOmsOrderSetting(omsOrderSetting));
//    }

    /**
     * 修改订单设置
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        OmsOrderSetting omsOrderSetting = omsOrderSettingService.getById(id);
        mmap.put("omsOrderSetting", omsOrderSetting);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单设置
     */
    @RequiresPermissions("mall:orderSetting:edit")
    @Log(title = "订单设置", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(OmsOrderSetting omsOrderSetting) {
        return toAjax(omsOrderSettingService.updateById(omsOrderSetting));
    }

//    /**
//     * 删除订单设置
//     */
//    @RequiresPermissions("mall:orderSetting:remove")
//    @Log(title = "订单设置", businessType = BusinessType.DELETE)
//    @PostMapping( "/remove")
//    @ResponseBody
//    public AjaxResult remove(String ids)
//    {
//        return toAjax(omsOrderSettingService.deleteOmsOrderSettingByIds(ids));
//    }
}
