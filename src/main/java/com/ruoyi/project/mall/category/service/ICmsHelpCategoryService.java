package com.ruoyi.project.mall.category.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.category.domain.CmsHelpCategory;

/**
 * <p>
 * 帮助分类表 服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
public interface ICmsHelpCategoryService extends IService<CmsHelpCategory> {

}
