package com.ruoyi.project.mall.category.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.mall.help.domain.CmsHelp;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.category.domain.CmsHelpCategory;
import com.ruoyi.project.mall.category.service.ICmsHelpCategoryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 帮助分类Controller
 *
 * @author mallplus
 * @date 2020-03-18
 */
@Controller
@RequestMapping("/mall/cmsHelpCategory")
public class CmsHelpCategoryController extends BaseController {
    private String prefix = "mall/category";

    @Autowired
    private ICmsHelpCategoryService cmsHelpCategoryService;

    @RequiresPermissions("mall:category:view")
    @GetMapping()
    public String category() {
        return prefix + "/category";
    }

    /**
     * 查询帮助分类列表
     */
    @RequiresPermissions("mall:category:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsHelpCategory cmsHelpCategory,Integer pageNum,Integer pageSize) {
        IPage<CmsHelpCategory> page = cmsHelpCategoryService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(null));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

    /**
     * 新增帮助分类
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存帮助分类
     */
    @RequiresPermissions("mall:category:add")
    @Log(title = "帮助分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsHelpCategory cmsHelpCategory) {
        return toAjax(cmsHelpCategoryService.save(cmsHelpCategory));
    }

    /**
     * 修改帮助分类
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        CmsHelpCategory cmsHelpCategory = cmsHelpCategoryService.getById(id);
        mmap.put("cmsHelpCategory", cmsHelpCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存帮助分类
     */
    @RequiresPermissions("mall:category:edit")
    @Log(title = "帮助分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsHelpCategory cmsHelpCategory) {
        return toAjax(cmsHelpCategoryService.updateById(cmsHelpCategory));
    }

    /**
     * 删除帮助分类
     */
    @RequiresPermissions("mall:category:remove")
    @Log(title = "帮助分类", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        String[] strArra = ids.split(",");
        for (String str : strArra) {
            cmsHelpCategoryService.removeById(str);
        }
        return toAjax(true);
    }
}
