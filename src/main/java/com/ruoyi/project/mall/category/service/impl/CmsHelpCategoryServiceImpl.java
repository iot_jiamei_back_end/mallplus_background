package com.ruoyi.project.mall.category.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.mall.category.domain.CmsHelpCategory;
import com.ruoyi.project.mall.category.mapper.CmsHelpCategoryMapper;
import com.ruoyi.project.mall.category.service.ICmsHelpCategoryService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 帮助分类表 服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@Service
@Primary
public class CmsHelpCategoryServiceImpl extends ServiceImpl<CmsHelpCategoryMapper, CmsHelpCategory> implements ICmsHelpCategoryService {

}
