package com.ruoyi.project.mall.category.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.category.domain.CmsHelpCategory;

/**
 * <p>
 * 帮助分类表 Mapper 接口
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
public interface CmsHelpCategoryMapper extends BaseMapper<CmsHelpCategory> {

}
