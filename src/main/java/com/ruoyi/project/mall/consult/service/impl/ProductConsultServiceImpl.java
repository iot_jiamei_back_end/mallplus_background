package com.ruoyi.project.mall.consult.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.consult.mapper.ProductConsultMapper;
import com.ruoyi.project.mall.consult.domain.ProductConsult;
import com.ruoyi.project.mall.consult.service.IProductConsultService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 产品咨询Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class ProductConsultServiceImpl implements IProductConsultService 
{
    @Autowired
    private ProductConsultMapper productConsultMapper;

    /**
     * 查询产品咨询
     * 
     * @param id 产品咨询ID
     * @return 产品咨询
     */
    @Override
    public ProductConsult selectProductConsultById(Integer id)
    {
        return productConsultMapper.selectProductConsultById(id);
    }

    /**
     * 查询产品咨询列表
     * 
     * @param productConsult 产品咨询
     * @return 产品咨询
     */
    @Override
    public List<ProductConsult> selectProductConsultList(ProductConsult productConsult)
    {
        return productConsultMapper.selectProductConsultList(productConsult);
    }

    /**
     * 新增产品咨询
     * 
     * @param productConsult 产品咨询
     * @return 结果
     */
    @Override
    public int insertProductConsult(ProductConsult productConsult)
    {
        return productConsultMapper.insertProductConsult(productConsult);
    }

    /**
     * 修改产品咨询
     * 
     * @param productConsult 产品咨询
     * @return 结果
     */
    @Override
    public int updateProductConsult(ProductConsult productConsult)
    {
        return productConsultMapper.updateProductConsult(productConsult);
    }

    /**
     * 删除产品咨询对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProductConsultByIds(String ids)
    {
        return productConsultMapper.deleteProductConsultByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品咨询信息
     * 
     * @param id 产品咨询ID
     * @return 结果
     */
    @Override
    public int deleteProductConsultById(Integer id)
    {
        return productConsultMapper.deleteProductConsultById(id);
    }
}
