package com.ruoyi.project.mall.consult.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 产品咨询对象 pms_product_consult
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class ProductConsult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 咨询编号 */
    private Integer id;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private Long goodsId;

    /** 库存id */
    @Excel(name = "库存id")
    private Long skuId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 咨询发布者会员编号(0：游客) */
    @Excel(name = "咨询发布者会员编号(0：游客)")
    private Long memberId;

    /** 会员名称 */
    @Excel(name = "会员名称")
    private String memberName;

    /** 店铺编号 */
    @Excel(name = "店铺编号")
    private Long storeId;

    /** 咨询发布者邮箱 */
    @Excel(name = "咨询发布者邮箱")
    private String email;

    /** 咨询内容 */
    @Excel(name = "咨询内容")
    private String consultContent;

    /** 咨询添加时间 */
    @Excel(name = "咨询添加时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date consultAddtime;

    /** 咨询回复内容 */
    @Excel(name = "咨询回复内容")
    private String consultReply;

    /** 咨询回复时间 */
    @Excel(name = "咨询回复时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date consultReplyTime;

    /** 0表示不匿名 1表示匿名 */
    @Excel(name = "0表示不匿名 1表示匿名")
    private Integer isanonymous;

    /** $column.columnComment */
    @Excel(name = "0表示不匿名 1表示匿名")
    private Integer isDel;

    /** 点赞 */
    @Excel(name = "点赞")
    private Long praise;

    /** $column.columnComment */
    @Excel(name = "点赞")
    private String pic;

    /** $column.columnComment */
    @Excel(name = "点赞")
    private String attr;

    /** $column.columnComment */
    @Excel(name = "点赞")
    private Long stars;

    /** $column.columnComment */
    @Excel(name = "点赞")
    private Long orderId;

    /** 1 商品 2 订单 */
    @Excel(name = "1 商品 2 订单")
    private Integer type;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 订单")
    private String images;

    /** 评分星数 */
    @Excel(name = "评分星数")
    private Long score;

    /** 0 显示评论  1隐藏评论 */
    @Excel(name = "0 显示评论  1隐藏评论")
    private Long status;

    /** 0 可以评论  1不可以评论 */
    @Excel(name = "0 可以评论  1不可以评论")
    private Long commentStatus;

    /** 分享图片 */
    @Excel(name = "分享图片")
    private String shareImg;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setGoodsId(Long goodsId) 
    {
        this.goodsId = goodsId;
    }

    public Long getGoodsId() 
    {
        return goodsId;
    }
    public void setSkuId(Long skuId) 
    {
        this.skuId = skuId;
    }

    public Long getSkuId() 
    {
        return skuId;
    }
    public void setGoodsName(String goodsName) 
    {
        this.goodsName = goodsName;
    }

    public String getGoodsName() 
    {
        return goodsName;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setMemberName(String memberName) 
    {
        this.memberName = memberName;
    }

    public String getMemberName() 
    {
        return memberName;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setConsultContent(String consultContent) 
    {
        this.consultContent = consultContent;
    }

    public String getConsultContent() 
    {
        return consultContent;
    }
    public void setConsultAddtime(Date consultAddtime) 
    {
        this.consultAddtime = consultAddtime;
    }

    public Date getConsultAddtime() 
    {
        return consultAddtime;
    }
    public void setConsultReply(String consultReply) 
    {
        this.consultReply = consultReply;
    }

    public String getConsultReply() 
    {
        return consultReply;
    }
    public void setConsultReplyTime(Date consultReplyTime) 
    {
        this.consultReplyTime = consultReplyTime;
    }

    public Date getConsultReplyTime() 
    {
        return consultReplyTime;
    }
    public void setIsanonymous(Integer isanonymous) 
    {
        this.isanonymous = isanonymous;
    }

    public Integer getIsanonymous() 
    {
        return isanonymous;
    }
    public void setIsDel(Integer isDel) 
    {
        this.isDel = isDel;
    }

    public Integer getIsDel() 
    {
        return isDel;
    }
    public void setPraise(Long praise) 
    {
        this.praise = praise;
    }

    public Long getPraise() 
    {
        return praise;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setAttr(String attr) 
    {
        this.attr = attr;
    }

    public String getAttr() 
    {
        return attr;
    }
    public void setStars(Long stars) 
    {
        this.stars = stars;
    }

    public Long getStars() 
    {
        return stars;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setImages(String images) 
    {
        this.images = images;
    }

    public String getImages() 
    {
        return images;
    }
    public void setScore(Long score) 
    {
        this.score = score;
    }

    public Long getScore() 
    {
        return score;
    }
    public void setStatus(Long status) 
    {
        this.status = status;
    }

    public Long getStatus() 
    {
        return status;
    }
    public void setCommentStatus(Long commentStatus) 
    {
        this.commentStatus = commentStatus;
    }

    public Long getCommentStatus() 
    {
        return commentStatus;
    }
    public void setShareImg(String shareImg) 
    {
        this.shareImg = shareImg;
    }

    public String getShareImg() 
    {
        return shareImg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("skuId", getSkuId())
            .append("goodsName", getGoodsName())
            .append("memberId", getMemberId())
            .append("memberName", getMemberName())
            .append("storeId", getStoreId())
            .append("email", getEmail())
            .append("consultContent", getConsultContent())
            .append("consultAddtime", getConsultAddtime())
            .append("consultReply", getConsultReply())
            .append("consultReplyTime", getConsultReplyTime())
            .append("isanonymous", getIsanonymous())
            .append("isDel", getIsDel())
            .append("praise", getPraise())
            .append("pic", getPic())
            .append("attr", getAttr())
            .append("stars", getStars())
            .append("orderId", getOrderId())
            .append("type", getType())
            .append("images", getImages())
            .append("score", getScore())
            .append("status", getStatus())
            .append("commentStatus", getCommentStatus())
            .append("shareImg", getShareImg())
            .toString();
    }
}
