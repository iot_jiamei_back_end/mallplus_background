package com.ruoyi.project.mall.consult.service;

import com.ruoyi.project.mall.consult.domain.ProductConsult;
import java.util.List;

/**
 * 产品咨询Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IProductConsultService 
{
    /**
     * 查询产品咨询
     * 
     * @param id 产品咨询ID
     * @return 产品咨询
     */
    public ProductConsult selectProductConsultById(Integer id);

    /**
     * 查询产品咨询列表
     * 
     * @param productConsult 产品咨询
     * @return 产品咨询集合
     */
    public List<ProductConsult> selectProductConsultList(ProductConsult productConsult);

    /**
     * 新增产品咨询
     * 
     * @param productConsult 产品咨询
     * @return 结果
     */
    public int insertProductConsult(ProductConsult productConsult);

    /**
     * 修改产品咨询
     * 
     * @param productConsult 产品咨询
     * @return 结果
     */
    public int updateProductConsult(ProductConsult productConsult);

    /**
     * 批量删除产品咨询
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProductConsultByIds(String ids);

    /**
     * 删除产品咨询信息
     * 
     * @param id 产品咨询ID
     * @return 结果
     */
    public int deleteProductConsultById(Integer id);
}
