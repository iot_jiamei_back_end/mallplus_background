package com.ruoyi.project.mall.consult.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.consult.domain.ProductConsult;
import com.ruoyi.project.mall.consult.service.IProductConsultService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 产品咨询Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/mall/consult")
public class ProductConsultController extends BaseController
{
    private String prefix = "mall/consult";

    @Autowired
    private IProductConsultService productConsultService;

    @RequiresPermissions("mall:consult:view")
    @GetMapping()
    public String consult()
    {
        return prefix + "/consult";
    }

    /**
     * 查询产品咨询列表
     */
    @RequiresPermissions("mall:consult:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductConsult productConsult)
    {
        startPage();
        List<ProductConsult> list = productConsultService.selectProductConsultList(productConsult);
        return getDataTable(list);
    }

    /**
     * 导出产品咨询列表
     */
    @RequiresPermissions("mall:consult:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductConsult productConsult)
    {
        List<ProductConsult> list = productConsultService.selectProductConsultList(productConsult);
        ExcelUtil<ProductConsult> util = new ExcelUtil<ProductConsult>(ProductConsult.class);
        return util.exportExcel(list, "consult");
    }

    /**
     * 新增产品咨询
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品咨询
     */
    @RequiresPermissions("mall:consult:add")
    @Log(title = "产品咨询", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductConsult productConsult)
    {
        return toAjax(productConsultService.insertProductConsult(productConsult));
    }

    /**
     * 修改产品咨询
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap)
    {
        ProductConsult productConsult = productConsultService.selectProductConsultById(id);
        mmap.put("productConsult", productConsult);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品咨询
     */
    @RequiresPermissions("mall:consult:edit")
    @Log(title = "产品咨询", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductConsult productConsult)
    {
        return toAjax(productConsultService.updateProductConsult(productConsult));
    }

    /**
     * 删除产品咨询
     */
    @RequiresPermissions("mall:consult:remove")
    @Log(title = "产品咨询", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productConsultService.deleteProductConsultByIds(ids));
    }
}
