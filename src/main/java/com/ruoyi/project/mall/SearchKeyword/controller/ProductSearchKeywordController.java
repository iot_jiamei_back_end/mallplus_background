package com.ruoyi.project.mall.SearchKeyword.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.SearchKeyword.domain.ProductSearchKeyword;
import com.ruoyi.project.mall.SearchKeyword.service.IProductSearchKeywordService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 搜索关键字Controller
 * 
 * @author mallplus
 * @date 2020-02-18
 */
@Controller
@RequestMapping("/mall/SearchKeyword")
public class ProductSearchKeywordController extends BaseController
{
    private String prefix = "mall/SearchKeyword";

    @Autowired
    private IProductSearchKeywordService productSearchKeywordService;

    @RequiresPermissions("mall:SearchKeyword:view")
    @GetMapping()
    public String SearchKeyword()
    {
        return prefix + "/SearchKeyword";
    }

    /**
     * 查询搜索关键字列表
     */
    @RequiresPermissions("mall:SearchKeyword:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductSearchKeyword productSearchKeyword)
    {
        startPage();
        List<ProductSearchKeyword> list = productSearchKeywordService.selectProductSearchKeywordList(productSearchKeyword);
        return getDataTable(list);
    }

    /**
     * 导出搜索关键字列表
     */
    @RequiresPermissions("mall:SearchKeyword:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductSearchKeyword productSearchKeyword)
    {
        List<ProductSearchKeyword> list = productSearchKeywordService.selectProductSearchKeywordList(productSearchKeyword);
        ExcelUtil<ProductSearchKeyword> util = new ExcelUtil<ProductSearchKeyword>(ProductSearchKeyword.class);
        return util.exportExcel(list, "SearchKeyword");
    }

    /**
     * 新增搜索关键字
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存搜索关键字
     */
    @RequiresPermissions("mall:SearchKeyword:add")
    @Log(title = "搜索关键字", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductSearchKeyword productSearchKeyword)
    {
        return toAjax(productSearchKeywordService.insertProductSearchKeyword(productSearchKeyword));
    }

    /**
     * 修改搜索关键字
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductSearchKeyword productSearchKeyword = productSearchKeywordService.selectProductSearchKeywordById(id);
        mmap.put("productSearchKeyword", productSearchKeyword);
        return prefix + "/edit";
    }

    /**
     * 修改保存搜索关键字
     */
    @RequiresPermissions("mall:SearchKeyword:edit")
    @Log(title = "搜索关键字", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductSearchKeyword productSearchKeyword)
    {
        return toAjax(productSearchKeywordService.updateProductSearchKeyword(productSearchKeyword));
    }

    /**
     * 删除搜索关键字
     */
    @RequiresPermissions("mall:SearchKeyword:remove")
    @Log(title = "搜索关键字", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productSearchKeywordService.deleteProductSearchKeywordByIds(ids));
    }
}
