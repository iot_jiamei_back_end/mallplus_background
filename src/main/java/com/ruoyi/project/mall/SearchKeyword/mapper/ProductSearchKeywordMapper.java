package com.ruoyi.project.mall.SearchKeyword.mapper;

import com.ruoyi.project.mall.SearchKeyword.domain.ProductSearchKeyword;
import java.util.List;

/**
 * 搜索关键字Mapper接口
 * 
 * @author mallplus
 * @date 2020-02-18
 */
public interface ProductSearchKeywordMapper 
{
    /**
     * 查询搜索关键字
     * 
     * @param id 搜索关键字ID
     * @return 搜索关键字
     */
    public ProductSearchKeyword selectProductSearchKeywordById(Long id);

    /**
     * 查询搜索关键字列表
     * 
     * @param productSearchKeyword 搜索关键字
     * @return 搜索关键字集合
     */
    public List<ProductSearchKeyword> selectProductSearchKeywordList(ProductSearchKeyword productSearchKeyword);

    /**
     * 新增搜索关键字
     * 
     * @param productSearchKeyword 搜索关键字
     * @return 结果
     */
    public int insertProductSearchKeyword(ProductSearchKeyword productSearchKeyword);

    /**
     * 修改搜索关键字
     * 
     * @param productSearchKeyword 搜索关键字
     * @return 结果
     */
    public int updateProductSearchKeyword(ProductSearchKeyword productSearchKeyword);

    /**
     * 删除搜索关键字
     * 
     * @param id 搜索关键字ID
     * @return 结果
     */
    public int deleteProductSearchKeywordById(Long id);

    /**
     * 批量删除搜索关键字
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProductSearchKeywordByIds(String[] ids);
}
