package com.ruoyi.project.mall.SearchKeyword.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.SearchKeyword.mapper.ProductSearchKeywordMapper;
import com.ruoyi.project.mall.SearchKeyword.domain.ProductSearchKeyword;
import com.ruoyi.project.mall.SearchKeyword.service.IProductSearchKeywordService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 搜索关键字Service业务层处理
 * 
 * @author mallplus
 * @date 2020-02-18
 */
@Service
public class ProductSearchKeywordServiceImpl implements IProductSearchKeywordService 
{
    @Autowired
    private ProductSearchKeywordMapper productSearchKeywordMapper;

    /**
     * 查询搜索关键字
     * 
     * @param id 搜索关键字ID
     * @return 搜索关键字
     */
    @Override
    public ProductSearchKeyword selectProductSearchKeywordById(Long id)
    {
        return productSearchKeywordMapper.selectProductSearchKeywordById(id);
    }

    /**
     * 查询搜索关键字列表
     * 
     * @param productSearchKeyword 搜索关键字
     * @return 搜索关键字
     */
    @Override
    public List<ProductSearchKeyword> selectProductSearchKeywordList(ProductSearchKeyword productSearchKeyword)
    {
        return productSearchKeywordMapper.selectProductSearchKeywordList(productSearchKeyword);
    }

    /**
     * 新增搜索关键字
     * 
     * @param productSearchKeyword 搜索关键字
     * @return 结果
     */
    @Override
    public int insertProductSearchKeyword(ProductSearchKeyword productSearchKeyword)
    {
        return productSearchKeywordMapper.insertProductSearchKeyword(productSearchKeyword);
    }

    /**
     * 修改搜索关键字
     * 
     * @param productSearchKeyword 搜索关键字
     * @return 结果
     */
    @Override
    public int updateProductSearchKeyword(ProductSearchKeyword productSearchKeyword)
    {
        return productSearchKeywordMapper.updateProductSearchKeyword(productSearchKeyword);
    }

    /**
     * 删除搜索关键字对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProductSearchKeywordByIds(String ids)
    {
        return productSearchKeywordMapper.deleteProductSearchKeywordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除搜索关键字信息
     * 
     * @param id 搜索关键字ID
     * @return 结果
     */
    @Override
    public int deleteProductSearchKeywordById(Long id)
    {
        return productSearchKeywordMapper.deleteProductSearchKeywordById(id);
    }
}
