package com.ruoyi.project.mall.user.entity.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/16
 **/

@Data
public class GoodsStatisticsVo {

    private Integer itHasBeenAddedTo;

    private Integer notListed;

    private Integer underReview;

    private List<ProductAttributesVo> productAttributesVo;

}
