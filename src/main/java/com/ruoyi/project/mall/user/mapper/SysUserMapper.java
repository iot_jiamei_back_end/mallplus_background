package com.ruoyi.project.mall.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.user.domain.SysUser;
import com.ruoyi.project.mall.user.entity.dto.SysUserDto;
import com.ruoyi.project.mall.user.entity.po.SysUserCoutmPo;

import java.util.List;

/**
 * 店铺Mapper接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 查询店铺
     *
     * @param id 店铺ID
     * @return 店铺
     */
    public SysUser selectStoreById(Long id);

    /**
     * 查询店铺列表
     *
     * @param store 店铺
     * @return 店铺集合
     */
    public List<SysUser> selectStoreList(SysUser store);

    /**
     * 新增店铺
     *
     * @param store 店铺
     * @return 结果
     */
    public int insertStore(SysUser store);

    /**
     * 修改店铺
     *
     * @param store 店铺
     * @return 结果
     */
    public int updateStore(SysUser store);

    /**
     * 删除店铺
     *
     * @param id 店铺ID
     * @return 结果
     */
    public int deleteStoreById(Long id);

    /**
     * 批量删除店铺
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreByIds(String[] ids);

    List<SysUserCoutmPo> list(SysUserDto sysUserDto);

}
