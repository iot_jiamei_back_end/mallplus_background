package com.ruoyi.project.mall.user.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class ProductAttributesVo {

    private Long id;

    private String name;

}
