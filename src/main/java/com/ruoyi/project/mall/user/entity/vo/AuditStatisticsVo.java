package com.ruoyi.project.mall.user.entity.vo;

import com.ruoyi.project.mall.product.domain.ProductCategory;
import lombok.Data;

import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/18
 **/

@Data
public class AuditStatisticsVo {

    private Integer pendingReview;

    private Integer examinationPassed;

    private Integer auditNotPassed;

    private List<ProductCategory> children;
}
