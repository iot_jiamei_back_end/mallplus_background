package com.ruoyi.project.mall.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 店铺对象 sys_store
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Data
@TableName("sys_user")
public class SysUser {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    @Excel
    private String username;

    @Excel
    private String password;

    @Excel
    private String icon;

    @Excel
    private String email;

    @Excel
    private String nickName;

    @Excel
    private String note;

    @Excel
    private Date createTime;

    @Excel
    private Date loginTime;

    @Excel
    private Integer status;

    @Excel
    private Integer supplyId;

    @Excel
    private Long storeId;

    @Excel
    private String storeName;

    @Excel
    private String phone;

    @Excel
    private Long deptId;

    @Excel
    private Integer sex;

    @Excel
    private Long postId;

    @Excel
    private Integer isAdmin;

}
