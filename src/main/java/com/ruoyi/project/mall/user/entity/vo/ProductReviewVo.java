package com.ruoyi.project.mall.user.entity.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ZuRongTang
 * @Date 2020/3/18
 **/

@Data
public class ProductReviewVo {

    private Long id;

    private String pic;

    private String name;

    private String goodsName;

    private String productCategoryName;

    private BigDecimal price;

    private Integer verifyStatus;

    private Date varifyTime;


}
