package com.ruoyi.project.mall.user.entity.dto;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class CompanyProductDto {

    private Long id;

    //0表示上架、1表示下架 //审核中
    private Integer type;

    //商品编号
    private Long productNumber;

    //商品名称
    private String productName;

    //商品属性
    private Long productAttributes;


    private String startTime;


    private String endTime;


}
