package com.ruoyi.project.mall.user.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/16
 **/

@Data
public class StoreVo {


    private String name;

    private String expireTime;

    private String contactName;

    private char contactMobile;

    private String servicePhone;

    private String contactEmail;

    private String brandName;

    private String companyAddress;

    private String businessScope;

    private String description;

    private String supportName;

    private String idCardFront;

    private String idCardReverse;

    private String logo;



}
