package com.ruoyi.project.mall.user.entity.dto;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/18
 **/

@Data
public class ProductReviewDto {

    private Integer approvalStatus;

    private Long id;

    private String name;

    private Long sortId;

    private String startTime;

    private String endTime;


}
