package com.ruoyi.project.mall.user.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.user.entity.dto.CompanyProductDto;
import com.ruoyi.project.mall.user.entity.dto.MerchantDto;
import com.ruoyi.project.mall.user.entity.dto.ProductReviewDto;
import com.ruoyi.project.mall.user.entity.dto.SysUserDto;
import com.ruoyi.project.mall.user.entity.po.SysUserCoutmPo;
import com.ruoyi.project.mall.user.entity.vo.AuditStatisticsVo;
import com.ruoyi.project.mall.user.entity.vo.ProductInfoVo;
import com.ruoyi.project.mall.user.entity.vo.ProductReviewVo;
import com.ruoyi.project.mall.user.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺Controller
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Controller
@RequestMapping("/mall/sysuser")
public class SysUserController extends BaseController {
    private String prefix = "mall/user";

    @Autowired
    private ISysUserService sysUserService;

    @RequiresPermissions("mall:user:view")
    @GetMapping()
    public String store() {
        return prefix + "/user";
    }

    /**
     * 获取商家列表
     */
    @RequiresPermissions("mall:user:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysUserDto sysUserDto) {
        startPage();
        List<SysUserCoutmPo> list = sysUserService.list(sysUserDto);
        return getDataTable(list);
    }


    /**
     * 启用商家列表信息
     *
     * @param userId
     * @param start
     * @return
     */
    @RequiresPermissions("mall:store:edit")
    @PostMapping("/disable")
    @ResponseBody
    public AjaxResult disable(Long userId, Integer start) {
        return toAjax(sysUserService.disable(userId, start));
    }


    /**
     * 查看合同详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("mall:store:edit")
    @ResponseBody
    @PostMapping("/viewBusinessInformation")
    public AjaxResult viewBusinessInformation(Long id) {
        return AjaxResult.success(sysUserService.viewBusinessInformation(id));
    }

    /**
     * 编辑商家信息
     *
     * @param store
     * @return
     */

    @RequiresPermissions("mall:store:edit")
    @ResponseBody
    @RequestMapping("/editCompanyInformation")
    public AjaxResult editCompanyInformation(Store store) {
        return toAjax(sysUserService.editCompanyInformation(store));
    }

    /**
     * 商家添加账号密码
     *
     * @param id
     * @param username
     * @param password
     * @return
     */

    @PostMapping("/addMerchantAccountInformation")
    public AjaxResult addMerchantAccountInformation(Long id, String username, String password) {
        sysUserService.addMerchantAccountInformation(id, username, password);
        return success();
    }

    /**
     * 获取商品详情统计、已上架、 以下架、审核中
     *
     * @param id
     * @return
     */
    @PostMapping("/commodityStatistics")
    public AjaxResult commodityStatistics(Long id, Integer type) {
        return AjaxResult.success(sysUserService.commodityStatistics(id, type));
    }


    /*
     * 获取商品列表
     *
     * @param companyProductDto
     * @return
     */

    @PostMapping("/companyProductList")
    public TableDataInfo companyProductList(CompanyProductDto companyProductDto) {
        startPage();
        return getDataTable(sysUserService.companyProductList(companyProductDto));
    }

    /**
     * 查看商品详情
     *
     * @param id
     * @return
     */
    @PostMapping("/viewProductDetails")
    public AjaxResult viewProductDetails(Long id) {
        ProductInfoVo productInfoVo = sysUserService.viewProductDetails(id);
        return AjaxResult.success(productInfoVo);
    }

    /**
     * 商品上下架
     *
     * @param id
     * @param start
     * @return
     */

    @PostMapping("/takeOff")
    public AjaxResult takeOff(Long id, Integer start) {
        sysUserService.takeOff(id, start);
        return AjaxResult.success();
    }

    @RequiresPermissions("mall:user:view")
    @GetMapping("/check")
    public String check() {
        return prefix + "/check";
    }


    //file:///C:/Users/Administrator/Desktop/商城运营后台/index.html#g=1&p=商家审核列表
    @PostMapping("/merchantReviewList")
    @ResponseBody
    public TableDataInfo merchantReviewList(Integer pageNum, Integer pageSize, MerchantDto merchantDto) {
        return getDataTable(sysUserService.merchantReviewList(merchantDto));
    }

    //file:///C:/Users/Administrator/Desktop/商城运营后台/index.html#g=1&p=商家详情（待审核）
    @PostMapping("/businessDetails")
    public AjaxResult businessDetails(Long id) {
        return AjaxResult.success(sysUserService.businessDetails(id));
    }

    //file:///C:/Users/Administrator/Desktop/商城运营后台/index.html#g=1&p=商家详情（审核通过or不通过）
    @PostMapping("/reviewDetails")
    public AjaxResult reviewDetails(Long id) {
        return AjaxResult.success(sysUserService.businessDetails(id));
    }

    //审核通过
    @PostMapping("/examinationPassed")
    public AjaxResult examinationPassed(Long id) {
        Store store = sysUserService.businessDetails(id);
        store.setStatus(1);
        sysUserService.examinationPassed(store);
        return AjaxResult.success();
    }

    //审核不通过
    @PostMapping("/auditNotPassed")
    public AjaxResult auditNotPassed(Long id, String conter) {
        Store store = sysUserService.businessDetails(id);
        store.setStatus(3);
        store.setVerifyComment(conter);
        sysUserService.examinationPassed(store);
        return AjaxResult.success();
    }

    /**
     * 审核统计
     *
     * @return
     */
    @PostMapping("/auditStatistics")
    public AjaxResult auditStatistics() {
        AuditStatisticsVo auditStatisticsVo = sysUserService.auditStatistics();
        return AjaxResult.success(auditStatisticsVo);
    }


    //file:///C:/Users/Administrator/Desktop/商城运营后台/index.html#g=1&p=商品审核列表
    @PostMapping("/productReviewList")
    public TableDataInfo productReviewList(ProductReviewDto productReviewDto) {
        startPage();
        List<ProductReviewVo> productReviewVos = sysUserService.productReviewList(productReviewDto);
        return getDataTable(productReviewVos);
    }

    /**
     * 审核详情
     *
     * @param id
     * @return
     */
    @PostMapping("/goodsReviewDetails")
    public AjaxResult goodsReviewDetails(Long id) {
        return AjaxResult.success(sysUserService.reviewDetails(id));
    }

    /**
     * 商品详情
     *
     * @param id
     * @return
     */
    //file:///C:/Users/Administrator/Desktop/商城运营后台/index.html#g=1&p=商品详情
    @PostMapping("/productDetails")
    public AjaxResult productDetails(Long id) {
        return AjaxResult.success(sysUserService.productDetails(id));
    }


}
