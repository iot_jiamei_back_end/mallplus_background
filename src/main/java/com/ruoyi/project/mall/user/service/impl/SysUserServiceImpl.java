package com.ruoyi.project.mall.user.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.mall.product.domain.PmsProduct;
import com.ruoyi.project.mall.product.domain.PmsProductVertifyRecord;
import com.ruoyi.project.mall.product.domain.ProductCategory;
import com.ruoyi.project.mall.product.mapper.PmsProductMapper;
import com.ruoyi.project.mall.product.mapper.PmsProductVertifyRecordMapper;
import com.ruoyi.project.mall.product.mapper.ProductCategoryMapper;
import com.ruoyi.project.mall.stock.domain.SkuStock;
import com.ruoyi.project.mall.stock.mapper.SkuStockMapper;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.mapper.StoreMapper;
import com.ruoyi.project.mall.user.domain.SysUser;
import com.ruoyi.project.mall.user.entity.dto.CompanyProductDto;
import com.ruoyi.project.mall.user.entity.dto.MerchantDto;
import com.ruoyi.project.mall.user.entity.dto.ProductReviewDto;
import com.ruoyi.project.mall.user.entity.dto.SysUserDto;
import com.ruoyi.project.mall.user.entity.po.ProductInfoPo;
import com.ruoyi.project.mall.user.entity.po.SysUserCoutmPo;
import com.ruoyi.project.mall.user.entity.vo.*;
import com.ruoyi.project.mall.user.mapper.SysUserMapper;
import com.ruoyi.project.mall.user.service.ISysUserService;
import org.springframework.beans.BeanUtils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 店铺Service业务层处理
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<StoreMapper, Store> implements ISysUserService {
    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private StoreMapper storeMapper;

    @Resource
    private PmsProductMapper pmsProductMapper;

    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Resource
    private SkuStockMapper skuStockMapper;

    @Resource
    private ProductCategoryMapper productCategoryMapper;

    @Resource
    private PmsProductVertifyRecordMapper pmsProductVertifyRecordMapper;

    @Override
    public List<SysUserCoutmPo> list(SysUserDto sysUserDto) {
        return sysUserMapper.list(sysUserDto);
    }

    @Override
    public int disable(Long userId, Integer start) {
        SysUser sysUser = sysUserMapper.selectStoreById(userId);
        sysUser.setStatus(start);
        return sysUserMapper.updateStore(sysUser);
    }

    @Override
    public StoreVo viewBusinessInformation(Long id) {
        StoreVo storeVo = new StoreVo();

        Store store = storeMapper.selectStoreById(id);

        if (store != null) {
            BeanUtils.copyProperties(store, storeVo);
        }
        return storeVo;
    }

    @Override
    public int editCompanyInformation(Store store) {
        return storeMapper.updateStore(store);
    }

    @Override
    public void addMerchantAccountInformation(Long id, String username, String password) {
        SysUser umsAdmin = new SysUser();
        String md5Password = passwordEncoder.encode(password);
        Store store = storeMapper.selectStoreById(id);

        umsAdmin.setSex(0);
        umsAdmin.setStatus(0);
        umsAdmin.setStoreId(id);
        umsAdmin.setCreateTime(new Date());
        umsAdmin.setIcon(store.getLogo());
        umsAdmin.setNickName(store.getName());
        umsAdmin.setEmail(store.getContactEmail());
        umsAdmin.setIsAdmin(1);
        umsAdmin.setPhone(store.getServicePhone());
        umsAdmin.setUsername(username);
        umsAdmin.setPassword(md5Password);

        sysUserMapper.insertStore(umsAdmin);

    }

    @Override
    public GoodsStatisticsVo commodityStatistics(Long id, Integer type) {

        List<PmsProduct> itHasBeenAddedToList = new ArrayList<>();
        List<PmsProduct> notListedList = new ArrayList<>();

        Integer itHasBeenAddedTo = 0;
        Integer notListed = 0;

        GoodsStatisticsVo goodsStatisticsVo = new GoodsStatisticsVo();

        PmsProduct pmsProduct = new PmsProduct();
        pmsProduct.setStoreId(id);
        pmsProduct.setVerifyStatus(1);

        List<PmsProduct> pmsProducts = pmsProductMapper.selectPmsProductList(pmsProduct);

        for (PmsProduct product : pmsProducts) {
            Integer publishStatus = product.getPublishStatus();

            if (publishStatus == 1) {
                itHasBeenAddedTo++;
                itHasBeenAddedToList.add(product);

            } else {
                notListed++;
                notListedList.add(product);
            }
        }

        pmsProduct.setStoreId(id);
        pmsProduct.setVerifyStatus(0);
        List<PmsProduct> pmsProducts1 = pmsProductMapper.selectPmsProductList(pmsProduct);

        goodsStatisticsVo.setItHasBeenAddedTo(itHasBeenAddedTo);
        goodsStatisticsVo.setNotListed(notListed);
        goodsStatisticsVo.setUnderReview(pmsProducts1.size());

        List<ProductAttributesVo> productAttributesList = new ArrayList<>();

        //已上架
        if (type == 0) {
            for (PmsProduct product : itHasBeenAddedToList) {
                Long productId = product.getId();
                SkuStock skuStock = new SkuStock();
                skuStock.setProductId(productId);
                List<SkuStock> skuStocks = skuStockMapper.selectSkuStockList(skuStock);

                for (SkuStock stock : skuStocks) {
                    ProductAttributesVo productAttributesVo = new ProductAttributesVo();
                    String name = "";

                    Long skuId = stock.getId();
                    String sp1 = stock.getSp1();
                    String sp2 = stock.getSp2();
                    String sp3 = stock.getSp3();
                    String sp4 = stock.getSp4();

                    if (!StringUtils.isEmpty(sp1)) {
                        name = sp1;
                    }
                    if (!StringUtils.isEmpty(sp2)) {
                        name = name + "+" + sp2;
                    }
                    if (!StringUtils.isEmpty(sp3)) {
                        name = name + "+" + sp3;
                    }
                    if (!StringUtils.isEmpty(sp4)) {
                        name = name + "+" + sp4;
                    }
                    productAttributesVo.setId(skuId);
                    productAttributesVo.setName(name);

                    productAttributesList.add(productAttributesVo);
                }


            }
            //已下架
        } else if (type == 1) {
            for (PmsProduct product : notListedList) {
                Long productId = product.getId();
                SkuStock skuStock = new SkuStock();
                skuStock.setProductId(productId);
                List<SkuStock> skuStocks = skuStockMapper.selectSkuStockList(skuStock);

                for (SkuStock stock : skuStocks) {
                    ProductAttributesVo productAttributesVo = new ProductAttributesVo();
                    String name = "";

                    Long skuId = stock.getId();
                    String sp1 = stock.getSp1();
                    String sp2 = stock.getSp2();
                    String sp3 = stock.getSp3();
                    String sp4 = stock.getSp4();

                    if (!StringUtils.isEmpty(sp1)) {
                        name = sp1;
                    }
                    if (!StringUtils.isEmpty(sp2)) {
                        name = name + "+" + sp2;
                    }
                    if (!StringUtils.isEmpty(sp3)) {
                        name = name + "+" + sp3;
                    }
                    if (!StringUtils.isEmpty(sp4)) {
                        name = name + "+" + sp4;
                    }
                    productAttributesVo.setId(skuId);
                    productAttributesVo.setName(name);

                    productAttributesList.add(productAttributesVo);
                }
            }

        } else {
            for (PmsProduct product : pmsProducts1) {
                Long productId = product.getId();
                SkuStock skuStock = new SkuStock();
                skuStock.setProductId(productId);
                List<SkuStock> skuStocks = skuStockMapper.selectSkuStockList(skuStock);

                for (SkuStock stock : skuStocks) {
                    ProductAttributesVo productAttributesVo = new ProductAttributesVo();
                    String name = "";

                    Long skuId = stock.getId();
                    String sp1 = stock.getSp1();
                    String sp2 = stock.getSp2();
                    String sp3 = stock.getSp3();
                    String sp4 = stock.getSp4();

                    if (!StringUtils.isEmpty(sp1)) {
                        name = sp1;
                    }
                    if (!StringUtils.isEmpty(sp2)) {
                        name = name + "+" + sp2;
                    }
                    if (!StringUtils.isEmpty(sp3)) {
                        name = name + "+" + sp3;
                    }
                    if (!StringUtils.isEmpty(sp4)) {
                        name = name + "+" + sp4;
                    }
                    productAttributesVo.setId(skuId);
                    productAttributesVo.setName(name);

                    productAttributesList.add(productAttributesVo);
                }
            }

        }

        goodsStatisticsVo.setProductAttributesVo(productAttributesList);

        return goodsStatisticsVo;
    }

    @Override
    public List<ProductInfoPo> companyProductList(CompanyProductDto companyProductDto) {
        return pmsProductMapper.companyProductList(companyProductDto);
    }

    @Override
    public ProductInfoVo viewProductDetails(Long id) {

        PmsProduct pmsProduct = pmsProductMapper.selectPmsProductById(id);
        SkuStock skuStock = new SkuStock();
        skuStock.setProductId(id);
        List<SkuStock> skuStocks = skuStockMapper.selectSkuStockList(skuStock);
        ProductInfoVo productInfoVo = new ProductInfoVo();
        BeanUtils.copyProperties(pmsProduct, productInfoVo);
        productInfoVo.setSkuStocks(skuStocks);

        return productInfoVo;
    }

    @Override
    public void takeOff(Long id, Integer start) {
        PmsProduct pmsProduct = pmsProductMapper.selectPmsProductById(id);
        pmsProduct.setPublishStatus(start);
        pmsProductMapper.updatePmsProduct(pmsProduct);

    }

    @Override
    public List<Store> merchantReviewList(MerchantDto merchantDto) {
        return null;
    }

    @Override
    public Store businessDetails(Long id) {
        return storeMapper.selectStoreById(id);
    }

    @Override
    public void examinationPassed(Store store) {
        storeMapper.updateStore(store);
    }

    @Override
    public List<ProductReviewVo> productReviewList(ProductReviewDto productReviewDto) {
        return pmsProductMapper.productReviewList(null, null, productReviewDto.getApprovalStatus(), productReviewDto.getId(), productReviewDto.getName(), productReviewDto.getSortId(), productReviewDto.getStartTime(), productReviewDto.getEndTime());

    }

    @Override
    public AuditStatisticsVo auditStatistics() {
        AuditStatisticsVo auditStatisticsVo = new AuditStatisticsVo();
        List<PmsProduct> pmsProducts = pmsProductMapper.selectPmsProductList(null);
        Integer pendingReview = 0;
        Integer examinationPassed = 0;
        Integer auditNotPassed = 0;

        if (pmsProducts.size() > 0) {
            for (PmsProduct pmsProduct : pmsProducts) {
                Integer verifyStatus = pmsProduct.getVerifyStatus();

                if (verifyStatus == 0) {
                    pendingReview++;
                } else if (verifyStatus == 1) {
                    examinationPassed++;
                } else {
                    auditNotPassed++;
                }
            }
        }
        List<ProductCategory> listOfCategories = productCategoryMapper.getListOfCategories();

        auditStatisticsVo.setChildren(listOfCategories);
        auditStatisticsVo.setPendingReview(pendingReview);
        auditStatisticsVo.setAuditNotPassed(auditNotPassed);
        auditStatisticsVo.setExaminationPassed(examinationPassed);
        return auditStatisticsVo;
    }

    @Override
    public List<PmsProductVertifyRecord> reviewDetails(Long id) {
        PmsProductVertifyRecord pmsProductVertifyRecord = new PmsProductVertifyRecord();
        pmsProductVertifyRecord.setProductId(id);
        return pmsProductVertifyRecordMapper.selectPmsProductVertifyRecordList(pmsProductVertifyRecord);
    }

    @Override
    public ProductInfoVo productDetails(Long id) {
        ProductInfoVo productInfoVo = new ProductInfoVo();
        ProductCategory productCategory = productCategoryMapper.selectProductCategoryById(id);
        SkuStock skuStock = new SkuStock();
        skuStock.setProductId(id);
        List<SkuStock> skuStocks = skuStockMapper.selectSkuStockList(skuStock);
        BeanUtils.copyProperties(productCategory, productInfoVo);
        productInfoVo.setSkuStocks(skuStocks);
        return productInfoVo;
    }

}
