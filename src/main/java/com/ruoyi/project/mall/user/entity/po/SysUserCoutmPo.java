package com.ruoyi.project.mall.user.entity.po;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/16
 **/

@Data
public class SysUserCoutmPo {

    private Integer id;

    private Integer userId;

    private String logo;

    private String name;

    private String contactName;

    private String servicePhone;

    private String username;

    private Integer status;

    private String createTime;

}
