package com.ruoyi.project.mall.user.entity.vo;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.project.mall.stock.domain.SkuStock;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class ProductInfoVo {

    private Long id;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long brandId;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long productCategoryId;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long feightTemplateId;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long productAttributeCategoryId;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pic;

    /**
     * 货号
     */
    @Excel(name = "货号")
    private String productSn;

    /**
     * 删除状态：0->未删除；1->已删除
     */
    @Excel(name = "删除状态：0->未删除；1->已删除")
    private Integer deleteStatus;

    /**
     * 上架状态：0->下架；1->上架
     */
    @Excel(name = "上架状态：0->下架；1->上架")
    private Integer publishStatus;

    /**
     * 新品状态:0->不是新品；1->新品
     */
    @Excel(name = "新品状态:0->不是新品；1->新品")
    private Integer newStatus;

    /**
     * 推荐状态；0->不推荐；1->推荐
     */
    @Excel(name = "推荐状态；0->不推荐；1->推荐")
    private Integer recommandStatus;

    /**
     * 审核状态：0->未审核；1->审核通过
     */
    @Excel(name = "审核状态：0->未审核；1->审核通过")
    private Integer verifyStatus;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Long sort;

    /**
     * 销量
     */
    @Excel(name = "销量")
    private Long sale;

    /**
     * $column.columnComment
     */
    @Excel(name = "销量")
    private Double price;

    /**
     * 促销价格
     */
    @Excel(name = "促销价格")
    private Double promotionPrice;

    /**
     * 赠送的成长值
     */
    @Excel(name = "赠送的成长值")
    private Long giftGrowth;

    /**
     * 赠送的积分
     */
    @Excel(name = "赠送的积分")
    private Long giftPoint;

    /**
     * 限制使用的积分数
     */
    @Excel(name = "限制使用的积分数")
    private Long usePointLimit;

    /**
     * 副标题
     */
    @Excel(name = "副标题")
    private String subTitle;

    /**
     * 商品描述
     */
    @Excel(name = "商品描述")
    private String description;

    /**
     * 市场价
     */
    @Excel(name = "市场价")
    private Double originalPrice;

    /**
     * 库存
     */
    @Excel(name = "库存")
    private Long stock;

    /**
     * 库存预警值
     */
    @Excel(name = "库存预警值")
    private Long lowStock;

    /**
     * 单位
     */
    @Excel(name = "单位")
    private String unit;

    /**
     * 商品重量，默认为克
     */
    @Excel(name = "商品重量，默认为克")
    private Double weight;

    /**
     * 是否为预告商品：0->不是；1->是
     */
    @Excel(name = "是否为预告商品：0->不是；1->是")
    private Integer previewStatus;

    /**
     * 以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮
     */
    @Excel(name = "以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮")
    private String serviceIds;

    /**
     * $column.columnComment
     */
    @Excel(name = "以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮")
    private String keywords;

    /**
     * $column.columnComment
     */
    @Excel(name = "以逗号分割的产品服务：1->无忧退货；2->快速退款；3->免费包邮")
    private String note;

    /**
     * 画册图片，连产品图片限制为5张，以逗号分割
     */
    @Excel(name = "画册图片，连产品图片限制为5张，以逗号分割")
    private String albumPics;

    /**
     * $column.columnComment
     */
    @Excel(name = "画册图片，连产品图片限制为5张，以逗号分割")
    private String detailTitle;

    /**
     * $column.columnComment
     */
    @Excel(name = "画册图片，连产品图片限制为5张，以逗号分割")
    private String detailDesc;

    /**
     * 产品详情网页内容
     */
    @Excel(name = "产品详情网页内容")
    private String detailHtml;

    /**
     * 移动端网页详情
     */
    @Excel(name = "移动端网页详情")
    private String detailMobileHtml;

    /**
     * 促销开始时间
     */
    @Excel(name = "促销开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date promotionStartTime;

    /**
     * 促销结束时间
     */
    @Excel(name = "促销结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date promotionEndTime;

    /**
     * 活动限购数量
     */
    @Excel(name = "活动限购数量")
    private Long promotionPerLimit;

    /**
     * 促销类型：0->没有促销使用原价;1->使用促销价；2->使用会员价；3->使用阶梯价格；4->使用满减价格；5->限时购
     */
    @Excel(name = "促销类型：0->没有促销使用原价;1->使用促销价；2->使用会员价；3->使用阶梯价格；4->使用满减价格；5->限时购")
    private Integer promotionType;

    /**
     * 品牌名称
     */
    @Excel(name = "品牌名称")
    private String brandName;

    /**
     * 商品分类名称
     */
    @Excel(name = "商品分类名称")
    private String productCategoryName;

    /**
     * $column.columnComment
     */
    @Excel(name = "商品分类名称")
    private Long supplyId;

    /**
     * $column.columnComment
     */
    @Excel(name = "商品分类名称")
    private Long schoolId;

    /**
     * 所属店铺
     */
    @Excel(name = "所属店铺")
    private Long storeId;

    /**
     * $column.columnComment
     */
    @Excel(name = "所属店铺")
    private Long memberId;

    /**
     * $column.columnComment
     */
    @Excel(name = "所属店铺")
    private Long hit;

    /**
     * $column.columnComment
     */
    @Excel(name = "所属店铺")
    private Long type;

    /**
     * $column.columnComment
     */
    @Excel(name = "所属店铺")
    private Long areaId;

    /**
     * $column.columnComment
     */
    @Excel(name = "所属店铺")
    private String areaName;

    /**
     * $column.columnComment
     */
    @Excel(name = "所属店铺")
    private String schoolName;

    private List<SkuStock> skuStocks;


}
