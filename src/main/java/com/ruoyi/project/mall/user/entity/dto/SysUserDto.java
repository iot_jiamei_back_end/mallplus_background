package com.ruoyi.project.mall.user.entity.dto;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/16
 **/

@Data
public class SysUserDto {

    private String name;

    private String startTime;

    private String endTime;

    private Integer status;
}
