package com.ruoyi.project.mall.user.entity.po;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class ProductInfoPo {

    private Long id;

    private String pic;

    private String name;

    private String sp1;

    private String sp2;

    private String sp3;

    private String sp4;

    private String productCategoryName;

    private BigDecimal originalPrice;

    private BigDecimal price;

    private Integer sale;

    private Integer type;

    private String publishTime;


}
