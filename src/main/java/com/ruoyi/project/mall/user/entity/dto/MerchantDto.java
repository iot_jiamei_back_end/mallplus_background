package com.ruoyi.project.mall.user.entity.dto;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/17
 **/

@Data
public class MerchantDto {

    private String name;

    private String startTime;

    private String endTime;

    private Integer status;
}
