package com.ruoyi.project.mall.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.product.domain.PmsProductVertifyRecord;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.user.entity.dto.CompanyProductDto;
import com.ruoyi.project.mall.user.entity.dto.MerchantDto;
import com.ruoyi.project.mall.user.entity.dto.ProductReviewDto;
import com.ruoyi.project.mall.user.entity.dto.SysUserDto;
import com.ruoyi.project.mall.user.entity.po.ProductInfoPo;
import com.ruoyi.project.mall.user.entity.po.SysUserCoutmPo;
import com.ruoyi.project.mall.user.entity.vo.*;

import java.util.List;

/**
 * 店铺Service接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface ISysUserService extends IService<Store> {

    List<SysUserCoutmPo> list(SysUserDto sysUserDto);

    int disable(Long userId, Integer start);

    StoreVo viewBusinessInformation(Long id);

    int editCompanyInformation(Store store);

    void addMerchantAccountInformation(Long id, String username, String password);

    GoodsStatisticsVo commodityStatistics(Long id, Integer type);

    List<ProductInfoPo> companyProductList(CompanyProductDto companyProductDto);

    ProductInfoVo viewProductDetails(Long id);

    void takeOff(Long id, Integer start);

    List<Store> merchantReviewList(MerchantDto merchantDto);

    Store businessDetails(Long id);

    void examinationPassed(Store store);

    List<ProductReviewVo> productReviewList(ProductReviewDto productReviewDto);

    AuditStatisticsVo auditStatistics();

    List<PmsProductVertifyRecord> reviewDetails(Long id);

    ProductInfoVo productDetails(Long id);
}
