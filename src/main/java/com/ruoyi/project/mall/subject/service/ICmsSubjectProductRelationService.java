package com.ruoyi.project.mall.subject.service;


import com.ruoyi.project.mall.subject.domain.CmsSubjectProductRelation;

import java.util.List;

/**
 * 专题商品关系Service接口
 * 
 * @author mallplus
 * @date 2020-02-15
 */
public interface ICmsSubjectProductRelationService 
{
    /**
     * 查询专题商品关系
     * 
     * @param id 专题商品关系ID
     * @return 专题商品关系
     */
    public CmsSubjectProductRelation selectCmsSubjectProductRelationById(Long id);

    /**
     * 查询专题商品关系列表
     * 
     * @param cmsSubjectProductRelation 专题商品关系
     * @return 专题商品关系集合
     */
    public List<CmsSubjectProductRelation> selectCmsSubjectProductRelationList(CmsSubjectProductRelation cmsSubjectProductRelation);

    /**
     * 新增专题商品关系
     * 
     * @param cmsSubjectProductRelation 专题商品关系
     * @return 结果
     */
    public int insertCmsSubjectProductRelation(CmsSubjectProductRelation cmsSubjectProductRelation);

    /**
     * 修改专题商品关系
     * 
     * @param cmsSubjectProductRelation 专题商品关系
     * @return 结果
     */
    public int updateCmsSubjectProductRelation(CmsSubjectProductRelation cmsSubjectProductRelation);

    /**
     * 批量删除专题商品关系
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsSubjectProductRelationByIds(String ids);

    /**
     * 删除专题商品关系信息
     * 
     * @param id 专题商品关系ID
     * @return 结果
     */
    public int deleteCmsSubjectProductRelationById(Long id);
}
