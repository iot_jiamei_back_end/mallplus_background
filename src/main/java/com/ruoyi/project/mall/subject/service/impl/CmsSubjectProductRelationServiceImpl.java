package com.ruoyi.project.mall.subject.service.impl;

import java.util.List;

import com.ruoyi.project.mall.subject.domain.CmsSubjectProductRelation;
import com.ruoyi.project.mall.subject.mapper.CmsSubjectProductRelationMapper;
import com.ruoyi.project.mall.subject.service.ICmsSubjectProductRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 专题商品关系Service业务层处理
 *
 * @author mallplus
 * @date 2020-02-15
 */
@Service
public class CmsSubjectProductRelationServiceImpl implements ICmsSubjectProductRelationService {
    @Autowired
    private CmsSubjectProductRelationMapper cmsSubjectProductRelationMapper;

    /**
     * 查询专题商品关系
     *
     * @param id 专题商品关系ID
     * @return 专题商品关系
     */
    @Override
    public CmsSubjectProductRelation selectCmsSubjectProductRelationById(Long id) {
        return cmsSubjectProductRelationMapper.selectCmsSubjectProductRelationById(id);
    }

    /**
     * 查询专题商品关系列表
     *
     * @param cmsSubjectProductRelation 专题商品关系
     * @return 专题商品关系
     */
    @Override
    public List<CmsSubjectProductRelation> selectCmsSubjectProductRelationList(CmsSubjectProductRelation cmsSubjectProductRelation) {
        return cmsSubjectProductRelationMapper.selectCmsSubjectProductRelationList(cmsSubjectProductRelation);
    }

    /**
     * 新增专题商品关系
     *
     * @param cmsSubjectProductRelation 专题商品关系
     * @return 结果
     */
    @Override
    public int insertCmsSubjectProductRelation(CmsSubjectProductRelation cmsSubjectProductRelation) {
        // 查询是否重复
        Long id = cmsSubjectProductRelationMapper.selectCmsSujectIdByProductIdAndSubjectId(cmsSubjectProductRelation.getProductId(), cmsSubjectProductRelation.getSubjectId());
        if (id != null) {
            return 0;
        }
        return cmsSubjectProductRelationMapper.insertCmsSubjectProductRelation(cmsSubjectProductRelation);
    }

    /**
     * 修改专题商品关系
     *
     * @param cmsSubjectProductRelation 专题商品关系
     * @return 结果
     */
    @Override
    public int updateCmsSubjectProductRelation(CmsSubjectProductRelation cmsSubjectProductRelation) {
        return cmsSubjectProductRelationMapper.updateCmsSubjectProductRelation(cmsSubjectProductRelation);
    }

    /**
     * 删除专题商品关系对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsSubjectProductRelationByIds(String ids) {
        return cmsSubjectProductRelationMapper.deleteCmsSubjectProductRelationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除专题商品关系信息
     *
     * @param id 专题商品关系ID
     * @return 结果
     */
    @Override
    public int deleteCmsSubjectProductRelationById(Long id) {
        return cmsSubjectProductRelationMapper.deleteCmsSubjectProductRelationById(id);
    }
}
