package com.ruoyi.project.mall.subject.controller;

import java.util.List;

import com.ruoyi.project.mall.subject.domain.CmsSubjectProductRelation;
import com.ruoyi.project.mall.subject.service.ICmsSubjectProductRelationService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 专题商品关系Controller
 *
 * @author mallplus
 * @date 2020-02-15
 */
@Controller
@RequestMapping("/mall/relation")
public class CmsSubjectProductRelationController extends BaseController {
    private String prefix = "mall/subject/relation";

    @Autowired
    private ICmsSubjectProductRelationService cmsSubjectProductRelationService;

    @RequiresPermissions("mall:relation:view")
    @GetMapping()
    public String relation() {
        return prefix + "/relation";
    }

    /**
     * 查询专题商品关系列表
     */
    @RequiresPermissions("mall:relation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsSubjectProductRelation cmsSubjectProductRelation) {
        startPage();
        List<CmsSubjectProductRelation> list = cmsSubjectProductRelationService.selectCmsSubjectProductRelationList(cmsSubjectProductRelation);
        return getDataTable(list);
    }

    /**
     * 导出专题商品关系列表
     */
    @RequiresPermissions("mall:relation:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsSubjectProductRelation cmsSubjectProductRelation) {
        List<CmsSubjectProductRelation> list = cmsSubjectProductRelationService.selectCmsSubjectProductRelationList(cmsSubjectProductRelation);
        ExcelUtil<CmsSubjectProductRelation> util = new ExcelUtil<CmsSubjectProductRelation>(CmsSubjectProductRelation.class);
        return util.exportExcel(list, "relation");
    }

    /**
     * 新增专题商品关系
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存专题商品关系
     */
    @RequiresPermissions("mall:relation:add")
    @Log(title = "专题商品关系", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsSubjectProductRelation cmsSubjectProductRelation) {
        if (cmsSubjectProductRelationService.insertCmsSubjectProductRelation(cmsSubjectProductRelation) > 0) {
            return AjaxResult.success();
        } else {
            return AjaxResult.error();
        }
    }

    /**
     * 修改专题商品关系
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        CmsSubjectProductRelation cmsSubjectProductRelation = cmsSubjectProductRelationService.selectCmsSubjectProductRelationById(id);
        mmap.put("cmsSubjectProductRelation", cmsSubjectProductRelation);
        return prefix + "/edit";
    }

    /**
     * 修改保存专题商品关系
     */
    @RequiresPermissions("mall:relation:edit")
    @Log(title = "专题商品关系", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsSubjectProductRelation cmsSubjectProductRelation) {
        return toAjax(cmsSubjectProductRelationService.updateCmsSubjectProductRelation(cmsSubjectProductRelation));
    }

    /**
     * 删除专题商品关系
     */
    @RequiresPermissions("mall:relation:remove")
    @Log(title = "专题商品关系", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(cmsSubjectProductRelationService.deleteCmsSubjectProductRelationByIds(ids));
    }
}
