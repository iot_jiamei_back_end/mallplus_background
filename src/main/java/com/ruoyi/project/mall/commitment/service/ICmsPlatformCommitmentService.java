package com.ruoyi.project.mall.commitment.service;

import com.ruoyi.project.mall.commitment.domain.CmsPlatformCommitment;
import java.util.List;

/**
 * 首页平台承诺Service接口
 * 
 * @author mallplus
 * @date 2020-02-15
 */
public interface ICmsPlatformCommitmentService 
{
    /**
     * 查询首页平台承诺
     * 
     * @param id 首页平台承诺ID
     * @return 首页平台承诺
     */
    public CmsPlatformCommitment selectCmsPlatformCommitmentById(Long id);

    /**
     * 查询首页平台承诺列表
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 首页平台承诺集合
     */
    public List<CmsPlatformCommitment> selectCmsPlatformCommitmentList(CmsPlatformCommitment cmsPlatformCommitment);

    /**
     * 新增首页平台承诺
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 结果
     */
    public int insertCmsPlatformCommitment(CmsPlatformCommitment cmsPlatformCommitment);

    /**
     * 修改首页平台承诺
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 结果
     */
    public int updateCmsPlatformCommitment(CmsPlatformCommitment cmsPlatformCommitment);

    /**
     * 批量删除首页平台承诺
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsPlatformCommitmentByIds(String ids);

    /**
     * 删除首页平台承诺信息
     * 
     * @param id 首页平台承诺ID
     * @return 结果
     */
    public int deleteCmsPlatformCommitmentById(Long id);

    int changeStatus(CmsPlatformCommitment cmsPlatformCommitment);
}
