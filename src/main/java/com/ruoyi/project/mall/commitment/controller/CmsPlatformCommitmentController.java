package com.ruoyi.project.mall.commitment.controller;

import java.util.List;

import com.ruoyi.project.mall.product.domain.PmsBrand;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.commitment.domain.CmsPlatformCommitment;
import com.ruoyi.project.mall.commitment.service.ICmsPlatformCommitmentService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 首页平台承诺Controller
 * 
 * @author mallplus
 * @date 2020-02-15
 */
@Controller
@RequestMapping("/mall/commitment")
public class CmsPlatformCommitmentController extends BaseController
{
    private String prefix = "mall/commitment";

    @Autowired
    private ICmsPlatformCommitmentService cmsPlatformCommitmentService;

    @RequiresPermissions("mall:commitment:view")
    @GetMapping()
    public String commitment()
    {
        return prefix + "/commitment";
    }

    /**
     * 查询首页平台承诺列表
     */
    @RequiresPermissions("mall:commitment:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsPlatformCommitment cmsPlatformCommitment)
    {
        startPage();
        List<CmsPlatformCommitment> list = cmsPlatformCommitmentService.selectCmsPlatformCommitmentList(cmsPlatformCommitment);
        return getDataTable(list);
    }

    /**
     * 导出首页平台承诺列表
     */
    @RequiresPermissions("mall:commitment:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsPlatformCommitment cmsPlatformCommitment)
    {
        List<CmsPlatformCommitment> list = cmsPlatformCommitmentService.selectCmsPlatformCommitmentList(cmsPlatformCommitment);
        ExcelUtil<CmsPlatformCommitment> util = new ExcelUtil<CmsPlatformCommitment>(CmsPlatformCommitment.class);
        return util.exportExcel(list, "commitment");
    }

    /**
     * 新增首页平台承诺
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存首页平台承诺
     */
    @RequiresPermissions("mall:commitment:add")
    @Log(title = "首页平台承诺", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsPlatformCommitment cmsPlatformCommitment)
    {
        return toAjax(cmsPlatformCommitmentService.insertCmsPlatformCommitment(cmsPlatformCommitment));
    }

    /**
     * 修改首页平台承诺
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CmsPlatformCommitment cmsPlatformCommitment = cmsPlatformCommitmentService.selectCmsPlatformCommitmentById(id);
        mmap.put("cmsPlatformCommitment", cmsPlatformCommitment);
        return prefix + "/edit";
    }

    /**
     * 修改保存首页平台承诺
     */
    @RequiresPermissions("mall:commitment:edit")
    @Log(title = "首页平台承诺", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsPlatformCommitment cmsPlatformCommitment)
    {
        return toAjax(cmsPlatformCommitmentService.updateCmsPlatformCommitment(cmsPlatformCommitment));
    }

    /**
     * 删除首页平台承诺
     */
    @RequiresPermissions("mall:commitment:remove")
    @Log(title = "首页平台承诺", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(cmsPlatformCommitmentService.deleteCmsPlatformCommitmentByIds(ids));
    }

    /**
     * 状态修改
     */
//    @Log(title = "品牌管理", businessType = BusinessType.UPDATE)
//    @RequiresPermissions("system:brand:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(CmsPlatformCommitment cmsPlatformCommitment)
    {
        return toAjax(cmsPlatformCommitmentService.changeStatus(cmsPlatformCommitment));
    }
}
