package com.ruoyi.project.mall.commitment.mapper;

import com.ruoyi.project.mall.commitment.domain.CmsPlatformCommitment;
import java.util.List;

/**
 * 首页平台承诺Mapper接口
 * 
 * @author mallplus
 * @date 2020-02-15
 */
public interface CmsPlatformCommitmentMapper 
{
    /**
     * 查询首页平台承诺
     * 
     * @param id 首页平台承诺ID
     * @return 首页平台承诺
     */
    public CmsPlatformCommitment selectCmsPlatformCommitmentById(Long id);

    /**
     * 查询首页平台承诺列表
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 首页平台承诺集合
     */
    public List<CmsPlatformCommitment> selectCmsPlatformCommitmentList(CmsPlatformCommitment cmsPlatformCommitment);

    /**
     * 新增首页平台承诺
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 结果
     */
    public int insertCmsPlatformCommitment(CmsPlatformCommitment cmsPlatformCommitment);

    /**
     * 修改首页平台承诺
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 结果
     */
    public int updateCmsPlatformCommitment(CmsPlatformCommitment cmsPlatformCommitment);

    /**
     * 删除首页平台承诺
     * 
     * @param id 首页平台承诺ID
     * @return 结果
     */
    public int deleteCmsPlatformCommitmentById(Long id);

    /**
     * 批量删除首页平台承诺
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsPlatformCommitmentByIds(String[] ids);
}
