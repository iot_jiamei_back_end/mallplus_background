package com.ruoyi.project.mall.commitment.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.mall.product.domain.PmsBrand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.commitment.mapper.CmsPlatformCommitmentMapper;
import com.ruoyi.project.mall.commitment.domain.CmsPlatformCommitment;
import com.ruoyi.project.mall.commitment.service.ICmsPlatformCommitmentService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 首页平台承诺Service业务层处理
 * 
 * @author mallplus
 * @date 2020-02-15
 */
@Service
public class CmsPlatformCommitmentServiceImpl implements ICmsPlatformCommitmentService 
{
    @Autowired
    private CmsPlatformCommitmentMapper cmsPlatformCommitmentMapper;

    /**
     * 查询首页平台承诺
     * 
     * @param id 首页平台承诺ID
     * @return 首页平台承诺
     */
    @Override
    public CmsPlatformCommitment selectCmsPlatformCommitmentById(Long id)
    {
        return cmsPlatformCommitmentMapper.selectCmsPlatformCommitmentById(id);
    }

    /**
     * 查询首页平台承诺列表
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 首页平台承诺
     */
    @Override
    public List<CmsPlatformCommitment> selectCmsPlatformCommitmentList(CmsPlatformCommitment cmsPlatformCommitment)
    {
        return cmsPlatformCommitmentMapper.selectCmsPlatformCommitmentList(cmsPlatformCommitment);
    }

    /**
     * 新增首页平台承诺
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 结果
     */
    @Override
    public int insertCmsPlatformCommitment(CmsPlatformCommitment cmsPlatformCommitment)
    {
        cmsPlatformCommitment.setCreateTime(DateUtils.getNowDate());
        return cmsPlatformCommitmentMapper.insertCmsPlatformCommitment(cmsPlatformCommitment);
    }

    /**
     * 修改首页平台承诺
     * 
     * @param cmsPlatformCommitment 首页平台承诺
     * @return 结果
     */
    @Override
    public int updateCmsPlatformCommitment(CmsPlatformCommitment cmsPlatformCommitment)
    {
        cmsPlatformCommitment.setUpdateTime(DateUtils.getNowDate());
        return cmsPlatformCommitmentMapper.updateCmsPlatformCommitment(cmsPlatformCommitment);
    }

    /**
     * 删除首页平台承诺对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsPlatformCommitmentByIds(String ids)
    {
        return cmsPlatformCommitmentMapper.deleteCmsPlatformCommitmentByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除首页平台承诺信息
     * 
     * @param id 首页平台承诺ID
     * @return 结果
     */
    @Override
    public int deleteCmsPlatformCommitmentById(Long id)
    {
        return cmsPlatformCommitmentMapper.deleteCmsPlatformCommitmentById(id);
    }

    /**
     * 状态修改
     *
     * @param cmsPlatformCommitment 信息
     * @return 结果
     */
    @Override
    public int changeStatus(CmsPlatformCommitment cmsPlatformCommitment)
    {
        return cmsPlatformCommitmentMapper.updateCmsPlatformCommitment(cmsPlatformCommitment);
    }
}
