package com.ruoyi.project.mall.member.entity.vo;

import com.ruoyi.project.mall.address.domain.UmsMemberReceiveAddress;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/12
 **/

@Data
public class MemberInfoVo {

    private Long memberId;

    private String username;

    private String nickname;

    private String birthday;

    private String phone;

    private String gender;

    private String createTime;

    private String sourceType;

    private String city;

    //消费金额
    private Double consumptionAmount;

    //订单数量
    private Integer orderQuantity;

    //商品评价
    private Integer commodityEvaluation;

    //退换货数量
    private Integer returnedQuantity;

    //优惠劵张数
    private Integer discountCoupons;

    //登录次数
    private Integer numberLogins;

    //收藏商品
    private Integer favoriteQuantity;

    //收货地址
    private List<UmsMemberReceiveAddress> umsMemberReceiveAddresses;


}
