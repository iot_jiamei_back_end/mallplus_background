package com.ruoyi.project.mall.member.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/12
 **/

@Data
public class ProductConsultVo {

    private Long id;

    private Long goodsId;

    private String goodsName;

    private String memberName;

    private Long memberId;

    private String consultContent;

    private Integer quantity;

    private String createTime;

    private Integer type;

}
