package com.ruoyi.project.mall.member.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/12
 **/

@Data
public class OrderVo {

    private Long id;

    private String orderSn;

    private String createTime;

    private String payType;

    private String sourceType;

    private String status;

}
