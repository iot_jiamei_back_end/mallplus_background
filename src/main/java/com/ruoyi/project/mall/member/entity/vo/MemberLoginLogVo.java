package com.ruoyi.project.mall.member.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/12
 **/

@Data
public class MemberLoginLogVo {

    private String createTime;

    private Long memberId;

    private String city;


}
