package com.ruoyi.project.mall.member.controller;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.mall.member.domain.UmsMember;
import com.ruoyi.project.mall.member.entity.vo.*;
import com.ruoyi.project.mall.member.service.IUmsMemberService;
import com.ruoyi.project.mall.store.domain.Store;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员Controller
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Controller
@RequestMapping("/mall/member")
public class UmsMemberController extends BaseController {
    private String prefix = "mall/member";

    @Autowired
    private IUmsMemberService umsMemberService;

    @RequiresPermissions("mall:member:view")
    @GetMapping()
    public String member() {
        return prefix + "/member";
    }

    /**
     * 查询会员列表
     */
    @RequiresPermissions("mall:member:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(UmsMember umsMember) {
        startPage();
        List<UmsMember> list = umsMemberService.selectUmsMemberList(umsMember);
        return getDataTable(list);
    }

    /**
     * 导出会员列表
     */
    @RequiresPermissions("mall:member:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UmsMember umsMember) {
        List<UmsMember> list = umsMemberService.selectUmsMemberList(umsMember);
        ExcelUtil<UmsMember> util = new ExcelUtil<UmsMember>(UmsMember.class);
        return util.exportExcel(list, "member");
    }

    /**
     * 新增会员
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存会员
     */
    @RequiresPermissions("mall:member:add")
    @Log(title = "会员", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(UmsMember umsMember) {
        return toAjax(umsMemberService.insertUmsMember(umsMember));
    }

    /**
     * 修改会员
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        UmsMember umsMember = umsMemberService.selectUmsMemberById(id);
        mmap.put("umsMember", umsMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员
     */
    @RequiresPermissions("mall:member:edit")
    @Log(title = "会员", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UmsMember umsMember) {
        return toAjax(umsMemberService.updateUmsMember(umsMember));
    }

    /**
     * 删除会员
     */
    @RequiresPermissions("mall:member:remove")
    @Log(title = "会员", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(umsMemberService.deleteUmsMemberByIds(ids));
    }

    /**
     * 商户状态修改
     */
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("mall:store:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(UmsMember umsMember) {
        return toAjax(umsMemberService.changeStatus(umsMember));
    }

    /**
     * 获取用户详情信息
     */

    @Log(title = "用户详情", businessType = BusinessType.UPDATE)
    @RequiresPermissions("mall:store:edit")
    @PostMapping("/getDetail")
    public String getDetail(Long id, Model model) {
        MemberInfoVo detail = umsMemberService.getDetail(id);
        model.addAttribute("detailInfo", detail);
        return prefix + "memberDetail.html";
    }

    @Log(title = "获取订单记录", businessType = BusinessType.UPDATE)
    @ResponseBody
    @RequiresPermissions("mall:member:list")
    @PostMapping("/orderRecord")
    public TableDataInfo orderRecord(Long id) {
        startPage();
        List<OrderVo> orderVos = umsMemberService.orderRecord(id);
        return getDataTable(orderVos);
    }


    //评价详情
    @Log(title = "获取用户评价详情", businessType = BusinessType.UPDATE)
    @RequiresPermissions("mall:member:list")
    @PostMapping("/getEvaluationDetails")
    public TableDataInfo getEvaluationDetails(Long id) {
        startPage();
        List<ProductConsultVo> evaluationDetails = umsMemberService.getEvaluationDetails(id);
        return getDataTable(evaluationDetails);
    }

    //缺少跳转

    @Log(title = "获取用户购物券详情", businessType = BusinessType.UPDATE)
    @RequiresPermissions("mall:member:list")
    @PostMapping("/voucherDetails")
    public TableDataInfo voucherDetails(Long id) {
        startPage();
        List<CouponHistoryVo> couponHistoryVos = umsMemberService.voucherDetails(id);
        return getDataTable(couponHistoryVos);
    }

    //缺少跳转

    @Log(title = "获取用户收藏详情", businessType = BusinessType.UPDATE)
    @RequiresPermissions("mall:member:list")
    @PostMapping("/favoritesList")
    public TableDataInfo favoritesList(Long id) {
        startPage();
        List<FavoriteVo> favoriteVos = umsMemberService.favoritesList(id);
        return getDataTable(favoriteVos);
    }

    //缺少跳转

    @Log(title = "获取用户登录详情", businessType = BusinessType.UPDATE)
    @RequiresPermissions("mall:member:list")
    @PostMapping("/loginsDetails")
    public TableDataInfo loginsDetails(Long id) {
        startPage();
        List<MemberLoginLogVo> memberLoginLogVos = umsMemberService.loginsDetails(id);
        return getDataTable(memberLoginLogVos);
    }

    /**
     * 评价详情、传入评价id
     *
     * @param id 消息id
     * @return
     */
    @Log(title = "获取用户此条评价得详情", businessType = BusinessType.UPDATE)
    @PostMapping("/evaluationDetails")
    public AjaxResult evaluationDetails(Long id) {
        EvaluationVo evaluationVo = umsMemberService.evaluationDetails(id);
        return AjaxResult.success(evaluationVo);
    }

    /**
     * 获取评价详情子评价
     */
    @Log(title = "获取用户此条子评价得详情", businessType = BusinessType.UPDATE)
    @PostMapping("/subEvaluation")
    public TableDataInfo subEvaluation(Long id) {
        startPage();
        return getDataTable(umsMemberService.subEvaluation(id));
    }

    @Log(title = "掌柜得回复评论", businessType = BusinessType.UPDATE)
    @PostMapping("/replyToComment")
    public AjaxResult replyToComment(Long id, String content) {
        umsMemberService.replyToComment(id, content);
        return AjaxResult.success();
    }

    @Log(title = "删除子评论", businessType = BusinessType.UPDATE)
    @PostMapping("/deletereplyEvaluation")
    public AjaxResult deletereplyEvaluation(Long id) {
        umsMemberService.deletereplyEvaluation(id);
        return AjaxResult.success();
    }


    @Log(title = "删除主评论", businessType = BusinessType.UPDATE)
    @PostMapping("/deletemainEvaluation")
    public AjaxResult deletemainEvaluation(Long id) {
        umsMemberService.deletemainEvaluation(id);
        return AjaxResult.success();
    }

    //todo 缺少退货记录







}
