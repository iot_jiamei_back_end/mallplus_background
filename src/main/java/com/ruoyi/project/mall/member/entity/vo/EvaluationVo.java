package com.ruoyi.project.mall.member.entity.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author ZuRongTang
 * @Date 2020/3/16
 **/

@Data
public class EvaluationVo {

    private Long id;

    private String pic;

    private String productName;

    private BigDecimal price;

    private String specification;

}
