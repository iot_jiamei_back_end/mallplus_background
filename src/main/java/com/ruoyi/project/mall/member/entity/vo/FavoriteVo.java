package com.ruoyi.project.mall.member.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/12
 **/

@Data
public class FavoriteVo {

    private Long id;

    private Long memberId;

    private String addTime;

    private String name;

}
