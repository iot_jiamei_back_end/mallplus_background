package com.ruoyi.project.mall.member.service;

import com.ruoyi.project.mall.member.domain.UmsMember;
import com.ruoyi.project.mall.member.entity.vo.*;
import com.ruoyi.project.mall.replay.domain.CommentReplay;
import com.ruoyi.project.mall.store.domain.Store;

import java.util.List;

/**
 * 会员Service接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface IUmsMemberService {
    /**
     * 查询会员
     *
     * @param id 会员ID
     * @return 会员
     */
    public UmsMember selectUmsMemberById(Long id);

    /**
     * 查询会员列表
     *
     * @param umsMember 会员
     * @return 会员集合
     */
    public List<UmsMember> selectUmsMemberList(UmsMember umsMember);

    /**
     * 新增会员
     *
     * @param umsMember 会员
     * @return 结果
     */
    public int insertUmsMember(UmsMember umsMember);

    /**
     * 修改会员
     *
     * @param umsMember 会员
     * @return 结果
     */
    public int updateUmsMember(UmsMember umsMember);

    /**
     * 批量删除会员
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUmsMemberByIds(String ids);

    /**
     * 删除会员信息
     *
     * @param id 会员ID
     * @return 结果
     */
    public int deleteUmsMemberById(Long id);

    /**
     * 删除会员信息
     *
     * @param 修改用户状态
     * @return 结果
     */

    int changeStatus(UmsMember umsMember);

    /**
     * 获取用户详情信息
     * @param id
     */

    MemberInfoVo getDetail(Long id);

    List<OrderVo> orderRecord(Long id);

    List<ProductConsultVo> getEvaluationDetails(Long id);

    List<CouponHistoryVo> voucherDetails(Long id);

    List<FavoriteVo> favoritesList(Long id);

    List<MemberLoginLogVo> loginsDetails(Long id);

    EvaluationVo evaluationDetails(Long id);

    List<CommentReplay> subEvaluation(Long id);

    void replyToComment(Long id, String content);

    void deletereplyEvaluation(Long id);

    void deletemainEvaluation(Long id);
}
