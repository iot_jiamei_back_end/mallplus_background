package com.ruoyi.project.mall.member.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.mall.address.domain.UmsMemberReceiveAddress;
import com.ruoyi.project.mall.address.mapper.UmsMemberReceiveAddressMapper;
import com.ruoyi.project.mall.consult.domain.ProductConsult;
import com.ruoyi.project.mall.consult.mapper.ProductConsultMapper;
import com.ruoyi.project.mall.favorite.domain.Favorite;
import com.ruoyi.project.mall.favorite.mapper.FavoriteMapper;
import com.ruoyi.project.mall.history.domain.SmsCouponHistory;
import com.ruoyi.project.mall.history.mapper.SmsCouponHistoryMapper;
import com.ruoyi.project.mall.item.domain.OmsOrderItem;
import com.ruoyi.project.mall.item.mapper.OmsOrderItemMapper;
import com.ruoyi.project.mall.log.domain.UmsMemberLoginLog;
import com.ruoyi.project.mall.log.mapper.UmsMemberLoginLogMapper;
import com.ruoyi.project.mall.member.domain.UmsMember;
import com.ruoyi.project.mall.member.entity.vo.*;
import com.ruoyi.project.mall.member.mapper.UmsMemberMapper;
import com.ruoyi.project.mall.member.service.IUmsMemberService;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import com.ruoyi.project.mall.order.mapper.OmsOrderMapper;
import com.ruoyi.project.mall.replay.domain.CommentReplay;
import com.ruoyi.project.mall.replay.mapper.CommentReplayMapper;
import com.ruoyi.project.mall.stock.domain.SkuStock;
import com.ruoyi.project.mall.stock.mapper.SkuStockMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 会员Service业务层处理
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Service
public class UmsMemberServiceImpl implements IUmsMemberService {

    @Resource
    private UmsMemberMapper umsMemberMapper;

    @Resource
    private FavoriteMapper favoriteMapper;

    @Resource
    private OmsOrderMapper omsOrderMapper;

    @Resource
    private SkuStockMapper skuStockMapper;

    @Resource
    private OmsOrderItemMapper omsOrderItemMapper;

    @Resource
    private CommentReplayMapper commentReplayMapper;

    @Resource
    private ProductConsultMapper productConsultMapper;

    @Resource
    private SmsCouponHistoryMapper smsCouponHistoryMapper;

    @Resource
    private UmsMemberLoginLogMapper umsMemberLoginLogMapper;

    @Resource
    private UmsMemberReceiveAddressMapper umsMemberReceiveAddressMapper;


    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy年MM月dd日");


    /**
     * 查询会员
     *
     * @param id 会员ID
     * @return 会员
     */
    @Override
    public UmsMember selectUmsMemberById(Long id) {
        return umsMemberMapper.selectUmsMemberById(id);
    }

    /**
     * 查询会员列表
     *
     * @param umsMember 会员
     * @return 会员
     */
    @Override
    public List<UmsMember> selectUmsMemberList(UmsMember umsMember) {
        return umsMemberMapper.selectUmsMemberList(umsMember);
    }

    /**
     * 新增会员
     *
     * @param umsMember 会员
     * @return 结果
     */
    @Override
    public int insertUmsMember(UmsMember umsMember) {
        umsMember.setCreateTime(DateUtils.getNowDate());
        return umsMemberMapper.insertUmsMember(umsMember);
    }

    /**
     * 修改会员
     *
     * @param umsMember 会员
     * @return 结果
     */
    @Override
    public int updateUmsMember(UmsMember umsMember) {
        umsMember.setUpdateTime(DateUtils.getNowDate());
        return umsMemberMapper.updateUmsMember(umsMember);
    }

    /**
     * 删除会员对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUmsMemberByIds(String ids) {
        return umsMemberMapper.deleteUmsMemberByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除会员信息
     *
     * @param id 会员ID
     * @return 结果
     */
    @Override
    public int deleteUmsMemberById(Long id) {
        return umsMemberMapper.deleteUmsMemberById(id);
    }

    @Override
    public int changeStatus(UmsMember umsMember) {
        return umsMemberMapper.updateUmsMember(umsMember);
    }

    /**
     * 获取用户详情信息
     *
     * @param id
     */

    @Override
    public MemberInfoVo getDetail(Long id) {

        MemberInfoVo memberInfoVo = new MemberInfoVo();

        UmsMember umsMember = umsMemberMapper.selectUmsMemberById(id);

        BeanUtils.copyProperties(umsMember, memberInfoVo);

        OmsOrder omsOrder = new OmsOrder();
        omsOrder.setStatus(3);
        Long memberId = umsMember.getId();
        omsOrder.setMemberId(memberId);

        //订单记录
        List<OmsOrder> omsOrders = omsOrderMapper.selectOmsOrderList(omsOrder);
        double count = 0D;

        for (OmsOrder order : omsOrders) {
            BigDecimal couponAmount = order.getCouponAmount();
            double v = couponAmount.doubleValue();
            count = count + v;
        }

        memberInfoVo.setConsumptionAmount(count);

        memberInfoVo.setOrderQuantity(omsOrders.size());

        ProductConsult productConsult = new ProductConsult();

        productConsult.setMemberId(memberId);

        //评论记录
        List<ProductConsult> productConsults = productConsultMapper.selectProductConsultList(productConsult);

        memberInfoVo.setFavoriteQuantity(productConsults.size());

        //退换货记录
        List<OmsOrder> aftersalesList = omsOrderMapper.getAftersalesList(memberId);

        memberInfoVo.setReturnedQuantity(aftersalesList.size());

        //优惠劵
        SmsCouponHistory smsCouponHistory = new SmsCouponHistory();
        smsCouponHistory.setMemberId(memberId);
        List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryMapper.selectSmsCouponHistoryList(smsCouponHistory);

        memberInfoVo.setDiscountCoupons(smsCouponHistories.size());


        //登录次数
        UmsMemberLoginLog umsMemberLoginLog = new UmsMemberLoginLog();
        umsMemberLoginLog.setMemberId(memberId);
        List<UmsMemberLoginLog> umsMemberLoginLogs = umsMemberLoginLogMapper.selectUmsMemberLoginLogList(umsMemberLoginLog);

        memberInfoVo.setNumberLogins(umsMemberLoginLogs.size());

        //收藏得商品
        Favorite favorite = new Favorite();
        favorite.setMemberId(memberId);
        List<Favorite> favorites = favoriteMapper.selectFavoriteList(favorite);

        memberInfoVo.setFavoriteQuantity(favorites.size());

        //地址管理
        UmsMemberReceiveAddress umsMemberReceiveAddress = new UmsMemberReceiveAddress();
        umsMemberReceiveAddress.setMemberId(memberId);
        List<UmsMemberReceiveAddress> umsMemberReceiveAddresses = umsMemberReceiveAddressMapper.selectUmsMemberReceiveAddressList(umsMemberReceiveAddress);

        memberInfoVo.setUmsMemberReceiveAddresses(umsMemberReceiveAddresses);

        memberInfoVo.setBirthday(formatter1.format(umsMember.getBirthday()));
        memberInfoVo.setCreateTime(formatter.format(umsMember.getCreateTime()));
        memberInfoVo.setGender(umsMember.getGender() == 0 ? "男" : "女");

        return memberInfoVo;

    }

    @Override
    public List<OrderVo> orderRecord(Long id) {
        OmsOrder omsOrder = new OmsOrder();
        omsOrder.setMemberId(id);
        List<OmsOrder> omsOrders = omsOrderMapper.selectOmsOrderList(omsOrder);

        List<OrderVo> result = new ArrayList<>();


        for (OmsOrder order : omsOrders) {
            OrderVo orderVo = new OrderVo();
            BeanUtils.copyProperties(order, orderVo);

            orderVo.setCreateTime(formatter.format(order.getCreateTime()));

            Integer payType = order.getPayType();

            if (payType == 0) {
                orderVo.setPayType("未支付");
            } else if (payType == 1) {
                orderVo.setPayType("支付宝");
            } else {
                orderVo.setPayType("微信");
            }

            Integer sourceType = order.getSourceType();

            if (sourceType == 0) {
                orderVo.setSourceType("PC订单");
            } else {
                orderVo.setSourceType("App订单");
            }

            Integer status = order.getStatus();

            if (status == 0) {
                orderVo.setStatus("待付款");
            } else if (status == 1) {
                orderVo.setStatus("待发货");
            } else if (status == 2) {
                orderVo.setStatus("已发货");
            } else if (status == 3) {
                orderVo.setStatus("已完成");
            } else if (status == 4) {
                orderVo.setStatus("已关闭");
            } else {
                orderVo.setStatus("无效订单");
            }
            result.add(orderVo);
        }
        return result;
    }

    @Override
    public List<ProductConsultVo> getEvaluationDetails(Long id) {
        List<ProductConsultVo> result = new ArrayList<>();

        CommentReplay commentReplay = new CommentReplay();

        ProductConsult productConsult = new ProductConsult();
        productConsult.setMemberId(id);

        List<ProductConsult> productConsults = productConsultMapper.selectProductConsultList(productConsult);

        if (productConsults.size() > 0) {
            for (ProductConsult consult : productConsults) {
                ProductConsultVo productConsultVo = new ProductConsultVo();
                Integer evaluaId = consult.getId();
                commentReplay.setCommentId(evaluaId.longValue());
                List<CommentReplay> commentReplays = commentReplayMapper.selectCommentReplayList(commentReplay);
                BeanUtils.copyProperties(consult, productConsultVo);

                for (CommentReplay replay : commentReplays) {
                    Integer type = replay.getType();
                    if (type == 1) {
                        productConsultVo.setType(1);
                    }
                }

                productConsultVo.setType(0);
                productConsultVo.setQuantity(commentReplays.size());
                productConsultVo.setCreateTime(formatter.format(consult.getCreateTime()));

                result.add(productConsultVo);
            }
        }
        return result;
    }

    @Override
    public List<CouponHistoryVo> voucherDetails(Long id) {
        SmsCouponHistory smsCouponHistory = new SmsCouponHistory();
        smsCouponHistory.setMemberId(id);
        List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryMapper.selectSmsCouponHistoryList(smsCouponHistory);

        List<CouponHistoryVo> result = new ArrayList<>();

        if (smsCouponHistories.size() > 0) {

            for (SmsCouponHistory couponHistory : smsCouponHistories) {
                CouponHistoryVo couponHistoryVo = new CouponHistoryVo();

                BeanUtils.copyProperties(couponHistory, couponHistoryVo);

                couponHistoryVo.setUseTime(formatter.format(couponHistory.getUseTime()));
                couponHistoryVo.setCreateTime(formatter.format(couponHistory.getCreateTime()));

                Integer getType = couponHistory.getGetType();

                if (getType == 0) {
                    couponHistoryVo.setGetType("后台赠送");
                } else {
                    couponHistoryVo.setGetType("主动获取");
                }

                Integer useStatus = couponHistory.getUseStatus();

                if (useStatus == 0) {
                    couponHistoryVo.setUseStatus("未使用");
                } else if (useStatus == 1) {
                    couponHistoryVo.setUseStatus("已使用");
                } else {
                    couponHistoryVo.setUseStatus("已过期");
                }

                result.add(couponHistoryVo);
            }

        }

        return result;
    }

    @Override
    public List<FavoriteVo> favoritesList(Long id) {
        Favorite favorite = new Favorite();
        favorite.setMemberId(id);
        List<Favorite> favorites = favoriteMapper.selectFavoriteList(favorite);

        List<FavoriteVo> result = new ArrayList<>();

        for (Favorite value : favorites) {
            FavoriteVo favoriteVo = new FavoriteVo();
            BeanUtils.copyProperties(value, favoriteVo);
            favoriteVo.setAddTime(formatter.format(value.getAddTime()));
            result.add(favoriteVo);
        }

        return result;
    }

    @Override
    public List<MemberLoginLogVo> loginsDetails(Long id) {
        List<MemberLoginLogVo> result = new ArrayList<>();

        UmsMemberLoginLog loginLog = new UmsMemberLoginLog();
        loginLog.setMemberId(id);
        List<UmsMemberLoginLog> umsMemberLoginLogs = umsMemberLoginLogMapper.selectUmsMemberLoginLogList(loginLog);

        for (UmsMemberLoginLog umsMemberLoginLog : umsMemberLoginLogs) {
            MemberLoginLogVo memberLoginLogVo = new MemberLoginLogVo();

            BeanUtils.copyProperties(umsMemberLoginLog, memberLoginLogVo);

            memberLoginLogVo.setCreateTime(formatter.format(umsMemberLoginLog.getCreateTime()));

            result.add(memberLoginLogVo);
        }
        return result;
    }

    @Override
    public EvaluationVo evaluationDetails(Long id) {
        EvaluationVo evaluationVo = new EvaluationVo();

        ProductConsult productConsult = productConsultMapper.selectProductConsultById(id.intValue());
        Long skuId = productConsult.getSkuId();
        SkuStock skuStock = skuStockMapper.selectSkuStockById(skuId);

        evaluationVo.setId(id);

        BeanUtils.copyProperties(skuStock, evaluationVo);

        return evaluationVo;
    }

    @Override
    public List<CommentReplay> subEvaluation(Long id) {
        CommentReplay commentReplay = new CommentReplay();
        commentReplay.setCommentId(id);
        return commentReplayMapper.selectCommentReplayList(commentReplay);
    }

    @Override
    public void replyToComment(Long id, String content) {
        CommentReplay commentReplay = new CommentReplay();
        commentReplay.setContent(content);
        commentReplay.setType(1);
        commentReplay.setCommentId(id);
        commentReplay.setMemberIcon(null);
        commentReplay.setMemberNickName("掌柜回复");
        commentReplay.setCreateTime(new Date());
        commentReplay.setUpdateTime(new Date());

        commentReplayMapper.insertCommentReplay(commentReplay);

    }

    @Override
    public void deletereplyEvaluation(Long id) {
        commentReplayMapper.deleteCommentReplayById(id);
    }

    @Override
    public void deletemainEvaluation(Long id) {
        productConsultMapper.deleteProductConsultById(id.intValue());
    }


}
