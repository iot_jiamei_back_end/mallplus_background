package com.ruoyi.project.mall.member.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/12
 **/

@Data
public class CouponHistoryVo {

    private String couponCodecouponCode;

    private String memberNickname;

    private String getType;

    private String useStatus;

    private String createTime;

    private String useTime;

    private String orderSn;


}
