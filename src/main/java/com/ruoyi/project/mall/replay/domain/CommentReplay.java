package com.ruoyi.project.mall.replay.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 产品评价回复对象 pms_comment_replay
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class CommentReplay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long commentId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String memberNickName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String memberIcon;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String content;

    /** 评论人员类型；0->会员；1->管理员 */
    @Excel(name = "评论人员类型；0->会员；1->管理员")
    private Integer type;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCommentId(Long commentId) 
    {
        this.commentId = commentId;
    }

    public Long getCommentId() 
    {
        return commentId;
    }
    public void setMemberNickName(String memberNickName) 
    {
        this.memberNickName = memberNickName;
    }

    public String getMemberNickName() 
    {
        return memberNickName;
    }
    public void setMemberIcon(String memberIcon) 
    {
        this.memberIcon = memberIcon;
    }

    public String getMemberIcon() 
    {
        return memberIcon;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("commentId", getCommentId())
            .append("memberNickName", getMemberNickName())
            .append("memberIcon", getMemberIcon())
            .append("content", getContent())
            .append("createTime", getCreateTime())
            .append("type", getType())
            .append("storeId", getStoreId())
            .toString();
    }
}
