package com.ruoyi.project.mall.replay.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.replay.mapper.CommentReplayMapper;
import com.ruoyi.project.mall.replay.domain.CommentReplay;
import com.ruoyi.project.mall.replay.service.ICommentReplayService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 产品评价回复Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class CommentReplayServiceImpl implements ICommentReplayService 
{
    @Autowired
    private CommentReplayMapper commentReplayMapper;

    /**
     * 查询产品评价回复
     * 
     * @param id 产品评价回复ID
     * @return 产品评价回复
     */
    @Override
    public CommentReplay selectCommentReplayById(Long id)
    {
        return commentReplayMapper.selectCommentReplayById(id);
    }

    /**
     * 查询产品评价回复列表
     * 
     * @param commentReplay 产品评价回复
     * @return 产品评价回复
     */
    @Override
    public List<CommentReplay> selectCommentReplayList(CommentReplay commentReplay)
    {
        return commentReplayMapper.selectCommentReplayList(commentReplay);
    }

    /**
     * 新增产品评价回复
     * 
     * @param commentReplay 产品评价回复
     * @return 结果
     */
    @Override
    public int insertCommentReplay(CommentReplay commentReplay)
    {
        commentReplay.setCreateTime(DateUtils.getNowDate());
        return commentReplayMapper.insertCommentReplay(commentReplay);
    }

    /**
     * 修改产品评价回复
     * 
     * @param commentReplay 产品评价回复
     * @return 结果
     */
    @Override
    public int updateCommentReplay(CommentReplay commentReplay)
    {
        commentReplay.setUpdateTime(DateUtils.getNowDate());
        return commentReplayMapper.updateCommentReplay(commentReplay);
    }

    /**
     * 删除产品评价回复对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCommentReplayByIds(String ids)
    {
        return commentReplayMapper.deleteCommentReplayByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品评价回复信息
     * 
     * @param id 产品评价回复ID
     * @return 结果
     */
    @Override
    public int deleteCommentReplayById(Long id)
    {
        return commentReplayMapper.deleteCommentReplayById(id);
    }
}
