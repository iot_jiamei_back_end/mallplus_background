package com.ruoyi.project.mall.replay.service;

import com.ruoyi.project.mall.replay.domain.CommentReplay;
import java.util.List;

/**
 * 产品评价回复Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface ICommentReplayService 
{
    /**
     * 查询产品评价回复
     * 
     * @param id 产品评价回复ID
     * @return 产品评价回复
     */
    public CommentReplay selectCommentReplayById(Long id);

    /**
     * 查询产品评价回复列表
     * 
     * @param commentReplay 产品评价回复
     * @return 产品评价回复集合
     */
    public List<CommentReplay> selectCommentReplayList(CommentReplay commentReplay);

    /**
     * 新增产品评价回复
     * 
     * @param commentReplay 产品评价回复
     * @return 结果
     */
    public int insertCommentReplay(CommentReplay commentReplay);

    /**
     * 修改产品评价回复
     * 
     * @param commentReplay 产品评价回复
     * @return 结果
     */
    public int updateCommentReplay(CommentReplay commentReplay);

    /**
     * 批量删除产品评价回复
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCommentReplayByIds(String ids);

    /**
     * 删除产品评价回复信息
     * 
     * @param id 产品评价回复ID
     * @return 结果
     */
    public int deleteCommentReplayById(Long id);
}
