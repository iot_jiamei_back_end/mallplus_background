package com.ruoyi.project.mall.replay.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.replay.domain.CommentReplay;
import com.ruoyi.project.mall.replay.service.ICommentReplayService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 产品评价回复Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/mall/replay")
public class CommentReplayController extends BaseController
{
    private String prefix = "mall/replay";

    @Autowired
    private ICommentReplayService commentReplayService;

    @RequiresPermissions("mall:replay:view")
    @GetMapping()
    public String replay()
    {
        return prefix + "/replay";
    }

    /**
     * 查询产品评价回复列表
     */
    @RequiresPermissions("mall:replay:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CommentReplay commentReplay)
    {
        startPage();
        List<CommentReplay> list = commentReplayService.selectCommentReplayList(commentReplay);
        return getDataTable(list);
    }

    /**
     * 导出产品评价回复列表
     */
    @RequiresPermissions("mall:replay:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CommentReplay commentReplay)
    {
        List<CommentReplay> list = commentReplayService.selectCommentReplayList(commentReplay);
        ExcelUtil<CommentReplay> util = new ExcelUtil<CommentReplay>(CommentReplay.class);
        return util.exportExcel(list, "replay");
    }

    /**
     * 新增产品评价回复
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品评价回复
     */
    @RequiresPermissions("mall:replay:add")
    @Log(title = "产品评价回复", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CommentReplay commentReplay)
    {
        return toAjax(commentReplayService.insertCommentReplay(commentReplay));
    }

    /**
     * 修改产品评价回复
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        CommentReplay commentReplay = commentReplayService.selectCommentReplayById(id);
        mmap.put("commentReplay", commentReplay);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品评价回复
     */
    @RequiresPermissions("mall:replay:edit")
    @Log(title = "产品评价回复", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CommentReplay commentReplay)
    {
        return toAjax(commentReplayService.updateCommentReplay(commentReplay));
    }

    /**
     * 删除产品评价回复
     */
    @RequiresPermissions("mall:replay:remove")
    @Log(title = "产品评价回复", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(commentReplayService.deleteCommentReplayByIds(ids));
    }
}
