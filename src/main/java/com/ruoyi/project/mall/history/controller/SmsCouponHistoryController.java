package com.ruoyi.project.mall.history.controller;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.project.mall.history.domain.SmsCoupon;
import com.ruoyi.project.mall.history.entity.dot.ScreenDto;
import com.ruoyi.project.mall.history.entity.vo.CoupCoutVo;
import com.ruoyi.project.mall.history.entity.vo.DiscountCardVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.history.domain.SmsCouponHistory;
import com.ruoyi.project.mall.history.service.ISmsCouponHistoryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 优惠券使用、领取历史Controller
 *
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/mall/history")
public class SmsCouponHistoryController extends BaseController {
    private String prefix = "mall/history";

    @Autowired
    private ISmsCouponHistoryService smsCouponHistoryService;

    @RequiresPermissions("mall:history:view")
    @GetMapping()
    public String history() {
        return prefix + "/history";
    }

    /**
     * 查询优惠券使用、领取历史列表
     */
    @RequiresPermissions("mall:history:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SmsCouponHistory smsCouponHistory) {
        startPage();
        List<SmsCouponHistory> list = smsCouponHistoryService.selectSmsCouponHistoryList(smsCouponHistory);
        List<CoupCoutVo> result = new ArrayList<>();
        SmsCouponHistory smsCouponHistory1 = new SmsCouponHistory();
        for (SmsCouponHistory couponHistory : list) {
            Integer use = 0;
            Integer unused = 0;
            Integer expire = 0;
            CoupCoutVo coupCoutVo = new CoupCoutVo();
            Long memberId = couponHistory.getMemberId();
            smsCouponHistory1.setMemberId(memberId);
            List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryService.selectSmsCouponHistoryList(smsCouponHistory1);

            for (SmsCouponHistory history : smsCouponHistories) {


                Integer useStatus = history.getUseStatus();

                if (useStatus == 0) {
                    unused++;
                } else if (useStatus == 1) {
                    use++;
                } else {
                    expire++;
                }
            }
            BeanUtils.copyProperties(couponHistory, coupCoutVo);

            coupCoutVo.setUsed(use);
            coupCoutVo.setUnused(unused);
            coupCoutVo.setExpired(expire);
            result.add(coupCoutVo);
        }
        return getDataTable(result);
    }

    /**
     * 导出优惠券使用、领取历史列表
     */
    @RequiresPermissions("mall:history:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SmsCouponHistory smsCouponHistory) {
        List<SmsCouponHistory> list = smsCouponHistoryService.selectSmsCouponHistoryList(smsCouponHistory);
        ExcelUtil<SmsCouponHistory> util = new ExcelUtil<SmsCouponHistory>(SmsCouponHistory.class);
        return util.exportExcel(list, "history");
    }

    /**
     * 新增优惠券使用、领取历史
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存优惠券使用、领取历史
     */
    @RequiresPermissions("mall:history:add")
    @Log(title = "优惠券使用、领取历史", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SmsCouponHistory smsCouponHistory) {
        return toAjax(smsCouponHistoryService.insertSmsCouponHistory(smsCouponHistory));
    }

    /**
     * 修改优惠券使用、领取历史
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        SmsCouponHistory smsCouponHistory = smsCouponHistoryService.selectSmsCouponHistoryById(id);
        mmap.put("smsCouponHistory", smsCouponHistory);
        return prefix + "/edit";
    }

    /**
     * 修改保存优惠券使用、领取历史
     */
    @RequiresPermissions("mall:history:edit")
    @Log(title = "优惠券使用、领取历史", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SmsCouponHistory smsCouponHistory) {
        return toAjax(smsCouponHistoryService.updateSmsCouponHistory(smsCouponHistory));
    }

    /**
     * 删除优惠券使用、领取历史
     */
    @RequiresPermissions("mall:history:remove")
    @Log(title = "优惠券使用、领取历史", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(smsCouponHistoryService.deleteSmsCouponHistoryByIds(ids));
    }


    //file:///C:/Users/Administrator/Desktop/商城运营后台/index.html#g=1&p=优惠券查询
    @GetMapping("/discountCardList")
    public TableDataInfo discountCardList(ScreenDto screenDto) {
        startPage();
        List<DiscountCardVo> discountCardVos = smsCouponHistoryService.discountCardList(screenDto);
        return getDataTable(discountCardVos);
    }

    /**
     * 获取赠送优惠券列表
     *
     * @return
     */
    @GetMapping("/giftCouponList")
    public TableDataInfo giftCouponList() {
        startPage();
        List<SmsCoupon> smsCoupons = smsCouponHistoryService.giftCouponList();
        return getDataTable(smsCoupons);
    }


    /**
     * 赠送优惠券
     *
     * @param memberId
     * @param coupId
     * @return
     */

    @PostMapping("/giveAwayCoupons")
    public AjaxResult giveAwayCoupons(Long memberId, String coupId) {
        smsCouponHistoryService.giveAwayCoupons(memberId, coupId);
        return AjaxResult.success();
    }

    /**
     * 用户优惠券列表
     *
     * @param smsCouponHistory
     * @return file:///C:/Users/Administrator/Desktop/文档/新版本/商城运营后台20200305/index.html#g=1&p=优惠券明细
     */

    @PostMapping("/listOfUserCoupons")
    public TableDataInfo listOfUserCoupons(SmsCouponHistory smsCouponHistory) {
        startPage();
        List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryService.selectSmsCouponHistoryList(smsCouponHistory);
        return getDataTable(smsCouponHistories);
    }

    //file:///C:/Users/Administrator/Desktop/文档/新版本/商城运营后台20200305/index.html#g=1&p=优惠券列表
    @PostMapping("/couponList")
    public TableDataInfo couponList(SmsCoupon smsCoupon) {
        startPage();
        List<SmsCoupon> smsCoupons = smsCouponHistoryService.couponList(smsCoupon);
        return getDataTable(smsCoupons);
    }

    //file:///C:/Users/Administrator/Desktop/文档/新版本/商城运营后台20200305/index.html#g=1&p=优惠券明细_1
    //上面的统计
    @PostMapping("/couponUsageDetails")
    public AjaxResult couponUsageDetails(Long id) {
        SmsCoupon smsCoupon = smsCouponHistoryService.couponUsageDetails(id);
        return AjaxResult.success(smsCoupon);
    }

    //下面的
    @PostMapping("/userCouponUsageDetails")
    public TableDataInfo userCouponUsageDetails(SmsCouponHistory smsCouponHistory) {
        startPage();
        List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryService.selectSmsCouponHistoryList(smsCouponHistory);
        return getDataTable(smsCouponHistories);
    }


    @PostMapping("/registerCoupon")
    public AjaxResult registerCoupon(SmsCoupon smsCoupon) {


        return null;
    }

    @GetMapping("/detail")
    public String detail(Long memberId, Model model) {
        model.addAttribute("memberId", memberId);
        return prefix + "/detail";
    }


    @PostMapping("/listAll")
    @ResponseBody
    public TableDataInfo listAll(SmsCouponHistory smsCouponHistory) {
        startPage();
        List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryService.selectSmsCouponHistoryList(smsCouponHistory);
        return getDataTable(smsCouponHistories);
    }









}
