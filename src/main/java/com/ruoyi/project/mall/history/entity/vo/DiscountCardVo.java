package com.ruoyi.project.mall.history.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/20
 **/

@Data
public class DiscountCardVo {

    private Long id;

    private String username;

    private String nickname;

    private Integer used;

    private Integer unused;

    private Integer expire;
}
