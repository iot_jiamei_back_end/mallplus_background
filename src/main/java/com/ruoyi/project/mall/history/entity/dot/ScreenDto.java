package com.ruoyi.project.mall.history.entity.dot;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/19
 **/

@Data
public class ScreenDto {


    private Long id;

    private String nickname;

}
