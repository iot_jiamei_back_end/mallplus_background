package com.ruoyi.project.mall.history.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.mall.history.domain.SmsCoupon;
import com.ruoyi.project.mall.history.entity.dot.ScreenDto;
import com.ruoyi.project.mall.history.entity.vo.DiscountCardVo;
import com.ruoyi.project.mall.history.mapper.SmsCouponMapper;
import com.ruoyi.project.mall.member.domain.UmsMember;
import com.ruoyi.project.mall.member.mapper.UmsMemberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.history.mapper.SmsCouponHistoryMapper;
import com.ruoyi.project.mall.history.domain.SmsCouponHistory;
import com.ruoyi.project.mall.history.service.ISmsCouponHistoryService;
import com.ruoyi.common.utils.text.Convert;

import javax.annotation.Resource;

/**
 * 优惠券使用、领取历史Service业务层处理
 *
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class SmsCouponHistoryServiceImpl implements ISmsCouponHistoryService {

    @Resource
    private UmsMemberMapper memberMapper;

    @Resource
    private SmsCouponHistoryMapper smsCouponHistoryMapper;

    @Resource
    private SmsCouponMapper smsCouponMapper;

    /**
     * 查询优惠券使用、领取历史
     *
     * @param id 优惠券使用、领取历史ID
     * @return 优惠券使用、领取历史
     */
    @Override
    public SmsCouponHistory selectSmsCouponHistoryById(Long id) {
        return smsCouponHistoryMapper.selectSmsCouponHistoryById(id);
    }

    /**
     * 查询优惠券使用、领取历史列表
     *
     * @param smsCouponHistory 优惠券使用、领取历史
     * @return 优惠券使用、领取历史
     */
    @Override
    public List<SmsCouponHistory> selectSmsCouponHistoryList(SmsCouponHistory smsCouponHistory) {
        return smsCouponHistoryMapper.selectSmsCouponHistoryList(smsCouponHistory);
    }

    /**
     * 新增优惠券使用、领取历史
     *
     * @param smsCouponHistory 优惠券使用、领取历史
     * @return 结果
     */
    @Override
    public int insertSmsCouponHistory(SmsCouponHistory smsCouponHistory) {
        smsCouponHistory.setCreateTime(DateUtils.getNowDate());
        return smsCouponHistoryMapper.insertSmsCouponHistory(smsCouponHistory);
    }

    /**
     * 修改优惠券使用、领取历史
     *
     * @param smsCouponHistory 优惠券使用、领取历史
     * @return 结果
     */
    @Override
    public int updateSmsCouponHistory(SmsCouponHistory smsCouponHistory) {
        smsCouponHistory.setUpdateTime(DateUtils.getNowDate());
        return smsCouponHistoryMapper.updateSmsCouponHistory(smsCouponHistory);
    }

    /**
     * 删除优惠券使用、领取历史对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSmsCouponHistoryByIds(String ids) {
        return smsCouponHistoryMapper.deleteSmsCouponHistoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除优惠券使用、领取历史信息
     *
     * @param id 优惠券使用、领取历史ID
     * @return 结果
     */
    @Override
    public int deleteSmsCouponHistoryById(Long id) {
        return smsCouponHistoryMapper.deleteSmsCouponHistoryById(id);
    }

    @Override
    public List<DiscountCardVo> discountCardList(ScreenDto screenDto) {
        List<DiscountCardVo> result = new ArrayList<>();
        UmsMember umsMember = new UmsMember();
        umsMember.setUsername(screenDto.getNickname());
        SmsCouponHistory smsCouponHistory = new SmsCouponHistory();
        List<UmsMember> umsMembers = memberMapper.selectUmsMemberList(umsMember);

        if (umsMembers.size() > 0) {
            for (UmsMember member : umsMembers) {
                Integer used = 0;
                Integer unused = 0;
                Integer expire = 0;
                Long id = member.getId();
                smsCouponHistory.setMemberId(id);
                DiscountCardVo discountCardVo = new DiscountCardVo();
                List<SmsCouponHistory> smsCouponHistories = smsCouponHistoryMapper.selectSmsCouponHistoryList(smsCouponHistory);

                if (smsCouponHistories.size() > 0) {
                    for (SmsCouponHistory couponHistory : smsCouponHistories) {
                        Integer useStatus = couponHistory.getUseStatus();

                        if (useStatus == 0) {
                            unused++;
                        } else if (useStatus == 1) {
                            used++;
                        } else {
                            expire++;
                        }


                    }
                }

                discountCardVo.setId(id);
                discountCardVo.setUsed(used);
                discountCardVo.setUnused(unused);
                discountCardVo.setExpire(expire);
                discountCardVo.setNickname(member.getNickname());
                discountCardVo.setUsername(member.getUsername());
                result.add(discountCardVo);
            }
        }

        return result;
    }

    @Override
    public List<SmsCoupon> giftCouponList() {
        return smsCouponMapper.selectSmsCouponList(null);
    }

    @Override
    public void giveAwayCoupons(Long memberId, String coupId) {
        Date date = new Date();
        SmsCouponHistory smsCouponHistory = new SmsCouponHistory();
        UmsMember umsMember = memberMapper.selectUmsMemberById(memberId);
        String[] split = coupId.split(",");

        for (String id : split) {
            SmsCoupon smsCoupon = smsCouponMapper.selectSmsCouponById(Long.valueOf(id));

            smsCouponHistory.setMemberId(memberId);
            smsCouponHistory.setCouponId(smsCoupon.getId());
            smsCouponHistory.setCouponCode(smsCoupon.getCode());
            smsCouponHistory.setMemberNickname(umsMember.getNickname());
            smsCouponHistory.setGetType(0);
            smsCouponHistory.setCreateTime(date);
            smsCouponHistory.setUseStatus(0);
            smsCouponHistory.setEndTime(smsCoupon.getEndTime());
            smsCouponHistory.setStartTime(smsCoupon.getStartTime());
            smsCouponHistory.setStoreId(smsCoupon.getStoreId());
            smsCouponHistory.setAmount(smsCoupon.getCount());
            smsCouponHistory.setIsDelete(1);
            smsCouponHistoryMapper.insertSmsCouponHistory(smsCouponHistory);

        }


    }

    @Override
    public List<SmsCoupon> couponList(SmsCoupon smsCoupon) {
        return smsCouponMapper.selectSmsCouponList(smsCoupon);
    }

    @Override
    public SmsCoupon couponUsageDetails(Long id) {
       return smsCouponMapper.selectSmsCouponById(id);
    }
}
