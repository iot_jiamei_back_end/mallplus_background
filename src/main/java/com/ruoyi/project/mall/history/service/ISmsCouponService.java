package com.ruoyi.project.mall.history.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.history.domain.SmsCoupon;

import java.util.List;

/**
 * 优惠卷Service接口
 * 
 * @author mallplus
 * @date 2020-03-19
 */
public interface ISmsCouponService extends IService<SmsCoupon>
{
    /**
     * 查询优惠卷
     * 
     * @param id 优惠卷ID
     * @return 优惠卷
     */
    public SmsCoupon selectSmsCouponById(Long id);

    /**
     * 查询优惠卷列表
     * 
     * @param smsCoupon 优惠卷
     * @return 优惠卷集合
     */
    public List<SmsCoupon> selectSmsCouponList(SmsCoupon smsCoupon);

    /**
     * 新增优惠卷
     * 
     * @param smsCoupon 优惠卷
     * @return 结果
     */
    public int insertSmsCoupon(SmsCoupon smsCoupon);

    /**
     * 修改优惠卷
     * 
     * @param smsCoupon 优惠卷
     * @return 结果
     */
    public int updateSmsCoupon(SmsCoupon smsCoupon);

    /**
     * 批量删除优惠卷
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsCouponByIds(String ids);

    /**
     * 删除优惠卷信息
     * 
     * @param id 优惠卷ID
     * @return 结果
     */
    public int deleteSmsCouponById(Long id);
}
