package com.ruoyi.project.mall.history.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Date;

/**
 * 优惠卷对象 sms_coupon
 *
 * @author mallplus
 * @date 2020-03-19
 */
@TableName("sms_coupon")
@Data
public class SmsCoupon {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 优惠卷类型；0->全场赠券；1->会员赠券；2->购物赠券；3->注册赠券
     */
    @Excel(name = "优惠卷类型；0->全场赠券；1->会员赠券；2->购物赠券；3->注册赠券")
    private Integer type;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String name;

    /**
     * 使用平台：0->全部；1->移动；2->PC
     */
    @Excel(name = "使用平台：0->全部；1->移动；2->PC")
    private Integer platform;

    /**
     * 数量
     */
    @Excel(name = "数量")
    private Long count;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    /**
     * 金额
     */
    @Excel(name = "金额")
    private Double amount;

    /**
     * 每人限领张数
     */
    @Excel(name = "每人限领张数")
    private Long perLimit;

    /**
     * 使用门槛；0表示无门槛
     */
    @Excel(name = "使用门槛；0表示无门槛")
    private Double minPoint;

    /**
     * 开始时间
     */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /**
     * 结束时间
     */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /**
     * 使用类型：0->全场通用；1->指定分类；2->指定商品
     */
    @Excel(name = "使用类型：0->全场通用；1->指定分类；2->指定商品")
    private Integer useType;

    /**
     * 发行数量
     */
    @Excel(name = "发行数量")
    private Long publishCount;

    /**
     * 已使用数量
     */
    @Excel(name = "已使用数量")
    private Long useCount;

    /**
     * 领取数量
     */
    @Excel(name = "领取数量")
    private Long receiveCount;

    /**
     * 可以领取的日期
     */
    @Excel(name = "可以领取的日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date enableTime;

    /**
     * 优惠码
     */
    @Excel(name = "优惠码")
    private String code;

    /**
     * 可领取的会员类型：0->无限时
     */
    @Excel(name = "可领取的会员类型：0->无限时")
    private Integer memberLevel;

    /**
     * 所属店铺
     */
    @Excel(name = "所属店铺")
    private Long storeId;


}
