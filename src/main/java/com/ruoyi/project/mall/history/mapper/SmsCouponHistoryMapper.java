package com.ruoyi.project.mall.history.mapper;

import com.ruoyi.project.mall.history.domain.SmsCouponHistory;
import java.util.List;

/**
 * 优惠券使用、领取历史Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface SmsCouponHistoryMapper 
{
    /**
     * 查询优惠券使用、领取历史
     * 
     * @param id 优惠券使用、领取历史ID
     * @return 优惠券使用、领取历史
     */
    public SmsCouponHistory selectSmsCouponHistoryById(Long id);

    /**
     * 查询优惠券使用、领取历史列表
     * 
     * @param smsCouponHistory 优惠券使用、领取历史
     * @return 优惠券使用、领取历史集合
     */
    public List<SmsCouponHistory> selectSmsCouponHistoryList(SmsCouponHistory smsCouponHistory);

    /**
     * 新增优惠券使用、领取历史
     * 
     * @param smsCouponHistory 优惠券使用、领取历史
     * @return 结果
     */
    public int insertSmsCouponHistory(SmsCouponHistory smsCouponHistory);

    /**
     * 修改优惠券使用、领取历史
     * 
     * @param smsCouponHistory 优惠券使用、领取历史
     * @return 结果
     */
    public int updateSmsCouponHistory(SmsCouponHistory smsCouponHistory);

    /**
     * 删除优惠券使用、领取历史
     * 
     * @param id 优惠券使用、领取历史ID
     * @return 结果
     */
    public int deleteSmsCouponHistoryById(Long id);

    /**
     * 批量删除优惠券使用、领取历史
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsCouponHistoryByIds(String[] ids);
}
