package com.ruoyi.project.mall.history.entity.vo;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/24
 **/

@Data
public class CoupCoutVo {

    private Long id;

    private Long couponId;

    private Long memberId;

    private String memberNickname;

    private Integer used;

    private Integer unused;

    private Integer expired;

}
