package com.ruoyi.project.mall.history.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.mall.history.domain.SmsCoupon;
import com.ruoyi.project.mall.history.service.ISmsCouponService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 优惠卷Controller
 *
 * @author mallplus
 * @date 2020-03-19
 */
@Controller
@RequestMapping("/mall/coupon")
public class SmsCouponController extends BaseController {
    private String prefix = "mall/coupon";

    @Autowired
    private ISmsCouponService smsCouponService;

    @RequiresPermissions("mall:coupon:view")
    @GetMapping()
    public String coupon() {
        return prefix + "/coupon";
    }

    /**
     * 查询优惠卷列表
     */
    @RequiresPermissions("mall:coupon:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageNum, Integer pageSize, SmsCoupon smsCoupon) {
        TableDataInfo tableDataInfo = new TableDataInfo();
        if (StringUtils.isEmpty(smsCoupon.getName())) {
            smsCoupon.setName(null);
        }
        if (StringUtils.isEmpty(smsCoupon.getNote())) {
            smsCoupon.setNote(null);
        }
        if (StringUtils.isEmpty(smsCoupon.getCode())) {
            smsCoupon.setCode(null);
        }
        IPage<SmsCoupon> page = smsCouponService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(smsCoupon));
        tableDataInfo.setTotal(page.getTotal());
        tableDataInfo.setRows(page.getRecords());
        return tableDataInfo;
    }

    /**
     * 导出优惠卷列表
     */
    @RequiresPermissions("mall:coupon:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SmsCoupon smsCoupon) {
        List<SmsCoupon> list = smsCouponService.selectSmsCouponList(smsCoupon);
        ExcelUtil<SmsCoupon> util = new ExcelUtil<SmsCoupon>(SmsCoupon.class);
        return util.exportExcel(list, "coupon");
    }

    /**
     * 新增优惠卷
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存优惠卷
     */
    @RequiresPermissions("mall:coupon:add")
    @Log(title = "优惠卷", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SmsCoupon smsCoupon) {
        return toAjax(smsCouponService.insertSmsCoupon(smsCoupon));
    }

    /**
     * 修改优惠卷
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        SmsCoupon smsCoupon = smsCouponService.selectSmsCouponById(id);
        mmap.put("smsCoupon", smsCoupon);
        return prefix + "/edit";
    }

    /**
     * 修改保存优惠卷
     */
    @RequiresPermissions("mall:coupon:edit")
    @Log(title = "优惠卷", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SmsCoupon smsCoupon) {
        return toAjax(smsCouponService.updateSmsCoupon(smsCoupon));
    }

    /**
     * 删除优惠卷
     */
    @RequiresPermissions("mall:coupon:remove")
    @Log(title = "优惠卷", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(smsCouponService.deleteSmsCouponByIds(ids));
    }

    @GetMapping("/detail")
    public String detail(Long id, Model model) {
        model.addAttribute("id", id);
        return prefix + "/detail";

    }
}
