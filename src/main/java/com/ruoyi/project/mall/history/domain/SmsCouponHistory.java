package com.ruoyi.project.mall.history.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 优惠券使用、领取历史对象 sms_coupon_history
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class SmsCouponHistory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 优惠券id */
    @Excel(name = "优惠券id")
    private Long couponId;

    /** 用户id */
    @Excel(name = "用户id")
    private Long memberId;

    /** 优惠券码 */
    @Excel(name = "优惠券码")
    private String couponCode;

    /** 领取人昵称 */
    @Excel(name = "领取人昵称")
    private String memberNickname;

    /** 获取类型：0->后台赠送；1->主动获取 */
    @Excel(name = "获取类型：0->后台赠送；1->主动获取")
    private Integer getType;

    /** 使用状态：0->未使用；1->已使用；2->已过期 */
    @Excel(name = "使用状态：0->未使用；1->已使用；2->已过期")
    private Integer useStatus;

    /** 使用时间 */
    @Excel(name = "使用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date useTime;

    /** 订单id */
    @Excel(name = "订单id")
    private Long orderId;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderSn;

    /** 开始时间 */
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    /** 优惠券金额 */
    @Excel(name = "优惠券金额")
    private Long amount;

    /** 0 已删除  1未删除 */
    @Excel(name = "0 已删除  1未删除")
    private Integer isDelete;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCouponId(Long couponId) 
    {
        this.couponId = couponId;
    }

    public Long getCouponId() 
    {
        return couponId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setCouponCode(String couponCode) 
    {
        this.couponCode = couponCode;
    }

    public String getCouponCode() 
    {
        return couponCode;
    }
    public void setMemberNickname(String memberNickname) 
    {
        this.memberNickname = memberNickname;
    }

    public String getMemberNickname() 
    {
        return memberNickname;
    }
    public void setGetType(Integer getType) 
    {
        this.getType = getType;
    }

    public Integer getGetType() 
    {
        return getType;
    }
    public void setUseStatus(Integer useStatus) 
    {
        this.useStatus = useStatus;
    }

    public Integer getUseStatus() 
    {
        return useStatus;
    }
    public void setUseTime(Date useTime) 
    {
        this.useTime = useTime;
    }

    public Date getUseTime() 
    {
        return useTime;
    }
    public void setOrderId(Long orderId) 
    {
        this.orderId = orderId;
    }

    public Long getOrderId() 
    {
        return orderId;
    }
    public void setOrderSn(String orderSn) 
    {
        this.orderSn = orderSn;
    }

    public String getOrderSn() 
    {
        return orderSn;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }
    public void setAmount(Long amount) 
    {
        this.amount = amount;
    }

    public Long getAmount() 
    {
        return amount;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("couponId", getCouponId())
            .append("memberId", getMemberId())
            .append("couponCode", getCouponCode())
            .append("memberNickname", getMemberNickname())
            .append("getType", getGetType())
            .append("createTime", getCreateTime())
            .append("useStatus", getUseStatus())
            .append("useTime", getUseTime())
            .append("orderId", getOrderId())
            .append("orderSn", getOrderSn())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("note", getNote())
            .append("storeId", getStoreId())
            .append("amount", getAmount())
            .append("isDelete", getIsDelete())
            .toString();
    }
}
