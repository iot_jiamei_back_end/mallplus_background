package com.ruoyi.project.mall.history.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.mall.history.domain.SmsCoupon;
import com.ruoyi.project.mall.history.mapper.SmsCouponMapper;
import com.ruoyi.project.mall.history.service.ISmsCouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.text.Convert;

import javax.annotation.Resource;

/**
 * 优惠卷Service业务层处理
 *
 * @author mallplus
 * @date 2020-03-19
 */
@Service
public class SmsCouponServiceImpl extends ServiceImpl<SmsCouponMapper, SmsCoupon> implements ISmsCouponService {

    @Resource
    private SmsCouponMapper smsCouponMapper;

    /**
     * 查询优惠卷
     *
     * @param id 优惠卷ID
     * @return 优惠卷
     */
    @Override
    public SmsCoupon selectSmsCouponById(Long id) {
        return smsCouponMapper.selectSmsCouponById(id);
    }

    /**
     * 查询优惠卷列表
     *
     * @param smsCoupon 优惠卷
     * @return 优惠卷
     */
    @Override
    public List<SmsCoupon> selectSmsCouponList(SmsCoupon smsCoupon) {
        return smsCouponMapper.selectSmsCouponList(smsCoupon);
    }

    /**
     * 新增优惠卷
     *
     * @param smsCoupon 优惠卷
     * @return 结果
     */
    @Override
    public int insertSmsCoupon(SmsCoupon smsCoupon) {
        return smsCouponMapper.insertSmsCoupon(smsCoupon);
    }

    /**
     * 修改优惠卷
     *
     * @param smsCoupon 优惠卷
     * @return 结果
     */
    @Override
    public int updateSmsCoupon(SmsCoupon smsCoupon) {
        return smsCouponMapper.updateSmsCoupon(smsCoupon);
    }

    /**
     * 删除优惠卷对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSmsCouponByIds(String ids) {
        return smsCouponMapper.deleteSmsCouponByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除优惠卷信息
     *
     * @param id 优惠卷ID
     * @return 结果
     */
    @Override
    public int deleteSmsCouponById(Long id) {
        return smsCouponMapper.deleteSmsCouponById(id);
    }
}
