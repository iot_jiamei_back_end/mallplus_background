package com.ruoyi.project.mall.history.entity.dot;

import lombok.Data;

import java.util.Date;

/**
 * @Author ZuRongTang
 * @Date 2020/3/23
 **/

@Data
public class SmsCouponDto {


    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 优惠卷类型；0->全场赠券；1->会员赠券；2->购物赠券；3->注册赠券
     */
    private Integer type;

    /**
     * $column.columnComment
     */
    private String name;

    /**
     * 使用平台：0->全部；1->移动；2->PC
     */
    private Integer platform;

    /**
     * 数量
     */
    private Long count;

    /**
     * 备注
     */
    private String note;

    /**
     * 金额
     */
    private Double amount;

    /**
     * 每人限领张数
     */
    private Long perLimit;

    /**
     * 使用门槛；0表示无门槛
     */
    private Double minPoint;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 使用类型：0->全场通用；1->指定分类；2->指定商品
     */
    private Integer useType;

    /**
     * 发行数量
     */
    private Long publishCount;

    /**
     * 已使用数量
     */
    private Long useCount;

    /**
     * 领取数量
     */
    private Long receiveCount;

    /**
     * 可以领取的日期
     */
    private Date enableTime;

    /**
     * 优惠码
     */
    private String code;

    private Integer memberLevel;

    private Long storeId;


}
