package com.ruoyi.project.mall.check.entiry.dto;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/24
 **/

@Data
public class BusinessReviewDto {

    private String name;

    private Integer status;

    private String startTime;

    private String endTime;
}
