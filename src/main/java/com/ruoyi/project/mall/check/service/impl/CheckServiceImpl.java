package com.ruoyi.project.mall.check.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.mall.check.entiry.dto.BusinessReviewDto;
import com.ruoyi.project.mall.check.service.CheckService;
import com.ruoyi.project.mall.product.mapper.PmsProductMapper;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.mapper.StoreMapper;
import com.ruoyi.project.mall.user.entity.dto.ProductReviewDto;
import com.ruoyi.project.mall.user.entity.vo.ProductReviewVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/24
 **/

@Service
public class CheckServiceImpl extends ServiceImpl<StoreMapper, Store> implements CheckService {

    @Resource
    private StoreMapper storeMapper;

    @Resource
    private PmsProductMapper pmsProductMapper;

    @Override
    public List<Store> lists(Integer pageNum, Integer pageSize, BusinessReviewDto businessReviewDto) {
        return storeMapper.merchantReviewList(pageNum, pageSize, businessReviewDto.getName(), businessReviewDto.getStatus(), businessReviewDto.getStartTime(), businessReviewDto.getEndTime());
    }

    @Override
    public List<Store> listAll(BusinessReviewDto businessReviewDto) {
        return storeMapper.merchantReviewListAll(businessReviewDto.getName(), businessReviewDto.getStatus(), businessReviewDto.getStartTime(), businessReviewDto.getEndTime());
    }

    @Override
    public List<ProductReviewVo> goodsList(Integer pageNum, Integer pageSize, ProductReviewDto productReviewDto) {
        return pmsProductMapper.productReviewList(pageNum, pageSize, productReviewDto.getApprovalStatus(), productReviewDto.getId(), productReviewDto.getName(), productReviewDto.getSortId(), productReviewDto.getStartTime(), productReviewDto.getEndTime());
    }


}
