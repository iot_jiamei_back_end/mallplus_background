package com.ruoyi.project.mall.check.entiry.vo;

import com.ruoyi.project.mall.stock.domain.SkuStock;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/25
 **/

@Data
public class GoodsDtailsInfoVo {

    private Long id;


    private Long brandId;


    private Long productCategoryId;


    private Long feightTemplateId;


    private Long productAttributeCategoryId;

    private String name;

    private String pic;


    private String productSn;


    private Integer deleteStatus;


    private Integer publishStatus;


    private Integer newStatus;


    private Integer recommandStatus;


    private Date varifyTime;


    private Integer verifyStatus;


    private Long sort;


    private Long sale;


    private Double price;


    private Double promotionPrice;


    private Long giftGrowth;


    private Long giftPoint;


    private Long usePointLimit;


    private String subTitle;


    private String description;


    private Double originalPrice;


    private Long stock;


    private Long lowStock;


    private String unit;


    private Double weight;


    private Integer previewStatus;


    private String serviceIds;


    private String keywords;


    private String note;


    private String albumPics;


    private String detailTitle;


    private String detailDesc;


    private String detailHtml;


    private String detailMobileHtml;


    private Date promotionStartTime;


    private Date promotionEndTime;


    private Long promotionPerLimit;


    private Integer promotionType;


    private String brandName;


    private String productCategoryName;


    private Long supplyId;


    private Long schoolId;


    private Long storeId;


    private Long memberId;


    private Long hit;


    private Long type;

    private Long areaId;


    private String areaName;


    private String schoolName;

    private String companyName;

    List<SkuStock> skuStocks;

}
