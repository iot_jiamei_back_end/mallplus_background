package com.ruoyi.project.mall.check.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.mall.check.entiry.dto.BusinessReviewDto;
import com.ruoyi.project.mall.check.entiry.vo.GoodsDtailsInfoVo;
import com.ruoyi.project.mall.check.service.CheckService;
import com.ruoyi.project.mall.product.domain.PmsProduct;
import com.ruoyi.project.mall.product.domain.PmsProductVertifyRecord;
import com.ruoyi.project.mall.product.mapper.PmsProductVertifyRecordMapper;
import com.ruoyi.project.mall.product.service.IPmsProductService;
import com.ruoyi.project.mall.stock.domain.SkuStock;
import com.ruoyi.project.mall.stock.mapper.SkuStockMapper;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.mapper.StoreMapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 审核管理
 *
 * @Author ZuRongTang
 * @Date 2020/3/24
 **/

@Controller
@RequestMapping("/mall/check")
public class CheckController extends BaseController {

    @Resource
    private StoreMapper storeMapper;

    @Resource
    private CheckService checkService;

    @Resource
    private SkuStockMapper skuStockMapper;

    @Resource
    private IPmsProductService pmsProductService;

    @Resource
    private PmsProductVertifyRecordMapper pmsProductVertifyRecordMapper;

    private String prefix = "mall/check";

    @RequiresPermissions("mall:check:view")
    @GetMapping()
    public String commitment() {
        return prefix + "/check";
    }

    @RequiresPermissions("mall:check:view")
    @GetMapping("/goods")
    public String goodsCommitment() {
        return prefix + "/goods";
    }


    @RequiresPermissions("mall:check:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageNum, Integer pageSize, BusinessReviewDto businessReviewDto) {
        TableDataInfo tableDataInfo = new TableDataInfo();

        QueryWrapper<Store> queryWrapper = new QueryWrapper<>();

        String name = businessReviewDto.getName();
        Integer status = businessReviewDto.getStatus();
        String endTime = businessReviewDto.getEndTime();
        String startTime = businessReviewDto.getStartTime();
        if (!StringUtils.isEmpty(name)) {
            queryWrapper.eq("name", name);
        }
        if (status != null) {
            queryWrapper.eq("status", status);
        }
        if (!StringUtils.isEmpty(startTime)) {
            queryWrapper.between("create_time", startTime, endTime);
        }
        IPage<Store> page = checkService.page(new Page<>(pageNum, pageSize), queryWrapper);
        tableDataInfo.setTotal(page.getTotal());
        tableDataInfo.setRows(page.getRecords());
        return tableDataInfo;
    }

    @GetMapping("/detail")
    public String detail(Long id, ModelMap mmap) {
        Store byId = checkService.getById(id);
        mmap.addAttribute("store", byId);
        return prefix + "/detail";
    }

    @GetMapping("/reviewDetails")
    public String reviewDetails(Long id, ModelMap mmap) {
        Store byId = checkService.getById(id);
        mmap.addAttribute("store", byId);
        return prefix + "/reviewDetails";
    }

    @PostMapping("/approval")
    @ResponseBody
    public AjaxResult approval(Long id) {
        Store store = checkService.getById(id);
        store.setStatus(1);
        store.setVerifyTime(new Date());
        checkService.updateById(store);
        return AjaxResult.success();
    }

    @PostMapping("/checkRows")
    @ResponseBody
    public AjaxResult checkRows(Long id, String content) {
        Store store = checkService.getById(id);
        store.setStatus(3);
        store.setVerifyComment(content);
        store.setVerifyTime(new Date());
        checkService.updateById(store);
        return AjaxResult.success();
    }


    @PostMapping("/goods/list")
    @ResponseBody
    public TableDataInfo goodsList(PmsProduct pmsProduct) {
        startPage();
        if (StringUtils.isEmpty(pmsProduct.getName())) {
            pmsProduct.setName(null);
        }
        List<PmsProduct> list = pmsProductService.selectPmsProductList(pmsProduct);

//        List<GoodsDtailsVo> result = new ArrayList<>();
//        for (PmsProduct product : list) {
//            GoodsDtailsVo goodsDtailsVo = new GoodsDtailsVo();
//            Long storeId = product.getStoreId();
//            Store store = storeMapper.selectStoreById(storeId);
//            goodsDtailsVo.setCompanyName(store.getName());
//            BeanUtils.copyProperties(product, goodsDtailsVo);
//            result.add(goodsDtailsVo);
//        }

        return getDataTable(list);
    }

    @GetMapping("/goodsDetail")
    public String goodsDetail(Long id, Model model) {
        PmsProduct pmsProduct = pmsProductService.selectPmsProductById(id);
        GoodsDtailsInfoVo goodsDtailsInfoVo = new GoodsDtailsInfoVo();
        SkuStock skuStock = new SkuStock();
        skuStock.setProductId(id);
        List<SkuStock> skuStocks = skuStockMapper.selectSkuStockList(skuStock);
        BeanUtils.copyProperties(pmsProduct, goodsDtailsInfoVo);
        goodsDtailsInfoVo.setSkuStocks(skuStocks);
        model.addAttribute("goodsInfo", goodsDtailsInfoVo);
        return prefix + "/goodsDetail";
    }

    @PostMapping("/through")
    @ResponseBody
    public AjaxResult through(Long id) {
        PmsProduct pmsProduct = pmsProductService.selectPmsProductById(id);
        pmsProduct.setVerifyStatus(1);
        pmsProduct.setVarifyTime(new Date());
        pmsProductService.updatePmsProduct(pmsProduct);
        PmsProductVertifyRecord pmsProductVertifyRecord = new PmsProductVertifyRecord();

        pmsProductVertifyRecord.setProductId(id);
        pmsProductVertifyRecord.setDetail("无");
        pmsProductVertifyRecord.setStatus(0);
        pmsProductVertifyRecord.setCreateTime(new Date());
        pmsProductVertifyRecord.setStoreId(pmsProduct.getStoreId());
        pmsProductVertifyRecordMapper.insertPmsProductVertifyRecord(pmsProductVertifyRecord);

        return toAjax(1);

    }

    @PostMapping("/noThrough")
    @ResponseBody
    public AjaxResult noThrough(Long id, String content) {
        PmsProduct pmsProduct = pmsProductService.selectPmsProductById(id);
        pmsProduct.setVerifyStatus(2);
        pmsProduct.setVarifyTime(new Date());
        pmsProductService.updatePmsProduct(pmsProduct);
        PmsProductVertifyRecord pmsProductVertifyRecord = new PmsProductVertifyRecord();

        pmsProductVertifyRecord.setProductId(id);
        pmsProductVertifyRecord.setDetail(content);
        pmsProductVertifyRecord.setStatus(1);
        pmsProductVertifyRecord.setCreateTime(new Date());
        pmsProductVertifyRecord.setStoreId(pmsProduct.getStoreId());
        pmsProductVertifyRecordMapper.insertPmsProductVertifyRecord(pmsProductVertifyRecord);
        return toAjax(1);
    }

    @GetMapping("/vertifyDatail")
    public String vertifyDatail(Long id, Model model) {
        model.addAttribute("id", id);
        return prefix + "/record";
    }


}
