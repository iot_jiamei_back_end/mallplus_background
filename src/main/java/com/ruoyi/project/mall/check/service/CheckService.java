package com.ruoyi.project.mall.check.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.check.entiry.dto.BusinessReviewDto;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.user.entity.dto.ProductReviewDto;
import com.ruoyi.project.mall.user.entity.vo.ProductReviewVo;

import java.util.List;

public interface CheckService extends IService<Store> {

    List<Store> lists(Integer pageNum, Integer pageSize, BusinessReviewDto businessReviewDto);

    List<Store> listAll(BusinessReviewDto businessReviewDto);

    List<ProductReviewVo> goodsList(Integer pageNum, Integer pageSize, ProductReviewDto productReviewDto);
}
