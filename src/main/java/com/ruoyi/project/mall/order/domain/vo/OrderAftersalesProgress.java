package com.ruoyi.project.mall.order.domain.vo;

import lombok.Data;

import java.util.Date;

/**
 * @program: RuoYi
 * @description:
 * @author: loop.fu
 * @create: 2020-03-12 15:31
 */
@Data
public class OrderAftersalesProgress {

    /**
     * 记录id
     */
    private Long id;
    /**
     * 事件
     */
    private String eventStr;
    /**
     * 详情
     */
    private String detail;
    /**
     * 处理过后的状态
     */
    private String handStatusStr;
    /**
     * 操作人员
     */
    private String opeateMan;

    private Date createTime;

}
