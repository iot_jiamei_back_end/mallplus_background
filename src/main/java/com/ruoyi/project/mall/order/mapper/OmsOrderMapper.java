package com.ruoyi.project.mall.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.core.annotation.Order;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单Mapper接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface OmsOrderMapper extends BaseMapper<OmsOrder> {
    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    public OmsOrder selectOmsOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param omsOrder 订单
     * @return 订单集合
     */
    public List<OmsOrder> selectOmsOrderList(OmsOrder omsOrder);

    /**
     * 新增订单
     *
     * @param omsOrder 订单
     * @return 结果
     */
    public int insertOmsOrder(OmsOrder omsOrder);

    /**
     * 修改订单
     *
     * @param omsOrder 订单
     * @return 结果
     */
    public int updateOmsOrder(OmsOrder omsOrder);

    /**
     * 删除订单
     *
     * @param id 订单ID
     * @return 结果
     */
    public int deleteOmsOrderById(Long id);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOmsOrderByIds(String[] ids);


    List<OmsOrder> getAftersalesList(@Param("memberId") Long memberId);

    Integer getOrderPayOrderNum(@Param("storeId") Integer storeId, @Param("date") Date date);

    BigDecimal getOrderOrderAmount(@Param("storeId") Integer storeId, @Param("date") Date date);

    BigDecimal getOrderPayAmount(@Param("storeId") Integer storeId, @Param("date") Date date);

    Integer getOrderTransactionUserNum(@Param("storeId") Integer storeId, @Param("date") Date date);

    List<OmsOrder> getSettlementOrder(@Param("storeId") Integer storeId, @Param("startTime") String startTime, @Param("endTime") String endTime);

    Integer getTotalOrderPayOrderNum(@Param("date") Date date);

    BigDecimal getTotalOrderOrderAmount(@Param("date") Date date);

    BigDecimal getTotalOrderPayAmount(@Param("date") Date date);

    Integer getTotalOrderTransactionUserNum(@Param("date") Date date);
}
