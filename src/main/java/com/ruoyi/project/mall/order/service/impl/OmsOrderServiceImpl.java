package com.ruoyi.project.mall.order.service.impl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.project.bill.aftersales.domain.BillAftersales;
import com.ruoyi.project.bill.aftersales.mapper.BillAftersalesMapper;
import com.ruoyi.project.bill.aftersalesItems.domain.BillAftersalesItems;
import com.ruoyi.project.bill.aftersalesItems.mapper.BillAftersalesItemsMapper;
import com.ruoyi.project.bill.aftersalesOperateHistory.domain.BillAftersalesOperateHistory;
import com.ruoyi.project.bill.aftersalesOperateHistory.mapper.BillAftersalesOperateHistoryMapper;
import com.ruoyi.project.mall.item.domain.OmsOrderItem;
import com.ruoyi.project.mall.item.mapper.OmsOrderItemMapper;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import com.ruoyi.project.mall.order.domain.vo.OrderAftersalesProgress;
import com.ruoyi.project.mall.order.domain.vo.OrderDetailVo;
import com.ruoyi.project.mall.order.domain.vo.OrderLogisticsVo;
import com.ruoyi.project.mall.order.mapper.OmsOrderMapper;
import com.ruoyi.project.mall.order.service.IOmsOrderService;
import com.ruoyi.project.mall.store.domain.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单Service业务层处理
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Service
public class OmsOrderServiceImpl extends ServiceImpl<OmsOrderMapper, OmsOrder> implements IOmsOrderService {
    @Autowired
    private OmsOrderMapper omsOrderMapper;

    @Autowired
    private OmsOrderItemMapper omsOrderItemMapper;

    @Autowired
    private BillAftersalesMapper billAftersalesMapper;

    @Autowired
    private BillAftersalesItemsMapper billAftersalesItemsMapper;

    @Autowired
    private BillAftersalesOperateHistoryMapper billAftersalesOperateHistoryMapper;


    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    @Override
    public OmsOrder selectOmsOrderById(Long id) {
        return omsOrderMapper.selectOmsOrderById(id);
    }

    /**
     * 查询订单列表
     *
     * @param omsOrder 订单
     * @return 订单
     */
    @Override
    public List<OmsOrder> selectOmsOrderList(OmsOrder omsOrder) {
        return omsOrderMapper.selectOmsOrderList(omsOrder);
    }

    /**
     * 新增订单
     *
     * @param omsOrder 订单
     * @return 结果
     */
    @Override
    public int insertOmsOrder(OmsOrder omsOrder) {
        omsOrder.setCreateTime(DateUtils.getNowDate());
        return omsOrderMapper.insertOmsOrder(omsOrder);
    }

    /**
     * 修改订单
     *
     * @param omsOrder 订单
     * @return 结果
     */
    @Override
    public int updateOmsOrder(OmsOrder omsOrder) {
        //omsOrder.setUpdateTime(DateUtils.getNowDate());
        return omsOrderMapper.updateOmsOrder(omsOrder);
    }

    /**
     * 删除订单对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteOmsOrderByIds(String ids) {
        return omsOrderMapper.deleteOmsOrderByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
    @Override
    public int deleteOmsOrderById(Long id) {
        return omsOrderMapper.deleteOmsOrderById(id);
    }

    /**
     * 订单详情
     *
     * @param orderId
     * @return
     */
    @Override
    public OrderDetailVo getOrderDetail(Long orderId) {
        // 获取订单信息
        OmsOrder omsOrder = omsOrderMapper.selectOmsOrderById(orderId);
        OmsOrderItem omsOrderItem = new OmsOrderItem();
        omsOrderItem.setOrderId(omsOrder.getId());
        // 获取订单商品信息
        List<OmsOrderItem> omsOrderItemList = omsOrderItemMapper.selectOmsOrderItemList(omsOrderItem);
        // 获取物流
        List<OrderLogisticsVo> orderLogisticsList = this.getOrderLogisticsList();
        // 获取用户售后信息
        BillAftersales billAftersales = new BillAftersales();
        billAftersales.setOrderId(orderId.toString());
        List<BillAftersales> billAftersalesList = billAftersalesMapper.selectBillAftersalesList(billAftersales);
        List<List<OrderAftersalesProgress>> orderAftersalesProgressList = new ArrayList<>();
        if (billAftersalesList.size() > 0) {// 有售后
            for (BillAftersales billAftersales1 : billAftersalesList) {
                // 获取售后记录
                List<BillAftersalesOperateHistory> history = billAftersalesOperateHistoryMapper.selectList(new QueryWrapper<BillAftersalesOperateHistory>().eq("aftersales_id", billAftersales1.getAftersalesId()));
                List<OrderAftersalesProgress> orderAftersalesProgList = this.getOrderAftersalesProgress(history);
                orderAftersalesProgressList.add(orderAftersalesProgList);
            }
        }
        OrderDetailVo orderDetailVo = new OrderDetailVo();
        orderDetailVo.setOmsOrder(omsOrder);
        orderDetailVo.setOmsOrderItemList(omsOrderItemList);
        orderDetailVo.setOrderLogisticsVoList(orderLogisticsList);
        return orderDetailVo;
    }

    private List<OrderAftersalesProgress> getOrderAftersalesProgress(List<BillAftersalesOperateHistory> history) {
        List<OrderAftersalesProgress> orderAftersalesProgList = new ArrayList<>();
        for (BillAftersalesOperateHistory billAftersalesOperateHistory : history) {

            OrderAftersalesProgress orderAftersalesProgress = new OrderAftersalesProgress();
            // 状态 0待处理 1退换货中 2已完成  3已拒绝
            if (billAftersalesOperateHistory.getNowApplyStatus().equals(0)) {
                orderAftersalesProgress.setHandStatusStr("待处理");
            } else if (billAftersalesOperateHistory.getNowApplyStatus().equals(1)) {
                orderAftersalesProgress.setHandStatusStr("退换货中");
            } else if (billAftersalesOperateHistory.getNowApplyStatus().equals(2)) {
                orderAftersalesProgress.setHandStatusStr("已完成");
            }
            if (billAftersalesOperateHistory.getNowApplyStatus().equals(3)) {
                orderAftersalesProgress.setHandStatusStr("已拒绝");
            }
            orderAftersalesProgress.setEventStr(orderAftersalesProgress.getHandStatusStr());
            orderAftersalesProgress.setDetail(billAftersalesOperateHistory.getNote());
            orderAftersalesProgress.setId(billAftersalesOperateHistory.getId());
            orderAftersalesProgress.setOpeateMan(billAftersalesOperateHistory.getOperateMan());
            orderAftersalesProgress.setCreateTime(billAftersalesOperateHistory.getCreateTime());

            orderAftersalesProgList.add(orderAftersalesProgress);
        }
        return orderAftersalesProgList;
    }

    @Override
    public Integer getOrderPayOrderNum(Store store, Date yesterdayDate) {
        return omsOrderMapper.getOrderPayOrderNum(store.getId(), yesterdayDate);
    }

    @Override
    public BigDecimal getOrderOrderAmount(Store store, Date yesterdayDate) {
        return omsOrderMapper.getOrderOrderAmount(store.getId(), yesterdayDate);
    }

    @Override
    public BigDecimal getOrderPayAmount(Store store, Date yesterdayDate) {
        return omsOrderMapper.getOrderPayAmount(store.getId(), yesterdayDate);
    }

    @Override
    public Integer getOrderTransactionUserNum(Store store, Date yesterdayDate) {
        return omsOrderMapper.getOrderTransactionUserNum(store.getId(), yesterdayDate);
    }

    @Override
    public Integer getTotalOrderPayOrderNum(Date date) {
        return omsOrderMapper.getTotalOrderPayOrderNum(date);
    }

    @Override
    public BigDecimal getTotalOrderOrderAmount(Date date) {
        return omsOrderMapper.getTotalOrderOrderAmount(date);
    }

    @Override
    public BigDecimal getTotalOrderPayAmount(Date date) {
        return omsOrderMapper.getTotalOrderPayAmount(date);
    }

    @Override
    public Integer getTotalOrderTransactionUserNum(Date date) {
        return omsOrderMapper.getTotalOrderTransactionUserNum(date);
    }

    private List<OrderLogisticsVo> getOrderLogisticsList() {
        List<OrderLogisticsVo> list = new ArrayList<>();
        OrderLogisticsVo orderLogisticsVo1 = new OrderLogisticsVo();
        orderLogisticsVo1.setAcceptStation("正在派件(派件人:邓裕富1,电话:18718866310)[深圳市]");
        orderLogisticsVo1.setAcceptTime("2014/06/25 08:05:37");
        orderLogisticsVo1.setLogisticsName("顺丰快递");
        list.add(orderLogisticsVo1);

        OrderLogisticsVo orderLogisticsVo2 = new OrderLogisticsVo();
        orderLogisticsVo2.setAcceptStation("正在派件(派件人:邓裕富2,电话:18718866310)[深圳市]");
        orderLogisticsVo2.setAcceptTime("2014/06/25 08:05:37");
        orderLogisticsVo2.setLogisticsName("顺丰快递");
        list.add(orderLogisticsVo2);

        OrderLogisticsVo orderLogisticsVo3 = new OrderLogisticsVo();
        orderLogisticsVo3.setAcceptStation("正在派件(派件人:邓裕富3,电话:18718866310)[深圳市]");
        orderLogisticsVo3.setAcceptTime("2014/06/25 08:05:37");
        orderLogisticsVo3.setLogisticsName("顺丰快递");
        list.add(orderLogisticsVo3);

        return list;
    }
}
