package com.ruoyi.project.mall.order.domain.vo;

import com.ruoyi.project.mall.item.domain.OmsOrderItem;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: RuoYi
 * @description:
 * @author: loop.fu
 * @create: 2020-03-12 15:15
 */
@Data
public class OrderDetailVo {

    // 订单信息
    private OmsOrder omsOrder;
    // 商品信息
    private List<OmsOrderItem> omsOrderItemList;
    // 物流信息,暂时没有对接
    private List<OrderLogisticsVo> orderLogisticsVoList;
    // 售后信息
//    private List<OrderAftersalesProgress> orderAftersalesProgressList;
    private List<List<OrderAftersalesProgress>> orderAftersalesProgressList;
}
