package com.ruoyi.project.mall.order.domain.vo;

import lombok.Data;

/**
 * @program: RuoYi
 * @description:
 * @author: loop.fu
 * @create: 2020-03-12 15:43
 */
@Data
public class OrderLogisticsVo {
    /**
     * 时间
     */
    private String acceptTime;
    /**
     * 状态
     */
    private String acceptStation;
    /**
     * 物流名称
     */
    private String logisticsName;

}
