package com.ruoyi.project.mall.order.domain.vo;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单对象 oms_order
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Data
public class OmsOrderVo{

    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    private Long id;
    /**
     * 公司名称
     */
    @Excel(name = "公司名称")
    private String companyName;
    /**
     * 所属店铺
     */
    @Excel(name = "所属店铺")
    private Long storeId;
    /**
     * 订单编号
     */
    @Excel(name = "订单编号")
    private String orderSn;
    /**
     * 结算总金额
     */
    @Excel(name = "结算总金额")
    private BigDecimal   settlementAmount ;
    /**
     * 订单总金额
     */
    @Excel(name = "订单总金额")
    private BigDecimal totalAmount;

    /**
     * 应付金额（实际支付金额）
     */
    @Excel(name = "应付金额")
    private BigDecimal payAmount;

    /**
     * 运费金额
     */
    @Excel(name = "运费金额")
    private BigDecimal freightAmount;

    /**
     * 优惠券金额
     */
    @Excel(name = "优惠券金额")
    private BigDecimal couponAmount;

    /**
     * 支付方式：0->未支付；1->支付宝；2->微信
     */
    @Excel(name = "支付方式：0->未支付；1->支付宝；2->微信")
    private Integer payType;

    /**
     * 订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单
     */
    @Excel(name = "订单状态：0->待付款；1->待发货；2->已发货；3->已完成；4->已关闭；5->无效订单")
    private Integer status;

    /**
     * 收货人姓名
     */
    @Excel(name = "收货人姓名")
    private String receiverName;

    /**
     * 收货人电话
     */
    @Excel(name = "收货人电话")
    private String receiverPhone;

    /**
     * 下单时间
     */
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date orderTime;

    /**
     * 支付时间
     */
    @Excel(name = "支付时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date paymentTime;

}
