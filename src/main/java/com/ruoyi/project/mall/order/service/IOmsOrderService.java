package com.ruoyi.project.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import com.ruoyi.project.mall.order.domain.vo.OrderDetailVo;
import com.ruoyi.project.mall.store.domain.Store;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单Service接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface IOmsOrderService extends IService<OmsOrder>{
    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    public OmsOrder selectOmsOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param omsOrder 订单
     * @return 订单集合
     */
    public List<OmsOrder> selectOmsOrderList(OmsOrder omsOrder);

    /**
     * 新增订单
     *
     * @param omsOrder 订单
     * @return 结果
     */
    public int insertOmsOrder(OmsOrder omsOrder);

    /**
     * 修改订单
     *
     * @param omsOrder 订单
     * @return 结果
     */
    public int updateOmsOrder(OmsOrder omsOrder);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOmsOrderByIds(String ids);

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
    public int deleteOmsOrderById(Long id);

    OrderDetailVo getOrderDetail(Long orderId);

    Integer getOrderPayOrderNum(Store store, Date yesterdayDate);

    BigDecimal getOrderOrderAmount(Store store, Date yesterdayDate);

    BigDecimal getOrderPayAmount(Store store, Date yesterdayDate);

    Integer getOrderTransactionUserNum(Store store, Date yesterdayDate);

    Integer getTotalOrderPayOrderNum(Date date);

    BigDecimal getTotalOrderOrderAmount(Date date);

    BigDecimal getTotalOrderPayAmount(Date date);

    Integer getTotalOrderTransactionUserNum(Date date);
}
