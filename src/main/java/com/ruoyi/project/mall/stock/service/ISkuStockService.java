package com.ruoyi.project.mall.stock.service;

import com.ruoyi.project.mall.stock.domain.SkuStock;
import java.util.List;

/**
 * sku的库存Service接口
 * 
 * @author mallplus
 * @date 2020-03-13
 */
public interface ISkuStockService 
{
    /**
     * 查询sku的库存
     * 
     * @param id sku的库存ID
     * @return sku的库存
     */
    public SkuStock selectSkuStockById(Long id);

    /**
     * 查询sku的库存列表
     * 
     * @param skuStock sku的库存
     * @return sku的库存集合
     */
    public List<SkuStock> selectSkuStockList(SkuStock skuStock);

    /**
     * 新增sku的库存
     * 
     * @param skuStock sku的库存
     * @return 结果
     */
    public int insertSkuStock(SkuStock skuStock);

    /**
     * 修改sku的库存
     * 
     * @param skuStock sku的库存
     * @return 结果
     */
    public int updateSkuStock(SkuStock skuStock);

    /**
     * 批量删除sku的库存
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSkuStockByIds(String ids);

    /**
     * 删除sku的库存信息
     * 
     * @param id sku的库存ID
     * @return 结果
     */
    public int deleteSkuStockById(Long id);
}
