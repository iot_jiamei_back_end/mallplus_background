package com.ruoyi.project.mall.stock.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * sku的库存对象 pms_sku_stock
 * 
 * @author mallplus
 * @date 2020-03-13
 */
public class SkuStock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long productId;

    /** sku编码 */
    @Excel(name = "sku编码")
    private String skuCode;

    /** 价格 */
    @Excel(name = "价格")
    private Double price;

    /** 库存 */
    @Excel(name = "库存")
    private Long stock;

    /** 预警库存 */
    @Excel(name = "预警库存")
    private Long lowStock;

    /** 销售属性1 */
    @Excel(name = "销售属性1")
    private String sp1;

    /** $column.columnComment */
    @Excel(name = "销售属性1")
    private String sp2;

    /** $column.columnComment */
    @Excel(name = "销售属性1")
    private String sp3;

    /** 展示图片 */
    @Excel(name = "展示图片")
    private String pic;

    /** 销量 */
    @Excel(name = "销量")
    private Long sale;

    /** 单品促销价格 */
    @Excel(name = "单品促销价格")
    private Double promotionPrice;

    /** 锁定库存 */
    @Excel(name = "锁定库存")
    private Long lockStock;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    /** $column.columnComment */
    @Excel(name = "所属店铺")
    private String productName;

    /** $column.columnComment */
    @Excel(name = "所属店铺")
    private String sp4;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setSkuCode(String skuCode) 
    {
        this.skuCode = skuCode;
    }

    public String getSkuCode() 
    {
        return skuCode;
    }
    public void setPrice(Double price) 
    {
        this.price = price;
    }

    public Double getPrice() 
    {
        return price;
    }
    public void setStock(Long stock) 
    {
        this.stock = stock;
    }

    public Long getStock() 
    {
        return stock;
    }
    public void setLowStock(Long lowStock) 
    {
        this.lowStock = lowStock;
    }

    public Long getLowStock() 
    {
        return lowStock;
    }
    public void setSp1(String sp1) 
    {
        this.sp1 = sp1;
    }

    public String getSp1() 
    {
        return sp1;
    }
    public void setSp2(String sp2) 
    {
        this.sp2 = sp2;
    }

    public String getSp2() 
    {
        return sp2;
    }
    public void setSp3(String sp3) 
    {
        this.sp3 = sp3;
    }

    public String getSp3() 
    {
        return sp3;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setSale(Long sale) 
    {
        this.sale = sale;
    }

    public Long getSale() 
    {
        return sale;
    }
    public void setPromotionPrice(Double promotionPrice) 
    {
        this.promotionPrice = promotionPrice;
    }

    public Double getPromotionPrice() 
    {
        return promotionPrice;
    }
    public void setLockStock(Long lockStock) 
    {
        this.lockStock = lockStock;
    }

    public Long getLockStock() 
    {
        return lockStock;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setSp4(String sp4) 
    {
        this.sp4 = sp4;
    }

    public String getSp4() 
    {
        return sp4;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productId", getProductId())
            .append("skuCode", getSkuCode())
            .append("price", getPrice())
            .append("stock", getStock())
            .append("lowStock", getLowStock())
            .append("sp1", getSp1())
            .append("sp2", getSp2())
            .append("sp3", getSp3())
            .append("pic", getPic())
            .append("sale", getSale())
            .append("promotionPrice", getPromotionPrice())
            .append("lockStock", getLockStock())
            .append("storeId", getStoreId())
            .append("productName", getProductName())
            .append("sp4", getSp4())
            .toString();
    }
}
