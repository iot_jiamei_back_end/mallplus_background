package com.ruoyi.project.mall.stock.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.stock.domain.SkuStock;
import com.ruoyi.project.mall.stock.service.ISkuStockService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * sku的库存Controller
 * 
 * @author mallplus
 * @date 2020-03-13
 */
@Controller
@RequestMapping("/mall/stock")
public class SkuStockController extends BaseController
{
    private String prefix = "mall/stock";

    @Autowired
    private ISkuStockService skuStockService;

    @RequiresPermissions("mall:stock:view")
    @GetMapping()
    public String stock()
    {
        return prefix + "/stock";
    }

    /**
     * 查询sku的库存列表
     */
    @RequiresPermissions("mall:stock:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SkuStock skuStock)
    {
        startPage();
        List<SkuStock> list = skuStockService.selectSkuStockList(skuStock);
        return getDataTable(list);
    }

    /**
     * 导出sku的库存列表
     */
    @RequiresPermissions("mall:stock:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SkuStock skuStock)
    {
        List<SkuStock> list = skuStockService.selectSkuStockList(skuStock);
        ExcelUtil<SkuStock> util = new ExcelUtil<SkuStock>(SkuStock.class);
        return util.exportExcel(list, "stock");
    }

    /**
     * 新增sku的库存
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存sku的库存
     */
    @RequiresPermissions("mall:stock:add")
    @Log(title = "sku的库存", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SkuStock skuStock)
    {
        return toAjax(skuStockService.insertSkuStock(skuStock));
    }

    /**
     * 修改sku的库存
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SkuStock skuStock = skuStockService.selectSkuStockById(id);
        mmap.put("skuStock", skuStock);
        return prefix + "/edit";
    }

    /**
     * 修改保存sku的库存
     */
    @RequiresPermissions("mall:stock:edit")
    @Log(title = "sku的库存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SkuStock skuStock)
    {
        return toAjax(skuStockService.updateSkuStock(skuStock));
    }

    /**
     * 删除sku的库存
     */
    @RequiresPermissions("mall:stock:remove")
    @Log(title = "sku的库存", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(skuStockService.deleteSkuStockByIds(ids));
    }
}
