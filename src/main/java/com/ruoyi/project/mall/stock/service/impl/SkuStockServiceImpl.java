package com.ruoyi.project.mall.stock.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.stock.mapper.SkuStockMapper;
import com.ruoyi.project.mall.stock.domain.SkuStock;
import com.ruoyi.project.mall.stock.service.ISkuStockService;
import com.ruoyi.common.utils.text.Convert;

/**
 * sku的库存Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-13
 */
@Service
public class SkuStockServiceImpl implements ISkuStockService 
{
    @Autowired
    private SkuStockMapper skuStockMapper;

    /**
     * 查询sku的库存
     * 
     * @param id sku的库存ID
     * @return sku的库存
     */
    @Override
    public SkuStock selectSkuStockById(Long id)
    {
        return skuStockMapper.selectSkuStockById(id);
    }

    /**
     * 查询sku的库存列表
     * 
     * @param skuStock sku的库存
     * @return sku的库存
     */
    @Override
    public List<SkuStock> selectSkuStockList(SkuStock skuStock)
    {
        return skuStockMapper.selectSkuStockList(skuStock);
    }

    /**
     * 新增sku的库存
     * 
     * @param skuStock sku的库存
     * @return 结果
     */
    @Override
    public int insertSkuStock(SkuStock skuStock)
    {
        return skuStockMapper.insertSkuStock(skuStock);
    }

    /**
     * 修改sku的库存
     * 
     * @param skuStock sku的库存
     * @return 结果
     */
    @Override
    public int updateSkuStock(SkuStock skuStock)
    {
        return skuStockMapper.updateSkuStock(skuStock);
    }

    /**
     * 删除sku的库存对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSkuStockByIds(String ids)
    {
        return skuStockMapper.deleteSkuStockByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除sku的库存信息
     * 
     * @param id sku的库存ID
     * @return 结果
     */
    @Override
    public int deleteSkuStockById(Long id)
    {
        return skuStockMapper.deleteSkuStockById(id);
    }
}
