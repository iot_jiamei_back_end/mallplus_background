package com.ruoyi.project.mall.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.user.entity.dto.MerchantDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 店铺Mapper接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface StoreMapper extends BaseMapper<Store> {
    /**
     * 查询店铺
     *
     * @param id 店铺ID
     * @return 店铺
     */
    public Store selectStoreById(Long id);

    /**
     * 查询店铺列表
     *
     * @param store 店铺
     * @return 店铺集合
     */
    public List<Store> selectStoreList(Store store);

    /**
     * 新增店铺
     *
     * @param store 店铺
     * @return 结果
     */
    public int insertStore(Store store);

    /**
     * 修改店铺
     *
     * @param store 店铺
     * @return 结果
     */
    public int updateStore(Store store);

    /**
     * 删除店铺
     *
     * @param id 店铺ID
     * @return 结果
     */
    public int deleteStoreById(Long id);

    /**
     * 批量删除店铺
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreByIds(String[] ids);

//    List<Store> merchantReviewList(MerchantDto merchantDto);

    List<Store> merchantReviewList(@Param("pageNum") Integer pageNum,@Param("pageSize") Integer pageSize,@Param("name") String name,@Param("status") Integer status,@Param("startTime") String startTime,@Param("endTime") String endTime);

    List<Store> merchantReviewListAll(@Param("name")String name,@Param("status") Integer status,@Param("startTime") String startTime,@Param("endTime") String endTime);
}
