package com.ruoyi.project.mall.store.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 店铺对象 sys_store
 *
 * @author mallplus
 * @date 2019-09-17
 */
@Data
@TableName("sys_store")
public class Store {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    private Long uid;
    /**
     * 联系人-姓名
     */
    @TableField("contact_name")
    private String contactName;
    /**
     * 联系人-手机号
     */
    @TableField("contact_mobile")
    private String contactMobile;
    /**
     * 联系人-邮箱
     */
    @TableField("contact_email")
    private String contactEmail;
    /**
     * 联系人-固定电话
     */
    @TableField("support_phone")
    private String supportPhone;
    /**
     * 公司信息-品牌名
     */
    @TableField("brand_name")
    private String brandName;
    /**
     * 公司信息-公司名字
     */
    private String name;
    /**
     * 公司信息-公司地址
     */
    @TableField("company_address")
    private String companyAddress;
    /**
     * 公司信息-经营范围
     */
    @TableField("business_scope")
    private String businessScope;
    /**
     * 公司信息-合作产品说明
     */
    private String description;
    /**
     * 资质信息-营业执照号
     */
    @TableField("business_license_no")
    private String businessLicenseNo;
    /**
     * 资质信息-营业执照pic
     */
    @TableField("support_name")
    private String supportName;
    /**
     * 资质信息-法人身份证正面
     */
    @TableField("id_card_front")
    private String idCardFront;
    /**
     * 资质信息-法人身份证反面
     */
    @TableField("id_card_reverse")
    private String idCardReverse;
    /**
     * 资质信息-企业LOGO
     */
    private String logo;

    private String verifyComment;

    private Date verifyTime;

    /**
     * 申请资料状态:  1 申请 2 拒绝 3 成功
     */
    private Integer status;
    /**
     * 申请时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 最后登录时间
     */
    @TableField("last_login_time")
    private Date lastLoginTime;
    /**
     * 是否删除
     */
    @TableField("is_deleted")
    private Integer isDeleted;
    /**
     * 删除时间
     */
    @TableField("delete_time")
    private Date deleteTime;
    /**
     * 过期时间
     */
    @TableField("expire_time")
    private Date expireTime;
    /**
     * 类型
     */
    private Integer type;
    ///////////////////////////////////////////////////////////////////////////////
    @TableField("contact_qq")
    private String contactQq;

    @TableField("sms_quantity")
    private Long smsQuantity;

    @TableField("register_type")
    private Integer registerType;

    @TableField("try_time")
    private Date tryTime;

    @TableField("address_province")
    private String addressProvince;

    @TableField("address_lat")
    private String addressLat;

    @TableField("address_detail")
    private String addressDetail;

    @TableField("address_city")
    private String addressCity;

    @TableField("address_lng")
    private String addressLng;

    @TableField("address_area")
    private String addressArea;

    @TableField("buy_plan_times")
    private Long buyPlanTimes;

    @TableField("service_phone")
    private String servicePhone;

    @TableField("is_checked")
    private Integer isChecked;

    @TableField("diy_profile")
    private String diyProfile;

    @TableField("industry_two")
    private Long industryTwo;

    @TableField("is_star")
    private Integer isStar;

    @TableField("is_try")
    private Integer isTry;

    @TableField("plan_id")
    private Long planId;

    /**
     * 二维码
     */
    @TableField("contact_qrcode")
    private String contactQrcode;

    @TableField("industry_one")
    private Long industryOne;

    private Integer hit;

    private Integer collect;
    @TableField("is_boutique")
    private Integer isBoutique;
    @TableField(exist = false)
    private Integer goodsCount;

    //    /**
//     * 短信数量
//     */
//    @Excel(name = "短信数量")
//    private Long smsQuantity;
//
//    /**
//     * 注册类型
//     */
//    @Excel(name = "注册类型")
//    private Integer registerType;
//
//    /**
//     * 到期时间
//     */
//    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date expireTime;
//
//    /**
//     * 尝试时间
//     */
//    @Excel(name = "尝试时间", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date tryTime;
//
//    /**
//     * 联系电话
//     */
//    @Excel(name = "联系电话")
//    private String contactMobile;
//
//    /**
//     * 地区省
//     */
//    @Excel(name = "地区省")
//    private String addressProvince;
//
//    /**
//     * 所购物品计划时间
//     */
//    @Excel(name = "所购物品计划时间")
//    private Long buyPlanTimes;
//
//    /**
//     * 是否选中
//     */
//    @Excel(name = "是否选中")
//    private Integer isChecked;
//
//    /**
//     * 是否删除
//     */
//    @Excel(name = "是否删除")
//    private Integer isDeleted;
//
//    /**
//     * 服务电话
//     */
//    @Excel(name = "服务电话")
//    private String servicePhone;
//
//    /**
//     * 地址名
//     */
//    @Excel(name = "地址名")
//    private String addressLat;
//
//    /**
//     * 联系人名
//     */
//    @Excel(name = "联系人名")
//    private String contactName;
//
//    /**
//     * 删除时间
//     */
//    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date deleteTime;
//
//    /**
//     * 自己配置文件
//     */
//    @Excel(name = "自己配置文件")
//    private String diyProfile;
//
//    /**
//     * 行业
//     */
//    @Excel(name = "行业")
//    private Long industryTwo;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private Integer isStar;
//
//    /**
//     * 尝试
//     */
//    @Excel(name = "尝试")
//    private Integer isTry;
//
//    /**
//     * 图标
//     */
//    @Excel(name = "图标")
//    private String logo;
//
//    /**
//     * 地址细节
//     */
//    @Excel(name = "地址细节")
//    private String addressDetail;
//
//    /**
//     * 计划id
//     */
//    @Excel(name = "计划id")
//    private Long planId;
//
//    /**
//     * 支持，维持名称
//     */
//    @Excel(name = "支持，维持名称")
//    private String supportName;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private String name;
//
//    /**
//     * 1 通过 2 审核中 3 拒绝
//     */
//    @Excel(name = "1 通过 2 审核中 3 拒绝")
//    private Integer status;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private Integer uid;
//
//    /**
//     * 类型
//     */
//    @Excel(name = "类型")
//    private Integer type;
//
//    /**
//     * 联系QQ
//     */
//    @Excel(name = "联系QQ")
//    private String contactQq;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private String addressLng;
//
//    /**
//     * null
//     */
//    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date lastLoginTime;
//
//    /**
//     * 支持电话
//     */
//    @Excel(name = "支持电话")
//    private String supportPhone;
//
//    /**
//     * 地址区域
//     */
//    @Excel(name = "地址区域")
//    private String addressArea;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private String contactQrcode;
//
//    /**
//     * 描述
//     */
//    @Excel(name = "描述")
//    private String description;
//
//    /**
//     * 行业1
//     */
//    @Excel(name = "行业1")
//    private Long industryOne;
//
//    /**
//     * 地址城市
//     */
//    @Excel(name = "地址城市")
//    private String addressCity;
//
//    /**
//     * null
//     */
//    private Long id;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private Long collect;
//
//    /**
//     * null
//     */
//    @Excel(name = "null")
//    private Long hit;
//
//    /**
//     * 添加商品
//     */
//    @Excel(name = "添加商品")
//    private Integer goodsCount;
//
//    /**
//     * 支付订单数
//     */
    @Excel(name = "支付订单数")
    private Integer orderCount;
    //
//    /**
//     * 文章数
//     */
    @Excel(name = "文章数")
    private Integer articleCount;
    //
//    /**
//     * 支付金额
//     */
    @Excel(name = "支付金额")
    private BigDecimal payAmount;
    //
//    /**
//     * 添加会员数
//     */
    @Excel(name = "添加会员数")
    private Integer memberCount;
//
//    /** 精品店铺标识,0:否，1:是 */
//    @Excel(name = "精品店铺标识,0:否，1:是")
//    private Integer isBoutique;
}
