package com.ruoyi.project.mall.cooperation.entity;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2020/3/26
 **/

@Data
public class AccessSettingVo {

    private Long id;

    private String name;

    private String logo;

    private String contactName;

    private String contactMobile;

    private String username;

    private String password;

    private Integer status;
}
