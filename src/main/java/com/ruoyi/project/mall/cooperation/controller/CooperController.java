package com.ruoyi.project.mall.cooperation.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.mall.check.entiry.dto.BusinessReviewDto;
import com.ruoyi.project.mall.cooperation.entity.AccessSettingVo;
import com.ruoyi.project.mall.cooperation.service.CooperService;
import com.ruoyi.project.mall.product.domain.PmsProduct;
import com.ruoyi.project.mall.product.service.IPmsProductService;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.user.domain.SysUser;
import com.ruoyi.project.mall.user.mapper.SysUserMapper;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/26
 **/

@RequestMapping("/mall/cooper")
@Controller
public class CooperController extends BaseController {

    @Resource
    private CooperService cooperService;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private IPmsProductService pmsProductService;

    private String prefix = "mall/cooperation";

    @RequiresPermissions("mall:cooper:view")
    @GetMapping()
    public String commitment() {
        return prefix + "/cooperation";
    }


    @RequiresPermissions("mall:check:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageNum, Integer pageSize, BusinessReviewDto businessReviewDto) {
        TableDataInfo tableDataInfo = new TableDataInfo();

        QueryWrapper<Store> queryWrapper = new QueryWrapper<>();

        String name = businessReviewDto.getName();
        Integer status = businessReviewDto.getStatus();
        String endTime = businessReviewDto.getEndTime();
        String startTime = businessReviewDto.getStartTime();
        if (!StringUtils.isEmpty(name)) {
            queryWrapper.eq("name", name);
        }
        if (status != null) {
            queryWrapper.eq("status", status);
        }
        if (!StringUtils.isEmpty(startTime)) {
            queryWrapper.between("create_time", startTime, endTime);
        }
        IPage<Store> page = cooperService.page(new Page<>(pageNum, pageSize), queryWrapper);
        tableDataInfo.setTotal(page.getTotal());
        tableDataInfo.setRows(page.getRecords());
        return tableDataInfo;
    }


    @GetMapping("/detail")
    public String detail(Long id, Model model) {
        Store byId = cooperService.getById(id);
        model.addAttribute("store", byId);
        return prefix + "/storeDetail";
    }

    @GetMapping("/setting")
    public String setting(Long id, Model model) {
        Store byId = cooperService.getById(id);

        QueryWrapper<SysUser> queryWrapper = new QueryWrapper();
        queryWrapper.eq("store_id", id).eq("is_admin", 0);
        SysUser sysUser = sysUserMapper.selectOne(queryWrapper);

        AccessSettingVo accessSettingVo = new AccessSettingVo();
        accessSettingVo.setId(byId.getId().longValue());
        accessSettingVo.setLogo(byId.getLogo());
        accessSettingVo.setName(byId.getName());
        accessSettingVo.setContactName(byId.getContactName());
        accessSettingVo.setContactMobile(byId.getContactMobile());

        if (sysUser != null) {
            accessSettingVo.setStatus(sysUser.getStatus());
            accessSettingVo.setUsername(sysUser.getUsername());
            accessSettingVo.setPassword(sysUser.getPassword());
        }
        model.addAttribute("user", accessSettingVo);
        return prefix + "/storeSetting";

    }

    @RequestMapping("/add")
    @ResponseBody
    public AjaxResult add(Long id, String username, String password) {
        cooperService.add(id, username, password);
        return AjaxResult.success();
    }

    @GetMapping("/storeProduct")
    public String storeProduct(Long id, Model model) {
        model.addAttribute("storeId", id);
        return prefix + "/storeProduct";
    }

    @PostMapping("/productList")
    @ResponseBody
    public TableDataInfo productList(PmsProduct pmsProduct) {

        if (StringUtils.isEmpty(pmsProduct.getName())) {
            pmsProduct.setName(null);
        }

        startPage();
        List<PmsProduct> list = pmsProductService.selectPmsProductList(pmsProduct);
        return getDataTable(list);
    }

    @RequiresPermissions("system:product:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(PmsProduct pmsProduct) {
        return toAjax(pmsProductService.changeStatus(pmsProduct));
    }


}
