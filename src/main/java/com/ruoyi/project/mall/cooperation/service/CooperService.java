package com.ruoyi.project.mall.cooperation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.mall.store.domain.Store;

public interface CooperService extends IService<Store> {
    void add(Long id, String username, String password);
}
