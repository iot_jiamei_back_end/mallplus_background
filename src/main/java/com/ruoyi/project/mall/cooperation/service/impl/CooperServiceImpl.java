package com.ruoyi.project.mall.cooperation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.mall.cooperation.service.CooperService;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.mapper.StoreMapper;
import com.ruoyi.project.mall.user.domain.SysUser;
import com.ruoyi.project.mall.user.mapper.SysUserMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author ZuRongTang
 * @Date 2020/3/26
 **/

@Service
public class CooperServiceImpl extends ServiceImpl<StoreMapper, Store> implements CooperService {

    @Resource
    private StoreMapper storeMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void add(Long id, String username, String password) {
        SysUser umsAdmin = new SysUser();
        String md5Password = passwordEncoder.encode(password);
        Store store = storeMapper.selectStoreById(id);
        umsAdmin.setSex(0);
        umsAdmin.setStatus(0);
        umsAdmin.setStoreId(id);
        umsAdmin.setCreateTime(new Date());
        umsAdmin.setIcon(store.getLogo());
        umsAdmin.setNickName(store.getName());
        umsAdmin.setEmail(store.getContactEmail());
        umsAdmin.setIsAdmin(1);
        umsAdmin.setPhone(store.getServicePhone());
        umsAdmin.setUsername(username);
        umsAdmin.setPassword(md5Password);
        sysUserMapper.insert(umsAdmin);
    }
}
