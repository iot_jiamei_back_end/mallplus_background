package com.ruoyi.project.mall.product.controller;

import java.util.List;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.project.mall.product.domain.ProductAttributeCategory;
import com.ruoyi.project.mall.product.service.IProductAttributeCategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 产品属性分类Controller
 * 
 * @author mallplus
 * @date 2020-02-15
 */
@Controller
@RequestMapping("/mall/attributeCategory")
public class ProductAttributeCategoryController extends BaseController
{
    private String prefix = "mall/product/attributeCategory";

    @Autowired
    private IProductAttributeCategoryService productAttributeCategoryService;

    @RequiresPermissions("mall:attributeCategory:view")
    @GetMapping()
    public String attributeCategory()
    {
        return prefix + "/attributeCategory";
    }

    /**
     * 查询产品属性分类列表
     */
    @RequiresPermissions("mall:attributeCategory:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductAttributeCategory productAttributeCategory)
    {
        startPage();
        List<ProductAttributeCategory> list = productAttributeCategoryService.selectProductAttributeCategoryList(productAttributeCategory);
        return getDataTable(list);
    }

    /**
     * 导出产品属性分类列表
     */
    @RequiresPermissions("mall:attributeCategory:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductAttributeCategory productAttributeCategory)
    {
        List<ProductAttributeCategory> list = productAttributeCategoryService.selectProductAttributeCategoryList(productAttributeCategory);
        ExcelUtil<ProductAttributeCategory> util = new ExcelUtil<ProductAttributeCategory>(ProductAttributeCategory.class);
        return util.exportExcel(list, "attributeCategory");
    }

    /**
     * 新增产品属性分类
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品属性分类
     */
    @RequiresPermissions("mall:attributeCategory:add")
    @Log(title = "产品属性分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductAttributeCategory productAttributeCategory)
    {
        return toAjax(productAttributeCategoryService.insertProductAttributeCategory(productAttributeCategory));
    }

    /**
     * 修改产品属性分类
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductAttributeCategory productAttributeCategory = productAttributeCategoryService.selectProductAttributeCategoryById(id);
        mmap.put("productAttributeCategory", productAttributeCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品属性分类
     */
    @RequiresPermissions("mall:attributeCategory:edit")
    @Log(title = "产品属性分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductAttributeCategory productAttributeCategory)
    {
        return toAjax(productAttributeCategoryService.updateProductAttributeCategory(productAttributeCategory));
    }

    /**
     * 删除产品属性分类
     */
    @RequiresPermissions("mall:attributeCategory:remove")
    @Log(title = "产品属性分类", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productAttributeCategoryService.deleteProductAttributeCategoryByIds(ids));
    }
}
