package com.ruoyi.project.mall.product.service;


import com.ruoyi.project.mall.product.domain.ProductAttributeCategory;

import java.util.List;

/**
 * 产品属性分类Service接口
 * 
 * @author mallplus
 * @date 2020-02-15
 */
public interface IProductAttributeCategoryService 
{
    /**
     * 查询产品属性分类
     * 
     * @param id 产品属性分类ID
     * @return 产品属性分类
     */
    public ProductAttributeCategory selectProductAttributeCategoryById(Long id);

    /**
     * 查询产品属性分类列表
     * 
     * @param productAttributeCategory 产品属性分类
     * @return 产品属性分类集合
     */
    public List<ProductAttributeCategory> selectProductAttributeCategoryList(ProductAttributeCategory productAttributeCategory);

    /**
     * 新增产品属性分类
     * 
     * @param productAttributeCategory 产品属性分类
     * @return 结果
     */
    public int insertProductAttributeCategory(ProductAttributeCategory productAttributeCategory);

    /**
     * 修改产品属性分类
     * 
     * @param productAttributeCategory 产品属性分类
     * @return 结果
     */
    public int updateProductAttributeCategory(ProductAttributeCategory productAttributeCategory);

    /**
     * 批量删除产品属性分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProductAttributeCategoryByIds(String ids);

    /**
     * 删除产品属性分类信息
     * 
     * @param id 产品属性分类ID
     * @return 结果
     */
    public int deleteProductAttributeCategoryById(Long id);
}
