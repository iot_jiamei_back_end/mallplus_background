package com.ruoyi.project.mall.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.product.domain.PmsProductVertifyRecord;
import java.util.List;

/**
 * 商品审核记录Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-25
 */
public interface PmsProductVertifyRecordMapper extends BaseMapper<PmsProductVertifyRecord> {
    /**
     * 查询商品审核记录
     * 
     * @param id 商品审核记录ID
     * @return 商品审核记录
     */
    public PmsProductVertifyRecord selectPmsProductVertifyRecordById(Long id);

    /**
     * 查询商品审核记录列表
     * 
     * @param pmsProductVertifyRecord 商品审核记录
     * @return 商品审核记录集合
     */
    public List<PmsProductVertifyRecord> selectPmsProductVertifyRecordList(PmsProductVertifyRecord pmsProductVertifyRecord);

    /**
     * 新增商品审核记录
     * 
     * @param pmsProductVertifyRecord 商品审核记录
     * @return 结果
     */
    public int insertPmsProductVertifyRecord(PmsProductVertifyRecord pmsProductVertifyRecord);

    /**
     * 修改商品审核记录
     * 
     * @param pmsProductVertifyRecord 商品审核记录
     * @return 结果
     */
    public int updatePmsProductVertifyRecord(PmsProductVertifyRecord pmsProductVertifyRecord);

    /**
     * 删除商品审核记录
     * 
     * @param id 商品审核记录ID
     * @return 结果
     */
    public int deletePmsProductVertifyRecordById(Long id);

    /**
     * 批量删除商品审核记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePmsProductVertifyRecordByIds(String[] ids);
}
