package com.ruoyi.project.mall.product.service.impl;

import java.util.List;

import com.ruoyi.project.mall.product.domain.ProductAttributeCategory;
import com.ruoyi.project.mall.product.mapper.ProductAttributeCategoryMapper;
import com.ruoyi.project.mall.product.service.IProductAttributeCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 产品属性分类Service业务层处理
 * 
 * @author mallplus
 * @date 2020-02-15
 */
@Service
public class ProductAttributeCategoryServiceImpl implements IProductAttributeCategoryService
{
    @Autowired
    private ProductAttributeCategoryMapper productAttributeCategoryMapper;

    /**
     * 查询产品属性分类
     * 
     * @param id 产品属性分类ID
     * @return 产品属性分类
     */
    @Override
    public ProductAttributeCategory selectProductAttributeCategoryById(Long id)
    {
        return productAttributeCategoryMapper.selectProductAttributeCategoryById(id);
    }

    /**
     * 查询产品属性分类列表
     * 
     * @param productAttributeCategory 产品属性分类
     * @return 产品属性分类
     */
    @Override
    public List<ProductAttributeCategory> selectProductAttributeCategoryList(ProductAttributeCategory productAttributeCategory)
    {
        return productAttributeCategoryMapper.selectProductAttributeCategoryList(productAttributeCategory);
    }

    /**
     * 新增产品属性分类
     * 
     * @param productAttributeCategory 产品属性分类
     * @return 结果
     */
    @Override
    public int insertProductAttributeCategory(ProductAttributeCategory productAttributeCategory)
    {
        return productAttributeCategoryMapper.insertProductAttributeCategory(productAttributeCategory);
    }

    /**
     * 修改产品属性分类
     * 
     * @param productAttributeCategory 产品属性分类
     * @return 结果
     */
    @Override
    public int updateProductAttributeCategory(ProductAttributeCategory productAttributeCategory)
    {
        return productAttributeCategoryMapper.updateProductAttributeCategory(productAttributeCategory);
    }

    /**
     * 删除产品属性分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProductAttributeCategoryByIds(String ids)
    {
        return productAttributeCategoryMapper.deleteProductAttributeCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品属性分类信息
     * 
     * @param id 产品属性分类ID
     * @return 结果
     */
    @Override
    public int deleteProductAttributeCategoryById(Long id)
    {
        return productAttributeCategoryMapper.deleteProductAttributeCategoryById(id);
    }
}
