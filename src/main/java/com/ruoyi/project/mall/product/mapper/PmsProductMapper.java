package com.ruoyi.project.mall.product.mapper;

import com.ruoyi.project.mall.product.domain.PmsProduct;
import com.ruoyi.project.mall.user.entity.dto.CompanyProductDto;
import com.ruoyi.project.mall.user.entity.dto.ProductReviewDto;
import com.ruoyi.project.mall.user.entity.po.ProductInfoPo;
import com.ruoyi.project.mall.user.entity.vo.ProductReviewVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品信息Mapper接口
 *
 * @author mallplus
 * @date 2019-09-17
 */
public interface PmsProductMapper {
    /**
     * 查询商品信息
     *
     * @param id 商品信息ID
     * @return 商品信息
     */
    public PmsProduct selectPmsProductById(Long id);

    /**
     * 查询商品信息列表
     *
     * @param pmsProduct 商品信息
     * @return 商品信息集合
     */
    public List<PmsProduct> selectPmsProductList(PmsProduct pmsProduct);

    /**
     * 新增商品信息
     *
     * @param pmsProduct 商品信息
     * @return 结果
     */
    public int insertPmsProduct(PmsProduct pmsProduct);

    /**
     * 修改商品信息
     *
     * @param pmsProduct 商品信息
     * @return 结果
     */
    public int updatePmsProduct(PmsProduct pmsProduct);

    /**
     * 删除商品信息
     *
     * @param id 商品信息ID
     * @return 结果
     */
    public int deletePmsProductById(Long id);

    /**
     * 批量删除商品信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePmsProductByIds(String[] ids);

    List<ProductInfoPo> companyProductList(CompanyProductDto companyProductDto);

    List<ProductReviewVo> productReviewList(@Param("pageNum") Integer pageNum, @Param("pageSize") Integer pageSize, @Param("approvalStatus") Integer approvalStatus,
                                            @Param("id") Long id, @Param("name") String name, @Param("sortId") Long sortId, @Param("startTime") String startTime,
                                            @Param("endTime") String endTime);
}
