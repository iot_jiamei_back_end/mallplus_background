package com.ruoyi.project.mall.product.service;

import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.project.mall.product.domain.ProductCategory;

import java.util.List;

/**
 * 产品分类Service接口
 * 
 * @author mallplus
 * @date 2020-02-14
 */
public interface IProductCategoryService 
{
    /**
     * 查询产品分类
     * 
     * @param id 产品分类ID
     * @return 产品分类
     */
    public ProductCategory selectProductCategoryById(Long id);

    /**
     * 查询产品分类列表
     * 
     * @param productCategory 产品分类
     * @return 产品分类集合
     */
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory);

    /**
     * 新增产品分类
     * 
     * @param productCategory 产品分类
     * @return 结果
     */
    public int insertProductCategory(ProductCategory productCategory);

    /**
     * 修改产品分类
     * 
     * @param productCategory 产品分类
     * @return 结果
     */
    public int updateProductCategory(ProductCategory productCategory);

    /**
     * 批量删除产品分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteProductCategoryByIds(String ids);

    /**
     * 删除产品分类信息
     * 
     * @param id 产品分类ID
     * @return 结果
     */
    public int deleteProductCategoryById(Long id);

    /**
     * 查询产品分类树列表
     * 
     * @return 所有产品分类信息
     */
    public List<Ztree> selectProductCategoryTree();

    int changeStatus(ProductCategory productCategory);
}
