package com.ruoyi.project.mall.product.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.product.mapper.PmsFeightTemplateMapper;
import com.ruoyi.project.mall.product.domain.PmsFeightTemplate;
import com.ruoyi.project.mall.product.service.IPmsFeightTemplateService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 运费模版Service业务层处理
 *
 * @author mallplus
 * @date 2020-03-23
 */
@Service
public class PmsFeightTemplateServiceImpl extends ServiceImpl<PmsFeightTemplateMapper, PmsFeightTemplate> implements IPmsFeightTemplateService {
    @Autowired
    private PmsFeightTemplateMapper pmsFeightTemplateMapper;

    /**
     * 查询运费模版
     *
     * @param id 运费模版ID
     * @return 运费模版
     */
    @Override
    public PmsFeightTemplate selectPmsFeightTemplateById(Long id) {
        return pmsFeightTemplateMapper.selectPmsFeightTemplateById(id);
    }

    /**
     * 查询运费模版列表
     *
     * @param pmsFeightTemplate 运费模版
     * @return 运费模版
     */
    @Override
    public List<PmsFeightTemplate> selectPmsFeightTemplateList(PmsFeightTemplate pmsFeightTemplate) {
        return pmsFeightTemplateMapper.selectPmsFeightTemplateList(pmsFeightTemplate);
    }

    /**
     * 新增运费模版
     *
     * @param pmsFeightTemplate 运费模版
     * @return 结果
     */
    @Override
    public int insertPmsFeightTemplate(PmsFeightTemplate pmsFeightTemplate) {
        pmsFeightTemplate.setCreateTime(DateUtils.getNowDate());
        return pmsFeightTemplateMapper.insertPmsFeightTemplate(pmsFeightTemplate);
    }

    /**
     * 修改运费模版
     *
     * @param pmsFeightTemplate 运费模版
     * @return 结果
     */
    @Override
    public int updatePmsFeightTemplate(PmsFeightTemplate pmsFeightTemplate) {

        return pmsFeightTemplateMapper.updatePmsFeightTemplate(pmsFeightTemplate);
    }

    /**
     * 删除运费模版对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmsFeightTemplateByIds(String ids) {
        return pmsFeightTemplateMapper.deletePmsFeightTemplateByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除运费模版信息
     *
     * @param id 运费模版ID
     * @return 结果
     */
    @Override
    public int deletePmsFeightTemplateById(Long id) {
        return pmsFeightTemplateMapper.deletePmsFeightTemplateById(id);
    }

}
