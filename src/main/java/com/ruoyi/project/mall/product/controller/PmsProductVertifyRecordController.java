package com.ruoyi.project.mall.product.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.product.domain.PmsProductVertifyRecord;
import com.ruoyi.project.mall.product.service.IPmsProductVertifyRecordService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商品审核记录Controller
 *
 * @author mallplus
 * @date 2020-03-25
 */
@Controller
@RequestMapping("/mall/record")
public class PmsProductVertifyRecordController extends BaseController {
    private String prefix = "mall/record";

    @Autowired
    private IPmsProductVertifyRecordService pmsProductVertifyRecordService;

    @RequiresPermissions("mall:record:view")
    @GetMapping()
    public String record() {
        return prefix + "/record";
    }

    /**
     * 查询商品审核记录列表
     */
    @RequiresPermissions("mall:record:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageNum, Integer pageSize, PmsProductVertifyRecord pmsProductVertifyRecord) {
        if (StringUtils.isEmpty(pmsProductVertifyRecord.getVertifyMan())) {
            pmsProductVertifyRecord.setVertifyMan(null);
        }
        if (StringUtils.isEmpty(pmsProductVertifyRecord.getDetail())) {
            pmsProductVertifyRecord.setDetail(null);
        }
        TableDataInfo tableDataInfo = new TableDataInfo();
        IPage<PmsProductVertifyRecord> page = pmsProductVertifyRecordService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(pmsProductVertifyRecord));
        tableDataInfo.setTotal(page.getTotal());
        tableDataInfo.setRows(page.getRecords());
        return tableDataInfo;
    }

    /**
     * 导出商品审核记录列表
     */
    @RequiresPermissions("mall:record:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmsProductVertifyRecord pmsProductVertifyRecord) {
        List<PmsProductVertifyRecord> list = pmsProductVertifyRecordService.selectPmsProductVertifyRecordList(pmsProductVertifyRecord);
        ExcelUtil<PmsProductVertifyRecord> util = new ExcelUtil<PmsProductVertifyRecord>(PmsProductVertifyRecord.class);
        return util.exportExcel(list, "record");
    }

    /**
     * 新增商品审核记录
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存商品审核记录
     */
    @RequiresPermissions("mall:record:add")
    @Log(title = "商品审核记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmsProductVertifyRecord pmsProductVertifyRecord) {
        return toAjax(pmsProductVertifyRecordService.insertPmsProductVertifyRecord(pmsProductVertifyRecord));
    }

    /**
     * 修改商品审核记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PmsProductVertifyRecord pmsProductVertifyRecord = pmsProductVertifyRecordService.selectPmsProductVertifyRecordById(id);
        mmap.put("pmsProductVertifyRecord", pmsProductVertifyRecord);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品审核记录
     */
    @RequiresPermissions("mall:record:edit")
    @Log(title = "商品审核记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmsProductVertifyRecord pmsProductVertifyRecord) {
        return toAjax(pmsProductVertifyRecordService.updatePmsProductVertifyRecord(pmsProductVertifyRecord));
    }

    /**
     * 删除商品审核记录
     */
    @RequiresPermissions("mall:record:remove")
    @Log(title = "商品审核记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(pmsProductVertifyRecordService.deletePmsProductVertifyRecordByIds(ids));
    }
}
