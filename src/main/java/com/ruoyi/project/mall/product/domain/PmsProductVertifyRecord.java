package com.ruoyi.project.mall.product.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;

import java.util.Date;


/**
 * 商品审核记录对象 pms_product_vertify_record
 *
 * @author mallplus
 * @date 2020-03-25
 */
@Data
@TableName("pms_product_vertify_record")
public class PmsProductVertifyRecord {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long productId;

    /**
     * 审核人
     */
    @Excel(name = "审核人")
    private String vertifyMan;

    /**
     * $column.columnComment
     */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Integer status;

    /**
     * 反馈详情
     */
    @Excel(name = "反馈详情")
    private String detail;

    /**
     * 所属店铺
     */
    @Excel(name = "所属店铺")
    private Long storeId;

    private Date CreateTime;


}
