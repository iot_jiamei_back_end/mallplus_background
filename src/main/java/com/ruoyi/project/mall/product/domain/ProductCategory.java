package com.ruoyi.project.mall.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.TreeEntity;

/**
 * 产品分类对象 pms_product_category
 * 
 * @author mallplus
 * @date 2020-02-14
 */
public class ProductCategory extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** null */
    @Excel(name = "null")
    private String name;

    /** banner图 */
    @Excel(name = "banner图")
    private String bannerImg;

    /** 分类级别：0-&gt;1级；1-&gt;2级 */
    @Excel(name = "分类级别：0-&gt;1级；1-&gt;2级")
    private Integer level;

    /** null */
    @Excel(name = "null")
    private Long productCount;

    /** null */
    @Excel(name = "null")
    private String productUnit;

    /** 是否显示在导航栏：0-&gt;不显示；1-&gt;显示 */
    @Excel(name = "是否显示在导航栏：0-&gt;不显示；1-&gt;显示")
    private Integer navStatus;

    /** 显示状态：0-&gt;不显示；1-&gt;显示 */
    @Excel(name = "显示状态：0-&gt;不显示；1-&gt;显示")
    private Integer showStatus;

    /** 是否是首页分类0--&gt;不是，1--&gt;是 */
    @Excel(name = "是否是首页分类0--&gt;不是，1--&gt;是")
    private Integer indexStatus;

    /** null */
    @Excel(name = "null")
    private Long sort;

    /** 图标 */
    @Excel(name = "图标")
    private String icon;

    /** null */
    @Excel(name = "null")
    private String keywords;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setBannerImg(String bannerImg) 
    {
        this.bannerImg = bannerImg;
    }

    public String getBannerImg() 
    {
        return bannerImg;
    }
    public void setLevel(Integer level) 
    {
        this.level = level;
    }

    public Integer getLevel() 
    {
        return level;
    }
    public void setProductCount(Long productCount) 
    {
        this.productCount = productCount;
    }

    public Long getProductCount() 
    {
        return productCount;
    }
    public void setProductUnit(String productUnit) 
    {
        this.productUnit = productUnit;
    }

    public String getProductUnit() 
    {
        return productUnit;
    }
    public void setNavStatus(Integer navStatus) 
    {
        this.navStatus = navStatus;
    }

    public Integer getNavStatus() 
    {
        return navStatus;
    }
    public void setShowStatus(Integer showStatus) 
    {
        this.showStatus = showStatus;
    }

    public Integer getShowStatus() 
    {
        return showStatus;
    }
    public void setIndexStatus(Integer indexStatus) 
    {
        this.indexStatus = indexStatus;
    }

    public Integer getIndexStatus() 
    {
        return indexStatus;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setIcon(String icon) 
    {
        this.icon = icon;
    }

    public String getIcon() 
    {
        return icon;
    }
    public void setKeywords(String keywords) 
    {
        this.keywords = keywords;
    }

    public String getKeywords() 
    {
        return keywords;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("name", getName())
            .append("bannerImg", getBannerImg())
            .append("level", getLevel())
            .append("productCount", getProductCount())
            .append("productUnit", getProductUnit())
            .append("navStatus", getNavStatus())
            .append("showStatus", getShowStatus())
            .append("indexStatus", getIndexStatus())
            .append("sort", getSort())
            .append("icon", getIcon())
            .append("keywords", getKeywords())
            .append("description", getDescription())
            .append("storeId", getStoreId())
            .toString();
    }
}
