package com.ruoyi.project.mall.product.service;


import com.ruoyi.project.mall.product.domain.PmsBrand;

import java.util.List;

/**
 * 品牌Service接口
 * 
 * @author mallplus
 * @date 2020-02-14
 */
public interface IPmsBrandService 
{
    /**
     * 查询品牌
     * 
     * @param id 品牌ID
     * @return 品牌
     */
    public PmsBrand selectPmsBrandById(Long id);

    /**
     * 查询品牌列表
     * 
     * @param pmsBrand 品牌
     * @return 品牌集合
     */
    public List<PmsBrand> selectPmsBrandList(PmsBrand pmsBrand);

    /**
     * 新增品牌
     * 
     * @param pmsBrand 品牌
     * @return 结果
     */
    public int insertPmsBrand(PmsBrand pmsBrand);

    /**
     * 修改品牌
     * 
     * @param pmsBrand 品牌
     * @return 结果
     */
    public int updatePmsBrand(PmsBrand pmsBrand);

    /**
     * 批量删除品牌
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePmsBrandByIds(String ids);

    /**
     * 删除品牌信息
     * 
     * @param id 品牌ID
     * @return 结果
     */
    public int deletePmsBrandById(Long id);

    int changeStatus(PmsBrand pmsBrand);
}
