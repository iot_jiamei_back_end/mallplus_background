package com.ruoyi.project.mall.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 产品属性分类对象 pms_product_attribute_category
 * 
 * @author mallplus
 * @date 2020-02-15
 */
public class ProductAttributeCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** null */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 属性数量 */
    @Excel(name = "属性数量")
    private Long attributeCount;

    /** 参数数量 */
    @Excel(name = "参数数量")
    private Long paramCount;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    /** null */
    @Excel(name = "null")
    private String pic;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAttributeCount(Long attributeCount) 
    {
        this.attributeCount = attributeCount;
    }

    public Long getAttributeCount() 
    {
        return attributeCount;
    }
    public void setParamCount(Long paramCount) 
    {
        this.paramCount = paramCount;
    }

    public Long getParamCount() 
    {
        return paramCount;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("attributeCount", getAttributeCount())
            .append("paramCount", getParamCount())
            .append("storeId", getStoreId())
            .append("pic", getPic())
            .toString();
    }
}
