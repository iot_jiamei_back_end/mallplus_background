package com.ruoyi.project.mall.product.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.product.mapper.PmsProductVertifyRecordMapper;
import com.ruoyi.project.mall.product.domain.PmsProductVertifyRecord;
import com.ruoyi.project.mall.product.service.IPmsProductVertifyRecordService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 商品审核记录Service业务层处理
 *
 * @author mallplus
 * @date 2020-03-25
 */
@Service
public class PmsProductVertifyRecordServiceImpl extends ServiceImpl<PmsProductVertifyRecordMapper, PmsProductVertifyRecord> implements IPmsProductVertifyRecordService {
    @Autowired
    private PmsProductVertifyRecordMapper pmsProductVertifyRecordMapper;

    /**
     * 查询商品审核记录
     *
     * @param id 商品审核记录ID
     * @return 商品审核记录
     */
    @Override
    public PmsProductVertifyRecord selectPmsProductVertifyRecordById(Long id) {
        return pmsProductVertifyRecordMapper.selectPmsProductVertifyRecordById(id);
    }

    /**
     * 查询商品审核记录列表
     *
     * @param pmsProductVertifyRecord 商品审核记录
     * @return 商品审核记录
     */
    @Override
    public List<PmsProductVertifyRecord> selectPmsProductVertifyRecordList(PmsProductVertifyRecord pmsProductVertifyRecord) {
        return pmsProductVertifyRecordMapper.selectPmsProductVertifyRecordList(pmsProductVertifyRecord);
    }

    /**
     * 新增商品审核记录
     *
     * @param pmsProductVertifyRecord 商品审核记录
     * @return 结果
     */
    @Override
    public int insertPmsProductVertifyRecord(PmsProductVertifyRecord pmsProductVertifyRecord) {

        return pmsProductVertifyRecordMapper.insertPmsProductVertifyRecord(pmsProductVertifyRecord);
    }

    /**
     * 修改商品审核记录
     *
     * @param pmsProductVertifyRecord 商品审核记录
     * @return 结果
     */
    @Override
    public int updatePmsProductVertifyRecord(PmsProductVertifyRecord pmsProductVertifyRecord) {

        return pmsProductVertifyRecordMapper.updatePmsProductVertifyRecord(pmsProductVertifyRecord);
    }

    /**
     * 删除商品审核记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmsProductVertifyRecordByIds(String ids) {
        return pmsProductVertifyRecordMapper.deletePmsProductVertifyRecordByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品审核记录信息
     *
     * @param id 商品审核记录ID
     * @return 结果
     */
    @Override
    public int deletePmsProductVertifyRecordById(Long id) {
        return pmsProductVertifyRecordMapper.deletePmsProductVertifyRecordById(id);
    }
}
