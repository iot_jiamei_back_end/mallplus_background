package com.ruoyi.project.mall.product.service.impl;

import java.util.List;

import com.ruoyi.project.mall.product.domain.ProductAttribute;
import com.ruoyi.project.mall.product.mapper.ProductAttributeMapper;
import com.ruoyi.project.mall.product.service.IProductAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.stereotype.Service;

/**
 * 商品属性参数Service业务层处理
 * 
 * @author mallplus
 * @date 2020-02-15
 */
@Service
public class ProductAttributeServiceImpl implements IProductAttributeService
{
    @Autowired
    private ProductAttributeMapper productAttributeMapper;

    /**
     * 查询商品属性参数
     * 
     * @param id 商品属性参数ID
     * @return 商品属性参数
     */
    @Override
    public ProductAttribute selectProductAttributeById(Long id)
    {
        return productAttributeMapper.selectProductAttributeById(id);
    }

    /**
     * 查询商品属性参数列表
     * 
     * @param productAttribute 商品属性参数
     * @return 商品属性参数
     */
    @Override
    public List<ProductAttribute> selectProductAttributeList(ProductAttribute productAttribute)
    {
        return productAttributeMapper.selectProductAttributeList(productAttribute);
    }

    /**
     * 新增商品属性参数
     * 
     * @param productAttribute 商品属性参数
     * @return 结果
     */
    @Override
    public int insertProductAttribute(ProductAttribute productAttribute)
    {
        return productAttributeMapper.insertProductAttribute(productAttribute);
    }

    /**
     * 修改商品属性参数
     * 
     * @param productAttribute 商品属性参数
     * @return 结果
     */
    @Override
    public int updateProductAttribute(ProductAttribute productAttribute)
    {
        return productAttributeMapper.updateProductAttribute(productAttribute);
    }

    /**
     * 删除商品属性参数对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProductAttributeByIds(String ids)
    {
        return productAttributeMapper.deleteProductAttributeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除商品属性参数信息
     * 
     * @param id 商品属性参数ID
     * @return 结果
     */
    @Override
    public int deleteProductAttributeById(Long id)
    {
        return productAttributeMapper.deleteProductAttributeById(id);
    }
}
