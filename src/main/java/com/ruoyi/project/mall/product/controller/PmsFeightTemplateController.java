package com.ruoyi.project.mall.product.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.product.domain.PmsFeightTemplate;
import com.ruoyi.project.mall.product.service.IPmsFeightTemplateService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 运费模版Controller
 *
 * @author mallplus
 * @date 2020-03-23
 */
@Controller
@RequestMapping("/mall/template")
public class PmsFeightTemplateController extends BaseController {
    private String prefix = "mall/template";

    @Autowired
    private IPmsFeightTemplateService pmsFeightTemplateService;

    @RequiresPermissions("mall:template:view")
    @GetMapping()
    public String template() {
        return prefix + "/template";
    }

    /**
     * 查询运费模版列表
     */
    @RequiresPermissions("mall:template:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageSize, Integer pageNum, PmsFeightTemplate pmsFeightTemplate) {

        if (StringUtils.isEmpty(pmsFeightTemplate.getName())) {
            pmsFeightTemplate.setName(null);
        }
        if (StringUtils.isEmpty(pmsFeightTemplate.getDest())) {
            pmsFeightTemplate.setDest(null);
        }
        IPage<PmsFeightTemplate> page = pmsFeightTemplateService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(pmsFeightTemplate));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

    /**
     * 导出运费模版列表
     */
    @RequiresPermissions("mall:template:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmsFeightTemplate pmsFeightTemplate) {
        List<PmsFeightTemplate> list = pmsFeightTemplateService.selectPmsFeightTemplateList(pmsFeightTemplate);
        ExcelUtil<PmsFeightTemplate> util = new ExcelUtil<PmsFeightTemplate>(PmsFeightTemplate.class);
        return util.exportExcel(list, "template");
    }

    /**
     * 新增运费模版
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存运费模版
     */
    @RequiresPermissions("mall:template:add")
    @Log(title = "运费模版", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmsFeightTemplate pmsFeightTemplate) {
        return toAjax(pmsFeightTemplateService.insertPmsFeightTemplate(pmsFeightTemplate));
    }

    /**
     * 修改运费模版
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PmsFeightTemplate pmsFeightTemplate = pmsFeightTemplateService.getById(id);
        mmap.put("pmsFeightTemplate", pmsFeightTemplate);
        return prefix + "/edit";
    }

    /**
     * 修改保存运费模版
     */
    @RequiresPermissions("mall:template:edit")
    @Log(title = "运费模版", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmsFeightTemplate pmsFeightTemplate) {
        return toAjax(pmsFeightTemplateService.updatePmsFeightTemplate(pmsFeightTemplate));
    }

    /**
     * 删除运费模版
     */
    @RequiresPermissions("mall:template:remove")
    @Log(title = "运费模版", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        String[] split = ids.split(",");
        for (String s : split) {
            pmsFeightTemplateService.removeById(Long.valueOf(s));
        }
        return toAjax(1);
    }
}
