package com.ruoyi.project.mall.product.controller;

import java.util.List;

import com.ruoyi.project.mall.product.domain.PmsBrand;
import com.ruoyi.project.mall.product.domain.ProductCategory;
import com.ruoyi.project.mall.product.service.IProductCategoryService;
import com.ruoyi.project.system.menu.domain.Menu;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.domain.Ztree;

/**
 * 产品分类Controller
 * 
 * @author mallplus
 * @date 2020-02-14
 */
@Controller
@RequestMapping("/mall/category")
public class ProductCategoryController extends BaseController
{
    private String prefix = "mall/product/category";

    @Autowired
    private IProductCategoryService productCategoryService;

    @RequiresPermissions("mall:category:view")
    @GetMapping()
    public String category()
    {
        return prefix + "/category";
    }

    /**
     * 查询产品分类树列表
     */
    @RequiresPermissions("mall:category:list")
    @PostMapping("/list")
    @ResponseBody
    public List<ProductCategory> list(ProductCategory productCategory)
    {
        List<ProductCategory> list = productCategoryService.selectProductCategoryList(productCategory);
        return list;
    }

    /**
     * 导出产品分类列表
     */
    @RequiresPermissions("mall:category:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductCategory productCategory)
    {
        List<ProductCategory> list = productCategoryService.selectProductCategoryList(productCategory);
        ExcelUtil<ProductCategory> util = new ExcelUtil<ProductCategory>(ProductCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 新增产品分类
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("productCategory", productCategoryService.selectProductCategoryById(id));
        }
        else
        {
            ProductCategory productCategory=new ProductCategory();
            productCategory.setName("主目录");
            productCategory.setParentId(0L);
            mmap.put("productCategory", productCategory);
        }
        return prefix + "/add";
    }

    /**
     * 新增保存产品分类
     */
    @RequiresPermissions("mall:category:add")
    @Log(title = "产品分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductCategory productCategory)
    {
        if (productCategory.getLevel()==0){
            productCategory.setParentId(0L);
        }
        return toAjax(productCategoryService.insertProductCategory(productCategory));
    }

    /**
     * 修改产品分类
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductCategory productCategory = productCategoryService.selectProductCategoryById(id);
        if (productCategory.getParentId()==0L){
            productCategory.setParentName("主目录");
        }
        mmap.put("productCategory", productCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品分类
     */
    @RequiresPermissions("mall:category:edit")
    @Log(title = "产品分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductCategory productCategory)
    {
        return toAjax(productCategoryService.updateProductCategory(productCategory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("mall:category:remove")
    @Log(title = "产品分类", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(productCategoryService.deleteProductCategoryById(id));
    }

    /**
     * 选择产品分类树
     */
    @GetMapping(value = { "/selectCategoryTree/{id}", "/selectCategoryTree/" })
    public String selectCategoryTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("productCategory", productCategoryService.selectProductCategoryById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载产品分类树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = productCategoryService.selectProductCategoryTree();
        return ztrees;
    }

    /**
     * 状态修改
     */
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:category:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(ProductCategory productCategory)
    {
        return toAjax(productCategoryService.changeStatus(productCategory));
    }
}
