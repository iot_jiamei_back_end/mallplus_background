package com.ruoyi.project.mall.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 商品属性参数对象 pms_product_attribute
 * 
 * @author mallplus
 * @date 2020-02-15
 */
public class ProductAttribute extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 商品类型 */
    @Excel(name = "商品类型")
    private Long productAttributeCategoryId;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 属性选择类型：0-&gt;唯一；1-&gt;单选；2-&gt;多选 */
    @Excel(name = "属性选择类型：0-&gt;唯一；1-&gt;单选；2-&gt;多选")
    private Integer selectType;

    /** 属性录入方式：0-&gt;手工录入；1-&gt;从列表中选取 */
    @Excel(name = "属性录入方式：0-&gt;手工录入；1-&gt;从列表中选取")
    private Integer inputType;

    /** 可选值列表，以逗号隔开 */
    @Excel(name = "可选值列表，以逗号隔开")
    private String inputList;

    /** 排序字段：最高的可以单独上传图片 */
    @Excel(name = "排序字段：最高的可以单独上传图片")
    private Long sort;

    /** 分类筛选样式：1-&gt;普通；1-&gt;颜色 */
    @Excel(name = "分类筛选样式：1-&gt;普通；1-&gt;颜色")
    private Integer filterType;

    /** 检索类型；0-&gt;不需要进行检索；1-&gt;关键字检索；2-&gt;范围检索 */
    @Excel(name = "检索类型；0-&gt;不需要进行检索；1-&gt;关键字检索；2-&gt;范围检索")
    private Integer searchType;

    /** 相同属性产品是否关联；0-&gt;不关联；1-&gt;关联 */
    @Excel(name = "相同属性产品是否关联；0-&gt;不关联；1-&gt;关联")
    private Integer relatedStatus;

    /** 是否支持手动新增；0-&gt;不支持；1-&gt;支持 */
    @Excel(name = "是否支持手动新增；0-&gt;不支持；1-&gt;支持")
    private Integer handAddStatus;

    /** 属性的类型；0-&gt;规格；1-&gt;参数 */
    @Excel(name = "属性的类型；0-&gt;规格；1-&gt;参数")
    private Integer type;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductAttributeCategoryId(Long productAttributeCategoryId) 
    {
        this.productAttributeCategoryId = productAttributeCategoryId;
    }

    public Long getProductAttributeCategoryId() 
    {
        return productAttributeCategoryId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setSelectType(Integer selectType) 
    {
        this.selectType = selectType;
    }

    public Integer getSelectType() 
    {
        return selectType;
    }
    public void setInputType(Integer inputType) 
    {
        this.inputType = inputType;
    }

    public Integer getInputType() 
    {
        return inputType;
    }
    public void setInputList(String inputList) 
    {
        this.inputList = inputList;
    }

    public String getInputList() 
    {
        return inputList;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setFilterType(Integer filterType) 
    {
        this.filterType = filterType;
    }

    public Integer getFilterType() 
    {
        return filterType;
    }
    public void setSearchType(Integer searchType) 
    {
        this.searchType = searchType;
    }

    public Integer getSearchType() 
    {
        return searchType;
    }
    public void setRelatedStatus(Integer relatedStatus) 
    {
        this.relatedStatus = relatedStatus;
    }

    public Integer getRelatedStatus() 
    {
        return relatedStatus;
    }
    public void setHandAddStatus(Integer handAddStatus) 
    {
        this.handAddStatus = handAddStatus;
    }

    public Integer getHandAddStatus() 
    {
        return handAddStatus;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productAttributeCategoryId", getProductAttributeCategoryId())
            .append("name", getName())
            .append("selectType", getSelectType())
            .append("inputType", getInputType())
            .append("inputList", getInputList())
            .append("sort", getSort())
            .append("filterType", getFilterType())
            .append("searchType", getSearchType())
            .append("relatedStatus", getRelatedStatus())
            .append("handAddStatus", getHandAddStatus())
            .append("type", getType())
            .append("storeId", getStoreId())
            .toString();
    }
}
