package com.ruoyi.project.mall.product.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 运费模版对象 pms_feight_template
 *
 * @author mallplus
 * @date 2020-03-23
 */
@Data
@TableName("pms_feight_template")
public class PmsFeightTemplate {
    private static final long serialVersionUID = 1L;

    /**
     * $column.columnComment
     */
    private Long id;

    /**
     * 模板名称
     */
    @Excel(name = "模板名称")
    private String name;

    /**
     * 计费类型:0->按重量；1->按件数
     */
    @Excel(name = "计费类型:0->按重量；1->按件数")
    private Integer chargeType;

    /**
     * 首重kg
     */
    @Excel(name = "首重kg")
    private Double firstWeight;

    /**
     * 首费（元）
     */
    @Excel(name = "首费", readConverterExp = "元=")
    private Double firstFee;

    /**
     * 续重(kg)
     */
    @Excel(name = "续重(kg)")
    private Double continueWeight;

    /**
     * 续费(元)
     */
    @Excel(name = "续费(元)")
    private Double continmeFee;

    /**
     * 目的地（省、市）
     */
    @Excel(name = "目的地", readConverterExp = "省=、市")
    private String dest;

    /**
     * 所属店铺
     */
    @Excel(name = "所属店铺")
    private Long storeId;

    private Date createTime;


}
