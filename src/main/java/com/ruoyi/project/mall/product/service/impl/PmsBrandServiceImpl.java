package com.ruoyi.project.mall.product.service.impl;

import java.util.List;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.project.mall.product.domain.PmsBrand;
import com.ruoyi.project.mall.product.mapper.PmsBrandMapper;
import com.ruoyi.project.mall.product.service.IPmsBrandService;
import com.ruoyi.project.system.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 品牌Service业务层处理
 * 
 * @author mallplus
 * @date 2020-02-14
 */
@Service
public class PmsBrandServiceImpl implements IPmsBrandService
{
    @Autowired
    private PmsBrandMapper pmsBrandMapper;

    /**
     * 查询品牌
     * 
     * @param id 品牌ID
     * @return 品牌
     */
    @Override
    public PmsBrand selectPmsBrandById(Long id)
    {
        return pmsBrandMapper.selectPmsBrandById(id);
    }

    /**
     * 查询品牌列表
     * 
     * @param pmsBrand 品牌
     * @return 品牌
     */
    @Override
    public List<PmsBrand> selectPmsBrandList(PmsBrand pmsBrand)
    {
        return pmsBrandMapper.selectPmsBrandList(pmsBrand);
    }

    /**
     * 新增品牌
     * 
     * @param pmsBrand 品牌
     * @return 结果
     */
    @Override
    public int insertPmsBrand(PmsBrand pmsBrand)
    {
        return pmsBrandMapper.insertPmsBrand(pmsBrand);
    }

    /**
     * 修改品牌
     * 
     * @param pmsBrand 品牌
     * @return 结果
     */
    @Override
    public int updatePmsBrand(PmsBrand pmsBrand)
    {
        return pmsBrandMapper.updatePmsBrand(pmsBrand);
    }

    /**
     * 删除品牌对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmsBrandByIds(String ids)
    {
        return pmsBrandMapper.deletePmsBrandByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除品牌信息
     * 
     * @param id 品牌ID
     * @return 结果
     */
    @Override
    public int deletePmsBrandById(Long id)
    {
        return pmsBrandMapper.deletePmsBrandById(id);
    }


    /**
     * 状态修改
     *
     * @param pmsBrand 信息
     * @return 结果
     */
    @Override
    public int changeStatus(PmsBrand pmsBrand)
    {
        return pmsBrandMapper.updatePmsBrand(pmsBrand);
    }
}
