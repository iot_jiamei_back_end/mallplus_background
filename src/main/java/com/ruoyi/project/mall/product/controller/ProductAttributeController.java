package com.ruoyi.project.mall.product.controller;

import java.util.List;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.project.mall.product.domain.ProductAttribute;
import com.ruoyi.project.mall.product.domain.ProductAttributeCategory;
import com.ruoyi.project.mall.product.service.IProductAttributeCategoryService;
import com.ruoyi.project.mall.product.service.IProductAttributeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 商品属性参数Controller
 * 
 * @author mallplus
 * @date 2020-02-15
 */
@Controller
@RequestMapping("/mall/attribute")
public class ProductAttributeController extends BaseController
{
    private String prefix = "mall/product/attribute";

    @Autowired
    private IProductAttributeService productAttributeService;

    @Autowired
    private IProductAttributeCategoryService productAttributeCategoryService;


    @RequiresPermissions("mall:attribute:view")
    @GetMapping()
    public String attribute(String productAttributeCategoryId,String type, ModelMap mmap)
    {
        if (productAttributeCategoryId!=null){
            mmap.put("productAttributeCategoryId", productAttributeCategoryId);
            mmap.put("type", type);
            mmap.put("is", true);
        }else {
            mmap.put("is", false);
        }
        return prefix + "/attribute";
    }

    /**
     * 查询商品属性参数列表
     */
    @RequiresPermissions("mall:attribute:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ProductAttribute productAttribute)
    {
        startPage();
        List<ProductAttribute> list = productAttributeService.selectProductAttributeList(productAttribute);
        return getDataTable(list);
    }

    /**
     * 导出商品属性参数列表
     */
    @RequiresPermissions("mall:attribute:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ProductAttribute productAttribute)
    {
        List<ProductAttribute> list = productAttributeService.selectProductAttributeList(productAttribute);
        ExcelUtil<ProductAttribute> util = new ExcelUtil<ProductAttribute>(ProductAttribute.class);
        return util.exportExcel(list, "attribute");
    }

    /**
     * 新增商品属性参数
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        // 查询
        List<ProductAttributeCategory> productAttributeCategoryList = productAttributeCategoryService.selectProductAttributeCategoryList(null);
        System.out.println("商品类型列表："+productAttributeCategoryList.size());
        mmap.put("productAttributeCategoryList", productAttributeCategoryList);
        return prefix + "/add";
    }

    /**
     * 新增保存商品属性参数
     */
    @RequiresPermissions("mall:attribute:add")
    @Log(title = "商品属性参数", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ProductAttribute productAttribute)
    {
        return toAjax(productAttributeService.insertProductAttribute(productAttribute));
    }

    /**
     * 修改商品属性参数
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        ProductAttribute productAttribute = productAttributeService.selectProductAttributeById(id);
        mmap.put("productAttribute", productAttribute);
        // 查询
        List<ProductAttributeCategory> productAttributeCategoryList = productAttributeCategoryService.selectProductAttributeCategoryList(null);
        System.out.println("商品类型列表："+productAttributeCategoryList.size());
        mmap.put("productAttributeCategoryList", productAttributeCategoryList);
        return prefix + "/edit";
    }

    /**
     * 修改保存商品属性参数
     */
    @RequiresPermissions("mall:attribute:edit")
    @Log(title = "商品属性参数", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ProductAttribute productAttribute)
    {
        return toAjax(productAttributeService.updateProductAttribute(productAttribute));
    }

    /**
     * 删除商品属性参数
     */
    @RequiresPermissions("mall:attribute:remove")
    @Log(title = "商品属性参数", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(productAttributeService.deleteProductAttributeByIds(ids));
    }
}
