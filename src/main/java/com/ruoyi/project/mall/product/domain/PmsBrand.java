package com.ruoyi.project.mall.product.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 品牌对象 pms_brand
 * 
 * @author mallplus
 * @date 2020-02-14
 */
public class PmsBrand extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 首字母 */
    @Excel(name = "首字母")
    private String firstLetter;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 是否为品牌制造商：0 不是；是 */
    @Excel(name = "是否为品牌制造商：0-&gt;不是；1-&gt;是")
    private Integer factoryStatus;

    /** 0 不显示  1显示 */
    @Excel(name = "显示状态")
    private Integer showStatus;

    /** 产品数量 */
    @Excel(name = "产品数量")
    private Long productCount;

    /** 产品评论数量 */
    @Excel(name = "产品评论数量")
    private Long productCommentCount;

    /** 品牌logo */
    @Excel(name = "品牌logo")
    private String logo;

    /** 专区大图 */
    @Excel(name = "专区大图")
    private String bigPic;

    /** 品牌故事 */
    @Excel(name = "品牌故事")
    private String brandStory;

    /** 所属店铺 */
    @Excel(name = "所属店铺")
    private Long storeId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setFirstLetter(String firstLetter) 
    {
        this.firstLetter = firstLetter;
    }

    public String getFirstLetter() 
    {
        return firstLetter;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setFactoryStatus(Integer factoryStatus) 
    {
        this.factoryStatus = factoryStatus;
    }

    public Integer getFactoryStatus() 
    {
        return factoryStatus;
    }
    public void setShowStatus(Integer showStatus) 
    {
        this.showStatus = showStatus;
    }

    public Integer getShowStatus() 
    {
        return showStatus;
    }
    public void setProductCount(Long productCount) 
    {
        this.productCount = productCount;
    }

    public Long getProductCount() 
    {
        return productCount;
    }
    public void setProductCommentCount(Long productCommentCount) 
    {
        this.productCommentCount = productCommentCount;
    }

    public Long getProductCommentCount() 
    {
        return productCommentCount;
    }
    public void setLogo(String logo) 
    {
        this.logo = logo;
    }

    public String getLogo() 
    {
        return logo;
    }
    public void setBigPic(String bigPic) 
    {
        this.bigPic = bigPic;
    }

    public String getBigPic() 
    {
        return bigPic;
    }
    public void setBrandStory(String brandStory) 
    {
        this.brandStory = brandStory;
    }

    public String getBrandStory() 
    {
        return brandStory;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("firstLetter", getFirstLetter())
            .append("sort", getSort())
            .append("factoryStatus", getFactoryStatus())
            .append("showStatus", getShowStatus())
            .append("productCount", getProductCount())
            .append("productCommentCount", getProductCommentCount())
            .append("logo", getLogo())
            .append("bigPic", getBigPic())
            .append("brandStory", getBrandStory())
            .append("storeId", getStoreId())
            .toString();
    }
}
