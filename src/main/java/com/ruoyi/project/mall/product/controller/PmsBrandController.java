package com.ruoyi.project.mall.product.controller;

import java.util.List;

import com.ruoyi.project.mall.product.domain.PmsBrand;
import com.ruoyi.project.mall.product.service.IPmsBrandService;
import com.ruoyi.project.system.user.controller.UserController;
import com.ruoyi.project.system.user.domain.User;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 品牌Controller
 * 
 * @author mallplus
 * @date 2020-02-14
 */
@Controller
@RequestMapping("/mall/brand")
public class PmsBrandController extends BaseController
{
    private String prefix = "mall/product/brand";

    @Autowired
    private IPmsBrandService pmsBrandService;

    @RequiresPermissions("mall:brand:view")
    @GetMapping()
    public String brand()
    {
        return prefix + "/brand";
    }

    /**
     * 查询品牌列表
     */
    @RequiresPermissions("mall:brand:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmsBrand pmsBrand)
    {
        startPage();
        List<PmsBrand> list = pmsBrandService.selectPmsBrandList(pmsBrand);
        return getDataTable(list);
    }

    /**
     * 导出品牌列表
     */
    @RequiresPermissions("mall:brand:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PmsBrand pmsBrand)
    {
        List<PmsBrand> list = pmsBrandService.selectPmsBrandList(pmsBrand);
        ExcelUtil<PmsBrand> util = new ExcelUtil<PmsBrand>(PmsBrand.class);
        return util.exportExcel(list, "brand");
    }

    /**
     * 新增品牌
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存品牌
     */
    @RequiresPermissions("mall:brand:add")
    @Log(title = "品牌", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmsBrand pmsBrand)
    {
        return toAjax(pmsBrandService.insertPmsBrand(pmsBrand));
    }

    /**
     * 修改品牌
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        PmsBrand pmsBrand = pmsBrandService.selectPmsBrandById(id);
        mmap.put("pmsBrand", pmsBrand);
        return prefix + "/edit";
    }

    /**
     * 修改保存品牌
     */
    @RequiresPermissions("mall:brand:edit")
    @Log(title = "品牌", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmsBrand pmsBrand)
    {
        return toAjax(pmsBrandService.updatePmsBrand(pmsBrand));
    }

    /**
     * 删除品牌
     */
    @RequiresPermissions("mall:brand:remove")
    @Log(title = "品牌", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(pmsBrandService.deletePmsBrandByIds(ids));
    }

    /**
     * 状态修改
     */
    @Log(title = "品牌管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:brand:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(PmsBrand pmsBrand)
    {
        return toAjax(pmsBrandService.changeStatus(pmsBrand));
    }
}
