package com.ruoyi.project.mall.product.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.project.mall.product.domain.PmsBrand;
import com.ruoyi.project.mall.product.domain.ProductCategory;
import com.ruoyi.project.mall.product.mapper.ProductCategoryMapper;
import com.ruoyi.project.mall.product.service.IProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 产品分类Service业务层处理
 * 
 * @author mallplus
 * @date 2020-02-14
 */
@Service
public class ProductCategoryServiceImpl implements IProductCategoryService
{
    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    /**
     * 查询产品分类
     * 
     * @param id 产品分类ID
     * @return 产品分类
     */
    @Override
    public ProductCategory selectProductCategoryById(Long id)
    {
        return productCategoryMapper.selectProductCategoryById(id);
    }

    /**
     * 查询产品分类列表
     * 
     * @param productCategory 产品分类
     * @return 产品分类
     */
    @Override
    public List<ProductCategory> selectProductCategoryList(ProductCategory productCategory)
    {
        return productCategoryMapper.selectProductCategoryList(productCategory);
    }

    /**
     * 新增产品分类
     * 
     * @param productCategory 产品分类
     * @return 结果
     */
    @Override
    public int insertProductCategory(ProductCategory productCategory)
    {
        return productCategoryMapper.insertProductCategory(productCategory);
    }

    /**
     * 修改产品分类
     * 
     * @param productCategory 产品分类
     * @return 结果
     */
    @Override
    public int updateProductCategory(ProductCategory productCategory)
    {
        return productCategoryMapper.updateProductCategory(productCategory);
    }

    /**
     * 删除产品分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteProductCategoryByIds(String ids)
    {
        return productCategoryMapper.deleteProductCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除产品分类信息
     * 
     * @param id 产品分类ID
     * @return 结果
     */
    @Override
    public int deleteProductCategoryById(Long id)
    {
        return productCategoryMapper.deleteProductCategoryById(id);
    }

    /**
     * 查询产品分类树列表
     * 
     * @return 所有产品分类信息
     */
    @Override
    public List<Ztree> selectProductCategoryTree()
    {
        List<ProductCategory> productCategoryList = productCategoryMapper.selectProductCategoryList(new ProductCategory());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (ProductCategory productCategory : productCategoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(productCategory.getId());
            ztree.setpId(productCategory.getParentId());
            ztree.setName(productCategory.getName());
            ztree.setTitle(productCategory.getName());
            ztrees.add(ztree);
        }
        return ztrees;
    }

    /**
     * 状态修改
     *
     * @param productCategory 信息
     * @return 结果
     */
    @Override
    public int changeStatus(ProductCategory productCategory)
    {
        return productCategoryMapper.updateProductCategory(productCategory);
    }
}
