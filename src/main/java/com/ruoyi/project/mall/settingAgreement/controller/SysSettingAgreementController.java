package com.ruoyi.project.mall.settingAgreement.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import io.swagger.models.auth.In;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.settingAgreement.domain.SysSettingAgreement;
import com.ruoyi.project.mall.settingAgreement.service.ISysSettingAgreementService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * settingAgreementController
 *
 * @author mallplus
 * @date 2020-03-17
 */
@Controller
@RequestMapping("/mall/settingAgreement")
public class SysSettingAgreementController extends BaseController {
    private String prefix = "mall/settingAgreement";

    @Autowired
    private ISysSettingAgreementService sysSettingAgreementService;

    @RequiresPermissions("mall:settingAgreement:view")
    @GetMapping()
    public String settingAgreement() {
        return prefix + "/settingAgreement";
    }

    /**
     * 查询settingAgreement列表
     */
    @RequiresPermissions("mall:settingAgreement:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysSettingAgreement sysSettingAgreement, Integer pageSize, Integer pageNum) {
        IPage<SysSettingAgreement> page = sysSettingAgreementService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>());
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

    /**
     * 导出settingAgreement列表
     */
    @RequiresPermissions("mall:settingAgreement:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysSettingAgreement sysSettingAgreement, Integer pageSize, Integer pageNum) {
        IPage<SysSettingAgreement> page = sysSettingAgreementService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(sysSettingAgreement));
        List<SysSettingAgreement> list = page.getRecords();
        ExcelUtil<SysSettingAgreement> util = new ExcelUtil<SysSettingAgreement>(SysSettingAgreement.class);
        return util.exportExcel(list, "settingAgreement");
    }

    /**
     * 新增settingAgreement
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存settingAgreement
     */
    @RequiresPermissions("mall:settingAgreement:add")
    @Log(title = "settingAgreement", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysSettingAgreement sysSettingAgreement) {
        return toAjax(sysSettingAgreementService.save(sysSettingAgreement));
    }

    /**
     * 修改settingAgreement
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        SysSettingAgreement sysSettingAgreement = sysSettingAgreementService.getById(id);
        mmap.put("sysSettingAgreement", sysSettingAgreement);
        return prefix + "/edit";
    }

    /**
     * 修改保存settingAgreement
     */
    @RequiresPermissions("mall:settingAgreement:edit")
    @Log(title = "settingAgreement", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysSettingAgreement sysSettingAgreement) {
        return toAjax(sysSettingAgreementService.updateById(sysSettingAgreement));
    }

    /**
     * 删除settingAgreement
     */
    @RequiresPermissions("mall:settingAgreement:remove")
    @Log(title = "settingAgreement", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        String[] str = ids.split(",");
        for (String s : str) {
            sysSettingAgreementService.removeById(Long.parseLong(s));
        }
        return toAjax(true);
    }
}
