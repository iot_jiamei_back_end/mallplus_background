package com.ruoyi.project.mall.settingAgreement.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import com.ruoyi.project.finance.mapper.FinanceCheckAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.settingAgreement.mapper.SysSettingAgreementMapper;
import com.ruoyi.project.mall.settingAgreement.domain.SysSettingAgreement;
import com.ruoyi.project.mall.settingAgreement.service.ISysSettingAgreementService;
import com.ruoyi.common.utils.text.Convert;

/**
 * settingAgreementService业务层处理
 * 
 * @author mallplus
 * @date 2020-03-17
 */
@Service
@Primary
public class SysSettingAgreementServiceImpl extends ServiceImpl<SysSettingAgreementMapper, SysSettingAgreement> implements ISysSettingAgreementService
{
    @Autowired
    private SysSettingAgreementMapper sysSettingAgreementMapper;

    /**
     * 查询settingAgreement
     * 
     * @param id settingAgreementID
     * @return settingAgreement
     */
    @Override
    public SysSettingAgreement selectSysSettingAgreementById(Long id)
    {
        return sysSettingAgreementMapper.selectSysSettingAgreementById(id);
    }

    /**
     * 查询settingAgreement列表
     * 
     * @param sysSettingAgreement settingAgreement
     * @return settingAgreement
     */
    @Override
    public List<SysSettingAgreement> selectSysSettingAgreementList(SysSettingAgreement sysSettingAgreement)
    {
        return sysSettingAgreementMapper.selectSysSettingAgreementList(sysSettingAgreement);
    }

    /**
     * 新增settingAgreement
     * 
     * @param sysSettingAgreement settingAgreement
     * @return 结果
     */
    @Override
    public int insertSysSettingAgreement(SysSettingAgreement sysSettingAgreement)
    {
        sysSettingAgreement.setCreateTime(DateUtils.getNowDate());
        return sysSettingAgreementMapper.insertSysSettingAgreement(sysSettingAgreement);
    }

    /**
     * 修改settingAgreement
     * 
     * @param sysSettingAgreement settingAgreement
     * @return 结果
     */
    @Override
    public int updateSysSettingAgreement(SysSettingAgreement sysSettingAgreement)
    {
        //sysSettingAgreement.setUpdateTime(DateUtils.getNowDate());
        return sysSettingAgreementMapper.updateSysSettingAgreement(sysSettingAgreement);
    }

    /**
     * 删除settingAgreement对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysSettingAgreementByIds(String ids)
    {
        return sysSettingAgreementMapper.deleteSysSettingAgreementByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除settingAgreement信息
     * 
     * @param id settingAgreementID
     * @return 结果
     */
    @Override
    public int deleteSysSettingAgreementById(Long id)
    {
        return sysSettingAgreementMapper.deleteSysSettingAgreementById(id);
    }
}
