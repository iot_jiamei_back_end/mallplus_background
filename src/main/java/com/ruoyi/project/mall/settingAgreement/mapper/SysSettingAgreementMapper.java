package com.ruoyi.project.mall.settingAgreement.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.settingAgreement.domain.SysSettingAgreement;
import java.util.List;

/**
 * settingAgreementMapper接口
 * 
 * @author mallplus
 * @date 2020-03-17
 */
public interface SysSettingAgreementMapper  extends BaseMapper<SysSettingAgreement>
{
    /**
     * 查询settingAgreement
     * 
     * @param id settingAgreementID
     * @return settingAgreement
     */
    public SysSettingAgreement selectSysSettingAgreementById(Long id);

    /**
     * 查询settingAgreement列表
     * 
     * @param sysSettingAgreement settingAgreement
     * @return settingAgreement集合
     */
    public List<SysSettingAgreement> selectSysSettingAgreementList(SysSettingAgreement sysSettingAgreement);

    /**
     * 新增settingAgreement
     * 
     * @param sysSettingAgreement settingAgreement
     * @return 结果
     */
    public int insertSysSettingAgreement(SysSettingAgreement sysSettingAgreement);

    /**
     * 修改settingAgreement
     * 
     * @param sysSettingAgreement settingAgreement
     * @return 结果
     */
    public int updateSysSettingAgreement(SysSettingAgreement sysSettingAgreement);

    /**
     * 删除settingAgreement
     * 
     * @param id settingAgreementID
     * @return 结果
     */
    public int deleteSysSettingAgreementById(Long id);

    /**
     * 批量删除settingAgreement
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysSettingAgreementByIds(String[] ids);
}
