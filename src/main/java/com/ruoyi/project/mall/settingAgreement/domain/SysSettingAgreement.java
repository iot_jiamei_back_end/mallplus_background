package com.ruoyi.project.mall.settingAgreement.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Date;

/**
 * settingAgreement对象 sys_setting_agreement
 *
 * @author mallplus
 * @date 2020-03-17
 */
@TableName("sys_setting_agreement")
@Data
public class SysSettingAgreement {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 协议名称
     */
    @Excel(name = "协议名称")
    @TableField("name")
    private String name;

    /**
     * 协议内容
     */
    @Excel(name = "协议内容")
    @TableField("content")
    private String content;

    /**
     * 状态
     */
    @Excel(name = "状态")
    @TableField("status")
    private Long status;

    /**
     * 类型
     */
    @Excel(name = "类型")
    @TableField("type")
    private String type;

    /**
     * 添加时间
     */
    @Excel(name = "添加时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    @TableField("remark")
    private String remark;
}
