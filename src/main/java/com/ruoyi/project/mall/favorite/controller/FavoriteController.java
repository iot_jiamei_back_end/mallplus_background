package com.ruoyi.project.mall.favorite.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.favorite.domain.Favorite;
import com.ruoyi.project.mall.favorite.service.IFavoriteService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/mall/favorite")
public class FavoriteController extends BaseController
{
    private String prefix = "mall/favorite";

    @Autowired
    private IFavoriteService favoriteService;

    @RequiresPermissions("mall:favorite:view")
    @GetMapping()
    public String favorite()
    {
        return prefix + "/favorite";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("mall:favorite:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Favorite favorite)
    {
        startPage();
        List<Favorite> list = favoriteService.selectFavoriteList(favorite);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("mall:favorite:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Favorite favorite)
    {
        List<Favorite> list = favoriteService.selectFavoriteList(favorite);
        ExcelUtil<Favorite> util = new ExcelUtil<Favorite>(Favorite.class);
        return util.exportExcel(list, "favorite");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("mall:favorite:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Favorite favorite)
    {
        return toAjax(favoriteService.insertFavorite(favorite));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Favorite favorite = favoriteService.selectFavoriteById(id);
        mmap.put("favorite", favorite);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("mall:favorite:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Favorite favorite)
    {
        return toAjax(favoriteService.updateFavorite(favorite));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("mall:favorite:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(favoriteService.deleteFavoriteByIds(ids));
    }
}
