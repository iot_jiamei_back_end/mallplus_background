package com.ruoyi.project.mall.favorite.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 【请填写功能名称】对象 pms_favorite
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class Favorite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date addTime;

    /** 1 商品 2 文章 3 店铺 */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private Long type;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private Long objId;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private Long storeId;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private Long memberId;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private String name;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private String meno1;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private String meno2;

    /** $column.columnComment */
    @Excel(name = "1 商品 2 文章 3 店铺")
    private String meno3;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setAddTime(Date addTime) 
    {
        this.addTime = addTime;
    }

    public Date getAddTime() 
    {
        return addTime;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }
    public void setObjId(Long objId) 
    {
        this.objId = objId;
    }

    public Long getObjId() 
    {
        return objId;
    }
    public void setStoreId(Long storeId) 
    {
        this.storeId = storeId;
    }

    public Long getStoreId() 
    {
        return storeId;
    }
    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setMeno1(String meno1) 
    {
        this.meno1 = meno1;
    }

    public String getMeno1() 
    {
        return meno1;
    }
    public void setMeno2(String meno2) 
    {
        this.meno2 = meno2;
    }

    public String getMeno2() 
    {
        return meno2;
    }
    public void setMeno3(String meno3) 
    {
        this.meno3 = meno3;
    }

    public String getMeno3() 
    {
        return meno3;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("addTime", getAddTime())
            .append("type", getType())
            .append("objId", getObjId())
            .append("storeId", getStoreId())
            .append("memberId", getMemberId())
            .append("name", getName())
            .append("meno1", getMeno1())
            .append("meno2", getMeno2())
            .append("meno3", getMeno3())
            .toString();
    }
}
