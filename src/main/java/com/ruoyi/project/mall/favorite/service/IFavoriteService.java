package com.ruoyi.project.mall.favorite.service;

import com.ruoyi.project.mall.favorite.domain.Favorite;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IFavoriteService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public Favorite selectFavoriteById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param favorite 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Favorite> selectFavoriteList(Favorite favorite);

    /**
     * 新增【请填写功能名称】
     * 
     * @param favorite 【请填写功能名称】
     * @return 结果
     */
    public int insertFavorite(Favorite favorite);

    /**
     * 修改【请填写功能名称】
     * 
     * @param favorite 【请填写功能名称】
     * @return 结果
     */
    public int updateFavorite(Favorite favorite);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFavoriteByIds(String ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteFavoriteById(Long id);
}
