package com.ruoyi.project.mall.favorite.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.favorite.mapper.FavoriteMapper;
import com.ruoyi.project.mall.favorite.domain.Favorite;
import com.ruoyi.project.mall.favorite.service.IFavoriteService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class FavoriteServiceImpl implements IFavoriteService 
{
    @Autowired
    private FavoriteMapper favoriteMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public Favorite selectFavoriteById(Long id)
    {
        return favoriteMapper.selectFavoriteById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param favorite 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Favorite> selectFavoriteList(Favorite favorite)
    {
        return favoriteMapper.selectFavoriteList(favorite);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param favorite 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertFavorite(Favorite favorite)
    {
        return favoriteMapper.insertFavorite(favorite);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param favorite 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateFavorite(Favorite favorite)
    {
        return favoriteMapper.updateFavorite(favorite);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteFavoriteByIds(String ids)
    {
        return favoriteMapper.deleteFavoriteByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteFavoriteById(Long id)
    {
        return favoriteMapper.deleteFavoriteById(id);
    }
}
