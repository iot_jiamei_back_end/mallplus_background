package com.ruoyi.project.mall.service.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.bill.aftersales.domain.BillAftersales;
import com.ruoyi.project.bill.aftersales.mapper.BillAftersalesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import com.ruoyi.project.mall.service.mapper.PmsGoodsServiceMapper;
import com.ruoyi.project.mall.service.domain.PmsGoodsService;
import com.ruoyi.project.mall.service.service.IPmsGoodsServiceService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-18
 */
@Service
@Primary
public class PmsGoodsServiceServiceImpl extends ServiceImpl<PmsGoodsServiceMapper, PmsGoodsService> implements IPmsGoodsServiceService
{
    @Autowired
    private PmsGoodsServiceMapper pmsGoodsServiceMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public PmsGoodsService selectPmsGoodsServiceById(Long id)
    {
        return pmsGoodsServiceMapper.selectPmsGoodsServiceById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param pmsGoodsService 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PmsGoodsService> selectPmsGoodsServiceList(PmsGoodsService pmsGoodsService)
    {
        return pmsGoodsServiceMapper.selectPmsGoodsServiceList(pmsGoodsService);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param pmsGoodsService 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPmsGoodsService(PmsGoodsService pmsGoodsService)
    {
        //pmsGoodsService.setCreateTime(DateUtils.getNowDate());
        return pmsGoodsServiceMapper.insertPmsGoodsService(pmsGoodsService);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param pmsGoodsService 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePmsGoodsService(PmsGoodsService pmsGoodsService)
    {
        //pmsGoodsService.setUpdateTime(DateUtils.getNowDate());
        return pmsGoodsServiceMapper.updatePmsGoodsService(pmsGoodsService);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePmsGoodsServiceByIds(String ids)
    {
        return pmsGoodsServiceMapper.deletePmsGoodsServiceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deletePmsGoodsServiceById(Long id)
    {
        return pmsGoodsServiceMapper.deletePmsGoodsServiceById(id);
    }
}
