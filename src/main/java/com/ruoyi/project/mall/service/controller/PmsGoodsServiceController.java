package com.ruoyi.project.mall.service.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.mall.service.domain.PmsGoodsService;
import com.ruoyi.project.mall.service.service.IPmsGoodsServiceService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

import javax.annotation.Resource;

/**
 * 【请填写功能名称】Controller
 *
 * @author mallplus
 * @date 2020-03-18
 */
@Controller
@RequestMapping("/mall/service")
public class PmsGoodsServiceController extends BaseController {
    private String prefix = "mall/service";

    @Resource
    private IPmsGoodsServiceService pmsGoodsServiceService;

    @RequiresPermissions("mall:service:view")
    @GetMapping()
    public String service() {
        return prefix + "/service";
    }

    @RequiresPermissions("mall:service:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(PmsGoodsService entity, Integer pageNum, Integer pageSize) {
        IPage<PmsGoodsService> page = pmsGoodsServiceService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(entity));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

//    /**
//     * 导出【请填写功能名称】列表
//     */
//    @RequiresPermissions("mall:service:export")
//    @PostMapping("/export")
//    @ResponseBody
//    public AjaxResult export(PmsGoodsService pmsGoodsService) {
//        List<PmsGoodsService> list = pmsGoodsServiceService.selectPmsGoodsServiceList(pmsGoodsService);
//        ExcelUtil<PmsGoodsService> util = new ExcelUtil<PmsGoodsService>(PmsGoodsService.class);
//        return util.exportExcel(list, "service");
//    }

    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    @RequiresPermissions("mall:service:add")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(PmsGoodsService pmsGoodsService) {
        return toAjax(pmsGoodsServiceService.save(pmsGoodsService));
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        PmsGoodsService pmsGoodsService = pmsGoodsServiceService.getById(id);
        mmap.put("pmsGoodsService", pmsGoodsService);
        return prefix + "/edit";
    }

    @RequiresPermissions("mall:service:edit")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PmsGoodsService pmsGoodsService) {
        return toAjax(pmsGoodsServiceService.updateById(pmsGoodsService));
    }

    @RequiresPermissions("mall:service:remove")
//    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        String[] strArra = ids.split(",");
        for (String str : strArra) {
            pmsGoodsServiceService.removeById(str);
        }
        return toAjax(true);
    }
}
