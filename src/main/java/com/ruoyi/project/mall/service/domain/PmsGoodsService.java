package com.ruoyi.project.mall.service.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@TableName("pms_goods_service")
public class PmsGoodsService implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 对应商品表中的server_ids
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 服务标题
     */
    @TableField("service_title")
    private String serviceTitle;

    /**
     * 服务详情
     */
    @TableField("service_details")
    private String serviceDetails;

    /**
     * 状态0表示有效、1表示无效
     */
    private Integer state;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Long updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(String serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "PmsGoodsService{" +
        ", id=" + id +
        ", serviceTitle=" + serviceTitle +
        ", serviceDetails=" + serviceDetails +
        ", state=" + state +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
