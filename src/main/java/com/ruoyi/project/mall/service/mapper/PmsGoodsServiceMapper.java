package com.ruoyi.project.mall.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.mall.service.domain.PmsGoodsService;
import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-18
 */
public interface PmsGoodsServiceMapper extends BaseMapper<PmsGoodsService>
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public PmsGoodsService selectPmsGoodsServiceById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param pmsGoodsService 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<PmsGoodsService> selectPmsGoodsServiceList(PmsGoodsService pmsGoodsService);

    /**
     * 新增【请填写功能名称】
     * 
     * @param pmsGoodsService 【请填写功能名称】
     * @return 结果
     */
    public int insertPmsGoodsService(PmsGoodsService pmsGoodsService);

    /**
     * 修改【请填写功能名称】
     * 
     * @param pmsGoodsService 【请填写功能名称】
     * @return 结果
     */
    public int updatePmsGoodsService(PmsGoodsService pmsGoodsService);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deletePmsGoodsServiceById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePmsGoodsServiceByIds(String[] ids);
}
