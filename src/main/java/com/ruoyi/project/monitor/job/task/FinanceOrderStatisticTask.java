package com.ruoyi.project.monitor.job.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.bill.aftersales.service.IBillAftersalesService;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import com.ruoyi.project.finance.service.IFinanceOrderStatisticService;
import com.ruoyi.project.mall.order.service.IOmsOrderService;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.service.IStoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 商户订单统计
 */
@Slf4j
@Component("financeOrderStatisticTask")
public class FinanceOrderStatisticTask {

    @Resource
    private IOmsOrderService orderService;
    @Resource
    private IBillAftersalesService billAftersalesService;
    @Resource
    private IFinanceOrderStatisticService financeOrderStatisticService;
    @Resource
    private IStoreService storeService;

    /**
     * 商户订单数据今日统计
     * 支付订单数、订单金额、成交用户数、退款订单数、退款金额、支付金额
     */
    public void currentDayStoreDayStatics() throws InterruptedException {
        // 获取所有的商户
        List<Store> storeList = storeService.selectStoreList(null);
        log.info("商户订单数据日统计：{}，共{}个商户需要需要同步", DateUtils.getNowDate(), storeList.size());
        for (Store store : storeList) {
            //商家id
            Integer storeId = store.getId();
            String dateStr = DateUtils.dateTimeNow("yyyy-MM-dd"); // 时间
            //支付订单数
            Integer payOrderNum = orderService.getOrderPayOrderNum(store, new Date());
            //订单金额
            BigDecimal orderAmount = orderService.getOrderOrderAmount(store, new Date());
            //支付金额
            BigDecimal payAmount = orderService.getOrderPayAmount(store, new Date());
            //成交用户数
            Integer transactionUserNum = orderService.getOrderTransactionUserNum(store, new Date());
            // 售后
            //退款订单数
            Integer refundOrderNum = billAftersalesService.getRefundOrderNum(store, new Date());
            //退款金额
            BigDecimal refundAmount = billAftersalesService.getRefundAmount(store, new Date());
            if (refundAmount==null){
                refundAmount=BigDecimal.ZERO;
            }
            if (orderAmount==null){
                orderAmount=BigDecimal.ZERO;
            }
            if (payAmount==null){
                payAmount=BigDecimal.ZERO;
            }
            // 查询是否存在今日记录
            log.info("商户订单数据dateStr："+dateStr);
            FinanceOrderStatistic orderStatistic = new FinanceOrderStatistic();
            orderStatistic.setDateStr(dateStr);
            orderStatistic.setStoreId(storeId);
            FinanceOrderStatistic financeOrderStatistic = financeOrderStatisticService.getOne(new QueryWrapper<>(orderStatistic));
            log.info("商户订单数据financeOrderStatistic："+financeOrderStatistic);
            if (financeOrderStatistic == null) {
                financeOrderStatistic=new FinanceOrderStatistic();
                financeOrderStatistic.setDateStr(dateStr);
                financeOrderStatistic.setCreateTime(new Date());
                financeOrderStatistic.setUpdateTime(new Date());
                financeOrderStatistic.setOrderAmount(orderAmount);
                financeOrderStatistic.setPayAmount(payAmount);
                financeOrderStatistic.setTransactionUserNum(transactionUserNum);
                financeOrderStatistic.setPayOrderNum(payOrderNum);
                financeOrderStatistic.setRefundOrderNum(refundOrderNum);
                financeOrderStatistic.setRefundAmount(refundAmount);
                financeOrderStatistic.setStoreId(storeId);
                // 插入
                financeOrderStatisticService.save(financeOrderStatistic);
                log.info("插入商户订单数据日统计");
            } else {
                financeOrderStatistic.setUpdateTime(new Date());
                financeOrderStatistic.setOrderAmount(orderAmount);
                financeOrderStatistic.setPayAmount(payAmount);
                financeOrderStatistic.setTransactionUserNum(transactionUserNum);
                financeOrderStatistic.setPayOrderNum(payOrderNum);
                financeOrderStatistic.setRefundOrderNum(refundOrderNum);
                financeOrderStatistic.setRefundAmount(refundAmount);
                financeOrderStatistic.setStoreId(storeId);
                // 更新
                financeOrderStatisticService.updateById(financeOrderStatistic);
                log.info("更新商户订单数据日统计");
            }
            log.info("商户订单数据日统计end====：{}，商户ID={},商户名称={}", DateUtils.getNowDate(), store.getId(), store.getName());
        }
        log.info("商户订单数据日统计end====：{}", DateUtils.getNowDate());
    }

    /**
     * 商户订单数据昨日统计
     * 支付订单数、订单金额、成交用户数、退款订单数、退款金额、支付金额
     */
    public void yesterdayStoreDayStatics() throws InterruptedException {
        // 获取所有的商户
        List<Store> storeList = storeService.selectStoreList(null);
        log.info("商户订单数据日统计：{}，共{}个商户需要需要同步", DateUtils.getNowDate(), storeList.size());
        for (Store store : storeList) {
            //商家id
            Integer storeId = store.getId();
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            Date yesterdayDate = cal.getTime();
            SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
            //获取昨天日期
            String dateStr = sp.format(yesterdayDate);
            // 订单
            //支付订单数
            Integer payOrderNum = orderService.getOrderPayOrderNum(store, yesterdayDate);
            //订单金额
            BigDecimal orderAmount = orderService.getOrderOrderAmount(store, yesterdayDate);
            //支付金额
            BigDecimal payAmount = orderService.getOrderPayAmount(store, yesterdayDate);
            //成交用户数
            Integer transactionUserNum = orderService.getOrderTransactionUserNum(store, yesterdayDate);
            // 售后
            //退款订单数
            Integer refundOrderNum = billAftersalesService.getRefundOrderNum(store, yesterdayDate);
            //退款金额
            BigDecimal refundAmount = billAftersalesService.getRefundAmount(store, yesterdayDate);
            if (refundAmount==null){
                refundAmount=BigDecimal.ZERO;
            }
            if (orderAmount==null){
                orderAmount=BigDecimal.ZERO;
            }
            if (payAmount==null){
                payAmount=BigDecimal.ZERO;
            }
            // 查询是否存在昨日记录
            FinanceOrderStatistic orderStatistic = new FinanceOrderStatistic();
            orderStatistic.setDateStr(dateStr);
            orderStatistic.setStoreId(storeId);
            FinanceOrderStatistic financeOrderStatistic = financeOrderStatisticService.getOne(new QueryWrapper<>(orderStatistic));
            if (financeOrderStatistic == null) {
                financeOrderStatistic=new FinanceOrderStatistic();
                financeOrderStatistic.setDateStr(dateStr);
                financeOrderStatistic.setCreateTime(new Date());
                financeOrderStatistic.setUpdateTime(new Date());

                financeOrderStatistic.setOrderAmount(orderAmount);
                financeOrderStatistic.setPayAmount(payAmount);
                financeOrderStatistic.setRefundAmount(refundAmount);
                financeOrderStatistic.setTransactionUserNum(transactionUserNum);
                financeOrderStatistic.setPayOrderNum(payOrderNum);
                financeOrderStatistic.setRefundOrderNum(refundOrderNum);

                financeOrderStatistic.setStoreId(storeId);
                // 插入
                financeOrderStatisticService.save(financeOrderStatistic);
            } else {
                financeOrderStatistic.setUpdateTime(new Date());
                financeOrderStatistic.setOrderAmount(orderAmount);
                financeOrderStatistic.setPayAmount(payAmount);
                financeOrderStatistic.setTransactionUserNum(transactionUserNum);
                financeOrderStatistic.setPayOrderNum(payOrderNum);
                financeOrderStatistic.setRefundOrderNum(refundOrderNum);
                financeOrderStatistic.setRefundAmount(refundAmount);
                financeOrderStatistic.setStoreId(storeId);
                // 更新
                financeOrderStatisticService.updateById(financeOrderStatistic);
            }
            log.info("商户订单数据日统计end====：{}，商户ID={},商户名称={}", DateUtils.getNowDate(), store.getId(), store.getName());
        }
        log.info("商户订单数据日统计end====：{}", DateUtils.getNowDate());
    }

}
