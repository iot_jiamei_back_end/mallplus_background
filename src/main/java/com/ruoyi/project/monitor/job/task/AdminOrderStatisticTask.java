package com.ruoyi.project.monitor.job.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.bill.aftersales.service.IBillAftersalesService;
import com.ruoyi.project.finance.entity.AdminOrderStatistic;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import com.ruoyi.project.finance.service.IAdminOrderStatisticService;
import com.ruoyi.project.finance.service.IFinanceOrderStatisticService;
import com.ruoyi.project.mall.order.service.IOmsOrderService;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.service.IStoreService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 商户订单统计
 */
@Slf4j
@Component("adminOrderStatisticTask")
public class AdminOrderStatisticTask {

    @Resource
    private IOmsOrderService orderService;
    @Resource
    private IBillAftersalesService billAftersalesService;
    @Resource
    private IFinanceOrderStatisticService financeOrderStatisticService;
    @Resource
    private IStoreService storeService;

    @Resource
    private IAdminOrderStatisticService adminOrderStatisticService;

    /**
     * 订单数据今日统计
     * 支付订单数、订单金额、成交用户数、退款订单数、退款金额、支付金额
     */
    public void currentDayStoreDayStatics() throws InterruptedException {
        String dateStr = DateUtils.dateTimeNow("yyyy-MM-dd"); // 时间
        //支付订单数
        Integer payOrderNum = orderService.getTotalOrderPayOrderNum(new Date());
        //订单金额
        BigDecimal orderAmount = orderService.getTotalOrderOrderAmount(new Date());
        //支付金额
        BigDecimal payAmount = orderService.getTotalOrderPayAmount(new Date());
        //成交用户数
        Integer transactionUserNum = orderService.getTotalOrderTransactionUserNum(new Date());
        // 售后
        //退款订单数
        Integer refundOrderNum = billAftersalesService.getTotalRefundOrderNum(new Date());
        //退款金额
        BigDecimal refundAmount = billAftersalesService.getTotalRefundAmount(new Date());
        if (refundAmount==null){
            refundAmount=BigDecimal.ZERO;
        }
        if (orderAmount==null){
            orderAmount=BigDecimal.ZERO;
        }
        if (payAmount==null){
            payAmount=BigDecimal.ZERO;
        }
        // 查询是否存在今日记录
        AdminOrderStatistic orderStatistic = new AdminOrderStatistic();
        orderStatistic.setDateStr(dateStr);
        AdminOrderStatistic financeOrderStatistic = adminOrderStatisticService.getOne(new QueryWrapper<>(orderStatistic));
        if (financeOrderStatistic == null) {

            financeOrderStatistic=new AdminOrderStatistic();
            financeOrderStatistic.setDateStr(dateStr);
            financeOrderStatistic.setCreateTime(new Date());
            financeOrderStatistic.setUpdateTime(new Date());
            financeOrderStatistic.setOrderAmount(orderAmount);
            financeOrderStatistic.setPayAmount(payAmount);
            financeOrderStatistic.setTransactionUserNum(transactionUserNum);
            financeOrderStatistic.setPayOrderNum(payOrderNum);
            financeOrderStatistic.setRefundOrderNum(refundOrderNum);
            financeOrderStatistic.setRefundAmount(refundAmount);
            // 插入
            adminOrderStatisticService.save(financeOrderStatistic);
        } else {
            financeOrderStatistic.setUpdateTime(new Date());
            financeOrderStatistic.setOrderAmount(orderAmount);
            financeOrderStatistic.setPayAmount(payAmount);
            financeOrderStatistic.setTransactionUserNum(transactionUserNum);
            financeOrderStatistic.setPayOrderNum(payOrderNum);
            financeOrderStatistic.setRefundOrderNum(refundOrderNum);
            financeOrderStatistic.setRefundAmount(refundAmount);
            // 更新
            adminOrderStatisticService.updateById(financeOrderStatistic);
        }
    }

    /**
     * 商户订单数据昨日统计
     * 支付订单数、订单金额、成交用户数、退款订单数、退款金额、支付金额
     */
    public void yesterdayStoreDayStatics() throws InterruptedException {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date yesterdayDate = cal.getTime();
        SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
        //获取昨天日期
        String dateStr = sp.format(yesterdayDate);
        //支付订单数
        Integer payOrderNum = orderService.getTotalOrderPayOrderNum(new Date());
        //订单金额
        BigDecimal orderAmount = orderService.getTotalOrderOrderAmount(new Date());
        //支付金额
        BigDecimal payAmount = orderService.getTotalOrderPayAmount(new Date());
        //成交用户数
        Integer transactionUserNum = orderService.getTotalOrderTransactionUserNum(new Date());
        // 售后
        //退款订单数
        Integer refundOrderNum = billAftersalesService.getTotalRefundOrderNum(new Date());
        //退款金额
        BigDecimal refundAmount = billAftersalesService.getTotalRefundAmount(new Date());
        if (refundAmount==null){
            refundAmount=BigDecimal.ZERO;
        }
        if (orderAmount==null){
            orderAmount=BigDecimal.ZERO;
        }
        if (payAmount==null){
            payAmount=BigDecimal.ZERO;
        }
        // 查询是否存在今日记录
        AdminOrderStatistic orderStatistic = new AdminOrderStatistic();
        orderStatistic.setDateStr(dateStr);
        AdminOrderStatistic financeOrderStatistic = adminOrderStatisticService.getOne(new QueryWrapper<>(orderStatistic));
        if (financeOrderStatistic == null) {
            financeOrderStatistic=new AdminOrderStatistic();
            financeOrderStatistic.setDateStr(dateStr);
            financeOrderStatistic.setCreateTime(new Date());
            financeOrderStatistic.setUpdateTime(new Date());
            financeOrderStatistic.setOrderAmount(orderAmount);
            financeOrderStatistic.setPayAmount(payAmount);
            financeOrderStatistic.setTransactionUserNum(transactionUserNum);
            financeOrderStatistic.setPayOrderNum(payOrderNum);
            financeOrderStatistic.setRefundOrderNum(refundOrderNum);
            financeOrderStatistic.setRefundAmount(refundAmount);
            // 插入
            adminOrderStatisticService.save(financeOrderStatistic);
        } else {
            financeOrderStatistic.setUpdateTime(new Date());
            financeOrderStatistic.setOrderAmount(orderAmount);
            financeOrderStatistic.setPayAmount(payAmount);
            financeOrderStatistic.setTransactionUserNum(transactionUserNum);
            financeOrderStatistic.setPayOrderNum(payOrderNum);
            financeOrderStatistic.setRefundOrderNum(refundOrderNum);
            financeOrderStatistic.setRefundAmount(refundAmount);
            // 更新
            adminOrderStatisticService.updateById(financeOrderStatistic);
        }
    }

}
