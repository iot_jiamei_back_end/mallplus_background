package com.ruoyi.project.systemConfig.service;

import com.ruoyi.project.systemConfig.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 参数配置表 服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
public interface ISysConfigService extends IService<SysConfig> {

}
