package com.ruoyi.project.systemConfig.service.impl;

import com.ruoyi.project.systemConfig.entity.SysConfig;
import com.ruoyi.project.systemConfig.mapper.SysConfigMapper;
import com.ruoyi.project.systemConfig.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@Service
@Primary
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

}
