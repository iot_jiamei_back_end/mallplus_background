package com.ruoyi.project.systemConfig.controller;


import com.ruoyi.project.finance.entity.FinanceCheck;
import com.ruoyi.project.finance.entity.FinanceSettlement;
import com.ruoyi.project.finance.service.IFinanceCheckAccountService;
import com.ruoyi.project.finance.service.IFinanceCheckService;
import com.ruoyi.project.finance.service.IFinanceSettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
@Controller
@RequestMapping("/systemConfig/sysConfig")
public class SysConfigController {

    private String prefix = "systemConfig";

    @Autowired
    private IFinanceSettlementService financeSettlementService;

    @Autowired
    private IFinanceCheckService financeCheckService;

    @Autowired
    private IFinanceCheckAccountService financeCheckAccountService;

    /**
     * 购物设置
     *
     * @return
     */
    @GetMapping("checkAccountDetail")
    public String address(Long settlementId, ModelMap mmap) {
        FinanceSettlement financeSettlement = financeSettlementService.getById(settlementId);
        FinanceCheck financeCheck = financeCheckService.getById(financeSettlement.getCheckId());
        mmap.put("financeSettlement", financeSettlement);
        mmap.put("financeCheck", financeCheck);
        return prefix + "/checkAccountDetail";
    }

}

