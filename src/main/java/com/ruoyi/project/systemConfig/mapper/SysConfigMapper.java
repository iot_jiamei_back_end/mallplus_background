package com.ruoyi.project.systemConfig.mapper;

import com.ruoyi.project.systemConfig.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-18
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
