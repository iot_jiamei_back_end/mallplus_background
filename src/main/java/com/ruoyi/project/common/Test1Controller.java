package com.ruoyi.project.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 通用请求处理
 *
 * @author ruoyi
 */
@Controller
public class Test1Controller {

//    private static final Logger log = LoggerFactory.getLogger(Test1Controller.class);

    @GetMapping("common/storeDetail")
    public String storeDetail() {
        return "test/storeDetail";
    }

    @GetMapping("common/storeProduct")
    public String storeProduct() {
        return "test/storeProduct";
    }

    @GetMapping("common/storeSetting")
    public String storeSetting() {
        return "test/storeSetting";
    }

}
