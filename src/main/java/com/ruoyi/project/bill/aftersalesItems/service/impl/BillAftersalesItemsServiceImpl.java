package com.ruoyi.project.bill.aftersalesItems.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.aftersalesItems.mapper.BillAftersalesItemsMapper;
import com.ruoyi.project.bill.aftersalesItems.domain.BillAftersalesItems;
import com.ruoyi.project.bill.aftersalesItems.service.IBillAftersalesItemsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 售后单明细Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillAftersalesItemsServiceImpl extends ServiceImpl<BillAftersalesItemsMapper, BillAftersalesItems> implements IBillAftersalesItemsService
{
    @Autowired
    private BillAftersalesItemsMapper billAftersalesItemsMapper;

    /**
     * 查询售后单明细
     * 
     * @param id 售后单明细ID
     * @return 售后单明细
     */
    @Override
    public BillAftersalesItems selectBillAftersalesItemsById(Integer id)
    {
        return billAftersalesItemsMapper.selectBillAftersalesItemsById(id);
    }

    /**
     * 查询售后单明细列表
     * 
     * @param billAftersalesItems 售后单明细
     * @return 售后单明细
     */
    @Override
    public List<BillAftersalesItems> selectBillAftersalesItemsList(BillAftersalesItems billAftersalesItems)
    {
        return billAftersalesItemsMapper.selectBillAftersalesItemsList(billAftersalesItems);
    }

    /**
     * 新增售后单明细
     * 
     * @param billAftersalesItems 售后单明细
     * @return 结果
     */
    @Override
    public int insertBillAftersalesItems(BillAftersalesItems billAftersalesItems)
    {
        return billAftersalesItemsMapper.insertBillAftersalesItems(billAftersalesItems);
    }

    /**
     * 修改售后单明细
     * 
     * @param billAftersalesItems 售后单明细
     * @return 结果
     */
    @Override
    public int updateBillAftersalesItems(BillAftersalesItems billAftersalesItems)
    {
        return billAftersalesItemsMapper.updateBillAftersalesItems(billAftersalesItems);
    }

    /**
     * 删除售后单明细对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesItemsByIds(String ids)
    {
        return billAftersalesItemsMapper.deleteBillAftersalesItemsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除售后单明细信息
     * 
     * @param id 售后单明细ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesItemsById(Integer id)
    {
        return billAftersalesItemsMapper.deleteBillAftersalesItemsById(id);
    }
}
