package com.ruoyi.project.bill.aftersalesItems.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.aftersalesItems.domain.BillAftersalesItems;
import java.util.List;

/**
 * 售后单明细Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillAftersalesItemsMapper extends BaseMapper<BillAftersalesItems>
{
    /**
     * 查询售后单明细
     * 
     * @param id 售后单明细ID
     * @return 售后单明细
     */
    public BillAftersalesItems selectBillAftersalesItemsById(Integer id);

    /**
     * 查询售后单明细列表
     * 
     * @param billAftersalesItems 售后单明细
     * @return 售后单明细集合
     */
    public List<BillAftersalesItems> selectBillAftersalesItemsList(BillAftersalesItems billAftersalesItems);

    /**
     * 新增售后单明细
     * 
     * @param billAftersalesItems 售后单明细
     * @return 结果
     */
    public int insertBillAftersalesItems(BillAftersalesItems billAftersalesItems);

    /**
     * 修改售后单明细
     * 
     * @param billAftersalesItems 售后单明细
     * @return 结果
     */
    public int updateBillAftersalesItems(BillAftersalesItems billAftersalesItems);

    /**
     * 删除售后单明细
     * 
     * @param id 售后单明细ID
     * @return 结果
     */
    public int deleteBillAftersalesItemsById(Integer id);

    /**
     * 批量删除售后单明细
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillAftersalesItemsByIds(String[] ids);
}
