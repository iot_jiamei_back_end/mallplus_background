package com.ruoyi.project.bill.aftersalesItems.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.aftersalesItems.domain.BillAftersalesItems;
import com.ruoyi.project.bill.aftersalesItems.service.IBillAftersalesItemsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 售后单明细Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/aftersalesItems")
public class BillAftersalesItemsController extends BaseController
{
    private String prefix = "bill/aftersalesItems";

    @Autowired
    private IBillAftersalesItemsService billAftersalesItemsService;

    @RequiresPermissions("bill:aftersalesItems:view")
    @GetMapping()
    public String aftersalesItems()
    {
        return prefix + "/aftersalesItems";
    }

    /**
     * 查询售后单明细列表
     */
    @RequiresPermissions("bill:aftersalesItems:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillAftersalesItems billAftersalesItems)
    {
        startPage();
        List<BillAftersalesItems> list = billAftersalesItemsService.selectBillAftersalesItemsList(billAftersalesItems);
        return getDataTable(list);
    }

    /**
     * 导出售后单明细列表
     */
    @RequiresPermissions("bill:aftersalesItems:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillAftersalesItems billAftersalesItems)
    {
        List<BillAftersalesItems> list = billAftersalesItemsService.selectBillAftersalesItemsList(billAftersalesItems);
        ExcelUtil<BillAftersalesItems> util = new ExcelUtil<BillAftersalesItems>(BillAftersalesItems.class);
        return util.exportExcel(list, "aftersalesItems");
    }

    /**
     * 新增售后单明细
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存售后单明细
     */
    @RequiresPermissions("bill:aftersalesItems:add")
    @Log(title = "售后单明细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillAftersalesItems billAftersalesItems)
    {
        return toAjax(billAftersalesItemsService.insertBillAftersalesItems(billAftersalesItems));
    }

    /**
     * 修改售后单明细
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap)
    {
        BillAftersalesItems billAftersalesItems = billAftersalesItemsService.selectBillAftersalesItemsById(id);
        mmap.put("billAftersalesItems", billAftersalesItems);
        return prefix + "/edit";
    }

    /**
     * 修改保存售后单明细
     */
    @RequiresPermissions("bill:aftersalesItems:edit")
    @Log(title = "售后单明细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillAftersalesItems billAftersalesItems)
    {
        return toAjax(billAftersalesItemsService.updateBillAftersalesItems(billAftersalesItems));
    }

    /**
     * 删除售后单明细
     */
    @RequiresPermissions("bill:aftersalesItems:remove")
    @Log(title = "售后单明细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billAftersalesItemsService.deleteBillAftersalesItemsByIds(ids));
    }
}
