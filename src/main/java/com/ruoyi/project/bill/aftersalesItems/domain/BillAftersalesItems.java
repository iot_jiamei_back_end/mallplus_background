package com.ruoyi.project.bill.aftersalesItems.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 售后单明细表
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@TableName("bill_aftersales_items")
public class BillAftersalesItems implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 鍞悗鍗昳d
     */
    @TableField("aftersales_id")
    private Long aftersalesId;

    /**
     * 璁㈠崟鏄庣粏ID 鍏宠仈order_items.id
     */
    @TableField("order_items_id")
    private Long orderItemsId;

    /**
     * 搴撳瓨ID
     */
    @TableField("sku_id")
    private Long skuId;

    /**
     * 鍟嗗搧id
     */
    @TableField("product_id")
    private Long productId;

    /**
     * 货品编码
     */
    private String sn;

    /**
     * 商品编码
     */
    private String bn;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 图片
     */
    @TableField("image_url")
    private String imageUrl;

    /**
     * 售后数量
     */
    private Integer nums;

    /**
     * 货品明细序列号存储
     */
    private String addon;

    /**
     * 申请时间
     */
    private LocalDateTime ctime;

    /**
     * 更新时间
     */
    private LocalDateTime utime;

    /**
     * 所属店铺
     */
    @TableField("store_id")
    private Integer storeId;

    /**
     * 售后：0正常 1退换货中2换货成功3退款成功4申请拒绝
     */
    private Integer status;

    /**
     * 商品销售单价
     */
    @TableField("product_price")
    private BigDecimal productPrice;

    /**
     * 商品实际支付单价
     */
    @TableField("product_real_price")
    private BigDecimal productRealPrice;

    /**
     * 商品属性
     */
    @TableField("product_attr")
    private String productAttr;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getAftersalesId() {
        return aftersalesId;
    }

    public void setAftersalesId(Long aftersalesId) {
        this.aftersalesId = aftersalesId;
    }

    public Long getOrderItemsId() {
        return orderItemsId;
    }

    public void setOrderItemsId(Long orderItemsId) {
        this.orderItemsId = orderItemsId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getBn() {
        return bn;
    }

    public void setBn(String bn) {
        this.bn = bn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getNums() {
        return nums;
    }

    public void setNums(Integer nums) {
        this.nums = nums;
    }

    public String getAddon() {
        return addon;
    }

    public void setAddon(String addon) {
        this.addon = addon;
    }

    public LocalDateTime getCtime() {
        return ctime;
    }

    public void setCtime(LocalDateTime ctime) {
        this.ctime = ctime;
    }

    public LocalDateTime getUtime() {
        return utime;
    }

    public void setUtime(LocalDateTime utime) {
        this.utime = utime;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    public BigDecimal getProductRealPrice() {
        return productRealPrice;
    }

    public void setProductRealPrice(BigDecimal productRealPrice) {
        this.productRealPrice = productRealPrice;
    }

    public String getProductAttr() {
        return productAttr;
    }

    public void setProductAttr(String productAttr) {
        this.productAttr = productAttr;
    }

    @Override
    public String toString() {
        return "BillAftersalesItems{" +
        ", id=" + id +
        ", aftersalesId=" + aftersalesId +
        ", orderItemsId=" + orderItemsId +
        ", skuId=" + skuId +
        ", productId=" + productId +
        ", sn=" + sn +
        ", bn=" + bn +
        ", name=" + name +
        ", imageUrl=" + imageUrl +
        ", nums=" + nums +
        ", addon=" + addon +
        ", ctime=" + ctime +
        ", utime=" + utime +
        ", storeId=" + storeId +
        ", status=" + status +
        ", productPrice=" + productPrice +
        ", productRealPrice=" + productRealPrice +
        ", productAttr=" + productAttr +
        "}";
    }
}
