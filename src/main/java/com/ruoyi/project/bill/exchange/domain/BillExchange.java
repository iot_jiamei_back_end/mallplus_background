package com.ruoyi.project.bill.exchange.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 发货单表
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@TableName("bill_exchange")
@Data
public class BillExchange implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "exchange_id", type = IdType.AUTO)
    private Long exchangeId;

    /**
     * 订单ID 关联order.id
     */
    @TableField("order_id")
    private String orderId;

    /**
     * 用户id 关联user.id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 卖家家确认收货时间
     */
    @TableField("seller_confirm_time")
    private LocalDateTime sellerConfirmTime;

    /**
     * 买家确认收货时间
     */
    @TableField("buy_confirm_time")
    private LocalDateTime buyConfirmTime;

    /**
     * 状态：1 待换货  2买家已发货  3卖家已收货  4 卖家处理换货中   5卖家已发货中  6买家已收货 7换货成功
     */
    private Integer status;

    /**
     * 买家备注
     */
    @TableField("buy_memo")
    private String buyMemo;

    /**
     * 卖家备注
     */
    @TableField("seller_memo")
    private String sellerMemo;

    /**
     * 申请时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 所属店铺
     */
    @TableField("store_id")
    private Integer storeId;

    /**
     * 卖家收货地址表id
     */
    @TableField("company_address_id")
    private Long companyAddressId;

    /**
     * 收货地区ID
     */
    @TableField("seller_ship_area_id")
    private Integer sellerShipAreaId;

    /**
     * 收货详细地址
     */
    @TableField("seller_ship_address")
    private String sellerShipAddress;

    /**
     * 收货人姓名
     */
    @TableField("seller_ship_name")
    private String sellerShipName;

    /**
     * 收货电话
     */
    @TableField("seller_ship_mobile")
    private String sellerShipMobile;

    /**
     * 收货地区ID
     */
    @TableField("buy_ship_area_id")
    private Integer buyShipAreaId;

    /**
     * 收货详细地址
     */
    @TableField("buy_ship_address")
    private String buyShipAddress;

    /**
     * 收货人姓名
     */
    @TableField("buy_ship_name")
    private String buyShipName;

    /**
     * 收货电话
     */
    @TableField("buy_ship_mobile")
    private String buyShipMobile;

    /**
     * 物流公司编码
     */
    @TableField("buy_logi_code")
    private String buyLogiCode;

    /**
     * 物流单号
     */
    @TableField("buy_logi_no")
    private String buyLogiNo;

    /**
     * 快递物流信息
     */
    @TableField("buy_logi_information")
    private String buyLogiInformation;

    /**
     * 0快递信息可能更新  1快递信息不更新了
     */
    @TableField("buy_logi_status")
    private Integer buyLogiStatus;

    /**
     * 物流公司编码
     */
    @TableField("seller_logi_code")
    private String sellerLogiCode;

    /**
     * 物流单号
     */
    @TableField("seller_logi_no")
    private String sellerLogiNo;

    /**
     * 快递物流信息
     */
    @TableField("seller_logi_information")
    private String sellerLogiInformation;

    /**
     * 0快递信息可能更新  1快递信息不更新了
     */
    @TableField("seller_logi_status")
    private Integer sellerLogiStatus;

    @TableField("aftersales_id")
    private Long aftersalesId;

    /**
     * 物流公司名字
     */
    @TableField("seller_logi_name")
    private String sellerLogiName;

    @TableField("buy_logi_name")
    private String buyLogiName;
}
