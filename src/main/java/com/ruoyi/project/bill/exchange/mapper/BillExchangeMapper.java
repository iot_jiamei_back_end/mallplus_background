package com.ruoyi.project.bill.exchange.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.exchange.domain.BillExchange;
import java.util.List;

/**
 * 发货单Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillExchangeMapper extends BaseMapper<BillExchange>
{
    /**
     * 查询发货单
     * 
     * @param exchangeId 发货单ID
     * @return 发货单
     */
    public BillExchange selectBillExchangeById(Long exchangeId);

    /**
     * 查询发货单列表
     * 
     * @param billExchange 发货单
     * @return 发货单集合
     */
    public List<BillExchange> selectBillExchangeList(BillExchange billExchange);

    /**
     * 新增发货单
     * 
     * @param billExchange 发货单
     * @return 结果
     */
    public int insertBillExchange(BillExchange billExchange);

    /**
     * 修改发货单
     * 
     * @param billExchange 发货单
     * @return 结果
     */
    public int updateBillExchange(BillExchange billExchange);

    /**
     * 删除发货单
     * 
     * @param exchangeId 发货单ID
     * @return 结果
     */
    public int deleteBillExchangeById(Long exchangeId);

    /**
     * 批量删除发货单
     * 
     * @param exchangeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillExchangeByIds(String[] exchangeIds);
}
