package com.ruoyi.project.bill.exchange.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.exchange.domain.BillExchange;
import com.ruoyi.project.bill.exchange.service.IBillExchangeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 发货单Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/exchange")
public class BillExchangeController extends BaseController
{
    private String prefix = "bill/exchange";

    @Autowired
    private IBillExchangeService billExchangeService;

    @RequiresPermissions("bill:exchange:view")
    @GetMapping()
    public String exchange()
    {
        return prefix + "/exchange";
    }

    /**
     * 查询发货单列表
     */
    @RequiresPermissions("bill:exchange:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillExchange billExchange)
    {
        startPage();
        List<BillExchange> list = billExchangeService.selectBillExchangeList(billExchange);
        return getDataTable(list);
    }

    /**
     * 导出发货单列表
     */
    @RequiresPermissions("bill:exchange:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillExchange billExchange)
    {
        List<BillExchange> list = billExchangeService.selectBillExchangeList(billExchange);
        ExcelUtil<BillExchange> util = new ExcelUtil<BillExchange>(BillExchange.class);
        return util.exportExcel(list, "exchange");
    }

    /**
     * 新增发货单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存发货单
     */
    @RequiresPermissions("bill:exchange:add")
    @Log(title = "发货单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillExchange billExchange)
    {
        return toAjax(billExchangeService.insertBillExchange(billExchange));
    }

    /**
     * 修改发货单
     */
    @GetMapping("/edit/{exchangeId}")
    public String edit(@PathVariable("exchangeId") Long exchangeId, ModelMap mmap)
    {
        BillExchange billExchange = billExchangeService.selectBillExchangeById(exchangeId);
        mmap.put("billExchange", billExchange);
        return prefix + "/edit";
    }

    /**
     * 修改保存发货单
     */
    @RequiresPermissions("bill:exchange:edit")
    @Log(title = "发货单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillExchange billExchange)
    {
        return toAjax(billExchangeService.updateBillExchange(billExchange));
    }

    /**
     * 删除发货单
     */
    @RequiresPermissions("bill:exchange:remove")
    @Log(title = "发货单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billExchangeService.deleteBillExchangeByIds(ids));
    }
}
