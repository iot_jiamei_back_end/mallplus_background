package com.ruoyi.project.bill.exchange.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.exchange.mapper.BillExchangeMapper;
import com.ruoyi.project.bill.exchange.domain.BillExchange;
import com.ruoyi.project.bill.exchange.service.IBillExchangeService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 发货单Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillExchangeServiceImpl extends ServiceImpl<BillExchangeMapper, BillExchange> implements IBillExchangeService
{
    @Autowired
    private BillExchangeMapper billExchangeMapper;

    /**
     * 查询发货单
     * 
     * @param exchangeId 发货单ID
     * @return 发货单
     */
    @Override
    public BillExchange selectBillExchangeById(Long exchangeId)
    {
        return billExchangeMapper.selectBillExchangeById(exchangeId);
    }

    /**
     * 查询发货单列表
     * 
     * @param billExchange 发货单
     * @return 发货单
     */
    @Override
    public List<BillExchange> selectBillExchangeList(BillExchange billExchange)
    {
        return billExchangeMapper.selectBillExchangeList(billExchange);
    }

    /**
     * 新增发货单
     * 
     * @param billExchange 发货单
     * @return 结果
     */
    @Override
    public int insertBillExchange(BillExchange billExchange)
    {
        return billExchangeMapper.insertBillExchange(billExchange);
    }

    /**
     * 修改发货单
     * 
     * @param billExchange 发货单
     * @return 结果
     */
    @Override
    public int updateBillExchange(BillExchange billExchange)
    {
        return billExchangeMapper.updateBillExchange(billExchange);
    }

    /**
     * 删除发货单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillExchangeByIds(String ids)
    {
        return billExchangeMapper.deleteBillExchangeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除发货单信息
     * 
     * @param exchangeId 发货单ID
     * @return 结果
     */
    @Override
    public int deleteBillExchangeById(Long exchangeId)
    {
        return billExchangeMapper.deleteBillExchangeById(exchangeId);
    }
}
