package com.ruoyi.project.bill.aftersalesOperateHistory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.aftersalesOperateHistory.domain.BillAftersalesOperateHistory;
import java.util.List;

/**
 * 订单操作历史记录Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillAftersalesOperateHistoryMapper extends BaseMapper<BillAftersalesOperateHistory>
{
    /**
     * 查询订单操作历史记录
     * 
     * @param id 订单操作历史记录ID
     * @return 订单操作历史记录
     */
    public BillAftersalesOperateHistory selectBillAftersalesOperateHistoryById(Long id);

    /**
     * 查询订单操作历史记录列表
     * 
     * @param billAftersalesOperateHistory 订单操作历史记录
     * @return 订单操作历史记录集合
     */
    public List<BillAftersalesOperateHistory> selectBillAftersalesOperateHistoryList(BillAftersalesOperateHistory billAftersalesOperateHistory);

    /**
     * 新增订单操作历史记录
     * 
     * @param billAftersalesOperateHistory 订单操作历史记录
     * @return 结果
     */
    public int insertBillAftersalesOperateHistory(BillAftersalesOperateHistory billAftersalesOperateHistory);

    /**
     * 修改订单操作历史记录
     * 
     * @param billAftersalesOperateHistory 订单操作历史记录
     * @return 结果
     */
    public int updateBillAftersalesOperateHistory(BillAftersalesOperateHistory billAftersalesOperateHistory);

    /**
     * 删除订单操作历史记录
     * 
     * @param id 订单操作历史记录ID
     * @return 结果
     */
    public int deleteBillAftersalesOperateHistoryById(Long id);

    /**
     * 批量删除订单操作历史记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillAftersalesOperateHistoryByIds(String[] ids);
}
