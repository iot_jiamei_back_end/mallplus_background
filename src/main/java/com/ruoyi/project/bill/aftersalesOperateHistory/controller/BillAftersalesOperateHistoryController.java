package com.ruoyi.project.bill.aftersalesOperateHistory.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.aftersalesOperateHistory.domain.BillAftersalesOperateHistory;
import com.ruoyi.project.bill.aftersalesOperateHistory.service.IBillAftersalesOperateHistoryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 订单操作历史记录Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/aftersalesOperateHistory")
public class BillAftersalesOperateHistoryController extends BaseController
{
    private String prefix = "bill/aftersalesOperateHistory";

    @Autowired
    private IBillAftersalesOperateHistoryService billAftersalesOperateHistoryService;

    @RequiresPermissions("bill:aftersalesOperateHistory:view")
    @GetMapping()
    public String aftersalesOperateHistory()
    {
        return prefix + "/aftersalesOperateHistory";
    }

    /**
     * 查询订单操作历史记录列表
     */
    @RequiresPermissions("bill:aftersalesOperateHistory:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        startPage();
        List<BillAftersalesOperateHistory> list = billAftersalesOperateHistoryService.selectBillAftersalesOperateHistoryList(billAftersalesOperateHistory);
        return getDataTable(list);
    }

    /**
     * 导出订单操作历史记录列表
     */
    @RequiresPermissions("bill:aftersalesOperateHistory:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        List<BillAftersalesOperateHistory> list = billAftersalesOperateHistoryService.selectBillAftersalesOperateHistoryList(billAftersalesOperateHistory);
        ExcelUtil<BillAftersalesOperateHistory> util = new ExcelUtil<BillAftersalesOperateHistory>(BillAftersalesOperateHistory.class);
        return util.exportExcel(list, "aftersalesOperateHistory");
    }

    /**
     * 新增订单操作历史记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存订单操作历史记录
     */
    @RequiresPermissions("bill:aftersalesOperateHistory:add")
    @Log(title = "订单操作历史记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        return toAjax(billAftersalesOperateHistoryService.insertBillAftersalesOperateHistory(billAftersalesOperateHistory));
    }

    /**
     * 修改订单操作历史记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BillAftersalesOperateHistory billAftersalesOperateHistory = billAftersalesOperateHistoryService.selectBillAftersalesOperateHistoryById(id);
        mmap.put("billAftersalesOperateHistory", billAftersalesOperateHistory);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单操作历史记录
     */
    @RequiresPermissions("bill:aftersalesOperateHistory:edit")
    @Log(title = "订单操作历史记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        return toAjax(billAftersalesOperateHistoryService.updateBillAftersalesOperateHistory(billAftersalesOperateHistory));
    }

    /**
     * 删除订单操作历史记录
     */
    @RequiresPermissions("bill:aftersalesOperateHistory:remove")
    @Log(title = "订单操作历史记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billAftersalesOperateHistoryService.deleteBillAftersalesOperateHistoryByIds(ids));
    }
}
