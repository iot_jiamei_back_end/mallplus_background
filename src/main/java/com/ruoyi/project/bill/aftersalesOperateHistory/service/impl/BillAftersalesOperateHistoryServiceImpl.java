package com.ruoyi.project.bill.aftersalesOperateHistory.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.aftersalesOperateHistory.mapper.BillAftersalesOperateHistoryMapper;
import com.ruoyi.project.bill.aftersalesOperateHistory.domain.BillAftersalesOperateHistory;
import com.ruoyi.project.bill.aftersalesOperateHistory.service.IBillAftersalesOperateHistoryService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 订单操作历史记录Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillAftersalesOperateHistoryServiceImpl extends ServiceImpl<BillAftersalesOperateHistoryMapper, BillAftersalesOperateHistory> implements IBillAftersalesOperateHistoryService
{
    @Autowired
    private BillAftersalesOperateHistoryMapper billAftersalesOperateHistoryMapper;

    /**
     * 查询订单操作历史记录
     * 
     * @param id 订单操作历史记录ID
     * @return 订单操作历史记录
     */
    @Override
    public BillAftersalesOperateHistory selectBillAftersalesOperateHistoryById(Long id)
    {
        return billAftersalesOperateHistoryMapper.selectBillAftersalesOperateHistoryById(id);
    }

    /**
     * 查询订单操作历史记录列表
     * 
     * @param billAftersalesOperateHistory 订单操作历史记录
     * @return 订单操作历史记录
     */
    @Override
    public List<BillAftersalesOperateHistory> selectBillAftersalesOperateHistoryList(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        return billAftersalesOperateHistoryMapper.selectBillAftersalesOperateHistoryList(billAftersalesOperateHistory);
    }

    /**
     * 新增订单操作历史记录
     * 
     * @param billAftersalesOperateHistory 订单操作历史记录
     * @return 结果
     */
    @Override
    public int insertBillAftersalesOperateHistory(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        //billAftersalesOperateHistory.setCreateTime(DateUtils.getNowDate());
        return billAftersalesOperateHistoryMapper.insertBillAftersalesOperateHistory(billAftersalesOperateHistory);
    }

    /**
     * 修改订单操作历史记录
     * 
     * @param billAftersalesOperateHistory 订单操作历史记录
     * @return 结果
     */
    @Override
    public int updateBillAftersalesOperateHistory(BillAftersalesOperateHistory billAftersalesOperateHistory)
    {
        //billAftersalesOperateHistory.setUpdateTime(DateUtils.getNowDate());
        return billAftersalesOperateHistoryMapper.updateBillAftersalesOperateHistory(billAftersalesOperateHistory);
    }

    /**
     * 删除订单操作历史记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesOperateHistoryByIds(String ids)
    {
        return billAftersalesOperateHistoryMapper.deleteBillAftersalesOperateHistoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除订单操作历史记录信息
     * 
     * @param id 订单操作历史记录ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesOperateHistoryById(Long id)
    {
        return billAftersalesOperateHistoryMapper.deleteBillAftersalesOperateHistoryById(id);
    }
}
