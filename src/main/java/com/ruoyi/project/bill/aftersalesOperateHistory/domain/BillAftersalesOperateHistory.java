package com.ruoyi.project.bill.aftersalesOperateHistory.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 订单操作历史记录
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@TableName("bill_aftersales_operate_history")
@Data
public class BillAftersalesOperateHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 订单id
     */
    @TableField("aftersales_id")
    private Long aftersalesId;

    /**
     * 操作人：用户；商户系统；后台管理员
     */
    @TableField("operate_man")
    private String operateMan;

    /**
     * 操作时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 现在售后状态
     */
    @TableField("aftersales_status")
    private Integer aftersalesStatus;

    /**
     * 备注
     */
    private String note;

    /**
     * 所属店铺
     */
    @TableField("store_id")
    private Integer storeId;

    /**
     * 原售后状态
     */
    @TableField("pre_status")
    private Integer preStatus;

    /**
     * 原退款状态
     */
    @TableField("pre_refund_status")
    private Integer preRefundStatus;

    /**
     * 现在退款状态
     */
    @TableField("now_refund_status")
    private Integer nowRefundStatus;

    /**
     * 现在申请状态
     */
    @TableField("now_apply_status")
    private Integer nowApplyStatus;

    /**
     * 原申请状态
     */
    @TableField("pre_apply_status")
    private Integer preApplyStatus;

    /**
     * 售后类型，1=只退款，2退货退款，3换货不退款
     */
    private Integer type;
}
