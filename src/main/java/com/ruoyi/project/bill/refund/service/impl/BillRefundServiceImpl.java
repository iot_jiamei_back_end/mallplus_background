package com.ruoyi.project.bill.refund.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.refund.mapper.BillRefundMapper;
import com.ruoyi.project.bill.refund.domain.BillRefund;
import com.ruoyi.project.bill.refund.service.IBillRefundService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 退款单Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillRefundServiceImpl extends ServiceImpl<BillRefundMapper, BillRefund> implements IBillRefundService
{
    @Autowired
    private BillRefundMapper billRefundMapper;

    /**
     * 查询退款单
     * 
     * @param refundId 退款单ID
     * @return 退款单
     */
    @Override
    public BillRefund selectBillRefundById(String refundId)
    {
        return billRefundMapper.selectBillRefundById(refundId);
    }

    /**
     * 查询退款单列表
     * 
     * @param billRefund 退款单
     * @return 退款单
     */
    @Override
    public List<BillRefund> selectBillRefundList(BillRefund billRefund)
    {
        return billRefundMapper.selectBillRefundList(billRefund);
    }

    /**
     * 新增退款单
     * 
     * @param billRefund 退款单
     * @return 结果
     */
    @Override
    public int insertBillRefund(BillRefund billRefund)
    {
        return billRefundMapper.insertBillRefund(billRefund);
    }

    /**
     * 修改退款单
     * 
     * @param billRefund 退款单
     * @return 结果
     */
    @Override
    public int updateBillRefund(BillRefund billRefund)
    {
        return billRefundMapper.updateBillRefund(billRefund);
    }

    /**
     * 删除退款单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillRefundByIds(String ids)
    {
        return billRefundMapper.deleteBillRefundByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除退款单信息
     * 
     * @param refundId 退款单ID
     * @return 结果
     */
    @Override
    public int deleteBillRefundById(String refundId)
    {
        return billRefundMapper.deleteBillRefundById(refundId);
    }
}
