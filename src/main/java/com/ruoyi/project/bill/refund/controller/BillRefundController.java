package com.ruoyi.project.bill.refund.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.refund.domain.BillRefund;
import com.ruoyi.project.bill.refund.service.IBillRefundService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 退款单Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/refund")
public class BillRefundController extends BaseController
{
    private String prefix = "bill/refund";

    @Autowired
    private IBillRefundService billRefundService;

    @RequiresPermissions("bill:refund:view")
    @GetMapping()
    public String refund()
    {
        return prefix + "/refund";
    }

    /**
     * 查询退款单列表
     */
    @RequiresPermissions("bill:refund:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillRefund billRefund)
    {
        startPage();
        List<BillRefund> list = billRefundService.selectBillRefundList(billRefund);
        return getDataTable(list);
    }

    /**
     * 导出退款单列表
     */
    @RequiresPermissions("bill:refund:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillRefund billRefund)
    {
        List<BillRefund> list = billRefundService.selectBillRefundList(billRefund);
        ExcelUtil<BillRefund> util = new ExcelUtil<BillRefund>(BillRefund.class);
        return util.exportExcel(list, "refund");
    }

    /**
     * 新增退款单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存退款单
     */
    @RequiresPermissions("bill:refund:add")
    @Log(title = "退款单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillRefund billRefund)
    {
        return toAjax(billRefundService.insertBillRefund(billRefund));
    }

    /**
     * 修改退款单
     */
    @GetMapping("/edit/{refundId}")
    public String edit(@PathVariable("refundId") String refundId, ModelMap mmap)
    {
        BillRefund billRefund = billRefundService.selectBillRefundById(refundId);
        mmap.put("billRefund", billRefund);
        return prefix + "/edit";
    }

    /**
     * 修改保存退款单
     */
    @RequiresPermissions("bill:refund:edit")
    @Log(title = "退款单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillRefund billRefund)
    {
        return toAjax(billRefundService.updateBillRefund(billRefund));
    }

    /**
     * 删除退款单
     */
    @RequiresPermissions("bill:refund:remove")
    @Log(title = "退款单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billRefundService.deleteBillRefundByIds(ids));
    }
}
