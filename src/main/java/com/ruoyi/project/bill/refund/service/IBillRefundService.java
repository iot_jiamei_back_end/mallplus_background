package com.ruoyi.project.bill.refund.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.bill.refund.domain.BillRefund;
import java.util.List;

/**
 * 退款单Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IBillRefundService  extends IService<BillRefund>
{
    /**
     * 查询退款单
     * 
     * @param refundId 退款单ID
     * @return 退款单
     */
    public BillRefund selectBillRefundById(String refundId);

    /**
     * 查询退款单列表
     * 
     * @param billRefund 退款单
     * @return 退款单集合
     */
    public List<BillRefund> selectBillRefundList(BillRefund billRefund);

    /**
     * 新增退款单
     * 
     * @param billRefund 退款单
     * @return 结果
     */
    public int insertBillRefund(BillRefund billRefund);

    /**
     * 修改退款单
     * 
     * @param billRefund 退款单
     * @return 结果
     */
    public int updateBillRefund(BillRefund billRefund);

    /**
     * 批量删除退款单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillRefundByIds(String ids);

    /**
     * 删除退款单信息
     * 
     * @param refundId 退款单ID
     * @return 结果
     */
    public int deleteBillRefundById(String refundId);
}
