package com.ruoyi.project.bill.aftersales.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.bill.aftersalesItems.domain.BillAftersalesItems;
import com.ruoyi.project.bill.aftersalesItems.mapper.BillAftersalesItemsMapper;
import com.ruoyi.project.bill.aftersalesOperateHistory.domain.BillAftersalesOperateHistory;
import com.ruoyi.project.bill.aftersalesOperateHistory.mapper.BillAftersalesOperateHistoryMapper;
import com.ruoyi.project.bill.exchange.domain.BillExchange;
import com.ruoyi.project.bill.exchange.mapper.BillExchangeMapper;
import com.ruoyi.project.bill.reship.domain.BillReship;
import com.ruoyi.project.bill.reship.mapper.BillReshipMapper;
import com.ruoyi.project.mall.store.domain.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.aftersales.mapper.BillAftersalesMapper;
import com.ruoyi.project.bill.aftersales.domain.BillAftersales;
import com.ruoyi.project.bill.aftersales.service.IBillAftersalesService;
import com.ruoyi.common.utils.text.Convert;
import org.springframework.transaction.annotation.Transactional;

/**
 * 退货单Service业务层处理
 *
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillAftersalesServiceImpl extends ServiceImpl<BillAftersalesMapper, BillAftersales> implements IBillAftersalesService {
    @Autowired
    private BillAftersalesMapper billAftersalesMapper;
    @Autowired
    private BillAftersalesItemsMapper billAftersalesItemsMapper;
    @Autowired
    private BillExchangeMapper billExchangeMapper;
    @Autowired
    private BillReshipMapper billReshipMapper;
    @Autowired
    private BillAftersalesOperateHistoryMapper billAftersalesOperateHistoryMapper;

    /**
     * 查询退货单
     *
     * @param aftersalesId 退货单ID
     * @return 退货单
     */
    @Override
    public BillAftersales selectBillAftersalesById(Long aftersalesId) {
        return billAftersalesMapper.selectBillAftersalesById(aftersalesId);
    }

    /**
     * 查询退货单列表
     *
     * @param billAftersales 退货单
     * @return 退货单
     */
    @Override
    public List<BillAftersales> selectBillAftersalesList(BillAftersales billAftersales) {
        return billAftersalesMapper.selectBillAftersalesList(billAftersales);
    }

    /**
     * 新增退货单
     *
     * @param billAftersales 退货单
     * @return 结果
     */
    @Override
    public int insertBillAftersales(BillAftersales billAftersales) {
        return billAftersalesMapper.insertBillAftersales(billAftersales);
    }

    /**
     * 修改退货单
     *
     * @param billAftersales 退货单
     * @return 结果
     */
    @Override
    public int updateBillAftersales(BillAftersales billAftersales) {
        return billAftersalesMapper.updateBillAftersales(billAftersales);
    }

    /**
     * 删除退货单对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesByIds(String ids) {
        return billAftersalesMapper.deleteBillAftersalesByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除退货单信息
     *
     * @param aftersalesId 退货单ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesById(Long aftersalesId) {
        return billAftersalesMapper.deleteBillAftersalesById(aftersalesId);
    }

    @Override
    public Integer getRefundOrderNum(Store store, Date yesterdayDate) {
        return billAftersalesMapper.getRefundOrderNum(store.getId(), yesterdayDate);
    }

    @Override
    public BigDecimal getRefundAmount(Store store, Date yesterdayDate) {
        return billAftersalesMapper.getRefundAmount(store.getId(), yesterdayDate);
    }

    /**
     * 处理发货
     *
     * @param aftersalesId
     * @param sellerLogiCode
     * @param sellerLogiNo
     */
    @Override
    public void handleDeliverGoods(Long aftersalesId, String sellerLogiCode, String sellerLogiNo) {
        BillAftersales billAftersales = billAftersalesMapper.selectById(aftersalesId);
        if (billAftersales.getType().equals(2)) { // 退货退款
            BillReship billReshipInfo = billReshipMapper.selectOne(new QueryWrapper<BillReship>().eq("aftersales_id", aftersalesId));
            // 买家发货
            billReshipInfo.setLogiCode(sellerLogiCode);
            billReshipInfo.setLogiNo(sellerLogiNo);
        } else if (billAftersales.getType().equals(3)) { // 换货
            BillExchange billExchangeInfo = billExchangeMapper.selectOne(new QueryWrapper<BillExchange>().eq("aftersales_id", aftersalesId));
            billExchangeInfo.setSellerLogiCode(sellerLogiCode);
            billExchangeInfo.setSellerLogiNo(sellerLogiNo);
            billExchangeMapper.updateById(billExchangeInfo);
            // 修改为卖家已发货
            billAftersales.setAftersalesStatus(5);
            billAftersalesMapper.updateById(billAftersales);
        }
    }

    @Override
    public Integer getTotalRefundOrderNum(Date date) {
        return billAftersalesMapper.getTotalRefundOrderNum(date);
    }

    @Override
    public BigDecimal getTotalRefundAmount(Date date) {
        return billAftersalesMapper.getTotalRefundAmount(date);
    }

    @Override
    public void doAudit(Long aftersalesId, Integer aftersalesType, Integer auditType, String remark) {
        BillAftersales billAftersales = this.baseMapper.selectById(aftersalesId);
        if (aftersalesType.equals(1)) { // 退货
            billAftersales.setType(2);
        } else if (aftersalesType.equals(2)) {  // 换货
            billAftersales.setType(3);
        }
        this.baseMapper.updateById(billAftersales);
        if (auditType.equals(1)) { // 确认退换货
            this.confirmReturn(aftersalesId, remark);
        } else if (auditType.equals(2)) {  // 拒绝退换货
            this.doRejectReturn(aftersalesId, remark);
        }

    }

    /**
     * 确认退换货
     *
     * @param aftersalesId
     * @param remark
     */
    private void confirmReturn(Long aftersalesId, String remark) {
        BillAftersales billAftersales = billAftersalesMapper.selectById(aftersalesId);
        BillAftersalesOperateHistory billAftersalesOperateHistory = new BillAftersalesOperateHistory();
        billAftersalesOperateHistory.setAftersalesId(aftersalesId);
        billAftersalesOperateHistory.setPreStatus(billAftersales.getAftersalesStatus());
        //billAftersalesOperateHistory.setStoreId(billAftersales.getStoreId());
        billAftersalesOperateHistory.setCreateTime(new Date());
        billAftersalesOperateHistory.setOperateMan("管理员");
        if (billAftersales.getType().equals(2)) {// 退货
            // 修改售后单状态
            billAftersales.setStatus(1);  // 1退换货中
            billAftersales.setRefundStatus(1); // 未退款
            billAftersales.setAftersalesStatus(2);// 2待买家发货
            billAftersalesMapper.updateById(billAftersales);
            billAftersalesOperateHistory.setAftersalesStatus(2);  // 2待买家发货
            // 修改售后商品状态
            List<BillAftersalesItems> billAftersalesItemList = billAftersalesItemsMapper.selectList(new QueryWrapper<BillAftersalesItems>().eq("aftersales_id", billAftersales.getAftersalesId()));
            for (BillAftersalesItems billAftersalesItems : billAftersalesItemList) {
                // 0正常 1退换货中2换货成功3退款成功4申请拒绝
                billAftersalesItems.setStatus(1);
                billAftersalesItemsMapper.updateById(billAftersalesItems);
            }
            // 添加售后操作记录
            billAftersalesOperateHistoryMapper.insert(billAftersalesOperateHistory);

            // 添加退货表记录
            BillReship billReship = new BillReship();

            billReship.setOrderId(billAftersales.getOrderId());
            billReship.setAftersalesId(String.valueOf(billAftersales.getAftersalesId()));
            billReship.setUserId(Integer.parseInt(billAftersales.getUserId().toString()));
            billReship.setStatus(1);
            billReship.setMemo(remark);
            billReship.setCtime(new Date());
            billReship.setUtime(new Date());
            billReship.setHandleTime(new Date());
            billReship.setHandleMan("admin");
            billReship.setReturnName(billAftersales.getLinkMan());
            billReship.setReturnPhone(billAftersales.getLinkPhone());
            billReshipMapper.insert(billReship);
        } else { // 换货
            // 修改售后单状态
            billAftersales.setStatus(1);
            billAftersales.setRefundStatus(1); // 未退款
            billAftersales.setAftersalesStatus(2);// 2待买家发货
            billAftersalesMapper.updateById(billAftersales);
            billAftersalesOperateHistory.setAftersalesStatus(4);  // 卖家已收货
            // 修改售后商品状态
            List<BillAftersalesItems> billAftersalesItemList = billAftersalesItemsMapper.selectList(new QueryWrapper<BillAftersalesItems>().eq("aftersales_id", billAftersales.getAftersalesId()));
            for (BillAftersalesItems billAftersalesItems : billAftersalesItemList) {
                // 0正常 1退换货中2换货成功3退款成功4申请拒绝
                billAftersalesItems.setStatus(1);
                billAftersalesItemsMapper.updateById(billAftersalesItems);
            }
            // 添加售后操作记录
            billAftersalesOperateHistoryMapper.insert(billAftersalesOperateHistory);

            // 添加换货表记录
            BillExchange billExchange = new BillExchange();
            billExchange.setOrderId(billAftersales.getOrderId());
            billExchange.setAftersalesId(aftersalesId);
            billExchange.setUserId(Integer.parseInt(billAftersales.getUserId().toString()));
            billExchange.setStatus(1);//状态：1 待换货  2买家已发货  3卖家已收货  4 卖家处理换货中   5卖家已发货中  6买家已收货 7换货成功
            billExchange.setBuyShipAddress(billAftersales.getAddress());
            billExchange.setBuyShipMobile(billAftersales.getLinkPhone());
            billExchange.setBuyShipName(billAftersales.getLinkMan());
            billExchange.setCtime(new Date());
            billExchange.setUtime(new Date());
            billExchangeMapper.insert(billExchange);
        }
    }

    /**
     * 拒绝退换货
     *
     * @param aftersalesId
     * @param remark
     */
    private void doRejectReturn(Long aftersalesId, String remark) {
        BillAftersales billAftersales = billAftersalesMapper.selectById(aftersalesId);

        BillAftersalesOperateHistory billAftersalesOperateHistory = new BillAftersalesOperateHistory();
        billAftersalesOperateHistory.setAftersalesId(aftersalesId);

        billAftersalesOperateHistory.setPreStatus(billAftersales.getAftersalesStatus());
        //billAftersalesOperateHistory.setStoreId(billAftersales.getStoreId());
        billAftersalesOperateHistory.setCreateTime(new Date());
        billAftersalesOperateHistory.setNote(remark);
        billAftersalesOperateHistory.setOperateMan("admin");

        // 修改售后单状态
        billAftersales.setStatus(3);
        billAftersales.setRefundStatus(1);
        billAftersales.setAftersalesStatus(1);
        billAftersalesMapper.updateById(billAftersales);
        billAftersalesOperateHistory.setAftersalesStatus(1);
        // 修改售后商品状态
        List<BillAftersalesItems> billAftersalesItemList = billAftersalesItemsMapper.selectList(new QueryWrapper<BillAftersalesItems>().eq("aftersales_id", billAftersales.getAftersalesId()));
        for (BillAftersalesItems billAftersalesItems : billAftersalesItemList) {
            // 0正常 1退换货中2换货成功3退款成功4申请拒绝
            billAftersalesItems.setStatus(4);
            billAftersalesItemsMapper.updateById(billAftersalesItems);
        }
        // 添加售后操作记录
        billAftersalesOperateHistoryMapper.insert(billAftersalesOperateHistory);
    }

}
