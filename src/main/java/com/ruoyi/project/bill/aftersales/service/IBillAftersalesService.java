package com.ruoyi.project.bill.aftersales.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.bill.aftersales.domain.BillAftersales;
import com.ruoyi.project.mall.store.domain.Store;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 退货单Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IBillAftersalesService  extends IService<BillAftersales>
{
    /**
     * 查询退货单
     * 
     * @param aftersalesId 退货单ID
     * @return 退货单
     */
    public BillAftersales selectBillAftersalesById(Long aftersalesId);

    /**
     * 查询退货单列表
     * 
     * @param billAftersales 退货单
     * @return 退货单集合
     */
    public List<BillAftersales> selectBillAftersalesList(BillAftersales billAftersales);

    /**
     * 新增退货单
     * 
     * @param billAftersales 退货单
     * @return 结果
     */
    public int insertBillAftersales(BillAftersales billAftersales);

    /**
     * 修改退货单
     * 
     * @param billAftersales 退货单
     * @return 结果
     */
    public int updateBillAftersales(BillAftersales billAftersales);

    /**
     * 批量删除退货单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillAftersalesByIds(String ids);

    /**
     * 删除退货单信息
     * 
     * @param aftersalesId 退货单ID
     * @return 结果
     */
    public int deleteBillAftersalesById(Long aftersalesId);

    Integer getRefundOrderNum(Store store, Date yesterdayDate);

    BigDecimal getRefundAmount(Store store, Date yesterdayDate);

    void handleDeliverGoods(Long aftersalesId, String sellerLogiCode, String sellerLogiNo);

    Integer getTotalRefundOrderNum(Date date);

    BigDecimal getTotalRefundAmount(Date date);

    void doAudit(Long aftersalesId, Integer aftersalesType, Integer auditType, String remark);
}
