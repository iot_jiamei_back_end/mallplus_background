package com.ruoyi.project.bill.aftersales.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.aftersales.domain.BillAftersales;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 退货单Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillAftersalesMapper extends BaseMapper<BillAftersales>
{
    /**
     * 查询退货单
     * 
     * @param aftersalesId 退货单ID
     * @return 退货单
     */
    public BillAftersales selectBillAftersalesById(Long aftersalesId);

    /**
     * 查询退货单列表
     * 
     * @param billAftersales 退货单
     * @return 退货单集合
     */
    public List<BillAftersales> selectBillAftersalesList(BillAftersales billAftersales);

    /**
     * 新增退货单
     * 
     * @param billAftersales 退货单
     * @return 结果
     */
    public int insertBillAftersales(BillAftersales billAftersales);

    /**
     * 修改退货单
     * 
     * @param billAftersales 退货单
     * @return 结果
     */
    public int updateBillAftersales(BillAftersales billAftersales);

    /**
     * 删除退货单
     * 
     * @param aftersalesId 退货单ID
     * @return 结果
     */
    public int deleteBillAftersalesById(Long aftersalesId);

    /**
     * 批量删除退货单
     * 
     * @param aftersalesIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillAftersalesByIds(String[] aftersalesIds);

    BigDecimal getOrderRefundAmount(@Param("orderId") Long orderId);

    Integer getTotalRefundOrderNum(@Param("date") Date date);

    BigDecimal getTotalRefundAmount(@Param("date") Date date);

    BigDecimal getRefundAmount(@Param("storeId") Integer storeId,@Param("date") Date date);

    Integer getRefundOrderNum(@Param("storeId") Integer storeId,@Param("date") Date date);
}
