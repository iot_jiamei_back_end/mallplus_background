package com.ruoyi.project.bill.aftersales.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ruoyi.project.bill.aftersalesItems.domain.BillAftersalesItems;
import com.ruoyi.project.bill.aftersalesOperateHistory.domain.BillAftersalesOperateHistory;
import com.ruoyi.project.bill.exchange.domain.BillExchange;
import com.ruoyi.project.bill.refund.domain.BillRefund;
import com.ruoyi.project.bill.reship.domain.BillReship;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 退货单表
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@Data
@TableName("bill_aftersales")
public class BillAftersales implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableField(exist = false)
    private String orderSn;

    @TableField(exist = false)
    private BillRefund billRefund;

    @TableField(exist = false)
    private OmsOrder omsOrder;

    @TableField(exist = false)
    private BillExchange billExchange;

    @TableField(exist = false)
    private BillReship billReship;

    @TableField(exist = false)
    private BillAftersalesItems billAftersalesItems;

    @TableField(exist = false)
    private List<BillAftersalesOperateHistory> billAftersalesOperateHistoryList;
    ///////////////////////////////////////////////////
    /**
     * 售后单id
     */
    @TableId(value = "aftersales_id", type = IdType.AUTO)
    private Long aftersalesId;

    /**
     * 订单ID 关联order.id
     */
    @TableField("order_id")
    private String orderId;


    /**
     * 用户ID 关联user.id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 售后类型，1=只退款，2退货退款，3换货不退款
     */
    private Integer type;

    /**
     * 退款金额
     */
    private BigDecimal refund;

    /**
     * 退换货原因
     */
    private String reason;

    /**
     * 卖家备注，如果审核失败了，会显示到前端
     */
    private String mark;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;
    /**
     * 所属店铺
     */
    @TableField("store_id")
    private Integer storeId;
    /**
     * 问题描述
     */
    private String description;

    /**
     * 凭证图片，以逗号隔开
     */
    @TableField("proof_pics")
    private String proofPics;

    /**
     * 联系人
     */
    @TableField("link_man")
    private String linkMan;

    /**
     * 联系电话
     */
    @TableField("link_phone")
    private String linkPhone;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 服务单号
     */
    @TableField("aftersales_sn")
    private String aftersalesSn;

    /**
     * 0待平台审核 1平台已拒绝  2待买家发货 3买家已发货，待商家收货 4商家已收货  5商家已发货 6买家已收货(换货完成)
     */
    @TableField("aftersales_status")
    private Integer aftersalesStatus;

    /**
     * 状态 1=未退款 2=已退款 3=退款失败，可以再次退款，4退款拒绝
     */
    @TableField("refund_status")
    private Integer refundStatus;

    /**
     * 状态 0待处理 1退换货中 2已完成  3已拒绝
     */
    private Integer status;

    /**
     * 退换货内容
     */
    @TableField("aftersales_content")
    private String aftersalesContent;
}
