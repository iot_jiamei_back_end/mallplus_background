package com.ruoyi.project.bill.aftersalesProgress.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@TableName("bill_aftersales_progress")
public class BillAftersalesProgress implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("aftersales_id")
    private Long aftersalesId;

    @TableField("aftersales_status")
    private Integer aftersalesStatus;

    private Integer state;

    @TableField("create_time")
    private Long createTime;

    @TableField("update_time")
    private Long updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAftersalesId() {
        return aftersalesId;
    }

    public void setAftersalesId(Long aftersalesId) {
        this.aftersalesId = aftersalesId;
    }

    public Integer getAftersalesStatus() {
        return aftersalesStatus;
    }

    public void setAftersalesStatus(Integer aftersalesStatus) {
        this.aftersalesStatus = aftersalesStatus;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "BillAftersalesProgress{" +
        ", id=" + id +
        ", aftersalesId=" + aftersalesId +
        ", aftersalesStatus=" + aftersalesStatus +
        ", state=" + state +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
