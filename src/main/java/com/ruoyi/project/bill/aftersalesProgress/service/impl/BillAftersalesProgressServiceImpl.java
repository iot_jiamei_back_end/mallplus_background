package com.ruoyi.project.bill.aftersalesProgress.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.aftersalesProgress.mapper.BillAftersalesProgressMapper;
import com.ruoyi.project.bill.aftersalesProgress.domain.BillAftersalesProgress;
import com.ruoyi.project.bill.aftersalesProgress.service.IBillAftersalesProgressService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 售后状态记录Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillAftersalesProgressServiceImpl extends ServiceImpl<BillAftersalesProgressMapper, BillAftersalesProgress> implements IBillAftersalesProgressService
{
    @Autowired
    private BillAftersalesProgressMapper billAftersalesProgressMapper;

    /**
     * 查询售后状态记录
     * 
     * @param id 售后状态记录ID
     * @return 售后状态记录
     */
    @Override
    public BillAftersalesProgress selectBillAftersalesProgressById(Long id)
    {
        return billAftersalesProgressMapper.selectBillAftersalesProgressById(id);
    }

    /**
     * 查询售后状态记录列表
     * 
     * @param billAftersalesProgress 售后状态记录
     * @return 售后状态记录
     */
    @Override
    public List<BillAftersalesProgress> selectBillAftersalesProgressList(BillAftersalesProgress billAftersalesProgress)
    {
        return billAftersalesProgressMapper.selectBillAftersalesProgressList(billAftersalesProgress);
    }

    /**
     * 新增售后状态记录
     * 
     * @param billAftersalesProgress 售后状态记录
     * @return 结果
     */
    @Override
    public int insertBillAftersalesProgress(BillAftersalesProgress billAftersalesProgress)
    {
        //billAftersalesProgress.setCreateTime(DateUtils.getNowDate());
        return billAftersalesProgressMapper.insertBillAftersalesProgress(billAftersalesProgress);
    }

    /**
     * 修改售后状态记录
     * 
     * @param billAftersalesProgress 售后状态记录
     * @return 结果
     */
    @Override
    public int updateBillAftersalesProgress(BillAftersalesProgress billAftersalesProgress)
    {
        //billAftersalesProgress.setUpdateTime(DateUtils.getNowDate());
        return billAftersalesProgressMapper.updateBillAftersalesProgress(billAftersalesProgress);
    }

    /**
     * 删除售后状态记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesProgressByIds(String ids)
    {
        return billAftersalesProgressMapper.deleteBillAftersalesProgressByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除售后状态记录信息
     * 
     * @param id 售后状态记录ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesProgressById(Long id)
    {
        return billAftersalesProgressMapper.deleteBillAftersalesProgressById(id);
    }
}
