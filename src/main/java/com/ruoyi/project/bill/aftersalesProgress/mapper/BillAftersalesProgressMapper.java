package com.ruoyi.project.bill.aftersalesProgress.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.aftersalesProgress.domain.BillAftersalesProgress;
import java.util.List;

/**
 * 售后状态记录Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillAftersalesProgressMapper extends BaseMapper<BillAftersalesProgress>
{
    /**
     * 查询售后状态记录
     * 
     * @param id 售后状态记录ID
     * @return 售后状态记录
     */
    public BillAftersalesProgress selectBillAftersalesProgressById(Long id);

    /**
     * 查询售后状态记录列表
     * 
     * @param billAftersalesProgress 售后状态记录
     * @return 售后状态记录集合
     */
    public List<BillAftersalesProgress> selectBillAftersalesProgressList(BillAftersalesProgress billAftersalesProgress);

    /**
     * 新增售后状态记录
     * 
     * @param billAftersalesProgress 售后状态记录
     * @return 结果
     */
    public int insertBillAftersalesProgress(BillAftersalesProgress billAftersalesProgress);

    /**
     * 修改售后状态记录
     * 
     * @param billAftersalesProgress 售后状态记录
     * @return 结果
     */
    public int updateBillAftersalesProgress(BillAftersalesProgress billAftersalesProgress);

    /**
     * 删除售后状态记录
     * 
     * @param id 售后状态记录ID
     * @return 结果
     */
    public int deleteBillAftersalesProgressById(Long id);

    /**
     * 批量删除售后状态记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillAftersalesProgressByIds(String[] ids);
}
