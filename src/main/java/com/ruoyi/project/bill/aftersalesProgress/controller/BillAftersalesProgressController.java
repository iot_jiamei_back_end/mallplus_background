package com.ruoyi.project.bill.aftersalesProgress.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.aftersalesProgress.domain.BillAftersalesProgress;
import com.ruoyi.project.bill.aftersalesProgress.service.IBillAftersalesProgressService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 售后状态记录Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/aftersalesProgress")
public class BillAftersalesProgressController extends BaseController
{
    private String prefix = "bill/aftersalesProgress";

    @Autowired
    private IBillAftersalesProgressService billAftersalesProgressService;

    @RequiresPermissions("bill:aftersalesProgress:view")
    @GetMapping()
    public String aftersalesProgress()
    {
        return prefix + "/aftersalesProgress";
    }

    /**
     * 查询售后状态记录列表
     */
    @RequiresPermissions("bill:aftersalesProgress:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillAftersalesProgress billAftersalesProgress)
    {
        startPage();
        List<BillAftersalesProgress> list = billAftersalesProgressService.selectBillAftersalesProgressList(billAftersalesProgress);
        return getDataTable(list);
    }

    /**
     * 导出售后状态记录列表
     */
    @RequiresPermissions("bill:aftersalesProgress:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillAftersalesProgress billAftersalesProgress)
    {
        List<BillAftersalesProgress> list = billAftersalesProgressService.selectBillAftersalesProgressList(billAftersalesProgress);
        ExcelUtil<BillAftersalesProgress> util = new ExcelUtil<BillAftersalesProgress>(BillAftersalesProgress.class);
        return util.exportExcel(list, "aftersalesProgress");
    }

    /**
     * 新增售后状态记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存售后状态记录
     */
    @RequiresPermissions("bill:aftersalesProgress:add")
    @Log(title = "售后状态记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillAftersalesProgress billAftersalesProgress)
    {
        return toAjax(billAftersalesProgressService.insertBillAftersalesProgress(billAftersalesProgress));
    }

    /**
     * 修改售后状态记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BillAftersalesProgress billAftersalesProgress = billAftersalesProgressService.selectBillAftersalesProgressById(id);
        mmap.put("billAftersalesProgress", billAftersalesProgress);
        return prefix + "/edit";
    }

    /**
     * 修改保存售后状态记录
     */
    @RequiresPermissions("bill:aftersalesProgress:edit")
    @Log(title = "售后状态记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillAftersalesProgress billAftersalesProgress)
    {
        return toAjax(billAftersalesProgressService.updateBillAftersalesProgress(billAftersalesProgress));
    }

    /**
     * 删除售后状态记录
     */
    @RequiresPermissions("bill:aftersalesProgress:remove")
    @Log(title = "售后状态记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billAftersalesProgressService.deleteBillAftersalesProgressByIds(ids));
    }
}
