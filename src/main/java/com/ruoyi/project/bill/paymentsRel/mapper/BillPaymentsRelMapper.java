package com.ruoyi.project.bill.paymentsRel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.paymentsRel.domain.BillPaymentsRel;
import java.util.List;

/**
 * 支付单明细Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillPaymentsRelMapper extends BaseMapper<BillPaymentsRel>
{
    /**
     * 查询支付单明细
     * 
     * @param paymentId 支付单明细ID
     * @return 支付单明细
     */
    public BillPaymentsRel selectBillPaymentsRelById(String paymentId);

    /**
     * 查询支付单明细列表
     * 
     * @param billPaymentsRel 支付单明细
     * @return 支付单明细集合
     */
    public List<BillPaymentsRel> selectBillPaymentsRelList(BillPaymentsRel billPaymentsRel);

    /**
     * 新增支付单明细
     * 
     * @param billPaymentsRel 支付单明细
     * @return 结果
     */
    public int insertBillPaymentsRel(BillPaymentsRel billPaymentsRel);

    /**
     * 修改支付单明细
     * 
     * @param billPaymentsRel 支付单明细
     * @return 结果
     */
    public int updateBillPaymentsRel(BillPaymentsRel billPaymentsRel);

    /**
     * 删除支付单明细
     * 
     * @param paymentId 支付单明细ID
     * @return 结果
     */
    public int deleteBillPaymentsRelById(String paymentId);

    /**
     * 批量删除支付单明细
     * 
     * @param paymentIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillPaymentsRelByIds(String[] paymentIds);
}
