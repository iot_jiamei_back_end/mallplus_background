package com.ruoyi.project.bill.paymentsRel.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.paymentsRel.mapper.BillPaymentsRelMapper;
import com.ruoyi.project.bill.paymentsRel.domain.BillPaymentsRel;
import com.ruoyi.project.bill.paymentsRel.service.IBillPaymentsRelService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 支付单明细Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillPaymentsRelServiceImpl extends ServiceImpl<BillPaymentsRelMapper, BillPaymentsRel> implements IBillPaymentsRelService
{
    @Autowired
    private BillPaymentsRelMapper billPaymentsRelMapper;

    /**
     * 查询支付单明细
     * 
     * @param paymentId 支付单明细ID
     * @return 支付单明细
     */
    @Override
    public BillPaymentsRel selectBillPaymentsRelById(String paymentId)
    {
        return billPaymentsRelMapper.selectBillPaymentsRelById(paymentId);
    }

    /**
     * 查询支付单明细列表
     * 
     * @param billPaymentsRel 支付单明细
     * @return 支付单明细
     */
    @Override
    public List<BillPaymentsRel> selectBillPaymentsRelList(BillPaymentsRel billPaymentsRel)
    {
        return billPaymentsRelMapper.selectBillPaymentsRelList(billPaymentsRel);
    }

    /**
     * 新增支付单明细
     * 
     * @param billPaymentsRel 支付单明细
     * @return 结果
     */
    @Override
    public int insertBillPaymentsRel(BillPaymentsRel billPaymentsRel)
    {
        return billPaymentsRelMapper.insertBillPaymentsRel(billPaymentsRel);
    }

    /**
     * 修改支付单明细
     * 
     * @param billPaymentsRel 支付单明细
     * @return 结果
     */
    @Override
    public int updateBillPaymentsRel(BillPaymentsRel billPaymentsRel)
    {
        return billPaymentsRelMapper.updateBillPaymentsRel(billPaymentsRel);
    }

    /**
     * 删除支付单明细对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillPaymentsRelByIds(String ids)
    {
        return billPaymentsRelMapper.deleteBillPaymentsRelByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除支付单明细信息
     * 
     * @param paymentId 支付单明细ID
     * @return 结果
     */
    @Override
    public int deleteBillPaymentsRelById(String paymentId)
    {
        return billPaymentsRelMapper.deleteBillPaymentsRelById(paymentId);
    }
}
