package com.ruoyi.project.bill.paymentsRel.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.paymentsRel.domain.BillPaymentsRel;
import com.ruoyi.project.bill.paymentsRel.service.IBillPaymentsRelService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 支付单明细Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/paymentsRel")
public class BillPaymentsRelController extends BaseController
{
    private String prefix = "bill/paymentsRel";

    @Autowired
    private IBillPaymentsRelService billPaymentsRelService;

    @RequiresPermissions("bill:paymentsRel:view")
    @GetMapping()
    public String paymentsRel()
    {
        return prefix + "/paymentsRel";
    }

    /**
     * 查询支付单明细列表
     */
    @RequiresPermissions("bill:paymentsRel:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillPaymentsRel billPaymentsRel)
    {
        startPage();
        List<BillPaymentsRel> list = billPaymentsRelService.selectBillPaymentsRelList(billPaymentsRel);
        return getDataTable(list);
    }

    /**
     * 导出支付单明细列表
     */
    @RequiresPermissions("bill:paymentsRel:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillPaymentsRel billPaymentsRel)
    {
        List<BillPaymentsRel> list = billPaymentsRelService.selectBillPaymentsRelList(billPaymentsRel);
        ExcelUtil<BillPaymentsRel> util = new ExcelUtil<BillPaymentsRel>(BillPaymentsRel.class);
        return util.exportExcel(list, "paymentsRel");
    }

    /**
     * 新增支付单明细
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存支付单明细
     */
    @RequiresPermissions("bill:paymentsRel:add")
    @Log(title = "支付单明细", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillPaymentsRel billPaymentsRel)
    {
        return toAjax(billPaymentsRelService.insertBillPaymentsRel(billPaymentsRel));
    }

    /**
     * 修改支付单明细
     */
    @GetMapping("/edit/{paymentId}")
    public String edit(@PathVariable("paymentId") String paymentId, ModelMap mmap)
    {
        BillPaymentsRel billPaymentsRel = billPaymentsRelService.selectBillPaymentsRelById(paymentId);
        mmap.put("billPaymentsRel", billPaymentsRel);
        return prefix + "/edit";
    }

    /**
     * 修改保存支付单明细
     */
    @RequiresPermissions("bill:paymentsRel:edit")
    @Log(title = "支付单明细", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillPaymentsRel billPaymentsRel)
    {
        return toAjax(billPaymentsRelService.updateBillPaymentsRel(billPaymentsRel));
    }

    /**
     * 删除支付单明细
     */
    @RequiresPermissions("bill:paymentsRel:remove")
    @Log(title = "支付单明细", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billPaymentsRelService.deleteBillPaymentsRelByIds(ids));
    }
}
