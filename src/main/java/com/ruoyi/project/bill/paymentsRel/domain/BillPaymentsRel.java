package com.ruoyi.project.bill.paymentsRel.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 支付单明细对象 bill_payments_rel
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public class BillPaymentsRel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 支付单编号 */
    @Excel(name = "支付单编号")
    private String paymentId;

    /** 资源编号 */
    @Excel(name = "资源编号")
    private String sourceId;

    /** 金额 */
    @Excel(name = "金额")
    private Double money;

    public void setPaymentId(String paymentId) 
    {
        this.paymentId = paymentId;
    }

    public String getPaymentId() 
    {
        return paymentId;
    }
    public void setSourceId(String sourceId) 
    {
        this.sourceId = sourceId;
    }

    public String getSourceId() 
    {
        return sourceId;
    }
    public void setMoney(Double money) 
    {
        this.money = money;
    }

    public Double getMoney() 
    {
        return money;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("paymentId", getPaymentId())
            .append("sourceId", getSourceId())
            .append("money", getMoney())
            .toString();
    }
}
