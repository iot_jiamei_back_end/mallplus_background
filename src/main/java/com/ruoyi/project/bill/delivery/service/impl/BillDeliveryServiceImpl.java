package com.ruoyi.project.bill.delivery.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.delivery.mapper.BillDeliveryMapper;
import com.ruoyi.project.bill.delivery.domain.BillDelivery;
import com.ruoyi.project.bill.delivery.service.IBillDeliveryService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 发货单Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillDeliveryServiceImpl extends ServiceImpl<BillDeliveryMapper, BillDelivery> implements IBillDeliveryService
{
    @Autowired
    private BillDeliveryMapper billDeliveryMapper;

    /**
     * 查询发货单
     * 
     * @param deliveryId 发货单ID
     * @return 发货单
     */
    @Override
    public BillDelivery selectBillDeliveryById(Long deliveryId)
    {
        return billDeliveryMapper.selectBillDeliveryById(deliveryId);
    }

    /**
     * 查询发货单列表
     * 
     * @param billDelivery 发货单
     * @return 发货单
     */
    @Override
    public List<BillDelivery> selectBillDeliveryList(BillDelivery billDelivery)
    {
        return billDeliveryMapper.selectBillDeliveryList(billDelivery);
    }

    /**
     * 新增发货单
     * 
     * @param billDelivery 发货单
     * @return 结果
     */
    @Override
    public int insertBillDelivery(BillDelivery billDelivery)
    {
        return billDeliveryMapper.insertBillDelivery(billDelivery);
    }

    /**
     * 修改发货单
     * 
     * @param billDelivery 发货单
     * @return 结果
     */
    @Override
    public int updateBillDelivery(BillDelivery billDelivery)
    {
        return billDeliveryMapper.updateBillDelivery(billDelivery);
    }

    /**
     * 删除发货单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillDeliveryByIds(String ids)
    {
        return billDeliveryMapper.deleteBillDeliveryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除发货单信息
     * 
     * @param deliveryId 发货单ID
     * @return 结果
     */
    @Override
    public int deleteBillDeliveryById(Long deliveryId)
    {
        return billDeliveryMapper.deleteBillDeliveryById(deliveryId);
    }
}
