package com.ruoyi.project.bill.delivery.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.delivery.domain.BillDelivery;
import com.ruoyi.project.bill.delivery.service.IBillDeliveryService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 发货单Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/delivery")
public class BillDeliveryController extends BaseController
{
    private String prefix = "bill/delivery";

    @Autowired
    private IBillDeliveryService billDeliveryService;

    @RequiresPermissions("bill:delivery:view")
    @GetMapping()
    public String delivery()
    {
        return prefix + "/delivery";
    }

    /**
     * 查询发货单列表
     */
    @RequiresPermissions("bill:delivery:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillDelivery billDelivery)
    {
        startPage();
        List<BillDelivery> list = billDeliveryService.selectBillDeliveryList(billDelivery);
        return getDataTable(list);
    }

    /**
     * 导出发货单列表
     */
    @RequiresPermissions("bill:delivery:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillDelivery billDelivery)
    {
        List<BillDelivery> list = billDeliveryService.selectBillDeliveryList(billDelivery);
        ExcelUtil<BillDelivery> util = new ExcelUtil<BillDelivery>(BillDelivery.class);
        return util.exportExcel(list, "delivery");
    }

    /**
     * 新增发货单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存发货单
     */
    @RequiresPermissions("bill:delivery:add")
    @Log(title = "发货单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillDelivery billDelivery)
    {
        return toAjax(billDeliveryService.insertBillDelivery(billDelivery));
    }

    /**
     * 修改发货单
     */
    @GetMapping("/edit/{deliveryId}")
    public String edit(@PathVariable("deliveryId") Long deliveryId, ModelMap mmap)
    {
        BillDelivery billDelivery = billDeliveryService.selectBillDeliveryById(deliveryId);
        mmap.put("billDelivery", billDelivery);
        return prefix + "/edit";
    }

    /**
     * 修改保存发货单
     */
    @RequiresPermissions("bill:delivery:edit")
    @Log(title = "发货单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillDelivery billDelivery)
    {
        return toAjax(billDeliveryService.updateBillDelivery(billDelivery));
    }

    /**
     * 删除发货单
     */
    @RequiresPermissions("bill:delivery:remove")
    @Log(title = "发货单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billDeliveryService.deleteBillDeliveryByIds(ids));
    }
}
