package com.ruoyi.project.bill.delivery.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 发货单表
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@TableName("bill_delivery")
public class BillDelivery implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "delivery_id", type = IdType.AUTO)
    private Long deliveryId;

    /**
     * 订单ID 关联order.id
     */
    @TableField("order_id")
    private String orderId;

    /**
     * 用户id 关联user.id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 物流公司编码
     */
    @TableField("logi_code")
    private String logiCode;

    /**
     * 物流单号
     */
    @TableField("logi_no")
    private String logiNo;

    /**
     * 快递物流信息
     */
    @TableField("logi_information")
    private String logiInformation;

    /**
     * 0快递信息可能更新  1快递信息不更新了
     */
    @TableField("logi_status")
    private Integer logiStatus;

    /**
     * 收货地区ID
     */
    @TableField("ship_area_id")
    private Integer shipAreaId;

    /**
     * 收货详细地址
     */
    @TableField("ship_address")
    private String shipAddress;

    /**
     * 收货人姓名
     */
    @TableField("ship_name")
    private String shipName;

    /**
     * 收货电话
     */
    @TableField("ship_mobile")
    private String shipMobile;

    /**
     * 确认收货时间
     */
    @TableField("confirm_time")
    private Long confirmTime;

    /**
     * 状态 1=准备发货 2=已发货 3=已确认 4=其他
     */
    private Boolean status;

    /**
     * 备注
     */
    private String memo;

    /**
     * 申请时间
     */
    private Long ctime;

    /**
     * 更新时间
     */
    private Long utime;

    /**
     * 所属店铺
     */
    @TableField("store_id")
    private Integer storeId;

    /**
     * 收货地址表id
     */
    @TableField("company_address_id")
    private Long companyAddressId;

    /**
     * 订单编号
     */
    @TableField("order_sn")
    private String orderSn;

    /**
     * 邮政编码
     */
    @TableField("ship_post_code")
    private String shipPostCode;

    /**
     * 物流公司名字
     */
    @TableField("logi_name")
    private String logiName;

    @TableField("delivery_sn")
    private String deliverySn;


    public Long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLogiCode() {
        return logiCode;
    }

    public void setLogiCode(String logiCode) {
        this.logiCode = logiCode;
    }

    public String getLogiNo() {
        return logiNo;
    }

    public void setLogiNo(String logiNo) {
        this.logiNo = logiNo;
    }

    public String getLogiInformation() {
        return logiInformation;
    }

    public void setLogiInformation(String logiInformation) {
        this.logiInformation = logiInformation;
    }

    public Integer getLogiStatus() {
        return logiStatus;
    }

    public void setLogiStatus(Integer logiStatus) {
        this.logiStatus = logiStatus;
    }

    public Integer getShipAreaId() {
        return shipAreaId;
    }

    public void setShipAreaId(Integer shipAreaId) {
        this.shipAreaId = shipAreaId;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipMobile() {
        return shipMobile;
    }

    public void setShipMobile(String shipMobile) {
        this.shipMobile = shipMobile;
    }

    public Long getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Long confirmTime) {
        this.confirmTime = confirmTime;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getUtime() {
        return utime;
    }

    public void setUtime(Long utime) {
        this.utime = utime;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Long getCompanyAddressId() {
        return companyAddressId;
    }

    public void setCompanyAddressId(Long companyAddressId) {
        this.companyAddressId = companyAddressId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getShipPostCode() {
        return shipPostCode;
    }

    public void setShipPostCode(String shipPostCode) {
        this.shipPostCode = shipPostCode;
    }

    public String getLogiName() {
        return logiName;
    }

    public void setLogiName(String logiName) {
        this.logiName = logiName;
    }

    public String getDeliverySn() {
        return deliverySn;
    }

    public void setDeliverySn(String deliverySn) {
        this.deliverySn = deliverySn;
    }

    @Override
    public String toString() {
        return "BillDelivery{" +
        ", deliveryId=" + deliveryId +
        ", orderId=" + orderId +
        ", userId=" + userId +
        ", logiCode=" + logiCode +
        ", logiNo=" + logiNo +
        ", logiInformation=" + logiInformation +
        ", logiStatus=" + logiStatus +
        ", shipAreaId=" + shipAreaId +
        ", shipAddress=" + shipAddress +
        ", shipName=" + shipName +
        ", shipMobile=" + shipMobile +
        ", confirmTime=" + confirmTime +
        ", status=" + status +
        ", memo=" + memo +
        ", ctime=" + ctime +
        ", utime=" + utime +
        ", storeId=" + storeId +
        ", companyAddressId=" + companyAddressId +
        ", orderSn=" + orderSn +
        ", shipPostCode=" + shipPostCode +
        ", logiName=" + logiName +
        ", deliverySn=" + deliverySn +
        "}";
    }
}
