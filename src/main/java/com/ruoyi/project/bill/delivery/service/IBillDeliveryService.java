package com.ruoyi.project.bill.delivery.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.bill.delivery.domain.BillDelivery;
import java.util.List;

/**
 * 发货单Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IBillDeliveryService  extends IService<BillDelivery>
{
    /**
     * 查询发货单
     * 
     * @param deliveryId 发货单ID
     * @return 发货单
     */
    public BillDelivery selectBillDeliveryById(Long deliveryId);

    /**
     * 查询发货单列表
     * 
     * @param billDelivery 发货单
     * @return 发货单集合
     */
    public List<BillDelivery> selectBillDeliveryList(BillDelivery billDelivery);

    /**
     * 新增发货单
     * 
     * @param billDelivery 发货单
     * @return 结果
     */
    public int insertBillDelivery(BillDelivery billDelivery);

    /**
     * 修改发货单
     * 
     * @param billDelivery 发货单
     * @return 结果
     */
    public int updateBillDelivery(BillDelivery billDelivery);

    /**
     * 批量删除发货单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillDeliveryByIds(String ids);

    /**
     * 删除发货单信息
     * 
     * @param deliveryId 发货单ID
     * @return 结果
     */
    public int deleteBillDeliveryById(Long deliveryId);
}
