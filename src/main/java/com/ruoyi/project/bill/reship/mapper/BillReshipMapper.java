package com.ruoyi.project.bill.reship.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.reship.domain.BillReship;
import java.util.List;

/**
 * 退货单Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillReshipMapper  extends BaseMapper<BillReship>
{
    /**
     * 查询退货单
     * 
     * @param reshipId 退货单ID
     * @return 退货单
     */
    public BillReship selectBillReshipById(Long reshipId);

    /**
     * 查询退货单列表
     * 
     * @param billReship 退货单
     * @return 退货单集合
     */
    public List<BillReship> selectBillReshipList(BillReship billReship);

    /**
     * 新增退货单
     * 
     * @param billReship 退货单
     * @return 结果
     */
    public int insertBillReship(BillReship billReship);

    /**
     * 修改退货单
     * 
     * @param billReship 退货单
     * @return 结果
     */
    public int updateBillReship(BillReship billReship);

    /**
     * 删除退货单
     * 
     * @param reshipId 退货单ID
     * @return 结果
     */
    public int deleteBillReshipById(Long reshipId);

    /**
     * 批量删除退货单
     * 
     * @param reshipIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillReshipByIds(String[] reshipIds);
}
