package com.ruoyi.project.bill.reship.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 退货单表
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-13
 */
@TableName("bill_reship")
@Data
public class BillReship implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "reship_id", type = IdType.AUTO)
    private Long reshipId;

    /**
     * 订单ID 关联order.id
     */
    @TableField("order_id")
    private String orderId;

    /**
     * 售后单id
     */
    @TableField("aftersales_id")
    private String aftersalesId;

    /**
     * 用户ID 关联user.id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 物流公司编码
     */
    @TableField("logi_code")
    private String logiCode;

    /**
     * 物流单号
     */
    @TableField("logi_no")
    private String logiNo;

    /**
     * 状态 1=审核通过待发货 2=已发退货 3=已收退货
     */
    private Integer status;

    /**
     * 买家备注
     */
    private String memo;

    /**
     * 创建时间
     */
    private Date ctime;

    /**
     * 更新时间
     */
    private Date utime;

    /**
     * 所属店铺
     */
    @TableField("store_id")
    private Integer storeId;

    /**
     * 收货地址表id
     */
    @TableField("company_address_id")
    private Long companyAddressId;

    /**
     * 处理时间
     */
    @TableField("handle_time")
    private Date handleTime;

    /**
     * 处理备注
     */
    @TableField("handle_note")
    private String handleNote;

    /**
     * 处理人员
     */
    @TableField("handle_man")
    private String handleMan;

    /**
     * 收货人
     */
    @TableField("receive_man")
    private String receiveMan;

    /**
     * 卖家收货时间
     */
    @TableField("receive_time")
    private LocalDateTime receiveTime;

    /**
     * 收货备注
     */
    @TableField("receive_note")
    private String receiveNote;

    /**
     * 退款金额
     */
    @TableField("return_amount")
    private BigDecimal returnAmount;

    /**
     * 退款人姓名
     */
    @TableField("return_name")
    private String returnName;

    /**
     * 退款人联系
     */
    @TableField("return_phone")
    private String returnPhone;

    @TableField("logi_name")
    private String logiName;
}
