package com.ruoyi.project.bill.reship.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.bill.reship.domain.BillReship;
import java.util.List;

/**
 * 退货单Service接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface IBillReshipService  extends IService<BillReship>
{
    /**
     * 查询退货单
     * 
     * @param reshipId 退货单ID
     * @return 退货单
     */
    public BillReship selectBillReshipById(Long reshipId);

    /**
     * 查询退货单列表
     * 
     * @param billReship 退货单
     * @return 退货单集合
     */
    public List<BillReship> selectBillReshipList(BillReship billReship);

    /**
     * 新增退货单
     * 
     * @param billReship 退货单
     * @return 结果
     */
    public int insertBillReship(BillReship billReship);

    /**
     * 修改退货单
     * 
     * @param billReship 退货单
     * @return 结果
     */
    public int updateBillReship(BillReship billReship);

    /**
     * 批量删除退货单
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillReshipByIds(String ids);

    /**
     * 删除退货单信息
     * 
     * @param reshipId 退货单ID
     * @return 结果
     */
    public int deleteBillReshipById(Long reshipId);
}
