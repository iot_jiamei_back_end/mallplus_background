package com.ruoyi.project.bill.reship.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.reship.mapper.BillReshipMapper;
import com.ruoyi.project.bill.reship.domain.BillReship;
import com.ruoyi.project.bill.reship.service.IBillReshipService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 退货单Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillReshipServiceImpl extends ServiceImpl<BillReshipMapper, BillReship> implements IBillReshipService
{
    @Autowired
    private BillReshipMapper billReshipMapper;

    /**
     * 查询退货单
     * 
     * @param reshipId 退货单ID
     * @return 退货单
     */
    @Override
    public BillReship selectBillReshipById(Long reshipId)
    {
        return billReshipMapper.selectBillReshipById(reshipId);
    }

    /**
     * 查询退货单列表
     * 
     * @param billReship 退货单
     * @return 退货单
     */
    @Override
    public List<BillReship> selectBillReshipList(BillReship billReship)
    {
        return billReshipMapper.selectBillReshipList(billReship);
    }

    /**
     * 新增退货单
     * 
     * @param billReship 退货单
     * @return 结果
     */
    @Override
    public int insertBillReship(BillReship billReship)
    {
        return billReshipMapper.insertBillReship(billReship);
    }

    /**
     * 修改退货单
     * 
     * @param billReship 退货单
     * @return 结果
     */
    @Override
    public int updateBillReship(BillReship billReship)
    {
        return billReshipMapper.updateBillReship(billReship);
    }

    /**
     * 删除退货单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillReshipByIds(String ids)
    {
        return billReshipMapper.deleteBillReshipByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除退货单信息
     * 
     * @param reshipId 退货单ID
     * @return 结果
     */
    @Override
    public int deleteBillReshipById(Long reshipId)
    {
        return billReshipMapper.deleteBillReshipById(reshipId);
    }
}
