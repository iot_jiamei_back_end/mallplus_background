package com.ruoyi.project.bill.reship.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.reship.domain.BillReship;
import com.ruoyi.project.bill.reship.service.IBillReshipService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 退货单Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/reship")
public class BillReshipController extends BaseController
{
    private String prefix = "bill/reship";

    @Autowired
    private IBillReshipService billReshipService;

    @RequiresPermissions("bill:reship:view")
    @GetMapping()
    public String reship()
    {
        return prefix + "/reship";
    }

    /**
     * 查询退货单列表
     */
    @RequiresPermissions("bill:reship:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillReship billReship)
    {
        startPage();
        List<BillReship> list = billReshipService.selectBillReshipList(billReship);
        return getDataTable(list);
    }

    /**
     * 导出退货单列表
     */
    @RequiresPermissions("bill:reship:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillReship billReship)
    {
        List<BillReship> list = billReshipService.selectBillReshipList(billReship);
        ExcelUtil<BillReship> util = new ExcelUtil<BillReship>(BillReship.class);
        return util.exportExcel(list, "reship");
    }

    /**
     * 新增退货单
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存退货单
     */
    @RequiresPermissions("bill:reship:add")
    @Log(title = "退货单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillReship billReship)
    {
        return toAjax(billReshipService.insertBillReship(billReship));
    }

    /**
     * 修改退货单
     */
    @GetMapping("/edit/{reshipId}")
    public String edit(@PathVariable("reshipId") Long reshipId, ModelMap mmap)
    {
        BillReship billReship = billReshipService.selectBillReshipById(reshipId);
        mmap.put("billReship", billReship);
        return prefix + "/edit";
    }

    /**
     * 修改保存退货单
     */
    @RequiresPermissions("bill:reship:edit")
    @Log(title = "退货单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillReship billReship)
    {
        return toAjax(billReshipService.updateBillReship(billReship));
    }

    /**
     * 删除退货单
     */
    @RequiresPermissions("bill:reship:remove")
    @Log(title = "退货单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billReshipService.deleteBillReshipByIds(ids));
    }
}
