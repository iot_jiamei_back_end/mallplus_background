package com.ruoyi.project.bill.deliveryItems.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.deliveryItems.domain.BillDeliveryItems;
import com.ruoyi.project.bill.deliveryItems.service.IBillDeliveryItemsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 发货单详情Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/deliveryItems")
public class BillDeliveryItemsController extends BaseController
{
    private String prefix = "bill/deliveryItems";

    @Autowired
    private IBillDeliveryItemsService billDeliveryItemsService;

    @RequiresPermissions("bill:deliveryItems:view")
    @GetMapping()
    public String deliveryItems()
    {
        return prefix + "/deliveryItems";
    }

    /**
     * 查询发货单详情列表
     */
    @RequiresPermissions("bill:deliveryItems:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillDeliveryItems billDeliveryItems)
    {
        startPage();
        List<BillDeliveryItems> list = billDeliveryItemsService.selectBillDeliveryItemsList(billDeliveryItems);
        return getDataTable(list);
    }

    /**
     * 导出发货单详情列表
     */
    @RequiresPermissions("bill:deliveryItems:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillDeliveryItems billDeliveryItems)
    {
        List<BillDeliveryItems> list = billDeliveryItemsService.selectBillDeliveryItemsList(billDeliveryItems);
        ExcelUtil<BillDeliveryItems> util = new ExcelUtil<BillDeliveryItems>(BillDeliveryItems.class);
        return util.exportExcel(list, "deliveryItems");
    }

    /**
     * 新增发货单详情
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存发货单详情
     */
    @RequiresPermissions("bill:deliveryItems:add")
    @Log(title = "发货单详情", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillDeliveryItems billDeliveryItems)
    {
        return toAjax(billDeliveryItemsService.insertBillDeliveryItems(billDeliveryItems));
    }

    /**
     * 修改发货单详情
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, ModelMap mmap)
    {
        BillDeliveryItems billDeliveryItems = billDeliveryItemsService.selectBillDeliveryItemsById(id);
        mmap.put("billDeliveryItems", billDeliveryItems);
        return prefix + "/edit";
    }

    /**
     * 修改保存发货单详情
     */
    @RequiresPermissions("bill:deliveryItems:edit")
    @Log(title = "发货单详情", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillDeliveryItems billDeliveryItems)
    {
        return toAjax(billDeliveryItemsService.updateBillDeliveryItems(billDeliveryItems));
    }

    /**
     * 删除发货单详情
     */
    @RequiresPermissions("bill:deliveryItems:remove")
    @Log(title = "发货单详情", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billDeliveryItemsService.deleteBillDeliveryItemsByIds(ids));
    }
}
