package com.ruoyi.project.bill.deliveryItems.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.deliveryItems.mapper.BillDeliveryItemsMapper;
import com.ruoyi.project.bill.deliveryItems.domain.BillDeliveryItems;
import com.ruoyi.project.bill.deliveryItems.service.IBillDeliveryItemsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 发货单详情Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillDeliveryItemsServiceImpl extends ServiceImpl<BillDeliveryItemsMapper, BillDeliveryItems> implements IBillDeliveryItemsService
{
    @Autowired
    private BillDeliveryItemsMapper billDeliveryItemsMapper;

    /**
     * 查询发货单详情
     * 
     * @param id 发货单详情ID
     * @return 发货单详情
     */
    @Override
    public BillDeliveryItems selectBillDeliveryItemsById(Integer id)
    {
        return billDeliveryItemsMapper.selectBillDeliveryItemsById(id);
    }

    /**
     * 查询发货单详情列表
     * 
     * @param billDeliveryItems 发货单详情
     * @return 发货单详情
     */
    @Override
    public List<BillDeliveryItems> selectBillDeliveryItemsList(BillDeliveryItems billDeliveryItems)
    {
        return billDeliveryItemsMapper.selectBillDeliveryItemsList(billDeliveryItems);
    }

    /**
     * 新增发货单详情
     * 
     * @param billDeliveryItems 发货单详情
     * @return 结果
     */
    @Override
    public int insertBillDeliveryItems(BillDeliveryItems billDeliveryItems)
    {
        return billDeliveryItemsMapper.insertBillDeliveryItems(billDeliveryItems);
    }

    /**
     * 修改发货单详情
     * 
     * @param billDeliveryItems 发货单详情
     * @return 结果
     */
    @Override
    public int updateBillDeliveryItems(BillDeliveryItems billDeliveryItems)
    {
        return billDeliveryItemsMapper.updateBillDeliveryItems(billDeliveryItems);
    }

    /**
     * 删除发货单详情对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillDeliveryItemsByIds(String ids)
    {
        return billDeliveryItemsMapper.deleteBillDeliveryItemsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除发货单详情信息
     * 
     * @param id 发货单详情ID
     * @return 结果
     */
    @Override
    public int deleteBillDeliveryItemsById(Integer id)
    {
        return billDeliveryItemsMapper.deleteBillDeliveryItemsById(id);
    }
}
