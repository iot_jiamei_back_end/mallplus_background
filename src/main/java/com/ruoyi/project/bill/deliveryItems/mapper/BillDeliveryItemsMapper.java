package com.ruoyi.project.bill.deliveryItems.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.deliveryItems.domain.BillDeliveryItems;
import java.util.List;

/**
 * 发货单详情Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillDeliveryItemsMapper extends BaseMapper<BillDeliveryItems>
{
    /**
     * 查询发货单详情
     * 
     * @param id 发货单详情ID
     * @return 发货单详情
     */
    public BillDeliveryItems selectBillDeliveryItemsById(Integer id);

    /**
     * 查询发货单详情列表
     * 
     * @param billDeliveryItems 发货单详情
     * @return 发货单详情集合
     */
    public List<BillDeliveryItems> selectBillDeliveryItemsList(BillDeliveryItems billDeliveryItems);

    /**
     * 新增发货单详情
     * 
     * @param billDeliveryItems 发货单详情
     * @return 结果
     */
    public int insertBillDeliveryItems(BillDeliveryItems billDeliveryItems);

    /**
     * 修改发货单详情
     * 
     * @param billDeliveryItems 发货单详情
     * @return 结果
     */
    public int updateBillDeliveryItems(BillDeliveryItems billDeliveryItems);

    /**
     * 删除发货单详情
     * 
     * @param id 发货单详情ID
     * @return 结果
     */
    public int deleteBillDeliveryItemsById(Integer id);

    /**
     * 批量删除发货单详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillDeliveryItemsByIds(String[] ids);
}
