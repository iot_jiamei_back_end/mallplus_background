package com.ruoyi.project.bill.aftersalesReason.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.bill.aftersalesReason.domain.BillAftersalesReason;
import java.util.List;

/**
 * 退货原因Mapper接口
 * 
 * @author mallplus
 * @date 2020-03-12
 */
public interface BillAftersalesReasonMapper extends BaseMapper<BillAftersalesReason>
{
    /**
     * 查询退货原因
     * 
     * @param id 退货原因ID
     * @return 退货原因
     */
    public BillAftersalesReason selectBillAftersalesReasonById(Long id);

    /**
     * 查询退货原因列表
     * 
     * @param billAftersalesReason 退货原因
     * @return 退货原因集合
     */
    public List<BillAftersalesReason> selectBillAftersalesReasonList(BillAftersalesReason billAftersalesReason);

    /**
     * 新增退货原因
     * 
     * @param billAftersalesReason 退货原因
     * @return 结果
     */
    public int insertBillAftersalesReason(BillAftersalesReason billAftersalesReason);

    /**
     * 修改退货原因
     * 
     * @param billAftersalesReason 退货原因
     * @return 结果
     */
    public int updateBillAftersalesReason(BillAftersalesReason billAftersalesReason);

    /**
     * 删除退货原因
     * 
     * @param id 退货原因ID
     * @return 结果
     */
    public int deleteBillAftersalesReasonById(Long id);

    /**
     * 批量删除退货原因
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBillAftersalesReasonByIds(String[] ids);
}
