package com.ruoyi.project.bill.aftersalesReason.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.bill.aftersalesReason.mapper.BillAftersalesReasonMapper;
import com.ruoyi.project.bill.aftersalesReason.domain.BillAftersalesReason;
import com.ruoyi.project.bill.aftersalesReason.service.IBillAftersalesReasonService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 退货原因Service业务层处理
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Service
public class BillAftersalesReasonServiceImpl extends ServiceImpl<BillAftersalesReasonMapper, BillAftersalesReason> implements IBillAftersalesReasonService
{
    @Autowired
    private BillAftersalesReasonMapper billAftersalesReasonMapper;

    /**
     * 查询退货原因
     * 
     * @param id 退货原因ID
     * @return 退货原因
     */
    @Override
    public BillAftersalesReason selectBillAftersalesReasonById(Long id)
    {
        return billAftersalesReasonMapper.selectBillAftersalesReasonById(id);
    }

    /**
     * 查询退货原因列表
     * 
     * @param billAftersalesReason 退货原因
     * @return 退货原因
     */
    @Override
    public List<BillAftersalesReason> selectBillAftersalesReasonList(BillAftersalesReason billAftersalesReason)
    {
        return billAftersalesReasonMapper.selectBillAftersalesReasonList(billAftersalesReason);
    }

    /**
     * 新增退货原因
     * 
     * @param billAftersalesReason 退货原因
     * @return 结果
     */
    @Override
    public int insertBillAftersalesReason(BillAftersalesReason billAftersalesReason)
    {
        //billAftersalesReason.setCreateTime(DateUtils.getNowDate());
        return billAftersalesReasonMapper.insertBillAftersalesReason(billAftersalesReason);
    }

    /**
     * 修改退货原因
     * 
     * @param billAftersalesReason 退货原因
     * @return 结果
     */
    @Override
    public int updateBillAftersalesReason(BillAftersalesReason billAftersalesReason)
    {
        //billAftersalesReason.setUpdateTime(DateUtils.getNowDate());
        return billAftersalesReasonMapper.updateBillAftersalesReason(billAftersalesReason);
    }

    /**
     * 删除退货原因对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesReasonByIds(String ids)
    {
        return billAftersalesReasonMapper.deleteBillAftersalesReasonByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除退货原因信息
     * 
     * @param id 退货原因ID
     * @return 结果
     */
    @Override
    public int deleteBillAftersalesReasonById(Long id)
    {
        return billAftersalesReasonMapper.deleteBillAftersalesReasonById(id);
    }
}
