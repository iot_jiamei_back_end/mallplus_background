package com.ruoyi.project.bill.aftersalesReason.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.bill.aftersalesReason.domain.BillAftersalesReason;
import com.ruoyi.project.bill.aftersalesReason.service.IBillAftersalesReasonService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 退货原因Controller
 * 
 * @author mallplus
 * @date 2020-03-12
 */
@Controller
@RequestMapping("/bill/aftersalesReason")
public class BillAftersalesReasonController extends BaseController
{
    private String prefix = "bill/aftersalesReason";

    @Autowired
    private IBillAftersalesReasonService billAftersalesReasonService;

    @RequiresPermissions("bill:aftersalesReason:view")
    @GetMapping()
    public String aftersalesReason()
    {
        return prefix + "/aftersalesReason";
    }

    /**
     * 查询退货原因列表
     */
    @RequiresPermissions("bill:aftersalesReason:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(BillAftersalesReason billAftersalesReason)
    {
        startPage();
        List<BillAftersalesReason> list = billAftersalesReasonService.selectBillAftersalesReasonList(billAftersalesReason);
        return getDataTable(list);
    }

    /**
     * 导出退货原因列表
     */
    @RequiresPermissions("bill:aftersalesReason:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(BillAftersalesReason billAftersalesReason)
    {
        List<BillAftersalesReason> list = billAftersalesReasonService.selectBillAftersalesReasonList(billAftersalesReason);
        ExcelUtil<BillAftersalesReason> util = new ExcelUtil<BillAftersalesReason>(BillAftersalesReason.class);
        return util.exportExcel(list, "aftersalesReason");
    }

    /**
     * 新增退货原因
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存退货原因
     */
    @RequiresPermissions("bill:aftersalesReason:add")
    @Log(title = "退货原因", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(BillAftersalesReason billAftersalesReason)
    {
        return toAjax(billAftersalesReasonService.insertBillAftersalesReason(billAftersalesReason));
    }

    /**
     * 修改退货原因
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        BillAftersalesReason billAftersalesReason = billAftersalesReasonService.selectBillAftersalesReasonById(id);
        mmap.put("billAftersalesReason", billAftersalesReason);
        return prefix + "/edit";
    }

    /**
     * 修改保存退货原因
     */
    @RequiresPermissions("bill:aftersalesReason:edit")
    @Log(title = "退货原因", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(BillAftersalesReason billAftersalesReason)
    {
        return toAjax(billAftersalesReasonService.updateBillAftersalesReason(billAftersalesReason));
    }

    /**
     * 删除退货原因
     */
    @RequiresPermissions("bill:aftersalesReason:remove")
    @Log(title = "退货原因", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(billAftersalesReasonService.deleteBillAftersalesReasonByIds(ids));
    }
}
