package com.ruoyi.project.finance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.finance.entity.FinanceCheck;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Mapper
public interface FinanceCheckMapper extends BaseMapper<FinanceCheck> {

}
