package com.ruoyi.project.finance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Mapper
public interface FinanceOrderStatisticMapper extends BaseMapper<FinanceOrderStatistic> {

    List<FinanceOrderStatistic> selectListForWeek(@Param("now") Date now, @Param("dateStr") String dateStr);

    List<FinanceOrderStatistic> selectListForMonth(@Param("now") Date now,@Param("dateStr")String dateStr);

    List<FinanceOrderStatistic> selectListForDay(@Param("now") Date now);
}
