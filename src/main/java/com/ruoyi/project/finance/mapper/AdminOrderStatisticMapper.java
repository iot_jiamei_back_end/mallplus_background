package com.ruoyi.project.finance.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.project.finance.entity.AdminOrderStatistic;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Mapper
public interface AdminOrderStatisticMapper extends BaseMapper<AdminOrderStatistic> {

    List<AdminOrderStatistic> selectListForWeek(@Param("now") Date now, @Param("dateStr") String dateStr);
//    @Select("select\n" +
//            "        id,\n" +
//            "        date_str as dateStr,\n" +
//            "        pay_order_num as payOrderNum,\n" +
//            "        order_amount as orderAmount,\n" +
//            "        refund_amount as refundAmount,\n" +
//            "        pay_amount as payAmount,\n" +
//            "        transaction_user_num as transactionUserNum,\n" +
//            "        refund_order_num as refundOrderNum,\n" +
//            "        create_time as createTime,\n" +
//            "        update_time as updateTime\n" +
//            "        from admin_order_statistic WHERE DATE_FORMAT(date_str, '%Y%m' ) = DATE_FORMAT( CURDATE( ) ,'%Y%m' )")
    List<AdminOrderStatistic> selectListForMonth(@Param("now") Date now, @Param("dateStr") String dateStr);
//    @Select("select\n" +
//            "        id,\n" +
//            "        date_str as dateStr,\n" +
//            "        pay_order_num as payOrderNum,\n" +
//            "        order_amount as orderAmount,\n" +
//            "        refund_amount as refundAmount,\n" +
//            "        pay_amount as payAmount,\n" +
//            "        transaction_user_num as transactionUserNum,\n" +
//            "        refund_order_num as refundOrderNum,\n" +
//            "        create_time as createTime,\n" +
//            "        update_time as updateTime\n" +
//            "        from admin_order_statistic where to_days(date_str) = to_days(#{now})")
    List<AdminOrderStatistic> selectListForDay(@Param("now") Date now);
}
