package com.ruoyi.project.finance.service.impl;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.bill.aftersales.domain.BillAftersales;
import com.ruoyi.project.bill.aftersales.mapper.BillAftersalesMapper;
import com.ruoyi.project.finance.entity.FinanceCheck;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import com.ruoyi.project.finance.entity.FinanceSettlement;
import com.ruoyi.project.finance.mapper.FinanceCheckAccountMapper;
import com.ruoyi.project.finance.mapper.FinanceCheckMapper;
import com.ruoyi.project.finance.mapper.FinanceSettlementMapper;
import com.ruoyi.project.finance.service.IFinanceSettlementService;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import com.ruoyi.project.mall.order.mapper.OmsOrderMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Service
@Primary
public class FinanceSettlementServiceImpl extends ServiceImpl<FinanceSettlementMapper, FinanceSettlement> implements IFinanceSettlementService {

    @Resource
    private OmsOrderMapper omsOrderMapper;

    @Resource
    private FinanceSettlementMapper financeSettlementMapper;

    @Resource
    private BillAftersalesMapper billAftersalesMapper;

    @Resource
    private FinanceCheckMapper financeCheckMapper;

    @Resource
    private FinanceCheckAccountMapper financeCheckAccountMapper;

    @Override
    @Transactional
    public void doSettlement(Integer storeId, String startTime, String endTime) {
        // 查询订单列表（订单已完成并且售后已完成）
        List<OmsOrder> omsOrderList = omsOrderMapper.getSettlementOrder(storeId, startTime, endTime);
        System.out.println("查询订单列表（订单已完成并且售后已完成）:" + omsOrderList.size());
        List<FinanceCheckAccount> financeCheckAccountList = new ArrayList<>();
        BigDecimal totalAmountSettleAmount = BigDecimal.ZERO;
        for (OmsOrder omsOrder : omsOrderList) {
            // 生成对账列表
            FinanceCheckAccount financeCheckAccount = new FinanceCheckAccount();
            financeCheckAccount.setCheckId(null);
            financeCheckAccount.setCreateTime(new Date());
            financeCheckAccount.setDateStr(DateUtils.parseDateToStr("yyyy-MM-dd", omsOrder.getCreateTime())); // 时间
            financeCheckAccount.setMemberId(omsOrder.getMemberId().intValue());
            financeCheckAccount.setMemberName(omsOrder.getMemberUsername());
            financeCheckAccount.setOrderSn(omsOrder.getOrderSn());
            financeCheckAccount.setStoreId(omsOrder.getStoreId().intValue());
            // 订单金额
            financeCheckAccount.setOrderAmount(omsOrder.getTotalAmount());
            financeCheckAccount.setPayAmount(omsOrder.getPayAmount());
            financeCheckAccount.setDiscountAmount(omsOrder.getDiscountAmount());
            // 查询退款金额
            BigDecimal refundAmount = billAftersalesMapper.getOrderRefundAmount(omsOrder.getId()); // 售后
            if (refundAmount == null) {
                refundAmount = BigDecimal.ZERO;
            }
            financeCheckAccount.setRefundAmount(refundAmount);
            // 计算结算金额=支付金额+退款金额

            BigDecimal settleAmount = omsOrder.getPayAmount().add(refundAmount);
            financeCheckAccount.setSettleAmount(settleAmount);
            financeCheckAccountList.add(financeCheckAccount);

            // 总的结算金额
            totalAmountSettleAmount.add(settleAmount);
        }
        // 生成结算信息
        FinanceSettlement financeSettlement = new FinanceSettlement();
        financeSettlement.setStoreId(storeId);
        financeSettlement.setCheckId(null);
        financeSettlement.setCreateTime(new Date());
        financeSettlement.setUpdateTime(new Date());
        financeSettlement.setName(startTime + "到" + endTime + "对账单");
        financeSettlement.setSettlementSn(UUID.randomUUID().toString());// 结算编号
        financeSettlement.setStartTime(new Date()); // 开始时间
        financeSettlement.setEndTime(new Date());   // 结束时间
        financeSettlement.setStatus(0); // 0 待确认
        financeSettlement.setTotalAmount(totalAmountSettleAmount);
        financeSettlementMapper.insert(financeSettlement);
        // 生成对账单
        FinanceCheck financeCheck = new FinanceCheck();
        financeCheck.setTotalDiscountAmount(BigDecimal.ZERO);
        financeCheck.setTotalOrderAmount(BigDecimal.ZERO);
        financeCheck.setTotalPayAmount(BigDecimal.ZERO);
        financeCheck.setTotalRefundAmount(BigDecimal.ZERO);
        financeCheck.setTotalSettleAmount(BigDecimal.ZERO);
        for (FinanceCheckAccount financeCheckAccount : financeCheckAccountList) {
            financeCheck.setTotalDiscountAmount(financeCheck.getTotalOrderAmount().add(financeCheckAccount.getDiscountAmount()));
            financeCheck.setTotalOrderAmount(financeCheck.getTotalOrderAmount().add(financeCheckAccount.getOrderAmount()));
            financeCheck.setTotalPayAmount(financeCheck.getTotalPayAmount().add(financeCheckAccount.getPayAmount()));
            financeCheck.setTotalRefundAmount(financeCheck.getTotalRefundAmount().add(financeCheckAccount.getRefundAmount()));
            financeCheck.setTotalSettleAmount(financeCheck.getTotalSettleAmount().add(financeCheckAccount.getSettleAmount()));
        }
        financeCheck.setCreateTime(new Date());
        financeCheck.setSettlementId(financeSettlement.getId());
        financeCheck.setStoreId(storeId);
        financeCheckMapper.insert(financeCheck);
        // 更新结算表的checkId
        financeSettlement.setCheckId(financeCheck.getId());
        financeSettlementMapper.updateById(financeSettlement);
        for (FinanceCheckAccount financeCheckAccount : financeCheckAccountList) {
            financeCheckAccount.setCheckId(financeCheck.getId());
            financeCheckAccountMapper.insert(financeCheckAccount);
        }

    }

}
