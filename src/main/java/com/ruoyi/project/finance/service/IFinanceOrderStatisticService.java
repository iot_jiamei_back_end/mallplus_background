package com.ruoyi.project.finance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import com.ruoyi.project.finance.entity.vo.OrderStatisticListInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
public interface IFinanceOrderStatisticService extends IService<FinanceOrderStatistic> {

    OrderStatisticListInfo getFinanceOrderStatisticList(FinanceOrderStatistic entity);
}
