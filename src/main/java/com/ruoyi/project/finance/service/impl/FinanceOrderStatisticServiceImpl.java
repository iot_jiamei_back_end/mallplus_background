package com.ruoyi.project.finance.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import com.ruoyi.project.finance.entity.vo.OrderStatisticListInfo;
import com.ruoyi.project.finance.entity.vo.OrderStatisticTotalInfo;
import com.ruoyi.project.finance.mapper.FinanceOrderStatisticMapper;
import com.ruoyi.project.finance.service.IFinanceOrderStatisticService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Service
@Primary
public class FinanceOrderStatisticServiceImpl extends ServiceImpl<FinanceOrderStatisticMapper, FinanceOrderStatistic> implements IFinanceOrderStatisticService {
    @Override
    @Transactional
    public OrderStatisticListInfo getFinanceOrderStatisticList(FinanceOrderStatistic entity) {
        OrderStatisticListInfo orderStatisticListInfo = new OrderStatisticListInfo();
        List<FinanceOrderStatistic> list = new ArrayList<>();
        if (entity.getDateFilteType() == null) {
            // 查询日期
            if (entity.getDateStr() != null) {
                list = this.baseMapper.selectList(new QueryWrapper<FinanceOrderStatistic>().eq("date_str", entity.getDateStr()));
                OrderStatisticTotalInfo orderStatisticTotalInfo = this.getOrderStatisticTotalInfo(list);
                orderStatisticListInfo.setOrderStatisticTotalInfo(orderStatisticTotalInfo);
            } else {
                list = this.baseMapper.selectList(new QueryWrapper<>());
                OrderStatisticTotalInfo orderStatisticTotalInfo = this.getOrderStatisticTotalInfo(list);
                orderStatisticListInfo.setOrderStatisticTotalInfo(orderStatisticTotalInfo);
            }
        } else {
            // 本日、本周、本月
            if (entity.getDateFilteType().equals(0)) {
                list = this.baseMapper.selectListForDay(new Date());
                OrderStatisticTotalInfo orderStatisticTotalInfo = this.getOrderStatisticTotalInfo(list);
                orderStatisticListInfo.setOrderStatisticTotalInfo(orderStatisticTotalInfo);
            } else if (entity.getDateFilteType().equals(1)) {
                // 本周  开始时间、结束时间
                list = this.baseMapper.selectListForWeek(new Date(),entity.getDateStr());
                OrderStatisticTotalInfo orderStatisticTotalInfo = this.getOrderStatisticTotalInfo(list);
                orderStatisticListInfo.setOrderStatisticTotalInfo(orderStatisticTotalInfo);
            } else if (entity.getDateFilteType().equals(2)) {
                // 本月  开始时间、结束时间
                // 获取本月第一日日期
                list = this.baseMapper.selectListForMonth(new Date(),entity.getDateStr());
                OrderStatisticTotalInfo orderStatisticTotalInfo = this.getOrderStatisticTotalInfo(list);
                orderStatisticListInfo.setOrderStatisticTotalInfo(orderStatisticTotalInfo);
            }
        }
        orderStatisticListInfo.setFinanceOrderStatisticList(list);
        return orderStatisticListInfo;
    }

    private OrderStatisticTotalInfo getOrderStatisticTotalInfo(List<FinanceOrderStatistic> list) {
        OrderStatisticTotalInfo orderStatisticTotalInfo = new OrderStatisticTotalInfo();
        orderStatisticTotalInfo.setTotalOrderAmount(BigDecimal.ZERO);
        orderStatisticTotalInfo.setTotalPayAmount(BigDecimal.ZERO);
        orderStatisticTotalInfo.setTotalRefundAmount(BigDecimal.ZERO);
        for (FinanceOrderStatistic financeOrderStatistic : list) {
            orderStatisticTotalInfo.setTotalRefundAmount(orderStatisticTotalInfo.getTotalRefundAmount().add(financeOrderStatistic.getRefundAmount()));
            orderStatisticTotalInfo.setTotalOrderAmount(orderStatisticTotalInfo.getTotalOrderAmount().add(financeOrderStatistic.getOrderAmount()));
            orderStatisticTotalInfo.setTotalPayAmount(orderStatisticTotalInfo.getTotalPayAmount().add(financeOrderStatistic.getPayAmount()));
        }
        return orderStatisticTotalInfo;
    }
}
