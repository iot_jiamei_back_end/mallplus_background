package com.ruoyi.project.finance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.finance.entity.AdminOrderStatistic;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
public interface IAdminOrderStatisticService extends IService<AdminOrderStatistic> {

    List<AdminOrderStatistic> getAdminOrderStatisticList(AdminOrderStatistic entity);
}
