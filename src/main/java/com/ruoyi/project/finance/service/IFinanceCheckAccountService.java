package com.ruoyi.project.finance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
public interface IFinanceCheckAccountService extends IService<FinanceCheckAccount> {

}
