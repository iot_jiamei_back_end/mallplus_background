package com.ruoyi.project.finance.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.finance.entity.FinanceCheck;
import com.ruoyi.project.finance.mapper.FinanceCheckMapper;
import com.ruoyi.project.finance.service.IFinanceCheckService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Service
@Primary
public class FinanceCheckServiceImpl extends ServiceImpl<FinanceCheckMapper, FinanceCheck> implements IFinanceCheckService {

}
