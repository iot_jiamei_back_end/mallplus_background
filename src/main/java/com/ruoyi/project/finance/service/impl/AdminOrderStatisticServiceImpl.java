package com.ruoyi.project.finance.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.project.finance.entity.AdminOrderStatistic;
import com.ruoyi.project.finance.mapper.AdminOrderStatisticMapper;
import com.ruoyi.project.finance.service.IAdminOrderStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Service
@Primary
public class AdminOrderStatisticServiceImpl extends ServiceImpl<AdminOrderStatisticMapper, AdminOrderStatistic> implements IAdminOrderStatisticService {
    @Autowired
    private AdminOrderStatisticMapper adminOrderStatisticMapper;
    @Override
    @Transactional
    public List<AdminOrderStatistic> getAdminOrderStatisticList(AdminOrderStatistic entity) {
        List<AdminOrderStatistic> list = new ArrayList<>();
        System.out.println("AdminOrderStatistic222："+entity);
        if (entity.getDateFilteType() == null) {
            // 查询日期
            if (entity.getDateStr() != null) {
                list = adminOrderStatisticMapper.selectList(new QueryWrapper<AdminOrderStatistic>().eq("date_str", entity.getDateStr()));
            } else {
                list = adminOrderStatisticMapper.selectList(new QueryWrapper<>());
            }
        } else if (entity != null) {
            // 本日、本周、本月
            if (entity.getDateFilteType().equals(0)) {
                list =adminOrderStatisticMapper.selectListForDay(new Date());
            } else if (entity.getDateFilteType().equals(1)) {
                // 本周  开始时间、结束时间
                list = adminOrderStatisticMapper.selectListForWeek(new Date(), entity.getDateStr());
            } else if (entity.getDateFilteType().equals(2)) {
                // 本月  开始时间、结束时间
                // 获取本月第一日日期
                list = adminOrderStatisticMapper.selectListForMonth(new Date(), entity.getDateStr());
            }
        }
        return list;
    }

}
