package com.ruoyi.project.finance.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.project.finance.entity.FinanceSettlement;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
public interface IFinanceSettlementService extends IService<FinanceSettlement> {

    void doSettlement(Integer storeId, String startTime, String endTime);
}
