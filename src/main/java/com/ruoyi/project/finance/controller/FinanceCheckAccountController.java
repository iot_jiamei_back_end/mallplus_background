package com.ruoyi.project.finance.controller;


import com.ruoyi.framework.web.controller.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Controller
@RequestMapping("/finance/financeCheckAccount")
public class FinanceCheckAccountController extends BaseController {

}

