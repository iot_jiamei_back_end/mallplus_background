package com.ruoyi.project.finance.controller;


import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.project.finance.entity.AdminOrderStatistic;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import com.ruoyi.project.finance.entity.vo.OrderStatisticListInfo;
import com.ruoyi.project.finance.entity.vo.OrderStatisticTotalInfo;
import com.ruoyi.project.finance.service.IAdminOrderStatisticService;
import com.ruoyi.project.finance.service.IFinanceOrderStatisticService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Controller
@RequestMapping("/admin/adminOrderStatistic")
public class AdminOrderStatisticController extends BaseController {

    @Resource
    private IAdminOrderStatisticService adminOrderStatisticService;

    /**
     * 订单统计页面
     *
     * @return
     */
    @GetMapping("")
    public String index(AdminOrderStatistic entity, ModelMap mmap) {
        System.out.println("AdminOrderStatistic111：" + entity);

        if (entity.getDateFilteType() == null) {
            entity.setDateFilteType(2);
        }
        List<AdminOrderStatistic> orderStatisticListInfo = adminOrderStatisticService.getAdminOrderStatisticList(entity);
        mmap.put("orderStatisticListInfo", orderStatisticListInfo);

        mmap.put("orderStatisticTotalInfo", this.getOrderStatisticTotalInfo(orderStatisticListInfo));
        mmap.put("dateFilteType", entity.getDateFilteType());
        return "finance/adminOrderStatics/index";
    }

    private OrderStatisticTotalInfo getOrderStatisticTotalInfo(List<AdminOrderStatistic> list) {
        OrderStatisticTotalInfo orderStatisticTotalInfo = new OrderStatisticTotalInfo();
        orderStatisticTotalInfo.setTotalOrderAmount(BigDecimal.ZERO);
        orderStatisticTotalInfo.setTotalPayAmount(BigDecimal.ZERO);
        orderStatisticTotalInfo.setTotalRefundAmount(BigDecimal.ZERO);
        for (AdminOrderStatistic adminOrderStatistic : list) {
            orderStatisticTotalInfo.setTotalRefundAmount(orderStatisticTotalInfo.getTotalRefundAmount().add(adminOrderStatistic.getRefundAmount()));
            orderStatisticTotalInfo.setTotalOrderAmount(orderStatisticTotalInfo.getTotalOrderAmount().add(adminOrderStatistic.getOrderAmount()));
            orderStatisticTotalInfo.setTotalPayAmount(orderStatisticTotalInfo.getTotalPayAmount().add(adminOrderStatistic.getPayAmount()));
        }
        return orderStatisticTotalInfo;
    }

}

