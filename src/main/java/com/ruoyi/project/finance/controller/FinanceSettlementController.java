package com.ruoyi.project.finance.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import com.ruoyi.project.finance.entity.FinanceSettlement;
import com.ruoyi.project.finance.service.IFinanceSettlementService;
import com.ruoyi.project.mall.address.domain.UmsMemberReceiveAddress;
import com.ruoyi.project.mall.address.service.IUmsMemberReceiveAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Controller
@RequestMapping("/finance/financeSettlement")
public class FinanceSettlementController extends BaseController {

    private String prefix = "finance/settlement";

    @Autowired
    private IUmsMemberReceiveAddressService umsMemberReceiveAddressService;

    @Autowired
    private IFinanceSettlementService financeSettlementService;

    /**
     * 结算列表页面
     *
     * @return
     */
    @GetMapping()
    public String address() {
        return prefix + "/index";
    }

    /**
     * 结算列表
     *
     * @return
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageSize, Integer pageNum, FinanceSettlement entity) {
        IPage<FinanceSettlement> page = financeSettlementService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(entity).orderByDesc("create_time"));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

    /**
     * 单个结算
     *
     * @return
     */
    @PostMapping("/settlement")
    @ResponseBody
    public AjaxResult settlement(Integer storeId, String startTime, String endTime) {
        try {
            if (storeId == null) {
                return AjaxResult.error("storeId不能为空！");
            }
            if (StringUtils.isEmpty(startTime)) {
                return AjaxResult.error("开始时间不能为空！");
            }
            if (StringUtils.isEmpty(endTime)) {
                return AjaxResult.error("结束时间不能为空！");
            }
            financeSettlementService.doSettlement(storeId, startTime, endTime);
            System.out.println("单个结算");
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult();
        }
        return AjaxResult.success();
    }

}

