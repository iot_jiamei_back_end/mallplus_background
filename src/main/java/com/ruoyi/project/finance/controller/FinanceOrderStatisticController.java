package com.ruoyi.project.finance.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import com.ruoyi.project.finance.entity.vo.OrderStatisticListInfo;
import com.ruoyi.project.finance.service.IFinanceOrderStatisticService;
import com.ruoyi.project.mall.order.domain.OmsOrder;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Controller
@RequestMapping("/finance/financeOrderStatistic")
public class FinanceOrderStatisticController extends BaseController {

    private String prefix = "finance/orderStatics";

    @Resource
    private IFinanceOrderStatisticService orderStatisticService;

    /**
     * 订单统计页面
     *
     * @return
     */
    @GetMapping("")
    public String index(FinanceOrderStatistic entity, ModelMap mmap) {
        if (entity.getDateFilteType() == null) {
            entity.setDateFilteType(2);
        }
        OrderStatisticListInfo orderStatisticListInfo = orderStatisticService.getFinanceOrderStatisticList(entity);
        mmap.put("dateFilteType", entity.getDateFilteType());
        mmap.put("orderStatisticListInfo", orderStatisticListInfo);
        return prefix + "/index";
    }

    /**
     * 对账订单列表
     *
     * @return
     */
    @GetMapping("order")
    public String order() {
        return "finance/order/index";
    }

}

