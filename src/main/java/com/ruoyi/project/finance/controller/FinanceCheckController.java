package com.ruoyi.project.finance.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.finance.entity.FinanceCheck;
import com.ruoyi.project.finance.entity.FinanceCheckAccount;
import com.ruoyi.project.finance.entity.FinanceSettlement;
import com.ruoyi.project.finance.service.IFinanceCheckAccountService;
import com.ruoyi.project.finance.service.IFinanceCheckService;
import com.ruoyi.project.finance.service.IFinanceSettlementService;
import com.ruoyi.project.mall.store.domain.Store;
import com.ruoyi.project.mall.store.service.IStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@Controller
@RequestMapping("/finance/financeCheck")
public class FinanceCheckController extends BaseController {

    private String prefix = "finance/settlement";

    @Autowired
    private IFinanceSettlementService financeSettlementService;

    @Autowired
    private IFinanceCheckService financeCheckService;

    @Autowired
    private IFinanceCheckAccountService financeCheckAccountService;
    @Autowired
    private IStoreService storeService;

    /**
     * 对账单详情
     *
     * @return
     */
    @GetMapping("checkAccountDetail")
    public String address(Long settlementId, ModelMap mmap) {
        FinanceSettlement financeSettlement = financeSettlementService.getById(settlementId);
        FinanceCheck financeCheck = financeCheckService.getById(financeSettlement.getCheckId());
        Store store = storeService.selectStoreById(Long.parseLong(financeSettlement.getStoreId().toString()));
        mmap.put("financeSettlement", financeSettlement);
        mmap.put("financeCheck", financeCheck);
        mmap.put("store", store);
        return prefix + "/checkAccountDetail";
    }

    /**
     * 对账单列表
     *
     * @return
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Integer pageSize, Integer pageNum, FinanceCheckAccount entity) {
        IPage<FinanceCheckAccount> page = financeCheckAccountService.page(new Page<>(pageNum, pageSize), new QueryWrapper<>(entity).
                orderByDesc("create_time"));
        TableDataInfo tableDataInfo = new TableDataInfo();
        tableDataInfo.setRows(page.getRecords());
        tableDataInfo.setTotal(page.getTotal());
        return tableDataInfo;
    }

}

