package com.ruoyi.project.finance.entity.vo;

import com.ruoyi.project.finance.entity.FinanceOrderStatistic;
import lombok.Data;

import java.util.List;

/**
 * @program: mallplus
 * @description:
 * @author: loop.fu
 * @create: 2020-03-10 10:47
 */
@Data
public class OrderStatisticListInfo {

    private List<FinanceOrderStatistic> financeOrderStatisticList;

    private OrderStatisticTotalInfo orderStatisticTotalInfo;

}
