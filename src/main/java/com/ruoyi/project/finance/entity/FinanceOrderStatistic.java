package com.ruoyi.project.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@TableName("finance_order_statistic")
@Data
public class FinanceOrderStatistic implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 日期  0本日  1本周  2本月
     */
    @TableField(exist = false)
    private Integer dateFilteType;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 时间
     */
    @TableField("date_str")
    private String dateStr;

    /**
     * 支付订单数
     */
    @TableField("pay_order_num")
    private Integer payOrderNum;

    /**
     * 订单金额
     */
    @TableField("order_amount")
    private BigDecimal orderAmount;

    /**
     * 退款金额
     */
    @TableField("refund_amount")
    private BigDecimal refundAmount;

    /**
     * 支付金额
     */
    @TableField("pay_amount")
    private BigDecimal payAmount;

    /**
     * 成交用户数
     */
    @TableField("transaction_user_num")
    private Integer transactionUserNum;

    /**
     * 退款订单数
     */
    @TableField("refund_order_num")
    private Integer refundOrderNum;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 商家id
     */
    @TableField("store_id")
    private Integer storeId;
}
