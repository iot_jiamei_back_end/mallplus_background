package com.ruoyi.project.finance.entity.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @program: mallplus
 * @description:
 * @author: loop.fu
 * @create: 2020-03-10 10:49
 */
@Data
public class OrderStatisticTotalInfo {
    /**
     * 订单金额
     */
    private BigDecimal totalOrderAmount;
    /**
     * 退款金额
     */
    private BigDecimal totalRefundAmount;
    /**
     * 支付金额
     */
    private BigDecimal totalPayAmount;
}
