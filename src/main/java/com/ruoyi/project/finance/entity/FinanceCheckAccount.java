package com.ruoyi.project.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@TableName("finance_check_account")
@Data
public class FinanceCheckAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 结算id
     */
    @TableField("check_id")
    private Long checkId;

    /**
     * 订单编号
     */
    @TableField("order_sn")
    private String orderSn;

    /**
     * 订单日期
     */
    @TableField("date_str")
    private String dateStr;

    @TableField("member_id")
    private Integer memberId;

    /**
     * 用户账号
     */
    @TableField("member_name")
    private String memberName;

    /**
     * 订单金额
     */
    @TableField("order_amount")
    private BigDecimal orderAmount;

    /**
     * 实付金额
     */
    @TableField("pay_amount")
    private BigDecimal payAmount;

    /**
     * 优惠金额
     */
    @TableField("discount_amount")
    private BigDecimal discountAmount;

    /**
     * 退款金额
     */
    @TableField("refund_amount")
    private BigDecimal refundAmount;

    /**
     * 对账单金额
     */
    @TableField("settle_amount")
    private BigDecimal settleAmount;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    @TableField("store_id")
    private Integer storeId;

}
