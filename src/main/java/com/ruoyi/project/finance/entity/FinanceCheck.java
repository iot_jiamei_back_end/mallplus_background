package com.ruoyi.project.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@TableName("finance_check")
@Data
public class FinanceCheck implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 结算id
     */
    @TableField("settlement_id")
    private Long settlementId;

    @TableField("total_order_amount")
    private BigDecimal totalOrderAmount;

    @TableField("total_pay_amount")
    private BigDecimal totalPayAmount;

    @TableField("total_discount_amount")
    private BigDecimal totalDiscountAmount;

    @TableField("total_refund_amount")
    private BigDecimal totalRefundAmount;

    @TableField("total_settle_amount")
    private BigDecimal totalSettleAmount;

    @TableField("create_time")
    private Date createTime;

    @TableField("store_id")
    private Integer storeId;

}
