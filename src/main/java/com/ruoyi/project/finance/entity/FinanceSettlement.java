package com.ruoyi.project.finance.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author loop.fu
 * @since 2020-03-16
 */
@TableName("finance_settlement")
@Data
public class FinanceSettlement implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 结算编号
     */
    @TableField("settlement_sn")
    private String settlementSn;

    /**
     * 对账单
     */
    @TableField("check_id")
    private Long checkId;

    /**
     * 结算开始时间
     */
    @TableField("start_time")
    private Date startTime;

    /**
     * 结算结束时间
     */
    @TableField("end_time")
    private Date endTime;

    /**
     * 结算状态：0 待确认   1已确认  2已结算
     */
    private Integer status;

    /**
     * 结算金额
     */
    @TableField("total_amount")
    private BigDecimal totalAmount;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;

    /**
     * 对账单名称
     */
    private String name;

    /**
     * 商家id
     */
    @TableField("store_id")
    private Integer storeId;
}
