package com.ruoyi.common.utils.bean;

import lombok.Data;

import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/9
 **/

@Data
public class VideoVo {

    private String platformId;

    private String fileId;

    private String cover;

    private String userId;

    private Long albumSizeSum;

    private List<ImgInfoVo> successFile;

    private List<String> errorFile;

    private List<Integer> indexs;



}
