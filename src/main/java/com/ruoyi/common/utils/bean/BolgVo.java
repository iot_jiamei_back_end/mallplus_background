package com.ruoyi.common.utils.bean;

import lombok.Data;

import java.util.List;

/**
 * @Author ZuRongTang
 * @Date 2020/3/4
 **/

@Data
public class BolgVo {

    private List<String> successFile;


    private List<String> errorFile;

    public BolgVo() {

    }
}
