package com.ruoyi.common.utils.bean;

import lombok.Data;

/**
 * @Author ZuRongTang
 * @Date 2019/8/26
 **/

@Data
public class ImgInfoVo {

    private Long createTime;

    private String userId;

    private String path;

    private Long size;
}
