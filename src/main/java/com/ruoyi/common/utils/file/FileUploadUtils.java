package com.ruoyi.common.utils.file;

import java.io.File;
import java.io.IOException;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.bean.BolgVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.exception.file.FileNameLengthLimitExceededException;
import com.ruoyi.common.exception.file.FileSizeLimitExceededException;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.Md5Utils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.config.RuoYiConfig;

/**
 * 文件上传工具类
 *
 * @author ruoyi
 */
public class FileUploadUtils {
    /**
     * 默认大小 50M
     */
    public static final long DEFAULT_MAX_SIZE = 50 * 1024 * 1024;

    /**
     * 默认的文件名最大长度 100
     */
    public static final int DEFAULT_FILE_NAME_LENGTH = 100;

    /**
     * 默认上传的地址
     */
    private static String defaultBaseDir = RuoYiConfig.getProfile();

    private static int counter = 0;

    public static void setDefaultBaseDir(String defaultBaseDir) {
        FileUploadUtils.defaultBaseDir = defaultBaseDir;
    }

    public static String getDefaultBaseDir() {
        return defaultBaseDir;
    }

    /**
     * 以默认配置进行文件上传
     *
     * @param file 上传的文件
     * @return 文件名称
     * @throws Exception
     */
    public static final String upload(MultipartFile file) throws IOException {
        try {
            return upload(getDefaultBaseDir(), file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        } catch (Exception e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * 根据文件路径上传
     *
     * @param baseDir 相对应用的基目录
     * @param file    上传的文件
     * @return 文件名称
     * @throws IOException
     */
    public static final String upload(String baseDir, MultipartFile file) throws IOException {
        try {
            return upload(baseDir, file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        } catch (Exception e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * 文件上传
     *
     * @param baseDir   相对应用的基目录
     * @param file      上传的文件
     * @param extension 上传文件类型
     * @return 返回上传成功的文件名
     * @throws FileSizeLimitExceededException       如果超出最大大小
     * @throws FileNameLengthLimitExceededException 文件名太长
     * @throws IOException                          比如读写文件出错时
     * @throws InvalidExtensionException            文件校验异常
     */
    public static final String upload(String baseDir, MultipartFile file, String[] allowedExtension)
            throws FileSizeLimitExceededException, IOException, FileNameLengthLimitExceededException,
            InvalidExtensionException {
        int fileNamelength = file.getOriginalFilename().length();
        if (fileNamelength > FileUploadUtils.DEFAULT_FILE_NAME_LENGTH) {
            throw new FileNameLengthLimitExceededException(FileUploadUtils.DEFAULT_FILE_NAME_LENGTH);
        }

        assertAllowed(file, allowedExtension);

        String fileName = extractFilename(file);

        File desc = getAbsoluteFile(baseDir, fileName);
        file.transferTo(desc);
        String pathFileName = getPathFileName(baseDir, fileName);
        return pathFileName;
    }

    /**
     * 编码文件名
     */
    public static final String extractFilename(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String extension = getExtension(file);
        fileName = DateUtils.datePath() + "/" + encodingFilename(fileName) + "." + extension;
        return fileName;
    }

    private static final File getAbsoluteFile(String uploadDir, String fileName) throws IOException {
        File desc = new File(uploadDir + File.separator + fileName);

        if (!desc.getParentFile().exists()) {
            desc.getParentFile().mkdirs();
        }
        if (!desc.exists()) {
            desc.createNewFile();
        }
        return desc;
    }

    private static final String getPathFileName(String uploadDir, String fileName) throws IOException {
//        int dirLastIndex = uploadDir.indexOf(RuoYiConfig.getProfile()) + 1;
//        String currentDir = StringUtils.substring(uploadDir, dirLastIndex);
//        String pathFileName = Constants.RESOURCE_PREFIX + "/" + currentDir + "/" + fileName;
        String currentDir = uploadDir.replace(RuoYiConfig.getProfile(), "");
        String pathFileName = Constants.RESOURCE_PREFIX + currentDir + "/" + fileName;
        return pathFileName;
    }

    /**
     * 编码文件名
     */
    private static final String encodingFilename(String fileName) {
        fileName = fileName.replace("_", " ");
        fileName = Md5Utils.hash(fileName + System.nanoTime() + counter++);
        return fileName;
    }

    /**
     * 文件大小校验
     *
     * @param file 上传的文件
     * @return
     * @throws FileSizeLimitExceededException 如果超出最大大小
     * @throws InvalidExtensionException
     */
    public static final void assertAllowed(MultipartFile file, String[] allowedExtension)
            throws FileSizeLimitExceededException, InvalidExtensionException {
        long size = file.getSize();
        if (DEFAULT_MAX_SIZE != -1 && size > DEFAULT_MAX_SIZE) {
            throw new FileSizeLimitExceededException(DEFAULT_MAX_SIZE / 1024 / 1024);
        }

        String fileName = file.getOriginalFilename();
        String extension = getExtension(file);
        if (allowedExtension != null && !isAllowedExtension(extension, allowedExtension)) {
            if (allowedExtension == MimeTypeUtils.IMAGE_EXTENSION) {
                throw new InvalidExtensionException.InvalidImageExtensionException(allowedExtension, extension,
                        fileName);
            } else if (allowedExtension == MimeTypeUtils.FLASH_EXTENSION) {
                throw new InvalidExtensionException.InvalidFlashExtensionException(allowedExtension, extension,
                        fileName);
            } else if (allowedExtension == MimeTypeUtils.MEDIA_EXTENSION) {
                throw new InvalidExtensionException.InvalidMediaExtensionException(allowedExtension, extension,
                        fileName);
            } else {
                throw new InvalidExtensionException(allowedExtension, extension, fileName);
            }
        }

    }

    /**
     * 判断MIME类型是否是允许的MIME类型
     *
     * @param extension
     * @param allowedExtension
     * @return
     */
    public static final boolean isAllowedExtension(String extension, String[] allowedExtension) {
        for (String str : allowedExtension) {
            if (str.equalsIgnoreCase(extension)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取文件名的后缀
     *
     * @param file 表单文件
     * @return 后缀名
     */
    public static final String getExtension(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtils.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(file.getContentType());
        }
        return extension;
    }

    public static String fileUpload(MultipartFile[] files) {

        Map<String, Object> fileMap = new HashMap<>();
        Map<String, Object> tokenMap = new HashMap<>();

        //调用oss 文件上传
        List<String> fileName = new ArrayList<>();
        //把数组转成集合
        List<MultipartFile> fileList = new ArrayList<>(Arrays.asList(files));

        for (MultipartFile file : files) {
            fileName.add(file.getOriginalFilename());
        }

        tokenMap.put("appId", "cJG3JSr5od9HkN6HJwy9g8ffPOUmfSUFVtdkXovyt5KqZVBiOeKQz0Zp9KmVRzYl");
        tokenMap.put("appSecret", "qVw9mEy7MDK7skv8BXa1mw==");

        String token = getToken(tokenMap);

        //文件上传
        fileMap.put("token", token);
        fileMap.put("appId", "cJG3JSr5od9HkN6HJwy9g8ffPOUmfSUFVtdkXovyt5KqZVBiOeKQz0Zp9KmVRzYl");
        fileMap.put("platformId", "");
        fileMap.put("bucketName", "personal");

        JSONObject jsonObject = agentFileUpload("http://127.0.0.1:8086/file/fileUploadBolg", fileMap, fileName, fileList);

        BolgVo bolgVo = null;
        try {
            bolgVo = JSONObject.parseObject(jsonObject.get("data").toString(), BolgVo.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> result = new ArrayList<>();
        List<String> successFile = bolgVo.getSuccessFile();

        for (String s : successFile) {
            result.add("http://192.168.1.83:8086/file/" + s);
        }
        bolgVo.setSuccessFile(result);
        return "http://192.168.1.83:8086/file/" + successFile.get(0);
    }

    private static JSONObject agentFileUpload(String url, Map<String, Object> fileMap, List<String> fileName, List<MultipartFile> fileList) {
        String fileResult = HttpClientUtil.httpPostRequest2(url, fileList, fileName, fileMap, 5000);

//        if (StringUtils.isEmpty(fileResult)) {
//            throw new CustomException(ResponseUtil.fall("-9001", "系统异常"));
//        }

        //        if (!fileJson.get("code").equals("200")) {
//            throw new CustomException(ResponseUtil.fall("-9001", "系统异常"));
//        }
        return JSONObject.parseObject(fileResult);
    }

    private JSONObject agentFileUpload(Map<String, Object> fileMap) {
        String fileResult = HttpClientUtil.postJson("http://127.0.0.1:8086/file/compression", fileMap);

//        if (StringUtils.isEmpty(fileResult)) {
//            throw new CustomException(ResponseUtil.fall("-9001", "系统异常"));
//        }

        //        if (!fileJson.get("code").equals("200")) {
//            throw new CustomException(ResponseUtil.fall("-9001", "系统异常"));
//        }
        return JSONObject.parseObject(fileResult);
    }

    private static String getToken(Map<String, Object> tokenMap) {
        String tokenResult = HttpClientUtil.postJson("http://127.0.0.1:8084/authoriz/getToken", tokenMap);

        if (org.springframework.util.StringUtils.isEmpty(tokenResult)) {
            return "1";
        }

        JSONObject tokenJson = JSONObject.parseObject(tokenResult);

        if (!tokenJson.get("code").equals("200")) {
            return "2";
        }

        return tokenJson.get("data").toString();
    }


}
